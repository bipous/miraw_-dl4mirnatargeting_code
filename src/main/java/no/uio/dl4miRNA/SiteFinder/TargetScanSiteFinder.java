/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import java.util.HashMap;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import org.slf4j.LoggerFactory;

/**
 * Candidate Site Selection method defined in TargetScan. 
 * The gene is scrolled and sites with a 
 * potential stable binding with a free energy lower than (0),
 * and a minium of 6 consecutive pairs within the seed region are considered as MBS. 
 * Seed region defined as the 6 nucleotides starting at the second nucleotide (in code it 
 * corresponds to 1).
 * 
 * In addition, the following non canonical sites are also accepted:
 * Imperfect seed: a wobble is allowed in perfect 7mers and 8mrs.
 * Centered site: Perfect  contiguous 11er between nucleotides 4 and 15.
 * Compensatory site: A minimum of 3
 * consecutive WC pairs between nucleotides 13 and 16 compensates an
 * imperfect seed match –onewobble, bulge or missmatch–
 * 
 * @author apla
 */
public class TargetScanSiteFinder extends CandidateSiteFinder {
    
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(TargetScanSiteFinder.class);
    
   

    private final int requiredConsecutiveCompenstory = 9; //number of contiguous WC pairs that
                                                    //are required to form a compensatory site.
                                                    //these must involve pairing in position 13 to 17
                                                    //see Robin C. Friedman 2009 http://genome.cshlp.org/content/19/1/92.long
                                                    //and targetScanS http://www.targetscan.org/docs/non-canonical.html

    /**
     * Initialize TargetScan parameters (minBindsInSeed = 6, maxFreeEnergy=0, 
     * seedRegionSize=6);
     */
    public TargetScanSiteFinder() {
        super();
        this.seedRegionSize = 6;
        this.minBindsSeed = 6;
        this.seedRegionStart = 1;
        this.maxFreeEnergy = 0;
    }

    @Override
    /**
     * Implements the search of candidate Mirna Binding sites fitting to Target
     * Scan S site selection method.
     * @param mirna transcript of the miRNA
     * @param mRNA transcript of the mRNA region
     * @return Array with the list of Candidate MBS
     */
    protected ArrayList<CandidateMBS> findCandidates(String mirna, String mRNA) {
        return obtainCandidatePlaces(mirna, mRNA);
    }

    /**
     * Given a miRNA, checks all the possible binidngs sites of a mRNA in the gene
     * in order to check if they are potential MBSs.
     * @param mirna miRNA transcript.
     * @param mRNA mRNA transcript to be checked.
     * @return Array list with potential MBSs.
     */
    private ArrayList<CandidateMBS> obtainCandidatePlacesSequential(String mirna, String mRNA) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        //At each possible miRNA binding site
        for (int i = mRNA.length() - mirna.length(); i >= 0; i--) {
            int bindSize = this.maxSiteLength;
            if (mRNA.length() - i < this.maxSiteLength) {
                bindSize = mRNA.length() - i;
            }
            //check if it is a candidate
            CandidateMBS candSite = isCandidate(mirna, mRNA.substring(i, i + bindSize));
            if (candSite != null) { //if it is a candidate site, enrich information of the candidate site

                candSite.setMatureMirnaTranscript(mirna);

                candSite.setGene3UTRStart(i);
                candSite.setGene3UTREnd(i + bindSize);
                candSite.setSiteTranscript(mRNA.substring(i, i + bindSize));
                candidateList.add(candSite);
            }

        }
        return candidateList;
    }
    
    /**
     * Given a miRNA, checks all the possible binidngs sites of a mRNA in the gene
     * in order to check if they are potential MBSs.
     * 
     * Optimized to run in parallel
     * 
     * @param mirna miRNA transcript.
     * @param mRNA mRNA transcript to be checked.
     * @return Array list with potential MBSs.     * 
     */
    private ArrayList<CandidateMBS> obtainCandidatePlaces(String mirna, String mRNA) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        HashMap<Integer, String> sitesToTest = new HashMap<>();
        HashMap<Integer, CandidateMBS> candidateSiteMap = new HashMap<>();

        //identify all sites of 3'UTR (squential)
        for (int i = mRNA.length() - this.maxSiteLength; i >= 0; i--) {
            int bindSize = this.maxSiteLength;
            if (mRNA.length() - i < this.maxSiteLength) {
                bindSize = mRNA.length() - i;
            }
            sitesToTest.put(i, mRNA.substring(i, i + bindSize));
        }

        //concurrently determine if the sites are candidate mbs
        sitesToTest.keySet().parallelStream().forEach((i) -> {
            String site = sitesToTest.get(i);
            CandidateMBS candSite = isCandidate(mirna, site);
            if (candSite != null) {
                candSite.setMatureMirnaTranscript(mirna);
                candSite.setGene3UTRStart(i);
                candSite.setGene3UTREnd(i + site.length());
                candSite.setSiteTranscript(site);
                candidateSiteMap.put(i, candSite);
            }
        });

        //save the candidate mbs in an ordered way
        for (int i = mRNA.length() - this.maxSiteLength; i > 0; i--) {
            if (candidateSiteMap.get(i) != null) {
                candidateList.add(candidateSiteMap.get(i));
            }
        }

        return candidateList;
    }
    
    /**
     * Checks if a microRNA can bind to a sequence. We follow standard seed
     * parameter settings and consider seeds of length 6–8 bases, beginning at
     * position 2 of the microRNA. For cannonical sites No mismatches or loops are 
     * allowed, but a single G:U wobble is allowed in 7- or 8-mers.
     * 
     * The following non-canonical sites are also considered:
     * Centered site: Perfect  contiguous 11er between nucleotides 4 and 15.
     * Compensatory site: A minimum of 3
     * consecutive WC pairs between nucleotides 13 and 16 compensates an
     * imperfect seed match –onewobble, bulge or missmatch–
     *
     * @param mirna microRNA sequence
     * @param seqB RNA sequence of the target. Returns false if it's longer than
     * MaxSiteLength
     *
     *
     * @return 
     */
    public CandidateMBS isCandidate(String mirna, String seqB) {
        //log.debug("Checking candidate site {} - {}", mirna, seqB);
        
        int WCPairs = 0;
        int wobblePairs = 0;

        //if the sequence is too long, returns false
        if (seqB.length() > this.maxSiteLength) {
            return null;
        }

        //Check if the two transcripts can bind.
        DnaFoldPrediction fold = RNAFolder.foldSeqs(seqB, mirna);
        if (fold==null){
            return null;
        }
        double freeEnergy = fold.deltaG();
        //if the squence cannot be fold (they do not form a stable bind returns false
        if (!RNAFolder.checkBindingFromStructure(fold.structure())) {
            return null;
        }

        //we check at which point the sequence and the mirna are binding. This is useful for future 'alignment'
        int seedRegionStartInSequence = this.findBindingStartInSequence(fold.structure());
        
        //The structure has a size of |seqB|+7+|mirna|
        //the seed region starts at structure.length - |mirna|+1
        int seedStart = fold.structure().length() - mirna.length() + seedRegionStart;
        int seedIndex = seedStart;
        int nPairs = 0;

        //Check if it is a canonical site
        int firstPairPosition = -1; //it will be used for determining centered sites

        while (seedIndex < fold.structure().length() && seedIndex < (seedStart + seedRegionSize)) {
            //counting number of WC and Wobble pairs.
            if (fold.structure().charAt(seedIndex) == ')' || fold.structure().charAt(seedIndex) == '}') {
                String pair = this.getPairAt(seedIndex, fold.sequence(), fold.structure());
                if (pair.length() > 1) {
                    if (this.isWobble(pair.charAt(0), pair.charAt(1))) {
                        wobblePairs++;
                    } else if (this.isWatsonCrick(pair.charAt(0), pair.charAt(1))) {
                        WCPairs++;
                        if (firstPairPosition == -1) { //the first pair still hasnt been found
                            //dfine first pair
                            firstPairPosition = seedIndex - seedStart;
                        }
                    }
                }
                nPairs++;
            }
            seedIndex++;
        }

        if (WCPairs >= 6) { //If it has 6 or more WC pairs it is a canonical site (that might allow a wobble)
            CandidateMBS cs = createCandidateSite(freeEnergy, nPairs, WCPairs, wobblePairs);
            cs.setSeedRegionStartInSequence(seedRegionStartInSequence);
            cs.setComment("TS:Cannonical");
            cs.setIsCanonical(true);
            return cs;
        }

        //it is not a canonical site, but it might be a non-canonical site
        if (nPairs > 2) {

            //Check for CenteredSite (countint consecutive pairs beyond seed region)
            boolean end = false;
            seedIndex = seedStart + firstPairPosition;
            int consecutives = 0;
            while (!end) {
                if (fold.structure().charAt(seedIndex) == ')') {
                    String pair = this.getPairAt(seedIndex, fold.sequence(), fold.structure());
                    if (pair.length() > 1) {
                        if (this.isWatsonCrick(pair.charAt(0), pair.charAt(1))) {
                            consecutives++;
                        } else {
                            end = true;
                        }
                    }

                } else {
                    end = true;
                }
                if (seedIndex - firstPairPosition-seedStart >= 12) {
                    end = true;
                }
                seedIndex++;
            }
            if (consecutives >= 11) { //is a centered site
                CandidateMBS cs = createCandidateSite(freeEnergy, nPairs, WCPairs, wobblePairs);
                cs.setComment("TS:Centered");
                cs.setSeedRegionStartInSequence(seedRegionStartInSequence);
                return cs;
            }
            //it is not a centered site, but it still might be a compensatory site

            int maxConsecutiveWC=0;
            //CCheck for compensatory site    
            int consecutiveCompensatoryPins = 0;
            if ((nPairs > 4 && wobblePairs > 0) || WCPairs > 4) {
                for (seedIndex = seedStart + (17-requiredConsecutiveCompenstory); seedIndex < fold.structure().length(); seedIndex++) {
                    if (fold.structure().charAt(seedIndex) == ')') { //looking for consecutive compensatory pins
                        
                        String pair = this.getPairAt(seedIndex, fold.sequence(), fold.structure());
                        if (pair.length() > 1) {
                            //System.out.print(pair.charAt(1));
                            if (this.isWatsonCrick(pair.charAt(0), pair.charAt(1))) {
                                consecutiveCompensatoryPins++;
                                maxConsecutiveWC = Math.max(consecutiveCompensatoryPins, maxConsecutiveWC);
                            }
                            if (this.isWobble(pair.charAt(0), pair.charAt(1))){
                                //System.out.print(".");
                                maxConsecutiveWC=0;
                            }
                        }
                    }
                    else {    //a mismatch means that the pairs are no longer consecutive. Restart counter 
                        consecutiveCompensatoryPins =0;
                    }
                }
                
                if (maxConsecutiveWC >= requiredConsecutiveCompenstory) {
                    CandidateMBS cs = createCandidateSite(freeEnergy, nPairs, WCPairs, wobblePairs);
                    cs.setComment("TS:Compensatory");
                    cs.setSeedRegionStartInSequence(seedRegionStartInSequence);
                    return cs;
                }
            }
        }
        //it is not a candidate site.
        return null;
    }

    /**
     * Creates a new candidate site with the defined parameters: 
     * Seed region start = 2;
     * Seed region end = 1+nPairs;
     * 
     * @param freeEnergy Free energy in the candidate site
     * @param nPairs number of basepairs in the seed region
     * @param WCPairs number of Watson Crick basepaires in the seed region
     * @param wobblePairs number of Wobble pairs in the seed region
     * @return new Candidate site
     */
    private CandidateMBS createCandidateSite(Double freeEnergy, int nPairs, int WCPairs, int wobblePairs) {
        CandidateMBS cs = new CandidateMBS();
        cs.setFreeEnergy(freeEnergy);
        cs.setBindsInSeed(nPairs);
        cs.setWCpairsInSeed(WCPairs);
        cs.setWobblePairsInSeed(wobblePairs);
        cs.setSeedRegionStart(1);
        cs.setSeedRegionEnd(0 + 6);
        return cs;
    }
    
    /**
     * Used to test/develop the site finder
     * @param args 
     */
    public static void main(String[] args){
        String mirna = "UGGAAUAUAAAGAAGUAUGUAU";
        String seqB = "AUGUGAAAGACCAUUAUUCCA";
        
        System.out.println(seqB);
        System.out.println(mirna);
        
        TargetScanSiteFinder instance = new TargetScanSiteFinder();
        CandidateMBS expResult = null;
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        System.out.println(result);
    }

}
