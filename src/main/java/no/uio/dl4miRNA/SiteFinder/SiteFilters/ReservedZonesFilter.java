/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder.SiteFilters;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.LoggerFactory;

/**
 *
 * @author albertpp
 */
public class ReservedZonesFilter implements CandidateSiteFilter {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SpecificZonesFilter.class);

    private HashMap<String, ArrayList<ArrayList<Integer>>> posStrandReservedAreas;
    private HashMap<String, ArrayList<ArrayList<Integer>>> negStrandReservedAreas;

    public ReservedZonesFilter() {
        posStrandReservedAreas = new HashMap<>();
        negStrandReservedAreas = new HashMap<>();
    }

    @Override
    public CandidateMBS evaluateSite(CandidateMBS cMBS) {
        //CandidateMBS nMBS = new CandidateMBS(cMBS);
        CandidateMBS nMBS = cMBS;
        boolean filtered = false;

        if (nMBS.getStrand() == '+') {
            ArrayList<Integer> specificSiteStartList = new ArrayList<>();
            ArrayList<Integer> specificSiteEndList = new ArrayList<>();

            if (this.posStrandReservedAreas.keySet().contains(nMBS.getChromosome())) {
                specificSiteStartList = posStrandReservedAreas.get(nMBS.getChromosome()).get(0);
                specificSiteEndList = posStrandReservedAreas.get(nMBS.getChromosome()).get(1);
            }

            for (int i = 0; i < specificSiteStartList.size(); i++) {
                if ((nMBS.getGene3UTRStart() > specificSiteStartList.get(i) && nMBS.getGene3UTRStart() < specificSiteEndList.get(i))
                        || (nMBS.getGene3UTREnd() > specificSiteStartList.get(i) && nMBS.getGene3UTREnd() < specificSiteEndList.get(i))) {
                    filtered = true;
                    cMBS.setFiltered(true);
                    cMBS.setFilteredReason("Site in reserved in " + nMBS.getStrand() + nMBS.getChromosome()
                            + ". Nucleotides:\t" + specificSiteStartList.get(i) + "\t" + specificSiteEndList.get(i));
                    nMBS = null;
                    break;
                }
            }
        } else if (nMBS.getStrand() == '-') {
            ArrayList<Integer> specificSiteStartList = new ArrayList<>();
            ArrayList<Integer> specificSiteEndList = new ArrayList<>();

            if (this.negStrandReservedAreas.keySet().contains(nMBS.getChromosome())) {
                specificSiteStartList = negStrandReservedAreas.get(nMBS.getChromosome()).get(0);
                specificSiteEndList = negStrandReservedAreas.get(nMBS.getChromosome()).get(1);
            }

            for (int i = 0; i < specificSiteStartList.size(); i++) {
                if ((nMBS.getGene3UTRStart() > specificSiteStartList.get(i) && nMBS.getGene3UTRStart() < specificSiteEndList.get(i))
                        || (nMBS.getGene3UTREnd() > specificSiteStartList.get(i) && nMBS.getGene3UTREnd() < specificSiteEndList.get(i))) {
                    filtered = true;
                    cMBS.setFiltered(true);
                    cMBS.setFilteredReason("Site in reserved in " + nMBS.getStrand() + nMBS.getChromosome()
                            + ". Nucleotides:\t" + specificSiteStartList.get(i) + "\t" + specificSiteEndList.get(i));
                    nMBS = null;
                    break;
                }
            }
        }

        if (filtered) {
            log.debug("Candidate" + cMBS.getGene3UTRStart() + " is filtered by Specific Zones");
        } else {
            log.debug("Candidate" + cMBS.getGene3UTRStart() + " is not filtered  by Specific Zones");
        }
        return nMBS;
    }

    @Override
    public List<CandidateMBS> filter(List<CandidateMBS> cMBSs) {
        ArrayList<CandidateMBS> filteredSites = new ArrayList<>();
        for (CandidateMBS cand : cMBSs) {
            if (this.evaluateSite(cand) != null) {
                filteredSites.add(cand);
            };
        }
        return filteredSites;
    }

    public void loadReservedAreasFromFile(String fileLocation) throws FileNotFoundException, IOException {
        FileReader fileReader = new FileReader(fileLocation);
        CSVFormat csvFileFormat = CSVFormat.RFC4180.withHeader().withDelimiter('\t');       //needs to be checked
        CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);
        List<CSVRecord> records = csvFileParser.getRecords();

        for (CSVRecord r : records) {
            String numChrom = null;
            String chrom = r.get("HCE chromosome");
            char strand = r.get("HCE strand").charAt(0);
            int start = Integer.parseInt(r.get("HCE start"));
            int end = Integer.parseInt(r.get("HCE end"));
            if (chrom.split("chr").length > 1) {
                numChrom = chrom.split("chr")[1];
            }

            if (strand == '+') {
                if (!this.posStrandReservedAreas.keySet().contains(chrom)) {
                    ArrayList<Integer> startL = new ArrayList<>();
                    ArrayList<Integer> endL = new ArrayList<>();
                    ArrayList<ArrayList<Integer>> regions = new ArrayList<>();
                    regions.add(startL);
                    regions.add(endL);
                    this.posStrandReservedAreas.put(chrom, regions);
                    if (numChrom != null) {
                        this.posStrandReservedAreas.put(numChrom, regions);
                    }
                }

                this.posStrandReservedAreas.get(chrom).get(0).add(start);
                this.posStrandReservedAreas.get(chrom).get(1).add(start);
                if (numChrom != null) {
                    this.posStrandReservedAreas.get(numChrom).get(0).add(start);
                    this.posStrandReservedAreas.get(numChrom).get(1).add(start);
                }
            } else if (strand == '-') {
                if (!this.negStrandReservedAreas.keySet().contains(chrom)) {
                    ArrayList<Integer> startL = new ArrayList<>();
                    ArrayList<Integer> endL = new ArrayList<>();
                    ArrayList<ArrayList<Integer>> regions = new ArrayList<>();
                    regions.add(startL);
                    regions.add(endL);
                    this.negStrandReservedAreas.put(chrom, regions);
                    if (numChrom != null) {
                        this.negStrandReservedAreas.put(numChrom, regions);
                    }
                }

                this.negStrandReservedAreas.get(chrom).get(0).add(start);
                this.negStrandReservedAreas.get(chrom).get(1).add(start);
                if (numChrom != null) {
                    this.negStrandReservedAreas.get(numChrom).get(0).add(start);
                    this.negStrandReservedAreas.get(numChrom).get(1).add(start);
                }
            }

        }

    }

}
