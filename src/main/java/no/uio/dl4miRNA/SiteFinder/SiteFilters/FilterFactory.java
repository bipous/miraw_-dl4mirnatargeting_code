/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder.SiteFilters;

import org.slf4j.LoggerFactory;

/**
 *
 * @author albertpp
 */
public class FilterFactory {
    
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(FilterFactory.class);
    
    public static CandidateSiteFilter getFilter(String filterAndParameters) {
        String[] atributes = filterAndParameters.split(";");
        
        CandidateSiteFilter csf = null;
        if (atributes[0].equalsIgnoreCase("AccessibilityEnergy")) {
            if (atributes[1] != null) {
                log.debug("AccessibilityEnergy filter added");
                try {
                    double th = Double.parseDouble(atributes[1]);
                    csf = new AccessibilityEnergyFilter(th);
                } catch (NumberFormatException nfe) {
                    csf = new AccessibilityEnergyFilter();
                }
            } else {
                csf = new AccessibilityEnergyFilter();
            }
        } else if (atributes[0].equalsIgnoreCase("SpecificZones")) {
            log.debug("SpecificZones filter added");
            csf = new SpecificZonesFilter();
            int i = 2;
            while (i < atributes.length) {
                try {
                    int start = Integer.parseInt(atributes[i - 1]);
                    int end = Integer.parseInt(atributes[i]);
                    ((SpecificZonesFilter) csf).addSpecificSiteRestriction(start, end);
                } catch (NumberFormatException nfe) {
                    try {
                        double start = Double.parseDouble(atributes[i - 1]);
                        double end = Double.parseDouble(atributes[i]);
                        ((SpecificZonesFilter) csf).addPercentileRestriction(start, end);
                    } catch (NumberFormatException nfe2) {
                        log.warn("Either Atrribute 1 or Attribute 2 are not numbers({},{})", atributes[i - 1], atributes[i]);
                    }
                }
                i++;
                i++;
            }
            
        }
        
        return csf;
    }
    
}
