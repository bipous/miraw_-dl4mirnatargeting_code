/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder.SiteFilters;

import java.util.ArrayList;
import java.util.List;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import org.slf4j.LoggerFactory;

/**
 *
 * @author albertpp
 */
public class SpecificZonesFilter implements CandidateSiteFilter {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SpecificZonesFilter.class);

    private ArrayList<Integer> specificSiteStartList;
    private ArrayList<Integer> specificSiteEndList;

    private ArrayList<Double> percentilStartList;
    private ArrayList<Double> percentilEndList;

    public SpecificZonesFilter() {
        this.specificSiteStartList = new ArrayList<>();
        this.specificSiteEndList = new ArrayList<>();
        this.percentilStartList = new ArrayList<>();
        this.percentilEndList = new ArrayList<>();
    }

    @Override
    public CandidateMBS evaluateSite(CandidateMBS cMBS) {
        //CandidateMBS nMBS = new CandidateMBS(cMBS);
        CandidateMBS nMBS = cMBS;

        double locPercStart = (double) nMBS.getGene3UTRStart() / (double) nMBS.getFull3UTRTranscript().length() * 100;
        double locPercEnd = (double) nMBS.getGene3UTREnd() / (double) nMBS.getFull3UTRTranscript().length() * 100;

        boolean filtered = false;

        for (int i = 0; i < specificSiteStartList.size(); i++) {
            if ((nMBS.getGene3UTRStart() > specificSiteStartList.get(i) && nMBS.getGene3UTRStart() < specificSiteEndList.get(i))
                    || (nMBS.getGene3UTREnd() > specificSiteStartList.get(i) && nMBS.getGene3UTREnd() < specificSiteEndList.get(i))) {
                filtered = true;
                cMBS.setFiltered(true);
                cMBS.setFilteredReason("Site in reserved area. Nucleotides:\t" + specificSiteStartList.get(i) + "\t" + specificSiteEndList.get(i));
                break;
            }
        }

        if (!filtered) {
            for (int i = 0; i < percentilStartList.size(); i++) {
                if ((locPercStart > percentilStartList.get(i) && locPercStart < percentilEndList.get(i))
                        || (locPercEnd > percentilStartList.get(i) && locPercEnd < percentilEndList.get(i))) {
                    filtered = true;
                    cMBS.setFiltered(true);
                    cMBS.setFilteredReason("Site in reserved area. Percentil:\t" + percentilStartList.get(i) + "\t" + percentilEndList.get(i));
                    break;
                    
                }
            }
        }

        if (filtered) {
            nMBS = null;
            log.debug("Candidate" + cMBS.getGene3UTRStart() + " is filtered by Specific Zones");
        } else {
            log.debug("Candidate" + cMBS.getGene3UTRStart() + " is not filtered  by Specific Zones");
        }
        return nMBS;
    }

    @Override
    public List<CandidateMBS> filter(List<CandidateMBS> cMBSs) {
        ArrayList<CandidateMBS> filteredSites = new ArrayList<>();
        for (CandidateMBS cand : cMBSs) {
            if (this.evaluateSite(cand) != null) {
                filteredSites.add(cand);
            };
        }
        return filteredSites;
    }

    public void addSpecificSiteRestriction(int start, int end) {
        this.specificSiteStartList.add(start);
        this.specificSiteEndList.add(end);
    }

    public void addPercentileRestriction(double start, double end) {
        this.percentilEndList.add(end);
        this.percentilStartList.add(start);
    }

}
