/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder.SiteFilters;

import java.util.List;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;

/**
 *
 * @author apla
 */
public interface CandidateSiteFilter {
    
    public CandidateMBS evaluateSite(CandidateMBS cMBS);
    
    public List<CandidateMBS> filter(List<CandidateMBS> cMBSs);
    
}
