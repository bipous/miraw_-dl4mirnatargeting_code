/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder.SiteFilters;

import java.util.ArrayList;
import java.util.List;
import no.uio.dl4miRNA.DL.GenePrediction;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import no.uio.dl4miRNA.SiteFinder.RNAFolder;
import org.slf4j.LoggerFactory;

/**
 *
 * @author apla
 */
public class AccessibilityEnergyFilter implements CandidateSiteFilter {

    private double threshold;
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(AccessibilityEnergyFilter.class);
   
  

    public AccessibilityEnergyFilter() {
        this.threshold=10;
    }
    
    public AccessibilityEnergyFilter(double threshold){
        this.threshold = threshold;
    }

    @Override
    public CandidateMBS evaluateSite(CandidateMBS cMBS) {
        //CandidateMBS nMBS = new CandidateMBS(cMBS);
        CandidateMBS nMBS = cMBS;
        double energy = getSiteEnergy(cMBS);
        cMBS.addProperty("AccessibilityEnergy", energy);
        nMBS.addProperty("AccessibilityEnergy", energy);
        if (energy>this.threshold){
            nMBS=null;
            cMBS.setFiltered(true);
            cMBS.setFilteredReason("Accessibility Energy:"+energy+">"+threshold);
            log.debug("Candidate"+cMBS.getGene3UTRStart()+" is filtered");
        }
        else{
            log.debug("Candidate"+cMBS.getGene3UTRStart()+" is not filtered");
        }
        return nMBS;
    }
    
    @Override
    public List<CandidateMBS> filter(List<CandidateMBS> cMBSs) {
        ArrayList<CandidateMBS> filteredSites = new ArrayList<>();
        for (CandidateMBS cand: cMBSs){
            if (cand.getProperty("AccessibilityEnergy")==null){
                this.evaluateSite(cand);
            }
            
            if ((double)cand.getProperty("AccessibilityEnergy")<=this.threshold){
                filteredSites.add(cand);
            }
        }
        
        return filteredSites;
    }
    
    private double getSiteEnergy(CandidateMBS cMBS){
        return RNAFolder.siteOpeningEnergy(cMBS.getFull3UTRTranscript(), cMBS.getGene3UTRStart(), cMBS.getGene3UTREnd());        
    }

    

}
