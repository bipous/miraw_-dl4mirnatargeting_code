/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import java.util.HashMap;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import org.slf4j.LoggerFactory;

/**
 * Candidate Site Selection method defined in PITA. 
 * The gene is scrolled and sites with a 
 * potential stable binding with a free energy lower than (0),
 * and a minium of 6 consecutive pairs within the seed region. Seed region
 * defined as the 8 nucleotides starting at the second nucleotide (in code it 
 * corresponds to 1). In 7mers (or > a wobbles is accepted).
 * 
 * @author apla
 */
public class PitaCandidateSiteFinder extends CandidateSiteFinder {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(PitaCandidateSiteFinder.class);
    //log.debug("Checking candidate site {} - {}", mirna, seqB)
   
    
    /**
     * Constructor.. Sets seedRegionSize to 8, minBindsSeed to 6, seedRegionSart to 1,
     * maxFreeEnergy to 0.
     */
    public PitaCandidateSiteFinder() {
        super();
        this.seedRegionSize = 8;
        this.minBindsSeed = 6;
        this.seedRegionStart = 1;
        this.maxFreeEnergy = 0;
    }

    /**
     * Implements the search of candidates. Candidates cosnsidered
     * follow standard seed
     * parameter settings and with perfect lengths of 6–8 bases, beginning at
     * position 2 of the microRNA. Minimum pairing is a perfect 6-mers. 
     * No mismatches or loops are allowed, but a
     * single G:U wobble is allowed in 7- or 8-mers.
     * @param mirna
     * @param mRNA
     * @return array with the list of candidate MBS
     */
    @Override
    protected ArrayList<CandidateMBS> findCandidates(String mirna, String mRNA) {
        return obtainCandidatePlaces(mirna, mRNA);
    }

    /**
     * Searches for all the target candidates of a mRNA given a miRNA. Returns a
     * ArrayList with the Candidate Sites;
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * seed region
     * @return ArrayList with all the candidate sites.
     */
    private ArrayList<CandidateMBS> obtainCandidatePlacesSequential(String mirna, String mRNA) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        for (int i = mRNA.length() - mirna.length(); i >= 0; i--) {
            int bindSize = this.maxSiteLength;
            if (mRNA.length() - i < this.maxSiteLength) {
                bindSize = mRNA.length() - i;
            }
            CandidateMBS candSite = isCandidate(mirna, mRNA.substring(i, i + bindSize));
            if (candSite != null) {
                
                candSite.setMatureMirnaTranscript(mirna);
                candSite.setGene3UTRStart(i);
                candSite.setGene3UTREnd(i + bindSize);
                candSite.setSiteTranscript(mRNA.substring(i, i + bindSize));
                candidateList.add(candSite);
            }

        }
        return candidateList;
    }
    
    /**
     * Searches for all the target candidates of a mRNA given a miRNA. Returns a
     * ArrayList with the Candidate Sites;
     * 
     * optimized to be parallel
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * seed region
     * @return ArrayList with all the candidate sites.
     */
    private ArrayList<CandidateMBS> obtainCandidatePlaces(String mirna, String mRNA) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        HashMap<Integer, String> sitesToTest = new HashMap<>();
        HashMap<Integer, CandidateMBS> candidateSiteMap = new HashMap<>();

        //identify all sites of 3'UTR (squential)
        for (int i = mRNA.length() - this.maxSiteLength; i >= 0; i--) {
            int bindSize = this.maxSiteLength;
            if (mRNA.length() - i < this.maxSiteLength) {
                bindSize = mRNA.length() - i;
            }
            sitesToTest.put(i, mRNA.substring(i, i + bindSize));
        }

        //concurrently determine if the sites are candidate mbs
        sitesToTest.keySet().parallelStream().forEach((i) -> {
            String site = sitesToTest.get(i);
            CandidateMBS candSite = isCandidate(mirna, site);
            if (candSite != null) {
                candSite.setMatureMirnaTranscript(mirna);
                candSite.setGene3UTRStart(i);
                candSite.setGene3UTREnd(i + site.length());
                candSite.setSiteTranscript(site);
                candidateSiteMap.put(i, candSite);
            }
        });

        //save the candidate mbs in an ordered way
        for (int i = mRNA.length() - this.maxSiteLength; i > 0; i--) {
            if (candidateSiteMap.get(i) != null) {
                candidateList.add(candidateSiteMap.get(i));
            }
        }

        return candidateList;
    }
    
    

    /**
     * Checks if a microRNA can bind to a sequence shorter than maxSiteLength. We follow standard seed
     * parameter settings and consider seeds of length 6–8 bases, beginning at
     * position 2 of the microRNA. Minimum pairing is a perfect 6-mers. 
     * No mismatches or loops are allowed, but a
     * single G:U wobble is allowed in 7- or 8-mers.
     *
     * @param mirna microRNA sequence (5' -> 3')
     * @param seqB RNA sequence of the target (5' -> 3'). Returns false if it's longer than 
     * maxSiteLengh
     *
     *
     * @return double[0] is the size of the seed region. double[1] the free
     * energy of the folding.
     */
    public CandidateMBS isCandidate(String mirna, String seqB) {        
        //log.debug("Checking candidate site {} - {}", mirna, seqB);
        
        
        int WCPairs = 0;
        int wobblePairs = 0;

        //if the sequence is too long, returns false
        if (seqB.length() > maxSiteLength) {
            return null;
        }

        DnaFoldPrediction fold = RNAFolder.foldSeqs(seqB, mirna);
        double freeEnergy = fold.deltaG();
        //if the squence cannot be fold returns false
        if (freeEnergy == 0) {
            return null;
        }
        //if the squence cannot be fold (they do not form a stable bind returns false
        if (!RNAFolder.checkBindingFromStructure(fold.structure())) {
            return null;
        }
        
        //we check at which point the sequence and the mirna are binding. This is useful for future 'alignment'
        int seedRegionStartInSequence = this.findBindingStartInSequence(fold.structure());
        
        
        String comment="PITA: canonnical";
        boolean canonnical=true;

        //The structure has a size of |seqB|+7+|mirna|
        //the seed region starts at structure.length - |mirna|+1
        int seedStart = fold.structure().length() - mirna.length() + seedRegionStart;
        int seedIndex = seedStart;
        int nPairs = 0;
        while (seedIndex < fold.structure().length() && seedIndex < (seedStart + seedRegionSize)) {
            //checking the seed region
            if (fold.structure().charAt(seedIndex) == ')' || fold.structure().charAt(seedIndex) == '}') {
                String pair = this.getPairAt(seedIndex, fold.sequence(), fold.structure());
                //counting pairs
                if (pair.length() > 1) {
                    if (this.isWobble(pair.charAt(0), pair.charAt(1))) { //check if it is a wobble (non-canonical)
                        wobblePairs++;
                        comment="PITA: noncanonical";
                        canonnical=false;
                    } else if (this.isWatsonCrick(pair.charAt(0), pair.charAt(1))) {
                        WCPairs++;
                    }

                }
                nPairs++;
            } else if (nPairs < 6) { //we have exited the seed region and not enough pairs
                                     //have been found, therefore it is not a candidate site
                return null;
            } else {                //we have exited the seed region, we exit the loop.
                break;
            }
            seedIndex++;
        }

        if (WCPairs < 6) { //too few WC pairs
            return null;
        }
        if (wobblePairs > 1) { //too much wobble pairs
            return null;
        }

        CandidateMBS cs = new CandidateMBS();
        cs.setFreeEnergy(freeEnergy);
        cs.setBindsInSeed(nPairs);
        cs.setWCpairsInSeed(WCPairs);
        cs.setWobblePairsInSeed(wobblePairs);
        cs.setSeedRegionStart(1);
        cs.setSeedRegionEnd(nPairs);
        cs.setComment(comment);
        cs.setIsCanonical(canonnical);
        cs.setSeedRegionStartInSequence(seedRegionStartInSequence);
        
        return cs;
    }
    
    /**
     * used to test/develop teh site finder
     * @param args 
     */
    public static void main(String[] args){
        String mirna = "UAAAGGGAUUAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT"; 
        
        
        System.out.println(seqB);
        System.out.println(mirna);
        
        System.out.println(seqB+"LLLLLLL"+mirna+"    <-- Main") ;
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();
        CandidateMBS expResult = null;
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        System.out.println(result);
    }
}
