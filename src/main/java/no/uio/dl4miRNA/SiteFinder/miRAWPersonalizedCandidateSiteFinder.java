/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;

/**
 *
 * @author apla
 */
public class miRAWPersonalizedCandidateSiteFinder extends CandidateSiteFinder {

    @Override
    /**
     * Returns the transcript of the candidate sites in a mRNA given a miRNA.
     * Given a miRNA returns the transcripts of the candidate binding sites in a
     * mRNA. A candidate site must have: - A free energy lower than
     * 'minimumFreeEnergy'. - A minimum of minBindsSeed pairs in the miRNA seed
     * Region. The size of the seed region and its starting point where manually
     * set by the user using 'personalize' (By default Size=10, start=1).
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * @return ArrayList with the the candidate sites.
     */
    protected ArrayList<CandidateMBS> findCandidates(String mirna, String mRNA) {

        //Obtain the candidate sites
        return obtainCandidatePlaces(mirna, mRNA);

    }

    /**
     * Searches for all the target candidates of a mRNA given a miRNA. Returns a
     * ArrayList with the Candidate Sites;
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * @return ArrayList with all the candidate sites.
     */
    private ArrayList<CandidateMBS> obtainCandidatePlaces(String mirna, String mRNA) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        //for each 30mer in the mRNA, check if it is a cnadidate
        for (int i = mRNA.length() - mirna.length(); i >= 0; i--) {
            int bindSize = this.maxSiteLength;
            if (mRNA.length() - i < this.maxSiteLength) {
                bindSize = mRNA.length() - i;
            }
            CandidateMBS candSite;
            candSite = isCandidate(mirna, mRNA.substring(i, i + bindSize));
            if (candSite != null) { //if it is a candidate, enrich with transcript, and other information.
                candSite.setMatureMirnaTranscript(mirna);

                candSite.setGene3UTRStart(i);
                candSite.setGene3UTREnd(i + bindSize);

                candSite.setSiteTranscript(mRNA.substring(i, i + bindSize));

                candidateList.add(candSite);
            }
        }
        return candidateList;
    }

    /**
     * Checks if a microRNA can bind to a  sequence. Returns a tuple
     * containing the number of pairs in the seed region ( 0 if the binding in
     * the seed region is smaller than 6) and the free energy of the binding ( 0
     * if the binding in the seed region is smaller than 6).
     *
     * @param mirna microRNA sequence
     * @param seqB RNA sequence of the target. Returns false if it's longer than
     * maxSiteLength
     *
     *
     * @return double[0] is the size of the seed region. double[1] the free
     * energy of the folding.
     */
    public CandidateMBS isCandidate(String mirna, String seqB) {

  
        int seedRegionBinds = 0;
        int WCPairs = 0;
        int wobblePairs = 0;

        //if the sequence is too long, returns false
        if (seqB.length() > this.maxSiteLength) {
            return null;
        }

        //Check if the transcript can bind.
        DnaFoldPrediction fold = RNAFolder.foldSeqs(seqB, mirna);
        double freeEnergy = fold.deltaG();
        //if the squence cannot be fold returns false
        if (freeEnergy >= 0 || freeEnergy > maxFreeEnergy) { //not a site
            return null;
        }
        //if the squence cannot be fold (they do not form a stable bind returns false
        if (!RNAFolder.checkBindingFromStructure(fold.structure())) {
            return null;
        }

        String foldStructure = fold.structure();
        
        //we check at which point the sequence and the mirna are binding. This is useful for future 'alignment'
        int seedRegionStartInSequence = this.findBindingStartInSequence(fold.structure());
        

        //The structure has a size of |seqB|+7+|mirna|
        //the seed region starts at structure.length - |mirna|+1
        int seedStart = foldStructure.length() - mirna.length() + seedRegionStart;
        int firstBinding = 99;
        int lastBinding = 0;
        int seedIndex = seedStart;
        int cannonicalCount=0; //we check if it is a cannonical site (perfect 6mer, 7mer, 8mer...)
        int nChecked=0; //number of nucleotides checked
        
        while (seedIndex < fold.structure().length() && seedIndex < (seedStart + seedRegionSize)) {
            if (foldStructure.charAt(seedIndex) == ')') { //count for WC and Wobble pairs.
                seedRegionBinds++;
                String pair = this.getPairAt(seedIndex, fold.sequence(), foldStructure);
                if (this.isWatsonCrick(pair.charAt(0), pair.charAt(1))) {
                    WCPairs++;
                    if ((seedIndex - seedStart) > 0 && (seedIndex - seedStart < 7)) {
                        cannonicalCount++;
                    }
                } else if (this.isWobble(pair.charAt(0), pair.charAt(1))) {
                    wobblePairs++;
                }
                if (firstBinding==99){
                    firstBinding=nChecked+seedRegionStart;
                }
                lastBinding=nChecked+seedRegionStart;
            }

            seedIndex++;
            nChecked++;
        }

        if (seedRegionBinds >= minBindsSeed) { //if there are enough binds in the seed region
                                               //create a new candidate site

            CandidateMBS cs = new CandidateMBS();
            cs.setSeedRegionStart(firstBinding); //seedStart
            cs.setSeedRegionEnd(lastBinding); //seedStart+seedRegionSize
            cs.setFreeEnergy(fold.deltaG());
            cs.setBindsInSeed(seedRegionBinds);
            cs.setWCpairsInSeed(WCPairs);
            cs.setWobblePairsInSeed(wobblePairs);
            cs.setSeedRegionStartInSequence(seedRegionStartInSequence);
            
            if (cannonicalCount>=6){  //is canonical
                cs.setComment("DEF: cannonical"); 
                cs.setIsCanonical(true);
            }
            else{
                cs.setComment("DEF: non-cannonical");
            }
            
            //result[1] = lastCharacter(fold.structure(), ')') - Lsize - seqB.length(); //position of the last binding size in the mirna
            //System.out.println(fold.sequence());
            //System.out.println(fold.structure());
            return cs;

        }
        //there are not enough binds in the seed
        return null;
    }
    
    /**
     * Defines the size of the seedRegion and its starting nucleotide.
     * @param seedRegionSize size of the seed region.
     * @param seedRegionStart startinc nucleotide of the seed region (Note that 
     * the first nucleotide of the miRNA corresponds to 0).
     */
    public void personalize(int seedRegionSize, int seedRegionStart,int minBindsSeed){
        this.seedRegionSize=seedRegionSize;
        this.setSeedRegionStart(seedRegionStart);
        this.minBindsSeed=minBindsSeed;
        
        if (this.minBindsSeed>this.seedRegionSize){
            this.setMinBindsSeed(seedRegionSize);
        }
    }    
    
}
