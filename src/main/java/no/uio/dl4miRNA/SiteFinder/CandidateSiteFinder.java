/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import org.slf4j.LoggerFactory;

/**
 * This is the abstract class for the MBS finder. Given a miRNA the Candidate
 * site finders scroll a gene using a sliding window and look for potential
 * miRNA binding sites. Different miRNA target prediction tools have used
 * different definitions of a MIRNA binding site, therefore this should be
 * reflected in the implementations of this abstract class.
 *
 * This abstract class implements some methods that can be useful for almost any
 * candidate site finder.
 *
 * The logic underneath a candidate site finder should be implemented in the
 * abstract method "findCandidates". To search for candidates, the method
 * 'getCandidateSites' should be instantiated: it calls the find candidates
 * methods and checks that the obtained potential MBS are not overlapped.
 *
 *
 * @author apla
 */
public abstract class CandidateSiteFinder {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(CandidateSiteFinder.class);

    /**
     * Nucleotide position where the seed region starts
     */
    protected int seedRegionStart;
    /**
     * Minimum number of base pairs in the seed region required to be considered
     * as a potential MBS.
     */
    protected int minBindsSeed;
    /**
     * Number of nucleotides that the sliding window advance at every movement.
     */
    protected int winStep;
    /**
     * Maximum free energy in the binding allowed to be considered a stable
     * binding site.
     */
    protected double maxFreeEnergy;
    /**
     * Size of the seed region.
     */
    protected int seedRegionSize;

    protected int maxMirnaLength;
    protected int maxSiteLength;
    protected int seedAlignementOffset;

    private int seedStartIndex;

    /**
     * Super constructor. By default it sets seedRegionStart = 1; minBindSeed =
     * 6 ; winStep=10; maxFreeEnergy=0; seedRegionSize=9;
     */
    public CandidateSiteFinder() {
        //seedRegionStart = 0;
        seedRegionStart = 1;
        minBindsSeed = 6;
        winStep = 5;
        maxFreeEnergy = 0;
        //seedRegionSize = 10;
        seedRegionSize = 9;
        maxMirnaLength = 30;
        maxSiteLength = 30;
        seedAlignementOffset = 10;
        seedStartIndex = maxSiteLength - seedAlignementOffset;

    }

    //##############################
    //####Getters & Setters ########
    //##############################
    public int getSeedRegionStart() {
        return seedRegionStart;
    }

    public void setSeedRegionStart(int seedRegionStart) {
        this.seedRegionStart = seedRegionStart;
    }

    public int getMinBindsSeed() {
        return minBindsSeed;
    }

    public void setMinBindsSeed(int minBindsSeed) {
        this.minBindsSeed = minBindsSeed;
    }

    public int getWinStep() {
        return winStep;
    }

    public void setWinStep(int winStep) {
        this.winStep = winStep;
    }

    public double getMaxFreeEnergy() {
        return maxFreeEnergy;
    }

    public void setMaxFreeEnergy(double maxFreeEnergy) {
        this.maxFreeEnergy = maxFreeEnergy;
    }

    public int getMaxMirnaLength() {
        return maxMirnaLength;
    }

    public void setMaxMirnaLength(int maxMirnaLength) {
        this.maxMirnaLength = maxMirnaLength;

    }

    public int getMaxSiteLength() {
        return maxSiteLength;

    }

    public void setMaxSiteLength(int maxSiteLength) {
        this.maxSiteLength = maxSiteLength;
        this.seedStartIndex = this.maxSiteLength - this.seedAlignementOffset;
    }

    public int getSeedAlignementOffset() {
        return seedAlignementOffset;
    }

    public void setSeedAlignementOffset(int seedAlignementOffset) {
        this.seedAlignementOffset = seedAlignementOffset;
        this.seedStartIndex = this.maxSiteLength - this.seedAlignementOffset;
    }

    //##############################
    //#########  Methods  ##########
    //##############################
    /**
     * Given a bracket from the 'ViennaRNA bracket notation', it returns its
     * oposite. THis is intended to facilitate the search of basePairs when
     * using bracket notation. Example: if simbol = ')' --> returns '('; if
     * simbol ='[' --> returns ']';
     *
     * @param simbol bracket character from the bracket notation
     * @return the oposite of the bracket character
     */
    protected char getComplementary(char simbol) {
        switch (simbol) {
            case ')':
                return '(';
            case '(':
                return ')';
            case '{':
                return '}';
            case '}':
                return '{';
            case ']':
                return '[';
            case '[':
                return ']';
            default:
                return simbol;
        }

    }

    //##############################
    //##### Abstract Methods #######
    //##############################
    protected abstract ArrayList<CandidateMBS> findCandidates(String mirna, String mRNA);

    //##############################
    //###### Final Methods #########
    //##############################
    /**
     * Returns true if the two nucleotides n1 and n2 form a Watson-Crick
     * basepair. (A-T, A-U, G-C and viceversa)
     *
     * @param n1 nucleotide 1
     * @param n2 nucleotide 2
     * @return true if they form a Watson-Crick basepair.
     */
    protected final boolean isWatsonCrick(char n1, char n2) {
        char naux1 = Character.toUpperCase(n1);
        char naux2 = Character.toUpperCase(n2);

        if ((naux1 == 'A') && (naux2 == 'U' || naux2 == 'T')) {
            return true;
        } else if (((naux2 == 'A') && (naux1 == 'U' || naux1 == 'T'))) {
            return true;
        } else if ((naux1 == 'G') && (naux2 == 'C')) {
            return true;
        } else if ((naux2 == 'G') && (naux1 == 'C')) {
            return true;
        }

        return false;
    }

    /**
     * Returns true if the two nucleotides n1 and n2 form a Wobble basepair.
     * (G-U, G-T, I-A, I-C, I-U)
     *
     * @param n1 nucleotide 1
     * @param n2 nucleotide 2
     * @return true if they form a Wobble basepair.
     */
    protected final boolean isWobble(char n1, char n2) {
        char naux1 = Character.toUpperCase(n1);
        char naux2 = Character.toUpperCase(n2);

        if (naux1 == 'I' || naux2 == 'I') {
            return (naux1 != 'G' && naux2 != 'G');
        }

        return ((naux1 == 'G' && (naux2 == 'T' || naux2 == 'U')) || ((naux1 == 'T' || naux1 == 'U') && naux2 == 'G'));

    }

    /**
     * Returns the nucleotide position within the site transcript in which the
     * targetSite and a miRNA start its binding.
     *
     * @param structure bracket notation folding structure defining the folding
     * between a site transcript and a mirna (Example: ...(((((((((&)))))))... )
     * (siteTranscriptBindings&miRNA bindings)
     * @return site nucleotide index in which the binding starts
     */
    public static final int findBindingStartInSequence(String structure) {

        if (structure.contains("&")) {
            int i = structure.indexOf("&");
            boolean found = false;
            int openPar = 0;
            while (i >= 0 && !found) {
                char curChar = structure.charAt(i);
                if (curChar == ')' || curChar == '}') {
                    openPar++;
                }
                if (curChar == '(' || curChar == '{') {
                    if (openPar == 0) {
                        return i;
                    }
                    openPar--;
                }
                i--;
            }
            log.warn("No match between SeqA & SeqB found.");
            return -1;
        }
        log.warn("Wrong kind of folded structure (missing &): " + structure);
        return -1;
    }

    /**
     * Returns the nucleotide position within the miRNA transcript in which the
     * targetSite and a miRNA start its binding.
     *
     * @param structure bracket notation folding structure defining the folding
     * between a site transcript and a mirna (Example: ...(((((((((&)))))))... )
     * (siteTranscriptBindings&miRNA bindings)
     * @return site nucleotide index in which the binding starts
     */
    public static final int findBindingStartInMirna(String structure) {

        if (structure.contains("&")) {
            int i = structure.indexOf("&");
            boolean found = false;
            int openPar = 0;
            while (i < structure.length() && !found) {
                char curChar = structure.charAt(i);
                if (curChar == '(' || curChar == '{') {
                    openPar++;
                }
                if (curChar == ')' || curChar == '}') {
                    if (openPar == 0) {
                        return i;
                    }
                    openPar--;
                }
                i++;
            }
            log.warn("No match between SeqA & SeqB found.");
            return -1;
        }
        log.warn("Wrong kind of folded structure (missing &): " + structure);
        return -1;
    }

    /**
     * Given a transcript and a predicted secondary structure following the
     * bracket notation, it returns the nucleotide at position 'pos' and its
     * corresponding nucleotide. If the nucleotide does not have a basepair, it
     * only returns the nucleotide at 'pos'
     *
     * @param pos position of the nucleotide
     * @param transcript transcript to be analyzed
     * @param structure secondary structure following bracket notation
     * @return
     */
    protected final String getPairAt(int pos, String transcript, String structure) {
        char structPos = structure.charAt(pos);
        char transPos = transcript.charAt(pos);
        char complement = getComplementary(structPos);

        //There is no bracket
        if (structPos == '.' || structPos == ',') {
            return structPos + "";
        }

        //if it is a closing bracket, we need to search 'to the left'
        if (structPos == ')' || structPos == '}') {

            int complFound = 0;
            boolean found = false;

            //we have to count how many closing brackets we find when we go left,
            //and to find the same number of opening brackets.
            int i = pos - 1;
            while (!found && i >= 0) {
                if (structure.charAt(i) == complement) {
                    if (complFound == 0) {
                        found = true;
                    } else {
                        complFound--;
                    }
                }
                if (structure.charAt(i) == structPos) {
                    complFound++;
                }
                i--;
            }

            if (found) {
                i++;
                return transcript.charAt(i) + "" + transPos;
            }
        }

        //if it is an opening bracket, we need to search 'to the right'
        if (structPos == '{' || structPos == '(') {

            int complFound = 0;
            boolean found = false;

            //we have to count how many opening brackets we find when we go right,
            //and to find the same number of closing brackets.
            int i = pos - 1;
            while (!found && i < structure.length()) {
                if (structure.charAt(i) == complement) {
                    if (complFound == 0) {
                        found = true;
                    } else {
                        complFound--;
                    }
                }
                if (structure.charAt(i) == structPos) {
                    complFound++;
                }
                i++;
            }

            if (found) {
                return transcript.charAt(i) + "" + transPos;
            }
        }
        return transPos + "";
    }

    /**
     * Returns the transcript of the candidate sites in a mRNA given a miRNA.
     * Given a miRNA returns the transcripts of the candidate miRNa binding
     * sites in the mRNA transcript. To do so, it instantiates the abstract
     * method findCandidates; then, it checks that the obtained candidate MBSs
     * do not overlap.
     *
     *
     * @param mirnaTranscript transcript of the miRNA target (5' -> 3')
     * @param mRNATranscript transcript of the mRNATranscript (5' -> 3')
     * @return ArrayList of CandidateSites, with the the detected candidate
     * sites.
     */
    public final ArrayList<CandidateMBS> getCandidateSites(String mirnaTranscript, String mRNATranscript) {
        //Check size of the mirna Trasncript
        if (mirnaTranscript.length() > this.maxMirnaLength) {
            log.error("the first parameter is not a miRNA!\n" + mirnaTranscript);
        }
        //check that the winStep is higher than 0 so we ensure that the sliding
        //window is moving
        if (winStep < 1) {
            winStep = 1;
        }
        //we ensure that the winStep is smaller than the mirnaTranscriptSize to
        //ensure that we cover all the options
        if (winStep > mirnaTranscript.length()) {
            winStep = mirnaTranscript.length();
        }

        //get the Candidate sites
        ArrayList<CandidateMBS> candidates = findCandidates(mirnaTranscript, mRNATranscript);

        //Create the list of canidates that will be returned
        ArrayList<CandidateMBS> candidateSequenceSites = new ArrayList<>();

        //checks that the candidate sites are not overlapped and they do not fall within
        //the winStep limit
        
        candidates.parallelStream().forEach((cs) -> {
            cs.setMatureMirnaTranscript(mirnaTranscript);
            if (cs.getBindsInSeed() > 0) {
                cs.alignAndExtend(mRNATranscript, this.maxSiteLength, this.seedStartIndex);                
            }
            
        });
        candidates.parallelStream().forEachOrdered((cs) -> {
            int seedSize = cs.getBindsInSeed();
            if (seedSize > 0) {
                if (candidateSequenceSites.isEmpty()) { //If the list of candidates is empty, we add the first candidate site to the list.
                    candidateSequenceSites.add(cs);
                } else {    //there are already candidates in the list. We check that the new candidate is not overlapping the last candidate of teh list. 

                    CandidateMBS tempCS = candidateSequenceSites.get(candidateSequenceSites.size() - 1);
                    if (tempCS.getGene3UTRStart() - cs.getGene3UTRStart() < winStep) {  //if there is overlapping, 
                        if (tempCS.getFreeEnergy() > cs.getFreeEnergy()) {  //keep the one with the lowest free energy
                            candidateSequenceSites.remove(candidateSequenceSites.size() - 1);
                            candidateSequenceSites.add(cs);
                        }
                    } else {
                        candidateSequenceSites.add(cs);
                    }
                }
            }
        });
        return candidateSequenceSites;

    }

}
