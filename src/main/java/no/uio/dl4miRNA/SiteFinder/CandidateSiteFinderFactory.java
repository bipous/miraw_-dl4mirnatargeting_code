/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;


import org.slf4j.LoggerFactory;

/**
 * Factory class to create CandidateSiteFinder Objects.
 * 
 * @author apla
 */
public class CandidateSiteFinderFactory {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(CandidateSiteFinderFactory.class);

    /**
     * Factory class to create CandidateSiteFinder Objects.
     * 
     * @param siteFinderType string that determines the type of candidateSiteFinder
     * to be created.
     * PITA --> PitaCandidateSiteFinder
     * Default --> DefaultCandidateSiteFinder
     * TargetScan --> TargetScanSiteFinder
     * Personalized;integer1;integer2 --> miRAWPersonalizedCandidateSiteFinder 
     * (with custom seed region: start at integer1; length at integer2).
     * @return CandidateSiteFinder object.
     */
    public static CandidateSiteFinder getSiteFinder(String siteFinderType) {
        if (siteFinderType != null) {
            if (siteFinderType.equalsIgnoreCase("PITA")) {
                return new PitaCandidateSiteFinder();
            } else if (siteFinderType.equalsIgnoreCase("Default")) {
                return new DefaultCandidateSiteFinder();
            }
            else if (siteFinderType.equalsIgnoreCase("Training")) {
                return new DefaultCandidateSiteFinder();
            }else if (siteFinderType.equalsIgnoreCase("TargetScan") || siteFinderType.equalsIgnoreCase("TS")) {
                return new TargetScanSiteFinder();
            }
            else if(siteFinderType.startsWith("Personalized")){
                miRAWPersonalizedCandidateSiteFinder siteFinder = new miRAWPersonalizedCandidateSiteFinder();
                String[] parts = siteFinderType.split(";");
                int seedReg=10; int seedStart=1; int minBindsSeed=6;
                if (parts.length>=2){
                    if (parts[1]!=null){
                        seedStart = Integer.parseInt(parts[1]);
                    }
                    if (parts[2]!=null){
                        seedReg = Integer.parseInt(parts[2]);
                    }
                    if (parts[3]!=null){
                        minBindsSeed = Integer.parseInt(parts[3]);
                    }
                }
                siteFinder.personalize(seedReg, seedStart,minBindsSeed);
                return siteFinder;
            
            } else {
                log.warn("Unknow SiteFinder type " + siteFinderType + ". Default siteFinder will be used");
                return new DefaultCandidateSiteFinder();
            }
        } else {
            log.warn(" SiteFinder type is null. Default siteFinder will be used");
            return new DefaultCandidateSiteFinder();
        }

    }
}
