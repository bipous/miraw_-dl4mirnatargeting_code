/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class acts as a bridge between miRAW and the wrapper for ViennaRNA
 * developed by Fulcrum Genomics LLC (https://github.com/fulcrumgenomics).
 *
 * @author apla
 */
public class RNAFolder {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(RNAFolder.class);
    
    private static final int TMP=37;
    private static final double FREE_SUBOPTIMAL_ENERGY=10.0;
    
    private static final int MAX_SITE_OPENING_LENGTH = 200;
    

    /**
     * Uses RNAFolder to foldSeqs the sequence A. (5' -> 3')
     *
     * @param seqA transcript of the sequence A (5' -> 3')
     * @return DnaFoldPrediction object with the result of the folding.
     */
    private static DnaFoldPrediction foldSeq(String seqA) {
    //    if (seqA.contains("&")) {
            RnaFoldPredictor RNAFolder;
            RNAFolder = new RnaFoldPredictor(getRNAFoldLocation(), (double) 37);
            DnaFoldPrediction result = RNAFolder.predictOptimalSecondaryStructure(seqA);
            try {
                RNAFolder.close();
            } catch (Exception e) {
                RNAFolder = null;
            }

            return result;
     //   }
     //   log.warn("There is only one sequence to cofold (no & found in the sequence)");
     //   return null;

    }

    /**
     * This class checks if the ViennaRNA fold binary is at the default linux
     * (/usr/bin/RNAfold) or OS X (/usr/local/bin/RNAfold)installation folders,
     * and returns it in File format.
     *
     * In the contrary case, it checs if an auxiliary file with the location of
     * RNAFold exists at the execution folder (./.rnaLcoation); if it exists, it
     * loads the RNAFold binary location from there.
     *
     * If the ./.rnaLocation does not exists, an error message will be prompted.
     * Following the code will create the ./.rnaLocation file with a message to
     * the user asking to manually set the rnaFold location.
     *
     * TODO: Automatically search RNAfold in the computer.
     *
     * @return File pointing to the Vienna RNAFold binary.
     */
    protected static File getRNAFoldLocation() {
        //Check OSX and Unix default locations.
        if (new File("/usr/local/bin/RNAfold").exists()) {
            return new File("/usr/local/bin/");
        }
        if (new File("/usr/bin/RNAfold").exists()) {
            return new File("/usr/bin/");
        }

        //It is not in the default location.
        //Checking if there is a configuration file specifying the loction.
        if (new File("./.rnaLocation").exists()) {
            //if the configuration file exists, read the RNAFold location.
            try {
                BufferedReader br = new BufferedReader(new FileReader("./.rnaLocation"));
                String line = br.readLine();
                String location = "";
                boolean located = false;
                while (line != null && !located) {
                    if (!line.startsWith("#")) { //ignore commentl ines
                        String[] parts = line.split("#");
                        location = parts[0];
                        while (location.endsWith(" ")) {
                            location = location.substring(0, location.length() - 1);
                        }
                        if (new File(location).exists()) { //check if the location is correct.
                            located = true;
                        }
                    }
                    line = br.readLine();
                }
                if (located) { //if the location is properly defined, returns the file pointint the binary.
                    return new File(location);
                }

            } catch (FileNotFoundException ex) {
                log.error(ex.toString());
            } catch (IOException ex) {
                Logger.getLogger(RNAFolder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //The configuration file does not exists or it is wrong. 
        //Create a new configuration file at the execution folder and show error messages.
        File RNALoc = new File("./.rnaLocation");
        if (RNALoc.exists()) {
            log.error("RNAFold could not be loaded from the specified location: \"" + RNALoc.getAbsolutePath() + "\"\n Please check that the location is correctly specified.");

        } else {
            log.error("RNAFolder could not be found in its default locations. Please specify its location in \"" + RNALoc.getAbsolutePath() + "\"");
            PrintWriter writer;
            try {
                writer = new PrintWriter("./.rnaLocation");
                writer.println("/usr/local/bin/"); //example with a defaul location, should be changed by the user
                writer.println("#specify the absolute path to the RNAFold binary. If you are working in windows, don't forget to include the .exe");
                writer.close();
            } catch (FileNotFoundException ex) {
                log.error("Could not create the ./.rnaLocation file: " + ex);
            }

        }

        return new File("/usr/local");
    }

    /**
     * Uses RNAFolder to fold SeqA with SeqB. Sequences assumed to be (5' -> 3')
     *
     * SeqA= "GGG", SeqB="CCC", folded sequence ="GGG&CCC"
     *
     * @param seqA transcript of the Sequence A (5' -> 3')
     * @param seqB transcript of the Sequence B (5' -> 3')
     * @return DnaFoldPrediction object with the result of the folding
     */
    public static DnaFoldPrediction foldSeqs(String seqA, String seqB) {
        if (seqA.length() > 0 && seqB.length() > 0) {
            String seqAux = seqA + "&" + seqB;
            //String seqAux = seqA + "LLLLLLL" + seqB;
            return foldSeq(seqAux);
        }
        return null;
    }

    /**
     * Uses RNAFolder to fold SeqA with SeqB. To do that, SeqA and SeqB are
     * concatenated using an auxiliary sequence of length l. Sequences are
     * assumed to be (5' -> 3')
     *
     *
     * @param seqA transcript of the Sequence A (5' -> 3')
     * @param seqB transcript of the Sequence B (5' -> 3')
     * @param l number of neutral nucleotides to be put between the two
     * sequences
     * @return DnaFoldPrediction object with the result of the folding
     */
    @Deprecated
    public static DnaFoldPrediction foldSeqs(String seqA, String seqB, int l) {
        String aux = "&";
        for (int i = 0; i < l; i++) {
            //aux = aux + "L";
        }
        String seqAux = seqA + aux + seqB;
        return foldSeq(seqAux);
    }

    /**
     * Checks if two sequences bind together. Sequences assumed to be (5' -> 3')
     *
     * @param seqA transcript A (5' -> 3')
     * @param seqB transcript B (5' -> 3')
     * @return true if they bind.
     */
    public static boolean checkBinding(String seqA, String seqB) {
        //Returns true if the freeenergy of the folding is lower than 0.
        DnaFoldPrediction res = RNAFolder.foldSeqs(seqA, seqB);
        if (res.deltaG() >= 0) {
            return false;
        }
        String foldStruct = res.structure();
        String sequence = res.sequence();

        int openPar = 0;
        int closePar = 0;

        for (int i = foldStruct.indexOf("&") + 1; i < foldStruct.length(); i++) {
            char ch = foldStruct.charAt(i);
            if (ch == '(' || ch == ('[') || ch == ('{')) {
                openPar++;
            }
            if (ch == ')' || ch == (']') || ch == ('}')) {
                closePar++;
            }
        }

        return (closePar > openPar);

    }
    
        /**
     * Checks if the structure of two sequences bind together. Sequences assumed to be (5' -> 3')
     *
     * @param seqA folded structure
     * @return true if they bind.
     */
    public static boolean checkBindingFromStructure(String structure) {
        //Returns true if the freeenergy of the folding is lower than 0.
        
        if (structure==null){
            log.warn("The structure is null");
            return false;
        }
        
        if (!structure.contains(("&"))){
            log.warn("The structure does not correspond to the folding of two sequences. & is missing");
            return false;
        }
        
        String foldStruct = structure;

        int openParentesis = 0;
        int closePar = 0;

        for (int i = foldStruct.indexOf("&") + 1; i < foldStruct.length(); i++) {
            char ch = foldStruct.charAt(i);
            if (ch == '(' || ch == ('[') || ch == ('{')) {
                openParentesis++;
            }
            if (ch == ')' || ch == (']') || ch == ('}')) {
                closePar++;
            }
        }

        return (closePar > openParentesis);

    }

    /**
     * Predicts the secondary structure of the targetSite sequence and the miRNa 
     * sequence. The seed region of the the miRNA sequence is forced to be binded
     * to the siteTarget (if possible), the seed region of the miRNA cannot bind 
     * to the 3' of the miRNA. 
     * 
     * @param targetSite
     * @param miRNA
     * @return Predicted DNA fold structure
     */
    public static DnaFoldPrediction getBestFoldWithSeedBinding(String targetSite,String miRNA){
        RnaFoldSubopt folder = new RnaFoldSubopt(getRNAFoldLocation(),TMP,FREE_SUBOPTIMAL_ENERGY);
        DnaFoldPrediction prediction = folder.obtainBestFoldWithSeedBinding(targetSite, miRNA);
        try {
                folder.close();
            } catch (Exception e) {
                folder = null;
            }
        return prediction;
    }
    
    /**
     * Returns the energy necessary to "open" a binding site in a sequence.
     * As predicted secondary strucutres are only reliable in sequences smaller than 250 nucleotides
     * only the surroundings of the gap are considered for the folding.
     * 
     * 
     * @param sequence nucleotide sequence to be tested
     * @param startOpeningRegion starting position of the site to be opened.
     * @param endOpeningRegion ending position of the site to be opened.
     * @param additionalOpenedPairs additional upstream and downstream nucleotides 
     * to be opened in the sequence
     * @return site accessibility energy (MFE(openedSite)-MFE(regularSite)) MFE=MinimumFreeEnergy 
     * The lower the SAE, the easier to access the site.
     */
    public static double siteOpeningEnergy(String sequence, int startOpeningRegion, int endOpeningRegion, int additionalOpenedPairs){
        //we reduce the size of the sequence to just the portion surrounding the gap to poen
        int sequenceChunk=MAX_SITE_OPENING_LENGTH-endOpeningRegion+startOpeningRegion;
        
        int subSeqStart = Math.max(startOpeningRegion - sequenceChunk/2,0);
        int subSeqEnd = Math.min(endOpeningRegion + sequenceChunk/2,sequence.length());
        String subSeq = sequence.substring(subSeqStart,subSeqEnd);
        
        double regEnergy = RNAFolder.foldSeq(subSeq).deltaG();
        
        int shift = subSeqStart;        
        int auxStartOpen=startOpeningRegion-shift;
        int auxEndOpen=endOpeningRegion-shift;        
        int gapStart = auxStartOpen-additionalOpenedPairs;
        int gapEnd = auxEndOpen+additionalOpenedPairs;
        
        DnaFoldPrediction folded = RNAFolder.foldSeqWithOpenGap(subSeq,gapStart,gapEnd);
       
        double openingEnergy = folded.deltaG();
        return openingEnergy - regEnergy  ;
    }
    
    /**
     * Returns the energy necessary to "open" a binding site in a sequence.
     * As predicted secondary strucutres are only reliable in sequences smaller than 250 nucleotides
     * only the surroundings of the gap are considered for the folding.
     * 
     * 
     * @param sequence nucleotide sequence to be tested
     * @param startOpeningRegion starting position of the site to be opened.
     * @param endOpeningRegion ending position of the site to be opened.
     * @return site accessibility energy (MFE(openedSite)-MFE(regularSite)) MFE=MinimumFreeEnergy 
     * The lower the SAE, the easier to access the site.
     */
    public static double siteOpeningEnergy(String sequence, int startOpeningRegion, int endOpeningRegion){
        
        return siteOpeningEnergy(sequence, startOpeningRegion, endOpeningRegion,0);
    }
    
    private static DnaFoldPrediction foldSeqWithOpenGap(String sequence, int gapStart, int gapEnd) {
       
       File RNAFoldLoc = getRNAFoldLocation();       
       RnaFoldSubopt Rnafolder = new RnaFoldSubopt(RNAFoldLoc,37,0);       
       DnaFoldPrediction ret = Rnafolder.obtainFoldWithOpenedSite(sequence, gapStart, gapEnd);          
       return      ret;
    }
    
    public static void main(String[] args) {

        double x = siteOpeningEnergy("AACTATAGTGAGGCCTTATTGCCAGGAGGGAGGGTTTTGGTTGCTGGCGCTTGTGTATAAAGGGGCAAGAGCAGCTCCTTTGGACTATTCCTGGGAGGACTCTGATGCAGGGCGTCTGTTGCTCCCCTGGGTCACCTCCTCCCTGCTCGCTGACATCTGGGGCTTTGACCCTTTCTTTTTTAATCTACTTTTGCTAAGATGCATTTAATAAAAAAAAAGAGAGAGAGAGAGAGGTGTGAGGGACAAAATGCAAACCTATT",100,160,0);
        System.out.println("Energy = "+x);
        System.out.println();
    }


}
