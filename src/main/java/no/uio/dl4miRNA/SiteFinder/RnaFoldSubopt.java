package no.uio.dl4miRNA.SiteFinder;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.jetty.io.RuntimeIOException;
import org.slf4j.LoggerFactory;

/**
 * Class that wraps ViennaRNAs RNAFold utility which can be used to estimate the
 * minimum free energy secondary structure of DNA and RNA molecules. When
 * constructed a background process is started running RNAFold, calls to
 * predict() then pipe input and output through RNAFold.
 *
 * @author Tim Fennell
 */
public class RnaFoldSubopt {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(RnaFoldSubopt.class);

    private final Process process;
    private final BufferedWriter out;
    private final BufferedReader in;

    /**
     * Constructs an instance that has a running copy of ViennaRNA's RNAFold
     * executable behind it that can then be interactively pushed a new sequence
     * and get back folding information.
     *
     * @param viennaFolder the parent dir of Vienna RNA's bin, lib, share
     * directories
     * @param tm the tm at which the calculations should be performed
     * @param energyMargin the maximum energy difference allowed between the
     * suboptimal bindings and the optimal binding.
     */
    public RnaFoldSubopt(final File viennaFolder, final double tm, double energyMargin) {
        //public RnaFoldSubopt(final File viennaBinnary, final double tm) {

        final List<String> args = new ArrayList<>();
        //args.add(viennaFolder.getAbsolutePath() + "RNAcofold");
        args.add(viennaFolder.getAbsolutePath()+"/RNAsubopt");
        args.add("--noconv");
        args.add("--temp=" + tm);
        args.add("--constraint");
        args.add("-e " + energyMargin);
        args.add("-s");
        //args.add("--noPS");

        //args.add("--paramFile=" + params.getAbsolutePath());
        try {
            final ProcessBuilder builder = new ProcessBuilder(args);
            builder.redirectError(ProcessBuilder.Redirect.INHERIT);
            this.process = builder.start();
            this.out = new BufferedWriter(new OutputStreamWriter(this.process.getOutputStream()));
            this.in = new BufferedReader(new InputStreamReader(this.process.getInputStream()));
        } catch (IOException ioe) {
            throw new RuntimeIOException(ioe);
        }
    }

    /**
     * Creates the secondary structure prediction for the input sequence.
     */
    public DnaFoldPrediction predict(final String sequenceA, final String sequenceB) {
        String sequence = sequenceA + "&" + sequenceB;

        String constraint = "";
        for (char c : sequenceA.toCharArray()) {
            constraint = constraint + ".";
        }
        constraint = constraint + "&";
        for (int i = 0; i < sequenceB.length(); i++) {
            if (i < 9) {
                constraint = constraint + ">";
            } else {
                constraint = constraint + ".";
            }
        }

        try {
            this.out.write(sequence);
            this.out.newLine();
            this.out.flush();
            this.out.write(constraint);
            this.out.newLine();
            this.out.flush();
            this.out.write("@");
            this.out.newLine();
            this.out.flush();

            final String seq2 = readLine();
            String result = seq2;

            while (result != null) {
                if (!seq2.split(" ")[0].equals(sequence)) {
                    throw new IllegalStateException("Return sequence '" + seq2 + "' does not match entered sequence '" + sequence + "'");
                }
                result = readLine();
            }

            //final String structure = result.substring(0, seq2.length());
            //final double dg = Double.parseDouble(result.substring(seq2.length()).replace("(", "").replace(")", ""));
            //return new DnaFoldPrediction(seq2, structure, dg);
            return null;
        } catch (IOException ioe) {
            throw new RuntimeIOException(ioe);
        }
    }

    /**
     * Creates the secondary structure prediction for the input sequence. It
     * uses constraints in the RNAcofold in order to prevent the microRNA seed
     * region to bind to the own microRNA: if the seed region binds to any site,
     * it will be to the mRNAtarget: Sequence: mRNAXXXXXXXXX&miRNAXXXXXXX
     * Constraint: .............&>>>>>>......
     *
     */
    public DnaFoldPrediction obtainBestFoldWithSeedBinding(final String mRNAtarget, final String miRNA) {
        DnaFoldPrediction checkedPrediction = null;

        String sequence = mRNAtarget + "&" + miRNA;

        String constraint = "";
        for (char c : mRNAtarget.toCharArray()) {
            constraint = constraint + ".";
        }
        constraint = constraint + "&";
        for (int i = 0; i < miRNA.length(); i++) {
            if (i < 9) {
                constraint = constraint + ">";
            } else {
                constraint = constraint + ".";
            }
        }
        
        log.debug("{} - {}",mRNAtarget,miRNA);

        try {
            this.out.write(sequence);
            this.out.newLine();
            this.out.flush();
            this.out.write(constraint);
            this.out.newLine();
            this.out.flush();
            this.out.write("@");
            this.out.newLine();
            this.out.flush();

            final String seq2 = readLine();
            if (seq2==null){
                return checkedPrediction;
            }
            
            if (!seq2.split(" ")[0].equals(sequence)) {
                throw new IllegalStateException("Return sequence '" + seq2 + "' does not match entered sequence '" + sequence + "'");
            }
            String structure = readLine();
            while (structure != null) {
                String seedStructure = structure.substring(structure.indexOf("&") + 1, structure.indexOf("&") + 9);
                if (seedStructure.matches(".*\\).*\\).*\\).*\\).*")) { //REGEX EXPRESSION that requires 4 cosing parenthesis character
                    String[] splits = structure.split(" ");
                    String bindStruct = splits[0];
                    double delta = Double.parseDouble(splits[splits.length - 1]);
                    if (delta >= 0) {
                        log.
                                error("Suboptimal fold has positive energy");
                    }
                    checkedPrediction = new DnaFoldPrediction(sequence, bindStruct, delta);

                    break;
                };

                structure = readLine();
            }

            //final String structure = result.substring(0, seq2.length());
            //final double dg = Double.parseDouble(result.substring(seq2.length()).replace("(", "").replace(")", ""));
            //return new DnaFoldPrediction(seq2, structure, dg);
            return checkedPrediction;
        } catch (IOException ioe) {
            throw new RuntimeIOException(ioe);
        }
    }

    /**
     * Creates the secondary structure prediction for the input sequence.
     */
    public DnaFoldPrediction obtainBestFoldWithSeedBinding(final String mRNAtarget, final String miRNA, int sequenceOffset) {
        DnaFoldPrediction checkedPrediction = null;

        String sequence = mRNAtarget + "&" + miRNA;

        String constraint = "";
        int j = 0;
        for (char c : mRNAtarget.toCharArray()) {
            if (j < mRNAtarget.length() - sequenceOffset) {
                constraint = constraint + ".";
            } else {
                constraint = constraint + "x";
            }
            j++;
        }
        constraint = constraint + "&";
        for (int i = 0; i < miRNA.length(); i++) {
            if (i < 9) {
                constraint = constraint + ">";
            } else {
                constraint = constraint + ".";
            }
        }

        try {
            this.out.write(sequence);
            this.out.newLine();
            this.out.flush();
            this.out.write(constraint);
            this.out.newLine();
            this.out.flush();
            this.out.write("@");
            this.out.newLine();
            this.out.flush();

            final String seq2 = readLine();

            if (!seq2.split(" ")[0].equals(sequence)) {
                throw new IllegalStateException("Return sequence '" + seq2 + "' does not match entered sequence '" + sequence + "'");
            }
            String structure = readLine();
            while (structure != null) {
                String seedStructure = structure.substring(structure.indexOf("&") + 1, structure.indexOf("&") + 9);
                if (seedStructure.matches(".*\\).*\\).*\\).*\\).*")) { //REGEX EXPRESSION that requires 4 cosing parenthesis character
                    String[] splits = structure.split(" ");
                    String bindStruct = splits[0];
                    double delta = Double.parseDouble(splits[splits.length - 1]);
                    if (delta >= 0) {
                        log.
                                error("Suboptimal fold has positive energy");
                    }
                    checkedPrediction = new DnaFoldPrediction(sequence, bindStruct, delta);

                    break;
                };

                structure = readLine();
            }

            //final String structure = result.substring(0, seq2.length());
            //final double dg = Double.parseDouble(result.substring(seq2.length()).replace("(", "").replace(")", ""));
            //return new DnaFoldPrediction(seq2, structure, dg);
            return checkedPrediction;
        } catch (IOException ioe) {
            throw new RuntimeIOException(ioe);
        }
    }

    /**
     * Reads a line from the input, ignoring warning lines.
     */
    private String readLine() throws IOException {
        while (true) {
            final String line = this.in.readLine();
            if (line == null || !line.startsWith("WARNING")) {
                return line;
            }
        }
    }

    /**
     * Kills the underlying process and makes future calls to predict() invalid.
     */
    public void close() throws IOException {
        this.out.close();
        if (this.process.isAlive()) {
            this.process.destroy();
        }
    }

 

    /**
     * Creates the secondary structure prediction for the input sequence. It
     * uses constraints in the RNAcofold in order to prevent the microRNA seed
     * region to bind to the own microRNA: if the seed region binds to any site,
     * it will be to the mRNAtarget: Sequence: mRNAXXXXXXXXX&miRNAXXXXXXX
     * Constraint: .............&>>>>>>......
     *
     */
    public DnaFoldPrediction obtainFoldWithOpenedSite(final String sequence, int openingStart, int openingEnd) {
        
        DnaFoldPrediction openedPrediction = null;

        String constraint = "";
        int i = 0;
        for (char c : sequence.toCharArray()) {
            if (i < openingStart || i > openingEnd) {
                constraint = constraint + ".";
            } else {
                constraint = constraint + "x";
            }
            i++;
        }        

        try {
          
            this.out.write(sequence);
            this.out.newLine();
            this.out.flush();
            this.out.write(constraint);
            this.out.newLine();            
            this.out.flush();
            this.out.write("@");
            this.out.newLine();
            this.out.flush();

            String header = readLine(); 
            String structure = readLine();
            
            if (structure != null) {
                    String[] splits = structure.split(" ");
                    String bindStruct = splits[0];
                    double delta = Double.parseDouble(splits[splits.length - 1]);
                    openedPrediction = new DnaFoldPrediction(sequence, bindStruct, delta);
            }

            return openedPrediction;
        } catch (IOException ioe) {
            throw new RuntimeIOException(ioe);
        }
    }

    
       public static void main(String args[]) {
        RnaFoldSubopt folder = new RnaFoldSubopt(RNAFolder.getRNAFoldLocation(), 37, 10);
        String sequence="AACTATAGTGAGGCCTTATTGCCAGGAGGGAGGGTTTTGGTTGCTGGCGCTTGTGTATAAAGGGGCAAGAGCAGCTCCTTTGGACTATTCCTGGGAGGACTCTGATGCAGGGCGTCTGTTGCTCCCCTGGGTCACCTCCTCCCTGCTCGCTGACATCTGGGGCTTTGACCCTTTCTTTTTTAATCTACTTTTGCTAAGATGCATTTAATAAAAAAAAAGAGAGAGAGAGAGAGGTGTGAGGGACAAAATGCAAACCTATT";
        DnaFoldPrediction x = folder.obtainFoldWithOpenedSite(sequence, 60,100);
                
                
                

        if (x != null) {
            System.out.println(x.sequence());
            System.out.println(x.structure() + "   " + x.deltaG());
        }

    }
}
