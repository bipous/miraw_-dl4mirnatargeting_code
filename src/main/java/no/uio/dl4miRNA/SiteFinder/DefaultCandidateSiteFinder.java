/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import java.util.HashMap;
import no.uio.dl4miRNA.DL.GenePrediction;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import org.slf4j.LoggerFactory;

/**
 * miRAW candidate site finder method. The gene is scroleld and sites with a
 * potential stable binding with a free energy lower than {@link maxFreeEnergy}
 * (0), and a minium of {@link minBindsSeed} (6) pairs within the seed region.
 * Seed region defined as the {@link seedRegionSize} (9) nucleotides starting at
 * {@link seedRegionStart}. Values in parentesis correspond to default values.
 *
 * @author apla
 */
public class DefaultCandidateSiteFinder extends CandidateSiteFinder {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(DefaultCandidateSiteFinder.class);

    /**
     * Returns the transcript of the candidate sites in a mRNA given a miRNA.
     * Given a miRNA returns the transcripts of the candidate binding sites in a
     * mRNA. A candidate site must have: - A free energy lower than
     * 'minimumFreeEnergy'. - A minimum of minBindsSeed pairs in the miRNA seed
     * Region. - Be at least at 'winSize' positions far from the start position
     * of other adjacent candidate sites
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * @return ArrayList<CandidateSite> with the the candidate sites.
     */
    @Override
    protected ArrayList<CandidateMBS> findCandidates(String mirna, String mRNA) {

        //Obtain the candidate sites
        return obtainCandidatePlaces(mirna, mRNA);

    }

    /**
     * Searches for all the target candidates of a mRNA given a miRNA. Returns a
     * ArrayList with the Candidate Sites;
     *
     * 
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA seed region
     * @return ArrayList with all the candidate sites.
     */
    private ArrayList<CandidateMBS> obtainCandidatePlacesSequentail(String mirna, String mRNA) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        for (int i = mRNA.length() - this.maxSiteLength; i >= 0; i--) {
            int bindSize = this.maxSiteLength;
            if (mRNA.length() - i < this.maxSiteLength) {
                bindSize = mRNA.length() - i;
            }
            CandidateMBS candSite;
            candSite = isCandidate(mirna, mRNA.substring(i, i + bindSize));
            if (candSite != null) {
                
                candSite.setMatureMirnaTranscript(mirna);
                candSite.setGene3UTRStart(i);
                candSite.setGene3UTREnd(i + bindSize);
                candSite.setSiteTranscript(mRNA.substring(i, i + bindSize));
                candidateList.add(candSite);
            }
        }
        return candidateList;
    }

    /**
     * Searches for all the target candidates of a mRNA given a miRNA. Returns a
     * ArrayList with the Candidate Sites;
     * 
     * Optimized to be parallelized
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA seed region
     * @return ArrayList with all the candidate sites.
     */
    private ArrayList<CandidateMBS> obtainCandidatePlaces(String mirna, String mRNA) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        HashMap<Integer, String> sitesToTest = new HashMap<>();
        HashMap<Integer, CandidateMBS> candidateSiteMap = new HashMap<>();

        //identify all sites of 3'UTR (squential)
        for (int i = mRNA.length() - this.maxSiteLength; i >= 0; i--) {
            int bindSize = this.maxSiteLength;
            if (mRNA.length() - i < this.maxSiteLength) {
                bindSize = mRNA.length() - i;
            }
            sitesToTest.put(i, mRNA.substring(i, i + bindSize));
        }

        //concurrently determine if the sites are candidate mbs
        sitesToTest.keySet().parallelStream().forEach((i) -> {
            String site = sitesToTest.get(i);
            CandidateMBS candSite = isCandidate(mirna, site);
            if (candSite != null) {
                candSite.setMatureMirnaTranscript(mirna);
                candSite.setGene3UTRStart(i);
                candSite.setGene3UTREnd(i + site.length());
                candSite.setSiteTranscript(site);
                candidateSiteMap.put(i, candSite);
            }
        });

        //save the candidate mbs in an ordered way
        for (int i = mRNA.length() - this.maxSiteLength; i > 0; i--) {
            if (candidateSiteMap.get(i) != null) {
                candidateList.add(candidateSiteMap.get(i));
            }
        }

        return candidateList;
    }

    /**
     * Checks if a microRNA can bind to a sequence of maxSiteLength. Returns a
     * candidate site if the miRNA can can pair with {@link seqB} and if it
     * meets the siteFinderMethod requirements. Null otherwise.
     *
     * @param mirna microRNA sequence
     * @param seqB RNA sequence of the target. Returns false if it's longer than
     * maxSiteLength
     *
     *
     * @return candidate site with the binding information, null if a candidate
     * site cannot be formed.
     */
    public CandidateMBS isCandidate(String mirna, String seqB) {
        //log.debug("Checking candidate site {} - {}", mirna, seqB);

        int seedRegionBinds = 0;
        int WCPairs = 0;
        int wobblePairs = 0;

        double[] result = {0, 0};

        //if the sequence is too long, returns false
        if (seqB.length() > this.maxSiteLength) {
            return null;
        }

        DnaFoldPrediction fold = RNAFolder.foldSeqs(seqB, mirna);
        double freeEnergy = fold.deltaG();
        //if the squence cannot be fold returns false
        if (freeEnergy == 0 || freeEnergy > maxFreeEnergy) {
            return null;
        }
        //if the squence cannot be fold (they do not form a stable bind returns false
        if (!RNAFolder.checkBindingFromStructure(fold.structure())) {
            return null;
        }

        //we check at which point the sequence and the mirna are binding. This is useful for future 'alignment'
        int seedRegionStartInSequence = findBindingStartInSequence(fold.structure());

        String foldStructure = fold.structure();

        //The structure has a size of |seqB|+7+|mirna|
        //the seed region starts at structure.length - |mirna|+1
        int seedStart = foldStructure.length() - mirna.length() + seedRegionStart;
        int seedIndex = seedStart;
        int cannonicalCount = 0;
        while (seedIndex < fold.structure().length() && seedIndex < (seedStart + seedRegionSize)) {
            if (foldStructure.charAt(seedIndex) == ')') {
                seedRegionBinds++;
                String pair = this.getPairAt(seedIndex, fold.sequence(), foldStructure);
                if (this.isWatsonCrick(pair.charAt(0), pair.charAt(1))) {
                    WCPairs++;
                    if ((seedIndex - seedStart) >= 0 && (seedIndex - seedStart < 6)) { //we count if the 6 pairs of the seed region are WC. THis means canonical site
                        cannonicalCount++;
                    }
                } else if (this.isWobble(pair.charAt(0), pair.charAt(1))) {
                    wobblePairs++;
                }
            }

            seedIndex++;
        }

        if (seedRegionBinds >= minBindsSeed) {

            CandidateMBS cs = new CandidateMBS();
            cs.setSeedRegionStart(1);
            cs.setSeedRegionEnd(0 + seedRegionSize);
            cs.setFreeEnergy(fold.deltaG());
            cs.setBindsInSeed(seedRegionBinds);
            cs.setWCpairsInSeed(WCPairs);
            cs.setWobblePairsInSeed(wobblePairs);
            cs.setSeedRegionStartInSequence(seedRegionStartInSequence);
            if (cannonicalCount >= 6) {
                cs.setComment("DEF: cannonical");
                cs.setIsCanonical(true);
            } else {
                cs.setComment("DEF: non-cannonical");
            }

            //result[1] = lastCharacter(fold.structure(), ')') - Lsize - seqB.length(); //position of the last binding size in the mirna
            //System.out.println(fold.sequence());
            //System.out.println(fold.structure());
            cs.setStructure(foldStructure);
            return cs;

        }

        return null;
    }

    /**
     * Used to test/develop the site finder
     *
     * @param args
     */
    public static void main(String[] args) {
        String mirna = "UGAGAACUGAAUUCCAUGGGUU";
        String seqB = "TCCACTGAGTCTGAGTCTTCAAGTTTTCA";

        System.out.println(seqB);
        System.out.println(mirna);

        System.out.println(seqB + "LLLLLLL" + mirna + "    <-- Main");
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();
        CandidateMBS expResult = null;
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        System.out.println(result);

    }

}
