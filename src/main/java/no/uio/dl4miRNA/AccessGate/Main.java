/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.AccessGate;

import no.uio.dl4miRNA.DL.GenePredictionExecution;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import no.uio.dl4miRNA.DCP.DataGenerationExecution;
import no.uio.dl4miRNA.DL.MLExecution;
import org.slf4j.LoggerFactory;

/**
 * Main class for miRAW.
 * Executes miRAW from the command line and.
 * @author apla
 */
public class Main {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Main.class);

    /**
     * @param args the command line arguments.
     * args[0]: is the miRAW fucntionality to be executed: 
     *          - CreateDatasets:   Execution of tasks related to the generation 
     *                              of new datasets.
     *          - TrainModel:       Not available
     *          - CrossValidation:  Execution of a CrossValidation to train/test
     *                              a Neural Network
     *          - GenePrediction:   Execution of the prediction of new targets.
     * args[1..n]: correspond to the parameters of the different functionalities.
     *          Check the help command to obtain details (miRAW functionalityName help)
     * args[n+1]: corresponds to the location of the configuration file
     */
    public static void main(String[] args) {
        Calendar x = Calendar.getInstance();
        String date = String.format("%d-%d-%d-%d-%d-%d",x.get(Calendar.YEAR),x.get(Calendar.MONTH),x.get(Calendar.DAY_OF_MONTH),x.get(Calendar.HOUR_OF_DAY),x.get(Calendar.MINUTE),x.get(Calendar.SECOND));
        System.setProperty("log.name", date);
        System.out.println(System.getProperty("log.name"));
        
        if (args.length > 0) {
            
            String[] subArgs = Arrays.copyOfRange(args, 1, args.length);
            switch (args[0].toLowerCase()) {
                case "help":
                    help(args);
                    break;
                case "geneprediction":
                    classifyGenes(subArgs);
                    break;
                case "geneclassification":
                    classifyGenes(subArgs);
                    break;
                case "trainmodel":
                    trainModel(subArgs);
                    break;
                case "createdatasets":
                    generateDataSets(subArgs);
                    break;
                case "crossvalidate":
                case "crossvalidation":
                    crossValidation(subArgs);
                    break;
                default:
                    help(args);
                    break;
            }

        } else {
            System.out.println("Please introduce a parameter to execute miRAW");
            help(args);
        }
    }

    /**
     * Predicts/Evaluates targets based on the parameters defined in the configuraiton file.
     * @param args arguments of the method. 
     * args[0]: is the type of execution:
     *          - predict:  Predicts if the miRNA-Gene pairs specified in the 
     *                      unified file defined in the configuration file are 
     *                      positive or negative targets.
     *          - evaluate: Classifies if the miRNA-Gene pairs specified in the 
     *                      unified file defined in the configuration file are 
     *                      positive or negative targets. Evaluates the 
     *                      classification using the class defined in the unified file.
     * args[1]:configuration file location. The configuration file must contain
     *          the following parameters:   
     *          - ExperimentName: name of the experiment
     *          - ExperimentFolder: output folder location
     *          - UnifiedFile: location of the input unified file
     *          - DLModel: location of the DL model
     *          - CandidateSiteFinder.Type: Type of CSSM (Default, Pita, TargetScan...)            
     */
    private static void classifyGenes(String[] args) {
        //check arguments
        if (args.length < 1) {
            System.out.println("insuficient number of parameters.\n"
                    + "Showing 'help':\n");
            classifyGenesHelp();
        } else if (args[0].equalsIgnoreCase("help")) { //call help
            classifyGenesHelp();
        } else { //correct number of arguments
            System.out.println("######################################");
            System.out.println("#### MiRAW: miRNA-Gene prediction ####");
            System.out.println("######################################");
            switch (args[0].toLowerCase()) {
                case "predict": //Predict new mirna-genes
                    System.out.println("\nPrediction mode\n");
                    GenePredictionExecution.genePrediction(args[1]);
                    break;
                case "evaluate": //Classify mirna-genes and compare results with the one saved at the Unified File.
                    System.out.println("\nEvaluation mode\n");
                    GenePredictionExecution.genePredictionEvaluation(args[1]);
                    break;
                default:
                    System.out.println("Unkown parameter " + args[0]);
                    System.out.println("Execute 'miRAW GeneClassification help' for usage information");
            }
        }
    }

    /**
     * Executes the steps required to generate miRAW datasets. This includes
     * annotating TarBase with mirBASE and Ensembl transcripts, annotate 
     * positive and negative candidate sites from a unified file, generate 
     * binary training/testing files for a deep neural network, and create Cross
     * Validation files.
     * Arguments of the method:
     *	args[0]: determines the type of dataset that will be executed:
     *       -	BuildUnifiedFile:
     *       -	GenerateCandidateSites:
     *       -	UnifyPosNeg:
     *       -	Binarize:
     *       -	GenerateCrossVal:
     *       -	All: Performs all the previous steps in a sequential way (generates both positive and negative candidate sites)
     *   args[1]: configuration file location. The configuration file must contain the following parameters:   
     *       -	TarBaseDBFile: location of the TarBase csv file
     *       -	Fasta3UTR: location of the ensemble fasta file containing the 3’UTR of genes. This information is override and obtained directly from ensemble if possible.
     *       -	FastaMatureMirna: location of the fasta file (from mirBase) containing the mature mirna transcripts.
     *       -	UnifiedFile: location of the outputfile that will contain the annotated TarBase dataset. (TarBaseDBfile annotated with Fasta3UTR and FastaMatureMirna)
     *       -	Species: Species that the model is being trained for (homo sapiens set as default).
     *       -	NegativeCandidatesLocation: location where the negative target sites are saved/loaded from.
     *       -	PositiveCandidatesLocation: location where the negative target sites are saved/loaded from.
     *       -	CompleteCandidateDatasetLocation: location where the negative and positive target sites are saved/loaded from.
     *       -	CandidateWindowOffset: sliding window step when selecting candidate sites.
     *       -	CandidateMaxFreeEnergy: Maximum free energy used to determine if a stable binding is a candidate site (default = 0)
     *       -	BinarizedDatasetLocation: location where the binarized target sites are located.
     *       -	CrossValidation.Type: Type of cross validation to generate (traintest (default) or traintestval)
     *       -	CrossValidation.Folder: Folder where the cross validation data will be stored
     *       -	CrossValidation.ClassColumn: Column of the class of the target (positive or negative). 0  by default.
     *       -	CrossValidation.TrainProportion
     *       -	CrossValidation.TestProportion
     *       -	CrossValidation.ValidationProportion
     *       -	CrossValidation.Folds: Number of folds of the CV (10 by default)
     *   args[2]: additional arguments related to the type of sites to identify 
     *   in the “GenerateCandidateSites” step. It can take values: 
     *   positive/1/negative/0/all(default). 
     * 
     * @param args 
     */
    private static void generateDataSets(String[] args) {
        //check the number of arguments (should be >0)
        if (args.length < 1) {
            System.out.println("insuficient number of parameters.\n"
                    + "Showing 'help':\n");
            generateDataSetsHelp();
        
        } else if (args[0].equalsIgnoreCase("help")) { //call help
            generateDataSetsHelp();
        } else { //check arguments
            System.out.println("#######################################");
            System.out.println("###### MiRAW: DataSet Generation ######");
            System.out.println("#######################################");
            switch (args[0].toLowerCase()) {
                case "builduinfiedfile": //BuildUnifiedFile
                    DataGenerationExecution.buildUnifiedFile(args[1]);
                    break;
                case "generatecandidatesites": //"GenerateCandidateSites" 
                    if (args.length == 3) {
                        DataGenerationExecution.findValidatedBindingSites(args[1], args[2]);
                    } else {
                        log.warn("Wrong number of parameters: 3 expected.\nGenerateCandidateSites\tConfigurationFile\tTypeOfSite");
                    }
                    break;
                case "unifyposneg": //"UnifyPosNeg" merges the positive and negative candidate sites file
                    if (args.length == 2) {
                        DataGenerationExecution.mergePositiveNegativeSiteS(args[1]);
                    } else {
                        log.warn("Wrong number of parameters: 2 expected.\nUnifyPosNeg\tConfigurationFile");
                    }
                    //unify posNeg
                    break;
                case "binarize": //"binarize" binarizes the candidate site files
                    if (args.length == 2) {
                        DataGenerationExecution.binarizeDataSet(args[1]);
                    } else {
                        log.warn("Wrong number of parameters: 2 expected.\nBinarize\tConfigurationFile");
                    }
                    //binarize
                    break;
                case "generatecrossval": //"generatecrossval" generates the files for performing a cross validation
                    if (args.length == 2) {
                        DataGenerationExecution.generateCrossValidation(args[1]);
                    } else {
                        log.warn("Wrong number of parameters: 2 expected.\nGenerateCrossVal\tConfigurationFile\t");
                    }
                    break;
                case "all": //Runs all the previous steps in a consecutive way
                    DataGenerationExecution.buildUnifiedFile(args[1]);
                    DataGenerationExecution.findValidatedBindingSites(args[1]);
                    DataGenerationExecution.mergePositiveNegativeSiteS(args[1]);
                    DataGenerationExecution.binarizeDataSet(args[1]);
                    DataGenerationExecution.generateCrossValidation(args[1]);
                    break;
                default: //Unknown parameter
                    System.out.println("Unkown parameter " + args[0]);
                    System.out.println("Showing help");
                    generateDataSetsHelp();
            }
        }
    }

    /**
     * Trains the neural network.
     * To be implemented.
     * @param args 
     */
    private static void trainModel(String[] args) {
        System.out.println("Currently neural networks can only be trained through cross validation");
    }

    /**
     * Performs a Cross Validation to train/test miRAW's Deep Network.
     * 
     * @param args: command line arguments
     * Arguments of the method:
     *     args[0]: configuration file location. The configuration file must contain the following parameters:   
     *          - ExperimentName: Name of the experiment results
     *          - ExperimentFolder: Location of the output folder
     *          - CrossValidation.Folder: Location of the CrossValidation files generated during the “Create Datasets” step.
     *          - CrossValidation.Type: Type of cross validation to be executed (traintest or traintestval).
     *          - CrossValidation.Folds: Number of folds to be executed for the CV (default = 10).
     *          - ANN.Dropout: Dropout rate for the neural network during training (default = 0).
     *          - ANN.LearningRate: Learning rate for the Neural Network (default 0.05).
     *          - ANN.MaxEpochs: Maximum number of epochs to be reached during the training (default 1500).
     *          - ANN.EpochsNoChange: When using a traintestval CV, this parameter defines the number of epochs without improving the validation results that will trigger the “auto stop” condition.
     *          - ANN.NodesLayer1: Number of nodes in the first layer of the Deep network.
     *          - ANN.NodesLayer2: Number of nodes in the second layer of the Deep network.
     *          - ANN.NodesLayerX: Number of nodes in the Xth layer of the Deep Network.
     *      args[1] (Optional): “webListeners” and “showProgress” open a web browser window with information regarding the evolution of the network training.
    */
    private static void crossValidation(String[] args) {

        switch (args.length) {
            case 0: //misisng arguments
                crossValidationHelp();
                break;
            case 1: //Contains the configuration file location argument
                if (args[0].equalsIgnoreCase("help")) { //help is asked
                    crossValidationHelp();
                    break;
                }
                MLExecution.crossValidate(args[0]);
                break;
            default: //Contains the configuration file location argument and an additional parameter
                boolean webListeners = false;
                if (args[1].equalsIgnoreCase("webListeners")
                        || args[1].equalsIgnoreCase("showProgress")) {
                    webListeners=true;
                }
                MLExecution.crossValidate(args[0], webListeners);
        }

    }

    /* ############################################
       ############ HELP METHODS ##################
       ############################################ 
     */
    private static void help(String[] args) {
        System.out.println("Usage:  miRAW option");
        System.out.println();
        System.out.println("Options:");
        System.out.println("\tGenePrediction: predict miRNA-gene interactions");
        System.out.println("\tClassifySites: ");

        System.out.println("\tTrainModel: ");
        System.out.println("\tCreateDataSets: ");
        System.out.println("\tCrossValidation: ");
        System.out.println();
        System.out.println("For additional help of any of the options type 'miRAW option help'");
    }

    private static void classifyGenesHelp() {
        System.out.println("miRAW GeneClassification evaluates a set of "
                + "3'UTR gene and microRNAs transcripts to predict their "
                + "interactions and target sites.");
        System.out.println();
        System.out.println("Usage: miRaw GeneClassification option configurationFile");
        System.out.println("\toption: predict - predicts the target sites and saves them to a file.");
        System.out.println("\toption: evaluate - predicts the gene-miRNA interactions and validates them against a Unified file.");
        System.out.println();
        System.out.println("\tConfiguration must have the following parametes");
        System.out.println("\t\tUnifiedFile: location csv file containing the geneName, geneId, miRRNAname, 3UTR transcritpt, mature miRNA transcript, validation (only for evaluation)");
        System.out.println("\t\tDLModel: location of the Deep Learning Model to use");
        System.out.println("\t\tExperimentFolder: folder where the experiment results will be saved");
        System.out.println("\t\tExperimentName: name of the experiment");
    }

    private static void generateDataSetsHelp() {
        System.out.println("miRAW CreateDataSets creates and generates the necessary files to train and crossvalidate miRAW's deep Neural Network");
        System.out.println();
        System.out.println("Usage: miRaw CreateDataSets option configurationFile (parameters)");
        System.out.println("\tOption list:");
        System.out.println("\tBuildUnifiedFile: Creates a file containing the transcripts of the miRNA-mRNa pairs.");
        System.out.println("\tGenerateCandidateSites (typeOfSite): Searches for the candidate sites in a validated targets file. TypeOfSite can be 'positive' 'negative' or 'any'");
        System.out.println("\tUnifyPosNeg: Merges a positive sites and a negative sites file.");
        System.out.println("\tBinarize: Binarizes the candidate sites so they can be feed to a Neural Network.");
        System.out.println("\tGenerateCrossVal: Generates cross validation files so a Neural Network can be trained and tested.");
        System.out.println("\tAll: Performs all the previous steps: "
                + "\n\t\t1. GenerateCandidateSites any "
                + "\n\t\t2. UnifyPosNeg"
                + "\n\t\t3. Binarize"
                + "\n\t\t4. GenerateCrossVal");
        System.out.println("\thelp: Shows a help message");
        System.out.println("\n\nParameters in the configurationFile:");

        System.out.println("\tFor buildUnifiedfile (* means mandatory parameter):\n"
                + "\t\ttarBaseDBFile*:csv from DIANA Tarbase containing a list of positive/negative targets \n"
                + "\t\tFasta3UTR*: fasta file containing the transcripts of the 3'UTR region for all the genes of a given species\n"
                + "\t\tfastaMatureMirna*:asta file (from mirBase) containing the transcripts of mature mirnas\n"
                + "\t\tfastaStemLoopFile: fasta file (from mirBase) containing the estimated stemLoops for mirnas (not used right now)\n"
                + "\t\tSpecies: Species used to generate the unified file (Deault = homo sapiens). \n"
                + "\t\tUnifiedFile*: Location of the file containing the experimentally verified (postitive and negative) miRNa-mRNA targets.\n"
                + "\n"
                + "\tFor generateCandidateSites \n"
                + "\t\tUnifiedFile* \n"
                + "\t\tNegativeCandidatesLocation*: Location where the identified negative sites are (will be) stored\n"
                + "\t\tPositiveCandidatesLocation*: Location where the identified positive sites are (will be) stored\n"
                + "\t\tWindowOffset: minimum separation between sites (default = 5)\n"
                + "\t\tmaxFreeEnergy: maximum free energy in a site. It is a non-positive value. (default = 0)\n"
                + "\t\tPositiveSelectionMethod: method used to determine positive candidates (deault = targetScan)\n"
                + "\t\tPositiveSelectionFile*: location of the file containing the positive candidate sites\n"
                + "\n"
                + "\tFor UnifyPosNeg:\n"
                + "\t\tPositiveCandidatesLocation*\n"
                + "\t\tNegativeCandidatesLocation*\n"
                + "\t\tCompleteCandidateDatasetLocation*: location of the file containing both the positive and negative candiate sites\n"
                + "\n"
                + "\tFor Binarize:\n"
                + "\t\tCompleteCandidateDatasetLocation*\n"
                + "\t\tBinarizedDatasetLocation*: location of the binarized file containing both the positive and negative candiate sites\n"
                + "\n"
                + "\tFor GenerateCrossVal:\n"
                + "\t\tBinarizedDatasetLocation*\n"
                + "\t\tCrossValidation.Folder*: Folder where the cross validation data will be saved.\n"
                + "\t\tCrossValidation.Type: Type of Cross Validation (testtrain, testtrainval, etc. Default = testtrain)\n"
                + "\t\t\n"
                + "\t\tCrossValidation.ClassColumn: Column where the labels are (default 0)\n"
                + "\t\tCrossValidation.TrainProportion: Train proportion for the CV (default = 90)\n"
                + "\t\tCrossValidation.TestProportion: Test proportion for the CV (Default = 10)\n"
                + "\t\tCrossValidation.ValProportion: Validation proportion for the CV (Default = 10. when validation is used, Train is set to 80 by default)");

    }

    private static void crossValidationHelp() {
        System.out.println("miRAW CrossValidation Creates and CrossValidates miRAW's deep Neural Network");
        System.out.println();
        System.out.println("Usage: miRaw CrossValidation configurationFile");
        System.out.println("\tNo options available");

        System.out.println("\n\nParameters in the configurationFile:");
        System.out.println("		ExperimentName*: Name of the experiment\n"
                + "		ExperimentFolder*: Folder where the experiment will be saved\n"
                + "\n"
                + "		CrossValidation.Folder*: Folder where the cross validation data is saved.\n"
                + "		CrossValidation.Type: Type of Cross Validation (testtrain, testtrainval, etc. Default = testtrain)\n"
                + "		CrossValidation.Folds: Number of folds in the CV data (Default = 10)\n"
                + "		\n"
                + "		ANN.Dropout: Dropout parameter [0,0.5] (Default = 0)\n"
                + "		ANN.LearningRate: Learning Rate parameter (Default = 0.005)\n"
                + "		ANN.maxEpochs: Maximum Trainning Epochs (Default = 1500)\n"
                + "		ANN.epochsNoChange: Maximum number of Epochs without change. Only affects CVT  (Default = 500)\n"
                + "		\n"
                + "		ANN.NodesLayer1*: number of nodes in hidden layer 1\n"
                + "		ANN.NodesLayer2*: number of nodes in hidden layer 2\n"
                + "		…\n"
                + "		ANN.NodesLayerN*: number of nodes in hidden layer N (last hidden layer)");
    }

}
