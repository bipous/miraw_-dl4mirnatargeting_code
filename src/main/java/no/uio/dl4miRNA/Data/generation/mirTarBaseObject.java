/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Data.generation;

/**
 * Models the information contained in a MirTarBase data instance. MirTarBase CSV files can be downloaded from 
 * http://mirtarbase.mbc.nctu.edu.tw/.
 * 
 * Finally mirTarBase was not considered as a data source from miRAW so this class
 * was never tested or used.
 * 
 * @author apla
 */

public class mirTarBaseObject {
    protected String mirTarBaseID;
    protected String miRNA;
    protected String Species;
    protected String targetGene;
    protected String targetGeneID;
    protected String experiments;
    protected String supportType;
    protected String referencesPMID;

    public mirTarBaseObject() {
    }

    public mirTarBaseObject(String mirTarBaseID, String miRNA, String Species, String targetGene, String targetGeneID, String experiments, String supportType, String referencesPMID) {
        this.mirTarBaseID = mirTarBaseID;
        this.miRNA = miRNA;
        this.Species = Species;
        this.targetGene = targetGene;
        this.targetGeneID = targetGeneID;
        this.experiments = experiments;
        this.supportType = supportType;
        this.referencesPMID = referencesPMID;
    }

    public String getMirTarBaseID() {
        return mirTarBaseID;
    }

    public void setMirTarBaseID(String mirTarBaseID) {
        this.mirTarBaseID = mirTarBaseID;
    }

    public String getMiRNA() {
        return miRNA;
    }

    public void setMiRNA(String miRNA) {
        this.miRNA = miRNA;
    }

    public String getSpecies() {
        return Species;
    }

    public void setSpecies(String Species) {
        this.Species = Species;
    }

    public String getTargetGene() {
        return targetGene;
    }

    public void setTargetGene(String targetGene) {
        this.targetGene = targetGene;
    }

    public String getTargetGeneID() {
        return targetGeneID;
    }

    public void setTargetGeneID(String targetGeneID) {
        this.targetGeneID = targetGeneID;
    }

    public String getExperiments() {
        return experiments;
    }

    public void setExperiments(String experiments) {
        this.experiments = experiments;
    }

    public String getSupportType() {
        return supportType;
    }

    public void setSupportType(String supportType) {
        this.supportType = supportType;
    }

    public String getReferencesPMID() {
        return referencesPMID;
    }

    public void setReferencesPMID(String referencesPMID) {
        this.referencesPMID = referencesPMID;
    }
    
    
    
    
}
