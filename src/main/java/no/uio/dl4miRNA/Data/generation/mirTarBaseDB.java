/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Data.generation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 * Models the information contained in MirTarBase CSV files, and handles the access
 * to the MirTarBase CSV files. MirTarBase CSV files can be downloaded from 
 * http://mirtarbase.mbc.nctu.edu.tw/.
 * 
 * Finally mirTarBase was not considered as a data source from miRAW so this class
 * was never tested or used.
 * 
 * @author apla
 */

public class mirTarBaseDB {

    /**
     * List of instances inside a mi
     */
    private ArrayList<mirTarBaseObject> validatedTargets;

    public mirTarBaseDB() {
        this.validatedTargets = new ArrayList<>();
    }

    public ArrayList<mirTarBaseObject> getValidatedTargets() {
        return validatedTargets;
    }

    public void setValidatedTargets(ArrayList<mirTarBaseObject> validatedTargets) {
        this.validatedTargets = validatedTargets;
    }

    public void addVAlidatedTarget(mirTarBaseObject mtbo) {
        this.validatedTargets.add(mtbo);
    }

    public ArrayList<String> getAllMirnas() {
        ArrayList<String> mirnaList = new ArrayList<>();
        for (mirTarBaseObject mtbo : this.validatedTargets) {
            mirnaList.add(mtbo.getMiRNA());
        }
        return mirnaList;
    }

    public ArrayList<String> getAllTargets() {
        ArrayList<String> targetList = new ArrayList<>();
        for (mirTarBaseObject mtbo : this.validatedTargets) {
            targetList.add(mtbo.getTargetGene());
        }
        return targetList;
    }

    public ArrayList<String> getAllTargetsID() {
        ArrayList<String> targetList = new ArrayList<>();
        for (mirTarBaseObject mtbo : this.validatedTargets) {
            targetList.add(mtbo.getTargetGeneID());
        }
        return targetList;
    }

    public void loadMirTarBaseFromFile(String fileName) throws FileNotFoundException, IOException {
        Reader in = new FileReader(fileName);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withDelimiter(';').withHeader().parse(in);
        for (CSVRecord record : records) {
            mirTarBaseObject mtbo;
            mtbo = new mirTarBaseObject();
            
            mtbo.setMiRNA(record.get("miRNA"));
            mtbo.setTargetGene(record.get("Target Gene"));
            mtbo.setTargetGeneID(record.get("Target Gene (Entrez Gene ID)"));
            mtbo.setSpecies(record.get("Species (miRNA)"));
            mtbo.setExperiments(record.get("Experiments"));
            mtbo.setMirTarBaseID(record.get("miRTarBase ID"));
            mtbo.setReferencesPMID(record.get("References (PMID)"));
            mtbo.setSupportType(record.get("Support Type"));
            
            this.addVAlidatedTarget(mtbo);
        }
        in.close();

    }

}