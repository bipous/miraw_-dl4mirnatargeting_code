/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Data.gathering;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Allows to access and retrieve data from Ensembl.
 *
 * @author apla
 */
public class EnsemblAccess {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(EnsemblAccess.class);
    /**
     * URI of the Ensembl API
     */
    private static final String EnsembleAccessPoint = "http://rest.ensembl.org/";

    /**
     * Given a gene name, it looks for its Ensembl id in the homo sapiens
     * species.
     *
     * @param geneName name of the gene
     * @return Ensembl id of the gene
     * @throws ParserConfigurationException
     * @throws MalformedURLException if the URL has a strange format
     * @throws IOException if ti cannot access the XML
     * @throws SAXException if the XML has a strange format.
     */
    public static String getEnsmbleIdOfGeneName(String geneName) throws ParserConfigurationException, MalformedURLException, IOException, SAXException {
        //Example
        //genName="TAOK1"
        // https://rest.ensembl.org/lookup/symbol/homo_sapiens/TAOK1?content-type=text/xml;format=condensed;db_type=core

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        URL url = new URL(EnsembleAccessPoint + "lookup/symbol/homo_sapiens/" + geneName + "?content-type=text/xml;format=condensed;db_type=core");
        log.debug("URL:" + url);
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());

        boolean found = false;
        NodeList nList;
        String id = null;

        //get data elements from the XML
        nList = doc.getElementsByTagName("data");

        //search for the EnsemblId attribute
        for (int temp = 0; temp < nList.getLength(); temp++) {
            if (!found) {
                id = ((Element) nList.item(temp)).getAttribute("id");
                //System.out.println(nNode.getTextContent());                
            }
        }

        return id;
    }

    /**
     * Given the ensembleId of a gene, it returns its transcript.
     *
     * @param ensembleId
     * @return transcript of the gene.
     * @throws ParserConfigurationException
     * @throws MalformedURLException if the URL has a strange format
     * @throws IOException if ti cannot access the XML
     * @throws SAXException if the XML has a strange format.
     */
    public static String getTranscriptOfEnsembleId(String ensembleId) throws ParserConfigurationException, MalformedURLException, IOException, SAXException {
        //ensembleId = "ENSE00001154485"

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        URL url = new URL(EnsembleAccessPoint + "sequence/id/" + ensembleId + "?content-type=text/x-seqxml%2Bxml");
        log.debug(url.toString());
        //System.out.println("URL:"+url);
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());

        boolean found = false;
        NodeList nList;
        String seq = null;

        nList = doc.getElementsByTagName("DNAseq");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            if (!found) {
                Node nNode = nList.item(temp);
                //System.out.println(nNode.getTextContent());
                seq = nNode.getTextContent();
                found = true;
            }
        }

        return seq;
    }

    /**
     * Given a species and a genome region, it returns its transcript
     *
     * @param species genome species (e.g. "homo sapiens")
     * @param region genome region (e.g. "6:2041792-20495201")
     * @return transcript of the genome region
     * @throws ParserConfigurationException
     * @throws MalformedURLException if the URL has a strange format
     * @throws IOException if ti cannot access the XML
     * @throws SAXException if the XML has a strange format.
     */
    public static String getTranscriptOfRegion(String species, String region) throws ParserConfigurationException, MalformedURLException, IOException, SAXException {
        //species = "human"
        //region = "6:20491792-20495201"

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        URL url = new URL(EnsembleAccessPoint + "sequence/region/" + species + "/" + region + "?content-type=text/x-seqxml%2Bxml");
        log.debug("URL:" + url);
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());

        boolean found = false;
        NodeList nList;
        String seq = null;

        nList = doc.getElementsByTagName("DNAseq");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            if (!found) {
                Node nNode = nList.item(temp);
                //System.out.println(nNode.getTextContent());
                seq = nNode.getTextContent();
                found = true;
            }
        }

        return seq;
    }

    /**
     * Given a gene name, it searches for its ensembl transcript.
     *
     * @param geneName name of the gene
     * @return Transcript of the gene.
     * @throws ParserConfigurationException
     * @throws MalformedURLException if the URL has a strange format
     * @throws IOException if ti cannot access the XML
     * @throws SAXException if the XML has a strange format.
     */
    public static String getTranscriptOfGeneName(String geneName) throws ParserConfigurationException, MalformedURLException, IOException, SAXException {
        String seq = null;
        String ensembleId = getEnsmbleIdOfGeneName(geneName);
        if (ensembleId != null) {
            seq = getTranscriptOfEnsembleId(ensembleId);
        }
        return seq;
    }

    public static String getChromosomeTrasncript(String species, String chromosome, int regStart, int regEnd, char strand, String coordinateVersion) throws MalformedURLException, IOException {
        if (coordinateVersion == null) {
            coordinateVersion = "GRCh38";
        }
        String strandInfo="";
        if (strand =='-'){
            strandInfo="-";
        }
        
        URL url = new URL(EnsembleAccessPoint + "sequence/region/" + species + "/" + chromosome+":"+regStart+".."+regEnd+":" + strandInfo+"1?content-type=text/plain;coord_system_version="+coordinateVersion);
        log.debug("URL:" + url);
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000);
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                    con.getInputStream()));
        String transcript = in.readLine();
        
        if (strand =='-'){
            transcript=new StringBuilder(transcript).reverse().toString();
        }
                
        
        //https://rest.ensembl.org/sequence/region/human/chr16:52382583..52382612:-1?content-type=text/plain;coord_system_version=GRCh37

        return transcript;
    }

}
