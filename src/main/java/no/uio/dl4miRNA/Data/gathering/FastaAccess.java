/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Data.gathering;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.slf4j.LoggerFactory;

/**
 * Class for dealing with Fasta files.
 *
 * @author apla
 */
public class FastaAccess {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(FastaAccess.class);
    
    /**
     * Identifies all the fasta transcripts in a FASTA file, and which
     * information is saved at each instance.
     *
     * @param file location of the fasta file.
     * @return List containing a list of attributes belonging to each data
     * instance.
     */
    public static ArrayList<ArrayList<String>> obtainHeaderParts(String file) {
        ArrayList<ArrayList<String>> headerParts = new ArrayList<>();

        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            String auxCurrent;
            String seqName = "";
            String seqValue = "";
            sCurrentLine = br.readLine();
            //read the whole file
            while (sCurrentLine != null) {
                //identify new data instances
                if (sCurrentLine.startsWith(">")) {
                    //it is a new fasta sequence
                    auxCurrent = sCurrentLine.replace(">", "");
                    //identify attributes in the data instance.
                    seqName = auxCurrent.split(" ")[0];
                    ArrayList<String> instance = new ArrayList<>();
                    for (String el : seqName.split("\\|")) {
                        instance.add(el);
                    }
                    headerParts.add(instance);

                }
                sCurrentLine = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return headerParts;

    }

    /**
     * Obtains all the sequence transcripts in a fasta file. If the species
     * prefix is specified, only the transcripts belonging to such species will
     * be retrieved.
     *
     * This method is based on fasta files following miRbase notation, and the
     * name for each sequence corresponds to the first element after triming the
     * sequence header by ' ' and '|' characters.
     *
     * If a miRNA name is repeated, only the first is kept.
     * 
     * @param file location of the file
     * @param speciesPrefix Prefix that indicates the species that needs to be
     * retrieved (Optional, if null, all the transcripts will be retrieved)
     * @return HashMap containing all the sequence names and their corresponding
     * trasncripts.
     */
    public static HashMap<String, String> obtainNamesAndSequences(String file, String speciesPrefix) {
        //in case of repeated entries, only the first one is kept.

        BufferedReader br = null;
        HashMap<String, String> map = new HashMap<>();
        try {

            br = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            String auxCurrent;
            String seqName = "";
            String seqValue = "";
            sCurrentLine = br.readLine();
            //read the whole file
            while (sCurrentLine != null) {

                if (sCurrentLine.startsWith(">")) {
                    //it is a new fasta sequence
                    auxCurrent = sCurrentLine.replace(">", "");
                    //obtain the name of the sequence
                    seqName = auxCurrent.split(" ")[0];
                    seqName = seqName.split("\\|")[0];
                    sCurrentLine = br.readLine();
                    seqValue = "";
                }
                //obtain the value of the sequence
                while (sCurrentLine != null && !sCurrentLine.startsWith(">")) {
                    seqValue = seqValue + sCurrentLine;
                    sCurrentLine = br.readLine();
                }
                //save the sequence transcript in the hashmap
                if (!map.containsKey(seqName) && !seqValue.equalsIgnoreCase("Sequence unavailable")) {
                    //if a species prefix has been specified, make sure that the
                    //transcript corresponds to the target species.
                    if (speciesPrefix != null) {
                        if (seqName.startsWith(speciesPrefix)) {
                            map.put(seqName.toLowerCase(), seqValue);
                        }
                    } else {
                        map.put(seqName, seqValue);
                    }
                }
            }
            br.close();

            return map;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    /**
     * Obtains all the sequence transcripts in a fasta file. The header of each
     * sequence is kept as in the original data file (no '|' or ' ' triming is
     * performed).
     *
     * @param file location of the file
     * @return HashMap containing all the sequence headers and their
     * corresponding trasncripts.
     */
    public static HashMap<String, String> obtainDataAndSequences(String file) {
        //in case of repeated entries, only the first one is kept.
        if (file==null){
            log.error("Fasta file name is null");
        }
        
        BufferedReader br = null;
        HashMap<String, String> map = new HashMap<>();
        try {

            br = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            String auxCurrent;
            String seqName = "";
            String seqValue = "";
            sCurrentLine = br.readLine();
            //read the whole file
            while (sCurrentLine != null) {

                if (sCurrentLine.startsWith(">")) {
                    //it is a new fasta sequence
                    auxCurrent = sCurrentLine.replace(">", "");
                    //obtain the name of the sequence
                    seqName = auxCurrent;//.split(" ")[0];
                    sCurrentLine = br.readLine();
                    seqValue = "";
                }
                //obtain the value of the sequence
                while (sCurrentLine != null && !sCurrentLine.startsWith(">")) {
                    seqValue = seqValue + sCurrentLine;
                    sCurrentLine = br.readLine();
                }
                //save the sequence transcript in the hashmap
                if (!map.containsKey(seqName) && !seqValue.equalsIgnoreCase("Sequence unavailable")) {
                    //if a species prefix has been specified, make sure that the
                    //transcript corresponds to the target species.
                    map.put(seqName, seqValue);
                }
            }

            return map;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(FastaAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return map;
    }

}
