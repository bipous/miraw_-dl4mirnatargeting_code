/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Data.gathering;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *  Allows to access information from the NCBI API: EUTILS.
 * 
 * @author apla
 */
public class EutilsAccess {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(EutilsAccess.class);
 
    /**
     * Searches publications in Pubmed that contain a specific keyword and that 
     * were published between a time interval.
     * @param keyWord keyword to search
     * @param initialDate Publication date lower interval yyyy/mm/dd
     * @param lastDate Publication date upper interval yyyy/mm/dd
     * @return ArrayList with the pubmedIds of the publications talking about the 
     * keyword.
     * @throws EncoderException
     * @throws IOException 
     */
    public static ArrayList<String> searchPublications(String keyWord, String initialDate, String lastDate) throws EncoderException, IOException {
        //date format= yyyy/mm/dd

        int currentSize = 0;
        int pastSize = -1;
        ArrayList<String> pubmedIds = new ArrayList<>();

        //initialize httpclient
        RequestConfig globalConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();

        HttpClient client;

        int maxResultsPerQuery = 1000;
        //HttpClient client = HttpClientBuilder.create().build();

        //client = HttpClients.custom().setDefaultRequestConfig(globalConfig).build();
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.client.protocol.ResponseProcessCookies", "fatal");

        boolean stop = false;
        while (!stop) {
            if (currentSize > 10000) {
                System.out.println("WARNING: High number of results. More than " + currentSize + " results found");
            }
            pastSize = currentSize;

            //setupclient
            client = HttpClientBuilder.create().setDefaultRequestConfig(globalConfig).build();

            //prepare search
            String searchTerm = keyWord + " AND (\"" + initialDate + "\"[Date - Publication] : \"" + lastDate + "\"[Date - Publication]) ";
            URLCodec codec = new URLCodec();
            String urlSearchTerm = codec.encode(searchTerm);

            //preapre http request
            HttpGet request = new HttpGet("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi"
                    + "?db=pubmed"
                    + "&retmode=json"
                    + "&retstart=" + currentSize
                    + "&retmax=" + maxResultsPerQuery
                    + "&term=" + urlSearchTerm);

            //exceute http request
            HttpResponse response = client.execute(request);

            //process response
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            String jsonText = "";
            while ((line = rd.readLine()) != null) {
                jsonText = jsonText + line;
            }
            rd.close();

            //parse JSON response
            JSONObject obj = new JSONObject(jsonText);
            JSONObject obj2 = obj.getJSONObject("esearchresult");
            JSONArray idList = obj2.getJSONArray("idlist");
            for (int i = 0; i < idList.length(); i++) {
                pubmedIds.add(idList.getString(i));
            }

            currentSize = pubmedIds.size();
            if (currentSize - pastSize < maxResultsPerQuery) {
                stop = true;
            }
            
        }

        return pubmedIds;
    }

    /**
     * Searches publications in Pubmed that contain a specific keyword.
     * @param keyWord
     * @return ArrayList with the pubmedIds of the publications talking about the 
     * keyword.
     * @throws EncoderException
     * @throws IOException 
     */
    public static ArrayList<String> searchPublications(String keyWord) throws EncoderException, IOException {
        return searchPublications(keyWord, "1900/01/01", "2900/01/01");
    }

    /**
     * Given a list of PubmedIds, it retrieves the XMLS containing relevant information
     * about the publication (e.g. title, abstract, authors, etc.)
     * @param ids ArrayList containging the Pubmed ids of the publications
     * @return ArrayList of strings in XML format containing the information regarding the publication.
     * @throws IOException 
     */
    public static ArrayList<String> getPubsInformation(ArrayList<String> ids) throws IOException {
        int blockSize = 50;
        int index = 0;

        //initialize httpclient
        RequestConfig globalConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();

        HttpClient client;

        ArrayList<String> XMLS = new ArrayList<>();

        while (index < ids.size()) {
            
            //build list of pubmedId queries
            String listOfIds4Query = "";
            for (int i = index; i < index + blockSize && i < ids.size(); i++) {
                listOfIds4Query = listOfIds4Query + ids.get(i) + ",";
            }

            //setupclient
            client = HttpClientBuilder.create().setDefaultRequestConfig(globalConfig).build();

            //perform query
            HttpGet request = new HttpGet("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&id=" + listOfIds4Query);

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";

            String xml = "";

            while ((line = rd.readLine()) != null) {
                xml = xml + line;
            }
            rd.close();
            XMLS.add(xml);
            index = index + blockSize;
        }
        return XMLS;
    }

    /**
     * Given its entrezID, gets the transcript of a mRNA
     * @param entrezID entrezID of the mRNA
     * @return List of transcripts for the entrezID.
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException 
     */
    public static ArrayList<String> get_mRNATranscriptIdsUsingEntrezID(String entrezID) throws IOException, ParserConfigurationException, SAXException {
       log.debug("get_mRNATranscriptIds:: getting mRNA transcripts IDS for entrezID {0}", entrezID);

        RequestConfig globalConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();

        ArrayList<String> transcriptIds = new ArrayList<>();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        URL url = new URL("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=gene&retmode=xml&id=" + entrezID);
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());

        log.debug( "get_mRNATranscriptIds:: XML for entrezID {0} obtained", entrezID);
        NodeList nList = doc.getElementsByTagName("Gene-commentary");

        int nmRNA = 0;

       log.debug("get_mRNATranscriptIds:: Parsing XML for entrezID {0}", entrezID);
        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                Node nNode2 = eElement.getElementsByTagName("Gene-commentary_type").item(0);
                Element eElement2 = (Element) nNode2;
                if (eElement2 != null) {
                    //     System.out.println(eElement2.getTagName());
                }
                if (eElement2.hasAttribute("value")) {
                    if (eElement2.getAttribute("value").equalsIgnoreCase("mRNA")) {

                        if (eElement.getElementsByTagName("Gene-commentary_heading").getLength() > 0) {
                            if (eElement.getElementsByTagName("Gene-commentary_heading").item(0).getTextContent().equalsIgnoreCase("mrna sequence")) {
                                log.debug("get_mRNATranscriptIds:: mRNA found in gene entrez {0} ", entrezID);
                                transcriptIds.add(eElement.getElementsByTagName("Gene-commentary_accession").item(0).getTextContent());
                                nmRNA++;
                            }
                        }
                    }

                }

            }

        }
        log.debug( "get_mRNATranscriptIds:: {0} mRNAs found for enterzID {1}", new Object[]{nmRNA, entrezID});
        return transcriptIds;

    }

    /**
     * Searches for the EntrezId of a particular gene in a particular species.
     * @param geneName Name of the gene to search (e.g. BRCA2).
     * @param speciesName Name of the species to search.
     * @return EntrezId for the query.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public static String get_GeneEntrezId(String geneName, String speciesName) throws ParserConfigurationException, SAXException, IOException {
        log.debug( "get_mRNATranscriptIds:: getting mRNA transcripts IDS for gene {} and species {}", new Object[]{geneName, speciesName});

        RequestConfig globalConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();

        ArrayList<String> geneIds = new ArrayList<>();

        ArrayList<String> transcriptIds = new ArrayList<>();

        //obtain entrez ID for the gene.
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
        URL url = new URL("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&retmode=xml&term=(" + geneName + "[Gene+Name])+AND+(" + speciesName + "[Organism])");
        log.debug(url.toString());
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());
        //https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&retmode=xml&term=(PICALM[Gene+Name])+AND+human[Organism]
        //

        log.debug("get_mRNATranscriptIds:: XML for gene {0} and species {1}", new Object[]{geneName, speciesName});
        NodeList nList = doc.getElementsByTagName("IdList");

        int nmRNA = 0;

        log.debug("get_mRNATranscriptIds:: Parsing XML for gene {0} and species {1}", new Object[]{geneName, speciesName});
        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                NodeList idNodes = eElement.getElementsByTagName("Id");

                for (int j = 0; j < idNodes.getLength(); j++) {
                    Element idElement = (Element) idNodes.item(j);
                    geneIds.add(idElement.getTextContent());
                }
            }
        }
        if (geneIds.size() > 0) {
            return geneIds.get(0);
        }
        return null;
    }

    /**
     * Given a gene name and a particular species, it searches for its mRNA transcript.
     * @param geneName name of the gene to search
     * @param speciesName name of the species to search
     * @return List of available transcripts for the gene/species.
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException 
     */
    public static ArrayList<String> get_mRNATranscriptIdsUsingGeneAndSpecies(String geneName, String speciesName) throws IOException, ParserConfigurationException, SAXException {
        log.debug("get_mRNATranscriptIds:: getting mRNA transcripts IDS for gene {0} and species {1}", new Object[]{geneName, speciesName});

        RequestConfig globalConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();

        ArrayList<String> geneIds = new ArrayList<>();

        ArrayList<String> transcriptIds = new ArrayList<>();

        //obtain entrez ID for the gene.
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        URL url = new URL("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&retmode=xml&term=(" + geneName + "[Gene+Name])+AND+(" + speciesName + "[Organism])");
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());
        //https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&retmode=xml&term=(PICALM[Gene+Name])+AND+human[Organism]
        //

        log.debug("get_mRNATranscriptIds:: XML for gene {0} and species {1}", new Object[]{geneName, speciesName});
        NodeList nList = doc.getElementsByTagName("IdList");

        int nmRNA = 0;

        log.debug("get_mRNATranscriptIds:: Parsing XML for gene {0} and species {1}", new Object[]{geneName, speciesName});
        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                NodeList idNodes = eElement.getElementsByTagName("Id");

                for (int j = 0; j < idNodes.getLength(); j++) {
                    Element idElement = (Element) idNodes.item(j);
                    geneIds.add(idElement.getTextContent());
                }
            }

        }

        for (String geneId : geneIds) {
            transcriptIds.addAll(get_mRNATranscriptIdsUsingEntrezID(geneId));
        }

        return transcriptIds;

    }

    /**
     * Obtains the mRNA transcript corresponding to the EntrezId.
     * @param id EntrezId of the mRNA to look for.
     * @return Transcript of the mRNA
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public static String get_mRNATranscript(String id) throws ParserConfigurationException, SAXException, IOException {
        //eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&retmode=xml&id=NM_001128602

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        URL url = new URL("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&retmode=xml&id=" + id);
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());

        NodeList nList = doc.getElementsByTagName("GBSeq_sequence");
        String seq = "";
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            //System.out.println(nNode.getTextContent());
            seq = seq + nNode.getTextContent();
        }

        return seq;
    }

    /**
     * Obtains the transcript of a nucleotide chain from NCBI.
     * @param nucCode code of the nucleotide chain
     * @return transcript of the nucleotide chain.
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException 
     */
    @Deprecated
    public static String get_nucleoTydeTranscript(String nucCode) throws ParserConfigurationException, IOException, SAXException {
        //https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&retmode=text&rettype=fasta&id=NM_001142498.1

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        URL url = new URL("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&retmode=xml&rettype=fasta&id=" + nucCode);
        URLConnection con = url.openConnection();
        con.setConnectTimeout(60000); // 10 seconds
        Document doc = dBuilder.parse(con.getInputStream());

        boolean found = false;
        NodeList nList;
        String seq = "";

        /* nList = doc.getElementsByTagName("GBSeq_sequence");
        
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            //System.out.println(nNode.getTextContent());
            seq = seq + nNode.getTextContent();
            found = true;
        }*/
        if (!found) {
            nList = doc.getElementsByTagName("TSeq_sequence");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                //System.out.println(nNode.getTextContent());
                seq = seq + nNode.getTextContent();
                found = true;
            }
        }

        return seq;

    }
}
