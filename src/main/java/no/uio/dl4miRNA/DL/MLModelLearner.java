/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DL;

import no.uio.dl4miRNA.DCP.Binarization;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.arbiter.optimize.api.termination.TerminationCondition;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;

import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculator;
import org.deeplearning4j.earlystopping.termination.EpochTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.termination.ScoreImprovementEpochTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.eval.ROC;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.ui.weights.HistogramIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.LoggerFactory;

/**
 * This class creates and trains miRAW's Deep Neural Network. The neural network
 * is created using DL4J library.
 *
 * @author apla
 */
public class MLModelLearner {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MLModelLearner.class);

    protected int batchSize;        //Number of instances that are feed into the 
    //neural network at every iteration.
    protected int randomSeed;             //Seed used for the random number generation
    protected double learningRate;  //Speed at which the neural network learns
    protected int nEpochs;          //Number of epochs that can be used for trainning the network

    private int numInputs;          //Number of neural network input nodes (number of miRNA nodes + number of Sequence nodes)

    protected int miRNAInputs;      //Number of neural network input nodes corresponding to the miRNA
    protected int seqInputs;        //Number of neural network input nodes corresponding to the sequence;
    protected int seedPosition;     //Position of the first node in the seqInputs that corresponds to the randomSeed region
    //The randomSeed region will go from seedPosition to the left:        n n n n n s s s s s S n n n n

    protected int numOutputs;       //Number of neural network output nodes (2, positive and negative)
    protected int numHiddenNodes;   //Number of nodes in the first layer.
    protected int[] networkShape = {numInputs, numHiddenNodes, (numHiddenNodes / 2), numHiddenNodes / 4, numHiddenNodes / 4, numOutputs};
    //Shape of the network defined by nodes at each layer
    protected int hiddenLayers;     //Number of hidden layers
    protected int nSets;            //Number of sets/folds used when performing Cross Validation
    protected double dropout;       //Dropout rate, % of nodes deactivated at each iteration in order to prevent overtrainning
    protected int epochsWithoutChange;  //When using a CrossValidation (TTV), the learning will stop
    //after 'epochsWithoutChange' Number of epochs in which the network
    //has not improved
    protected int labelLocation;    //Class column index
    protected LossFunctions.LossFunction lossFunction;
    
     

    /**
     * Default constructor. Batch size = 50, randomSeed=101, nEpochs=50,
     * learningRate=0.005, numInputs=240, outputs=2,numHiddenNodes=400;
     * nSets=10; dropout=0.2; hiddenLayers = 4;
     */
    public MLModelLearner() {
        batchSize = 50;
        randomSeed = 101;
        learningRate = 0.005;
        nEpochs = 50;

        miRNAInputs = 4 * 30;
        seqInputs = 4 * 30;

        numInputs = miRNAInputs + seqInputs;
        numOutputs = 2;

        numHiddenNodes = 400;
        nSets = 10;
        networkShape = makeShape();
        dropout = 0.2;
        hiddenLayers = networkShape.length - 2;

        epochsWithoutChange = 25;

        labelLocation = 0;
        
        lossFunction = LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD;

    }

    /**
     * Makes the shape of the neural network into a rocket shape: L0; L1; L2;
     * L3; L4; L5 inputs=240; numHiddenNodes; numHiddenNodes/2;
     * numHiddenNodes/4; numHiddenNodes/4; numOuputs
     *
     * @return shape wit the number of nodes per layer.
     */
    private int[] makeShape() {
        int[] newShape = {numInputs, numHiddenNodes, (numHiddenNodes / 2), numHiddenNodes / 4, numHiddenNodes / 4, numOutputs};
        return newShape;
    }

    /**
     * Generates a Cross Validation with train and testing.
     *
     * @param expName Name of the Cross validation experiment
     * @param experimentDirectory Location of the output files
     * @param dataFolder Location where the Cross Validation input data is
     * stored
     * @param setPrefix String defining the name of the cross validation csv
     * files
     * @param webListeners Opens a web browser tab to show the training process.
     * @throws FileNotFoundException
     */
    public void crossValidateNoConcurrent(String expName, String experimentDirectory, String dataFolder, String setPrefix, boolean webListeners) throws FileNotFoundException {
        log.info("initializing Cross Validation with train and test files.");
        //creating output folder
        if (!(new File(experimentDirectory)).exists()) {
            new File(experimentDirectory).mkdir();
        }

        String dataSource = dataFolder + "/" + setPrefix;

        //initializing writers
        PrintWriter cvExpNotes = crossValidationNotes(experimentDirectory, dataSource);
        PrintWriter evalResultsBest = crossValidationEvaluationOutput(experimentDirectory, "BestModelEval.txt");
        PrintWriter evalResults = crossValidationEvaluationOutput(experimentDirectory, "LastModelEval.txt");

        //points to plot a roc curve
        PrintWriter rocCurveBestPoints = new PrintWriter(new File(experimentDirectory + "/bestModelROC.txt"));
        PrintWriter rocCurveLastPoints = new PrintWriter(new File(experimentDirectory + "/lastModelROC.txt"));

        IntStream sets = IntStream.range(0, nSets - 1);

        //for each fold in the dataset
        sets.parallel().forEach(set -> {

        }
        );
        for (int set = 0; set < nSets; set++) {
            try {
                //SmallTrick to deal with wrong formated CSVs with an extra ',' at the end of each row and 'repair' the file in case it's broken.
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_train.csv");
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_test.csv");

                //save current time to keep track of execution times
                long dateStart = new Date().getTime();

                log.info("############################# SET " + set + " #############################");
                cvExpNotes.println("############################# SET " + set + " #############################");

                //Load the training data:
                RecordReader rrTrain = new CSVRecordReader();
                rrTrain.initialize(new FileSplit(new File(dataSource + set + "_train.csv")));
                DataSetIterator trainIter = new RecordReaderDataSetIterator(rrTrain, batchSize, labelLocation, 2);

                //Load the test/evaluation data:            We load it several times because we will evaluate the last trainned model and the 'best' trainned model. We also generate ROC Curves.
                RecordReader rrTest = new CSVRecordReader();
                rrTest.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, batchSize, labelLocation, 2);

                RecordReader rrTest2 = new CSVRecordReader();
                rrTest2.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIter2 = new RecordReaderDataSetIterator(rrTest2, batchSize, labelLocation, 2);

                RecordReader rrTestROC = new CSVRecordReader();
                rrTestROC.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIterROC = new RecordReaderDataSetIterator(rrTestROC, batchSize, labelLocation, 2);

                RecordReader rrTestROClast = new CSVRecordReader();
                rrTestROClast.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIterROClast = new RecordReaderDataSetIterator(rrTestROClast, batchSize, labelLocation, 2);

                long dateDataLoaded = new Date().getTime();

                //Create the ML model;
                MultiLayerNetwork model = createCustomModel(webListeners);
                long dateModelCreated = new Date().getTime();

                //train the model
                MultiLayerNetwork[] models = this.trainModel(model, trainIter, cvExpNotes);
                MultiLayerNetwork lastModel = models[0];
                MultiLayerNetwork bestModel = models[1];
                long dateModelTrained = new Date().getTime();

                //Save models to set directories
                File setFile = new File(experimentDirectory + "/Set" + set);
                setFile.mkdir();
                File modelFile = new File(setFile.getPath() + "/bestModel.bin");
                ModelSerializer.writeModel(bestModel, modelFile, true);
                File modelFile2 = new File(setFile.getPath() + "/lastModel.bin");
                ModelSerializer.writeModel(lastModel, modelFile2, true);
                long dateModelSaved = new Date().getTime();

                //evaluate the model
                Evaluation eval = evaluateModel(lastModel, testIter);
                Evaluation evalBest = evaluateModel(bestModel, testIter2);

                ROC rocCurveBest = getROC(bestModel, testIterROC);
                List<ROC.ROCValue> bestPoints = rocCurveBest.getResults();
                
                
                ROC rocCurveLast = getROC(lastModel, testIterROClast);
                List<ROC.ROCValue> lastPoints = rocCurveBest.getResults();
                
                long dateModelEvaluated = new Date().getTime();

                //save Time statistics
                cvExpNotes.println("Set " + set + " model creation time: " + ((dateModelCreated - dateStart) / 1000));
                cvExpNotes.println("Set " + set + " training time: " + ((dateModelTrained - dateModelCreated) / 1000));
                cvExpNotes.println("Set " + set + " model saving time: " + ((dateModelSaved - dateModelTrained) / 1000));
                cvExpNotes.println("Set " + set + " evaluation time: " + ((dateModelEvaluated - dateModelSaved) / 2000)); //2 evaluations done at the same time
                cvExpNotes.flush();

                log.info(eval.stats());

                //Generate ROC Curves file
                if (set == 0) {     //Create file and headers
                    rocCurveBestPoints.print("Fold/TH \t");
                    rocCurveLastPoints.print("Fold/TH \t");
                    for (int j = 0; j < bestPoints.size(); j++) {

                        String bestTH = String.format("%10.2f\t", bestPoints.get(j).getThreshold());
                        String lastTH = String.format("%10.2f\t", lastPoints.get(j).getThreshold());
                        rocCurveBestPoints.print(bestTH);
                        rocCurveLastPoints.print(lastTH);
                    }
                    rocCurveBestPoints.println();
                    rocCurveLastPoints.println();
                }
                rocCurveBestPoints.print(set + "TPR\t");
                rocCurveLastPoints.print(set + "TPR\t");
                for (int j = 0; j < bestPoints.size(); j++) {
                    rocCurveBestPoints.print(bestPoints.get(j).getTruePositiveRate() + "\t");
                    rocCurveLastPoints.print(lastPoints.get(j).getTruePositiveRate() + "\t");
                }
                rocCurveBestPoints.println();
                rocCurveLastPoints.println();

                rocCurveBestPoints.print(set + "FPR\t");
                rocCurveLastPoints.print(set + "FPR\t");
                for (int j = 0; j < bestPoints.size(); j++) {
                    rocCurveBestPoints.print(bestPoints.get(j).getFalsePositiveRate() + "\t");
                    rocCurveLastPoints.print(lastPoints.get(j).getFalsePositiveRate() + "\t");
                }
                rocCurveBestPoints.println();
                rocCurveLastPoints.println();

                rocCurveBestPoints.flush();
                rocCurveLastPoints.flush();

                //save eavaluation statistics for last model
                evalResults.println(set + "\t"
                        + eval.trueNegatives().get(1) + "\t"
                        + eval.falseNegatives().get(1) + "\t"
                        + eval.truePositives().get(1) + "\t"
                        + eval.falsePositives().get(1) + "\t"
                        + eval.accuracy() + "\t"
                        + eval.precision() + "\t"
                        + eval.recall() + "\t"
                        + eval.f1() + "\t"
                        + rocCurveLast.calculateAUC() + "\t");
                evalResults.flush();

                //save evaluation statistics for best model
                evalResultsBest.println(set + "\t"
                        + evalBest.trueNegatives().get(1) + "\t"
                        + evalBest.falseNegatives().get(1) + "\t"
                        + evalBest.truePositives().get(1) + "\t"
                        + evalBest.falsePositives().get(1) + "\t"
                        + evalBest.accuracy() + "\t"
                        + evalBest.precision() + "\t"
                        + evalBest.recall() + "\t"
                        + evalBest.f1() + "\t"
                        + rocCurveBest.calculateAUC() + "\t");;
                evalResultsBest.flush();

            } catch (Exception e) {
                log.error(e.getCause() + " : " + e.getMessage());
                evalResults.println("Error! in set: " + set + "\t" + e.toString());
                evalResults.flush();
                evalResultsBest.println("Error! in set: " + set + "\t" + e.toString());
                evalResultsBest.flush();
                e.printStackTrace();

            }

        }
        //Close files
        evalResults.close();
        evalResultsBest.close();
        cvExpNotes.close();
    }

    /**
     * Generates a Cross Validation with train and testing.
     *
     * @param expName Name of the Cross validation experiment
     * @param experimentDirectory Location of the output files
     * @param dataFolder Location where the Cross Validation input data is
     * stored
     * @param setPrefix String defining the name of the cross validation csv
     * files
     * @param webListeners Opens a web browser tab to show the training process.
     * @throws FileNotFoundException
     */
    public void crossValidate(String expName, String experimentDirectory, String dataFolder, String setPrefix, boolean webListeners) throws FileNotFoundException {
        log.info("initializing Cross Validation with train and test files.");
        //creating output folder
        if (!(new File(experimentDirectory)).exists()) {
            new File(experimentDirectory).mkdir();
        }

        final String dataSource = dataFolder + "/" + setPrefix;

        //initializing writers
        PrintWriter cvExpNotes = crossValidationNotes(experimentDirectory, dataSource);
        PrintWriter evalResultsBest = crossValidationEvaluationOutput(experimentDirectory, "BestModelEval.txt");
        String[] evalResultsBestString = new String[nSets];

        PrintWriter evalResults = crossValidationEvaluationOutput(experimentDirectory, "LastModelEval.txt");
        String[] evalResultsString = new String[nSets];

        //points to plot a roc curve
        PrintWriter rocCurveBestPoints = new PrintWriter(new File(experimentDirectory + "/bestModelROC.txt"));
        PrintWriter rocCurveLastPoints = new PrintWriter(new File(experimentDirectory + "/lastModelROC.txt"));

        String[] rocCurveLastPointsString = new String[nSets];
        String[] rocCurveBestPointsString = new String[nSets];

        for (int i = 0; i < nSets; i++) {
            evalResultsBestString[i] = ("");
            evalResultsString[i] = ("");
            rocCurveLastPointsString[i] = ("");
            rocCurveBestPointsString[i] = ("");
        }

        IntStream sets = IntStream.range(0, nSets - 1);

        //for each fold in the dataset
        sets.parallel().forEach(set -> {
            try {
                //SmallTrick to deal with wrong formated CSVs with an extra ',' at the end of each row and 'repair' the file in case it's broken.
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_train.csv");
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_test.csv");

                //save current time to keep track of execution times
                long dateStart = new Date().getTime();

                log.info("############################# SET " + set + " #############################");
                cvExpNotes.println("############################# SET " + set + " #############################");

                //Load the training data:
                RecordReader rrTrain = new CSVRecordReader();
                rrTrain.initialize(new FileSplit(new File(dataSource + set + "_train.csv")));
                DataSetIterator trainIter = new RecordReaderDataSetIterator(rrTrain, batchSize, labelLocation, 2);

                //Load the test/evaluation data:            We load it several times because we will evaluate the last trainned model and the 'best' trainned model. We also generate ROC Curves.
                RecordReader rrTest = new CSVRecordReader();
                rrTest.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, batchSize, labelLocation, 2);

                RecordReader rrTest2 = new CSVRecordReader();
                rrTest2.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIter2 = new RecordReaderDataSetIterator(rrTest2, batchSize, labelLocation, 2);

                RecordReader rrTestROC = new CSVRecordReader();
                rrTestROC.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIterROC = new RecordReaderDataSetIterator(rrTestROC, batchSize, labelLocation, 2);

                RecordReader rrTestROClast = new CSVRecordReader();
                rrTestROClast.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIterROClast = new RecordReaderDataSetIterator(rrTestROClast, batchSize, labelLocation, 2);

                long dateDataLoaded = new Date().getTime();

                //Create the ML model;
                MultiLayerNetwork model = createCustomModel(webListeners);
                long dateModelCreated = new Date().getTime();

                //train the model
                MultiLayerNetwork[] models = this.trainModel(model, trainIter, cvExpNotes);
                MultiLayerNetwork lastModel = models[0];
                MultiLayerNetwork bestModel = models[1];
                long dateModelTrained = new Date().getTime();

                //Save models to set directories
                File setFile = new File(experimentDirectory + "/Set" + set);
                setFile.mkdir();
                File modelFile = new File(setFile.getPath() + "/bestModel.bin");
                ModelSerializer.writeModel(bestModel, modelFile, true);
                File modelFile2 = new File(setFile.getPath() + "/lastModel.bin");
                ModelSerializer.writeModel(lastModel, modelFile2, true);
                long dateModelSaved = new Date().getTime();

                //evaluate the model
                Evaluation eval = evaluateModel(lastModel, testIter);
                Evaluation evalBest = evaluateModel(bestModel, testIter2);
                
                ROC rocCurveBest = getROC(bestModel, testIterROC); 
                
                List<ROC.ROCValue> bestPoints = rocCurveBest.getResults();
                
                ROC rocCurveLast = getROC(lastModel, testIterROClast);
                List<ROC.ROCValue> lastPoints = rocCurveBest.getResults();

                long dateModelEvaluated = new Date().getTime();

                //save Time statistics
                cvExpNotes.println("Set " + set + " model creation time: " + ((dateModelCreated - dateStart) / 1000));
                cvExpNotes.println("Set " + set + " training time: " + ((dateModelTrained - dateModelCreated) / 1000));
                cvExpNotes.println("Set " + set + " model saving time: " + ((dateModelSaved - dateModelTrained) / 1000));
                cvExpNotes.println("Set " + set + " evaluation time: " + ((dateModelEvaluated - dateModelSaved) / 2000)); //2 evaluations done at the same time
                cvExpNotes.flush();

                log.info("Set "+set+": "+eval.stats());

                //Generate ROC Curves file
                if (set == 0) {     //Create file and headers
                    //rocCurveBestPoints.print("Fold/TH \t");
                    rocCurveBestPointsString[set] = rocCurveBestPointsString[set] + "Fold/TH \t";
                    rocCurveLastPointsString[set] = rocCurveLastPointsString[set] + "Fold/TH \t";
                    for (int j = 0; j < bestPoints.size(); j++) {

                        String bestTH = String.format("%10.2f\t", bestPoints.get(j).getThreshold());
                        String lastTH = String.format("%10.2f\t", lastPoints.get(j).getThreshold());
                        rocCurveBestPointsString[set] = rocCurveBestPointsString[set] + (bestTH);
                        rocCurveLastPointsString[set] = rocCurveLastPointsString[set] + (lastTH);
                    }
                    rocCurveLastPointsString[set]=rocCurveLastPointsString[set]+"\n";
                    rocCurveBestPointsString[set]=rocCurveBestPointsString[set]+"\n";
                    
                }
                rocCurveBestPointsString[set] = rocCurveBestPointsString[set] +(set + "TPR\t");
                rocCurveLastPointsString[set] = rocCurveLastPointsString[set] +(set + "TPR\t");
                for (int j = 0; j < bestPoints.size(); j++) {
                    rocCurveBestPointsString[set] = rocCurveBestPointsString[set] +(bestPoints.get(j).getTruePositiveRate() + "\t");
                    rocCurveLastPointsString[set] = rocCurveLastPointsString[set] +(lastPoints.get(j).getTruePositiveRate() + "\t");
                }
                rocCurveLastPointsString[set]=rocCurveLastPointsString[set]+"\n";
                rocCurveBestPointsString[set]=rocCurveBestPointsString[set]+"\n";

                rocCurveBestPointsString[set]=rocCurveBestPointsString[set]+(set + "FPR\t");
                rocCurveLastPointsString[set]=rocCurveLastPointsString[set]+(set + "FPR\t");
                for (int j = 0; j < bestPoints.size(); j++) {
                    rocCurveBestPointsString[set] = rocCurveBestPointsString[set] +(bestPoints.get(j).getFalsePositiveRate() + "\t");
                    rocCurveLastPointsString[set] = rocCurveLastPointsString[set] +(lastPoints.get(j).getFalsePositiveRate() + "\t");
                }
                rocCurveLastPointsString[set]=rocCurveLastPointsString[set]+"\n";
                rocCurveBestPointsString[set]=rocCurveBestPointsString[set]+"\n";


                //save eavaluation statistics for last model
                evalResultsString[set]=evalResultsString[set]+(set + "\t"
                        + eval.trueNegatives().get(1) + "\t"
                        + eval.falseNegatives().get(1) + "\t"
                        + eval.truePositives().get(1) + "\t"
                        + eval.falsePositives().get(1) + "\t"
                        + eval.accuracy() + "\t"
                        + eval.precision() + "\t"
                        + eval.recall() + "\t"
                        + eval.f1() + "\t"
                        + rocCurveLast.calculateAUC() + "\t");
                evalResultsString[set]=evalResultsString[set]+"\n";

                //save evaluation statistics for best model
                evalResultsBestString[set]=evalResultsBestString[set]+(set + "\t"
                        + evalBest.trueNegatives().get(1) + "\t"
                        + evalBest.falseNegatives().get(1) + "\t"
                        + evalBest.truePositives().get(1) + "\t"
                        + evalBest.falsePositives().get(1) + "\t"
                        + evalBest.accuracy() + "\t"
                        + evalBest.precision() + "\t"
                        + evalBest.recall() + "\t"
                        + evalBest.f1() + "\t"
                        + rocCurveBest.calculateAUC() + "\t");;
                evalResultsBestString[set]=evalResultsBestString[set]+"\n";

            } catch (Exception e) {
                log.error(e.getCause() + " : " + e.getMessage());
                evalResultsString[set]=evalResultsString[set]+("Error! in set: " + set + "\t" + e.toString());
                evalResultsString[set]=evalResultsString[set]+"\n";
                evalResultsBestString[set]=evalResultsBestString[set]+("Error! in set: " + set + "\t" + e.toString());
                evalResultsBestString[set]=evalResultsBestString[set]+"\n";
                e.printStackTrace();

            }
        }
        );
        
        for (int i=0; i<nSets;i++){
            evalResults.print(evalResultsString[i]);
            evalResults.flush();
            evalResultsBest.print(evalResultsBestString[i]);
            evalResultsBest.flush();
            rocCurveBestPoints.print(rocCurveBestPointsString[i]);
            rocCurveBestPoints.flush();
            rocCurveLastPoints.print(rocCurveLastPointsString[i]);
            rocCurveLastPoints.flush();
            
        }

        //Close files
        evalResults.close();
        evalResultsBest.close();
        cvExpNotes.close();
    }

    /**
     * Generates a Cross Validation with train, testing and validation.
     * Validation is used to determine which of the learnt models in each set is
     * the best.
     *
     * @param expName Name of the Cross validation experiment
     * @param experimentDirectory Location of the output files
     * @param dataFolder Location where the Cross Validation input data is
     * stored
     * @param setPrefix String defining the name of the cross validation csv
     * files
     * @param webListeners Opens a web browser tab to show the training process.
     * @throws FileNotFoundException
     */
    public void crossValidateTVT(String expName, String experimentDirectory, String dataFolder, String setPrefix, boolean webListeners) throws FileNotFoundException {
        log.info("Cross Validation with train,validation and test files.");

        //Create output directories
        if (!(new File(experimentDirectory)).exists()) {
            new File(experimentDirectory).mkdir();
        }

        String dataSource = dataFolder + "/" + setPrefix;

        //initialize output files
        PrintWriter cvExpNotes = crossValidationNotes(experimentDirectory, dataSource);
        PrintWriter evalResultsBest = crossValidationEvaluationOutput(experimentDirectory, "BestModelEval.txt");
        PrintWriter evalResults = crossValidationEvaluationOutput(experimentDirectory, "LastModelEval.txt");

        //for each fold
        for (int set = 0; set < nSets; set++) {
            try {
                //SmallTrick to deal with wrong formated CSVs with an extra ',' at the end of each row and 'repair' the file.
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_train.csv");
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_test.csv");
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_val.csv");

                long dateStart = new Date().getTime();

                log.info("############################# SET " + set + " #############################");
                cvExpNotes.println("############################# SET " + set + " #############################");

                //Load the training data:
                RecordReader rrTrain = new CSVRecordReader();
                rrTrain.initialize(new FileSplit(new File(dataSource + set + "_train.csv")));
                DataSetIterator trainIter = new RecordReaderDataSetIterator(rrTrain, batchSize, labelLocation, 2);

                //Load the test/validation data:
                RecordReader rrTest = new CSVRecordReader();
                rrTest.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, batchSize, labelLocation, 2);

                RecordReader rrTest2 = new CSVRecordReader();
                rrTest2.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                DataSetIterator testIter2 = new RecordReaderDataSetIterator(rrTest2, batchSize, labelLocation, 2);

                RecordReader rrVal = new CSVRecordReader();
                rrVal.initialize(new FileSplit(new File(dataSource + set + "_val.csv")));
                DataSetIterator valIter = new RecordReaderDataSetIterator(rrVal, batchSize, labelLocation, 2);

                RecordReader rrVal2 = new CSVRecordReader();
                rrVal2.initialize(new FileSplit(new File(dataSource + set + "_val.csv")));
                DataSetIterator valIter2 = new RecordReaderDataSetIterator(rrVal2, batchSize, labelLocation, 2);

                long dateDataLoaded = new Date().getTime();

                //Create the model;
                MultiLayerNetwork model = createCustomModel(webListeners);
                long dateModelCreated = new Date().getTime();

                //train the model
                MultiLayerNetwork[] models = this.trainModel(model, trainIter, valIter, cvExpNotes);
                MultiLayerNetwork lastModel = models[0];
                MultiLayerNetwork bestModel = models[1];
                long dateModelTrained = new Date().getTime();

                //Save models to set directories
                File setFile = new File(experimentDirectory + "/Set" + set);
                setFile.mkdir();
                File modelFile = new File(setFile.getPath() + "/bestModel.bin");
                ModelSerializer.writeModel(bestModel, modelFile, true);
                File modelFile2 = new File(setFile.getPath() + "/lastModel.bin");
                ModelSerializer.writeModel(lastModel, modelFile2, true);
                long dateModelSaved = new Date().getTime();

                //evaluate the model
                Evaluation eval = evaluateModel(lastModel, testIter);
                Evaluation evalBest = evaluateModel(bestModel, testIter2);

                long dateModelEvaluated = new Date().getTime();

                //save Time statistics
                cvExpNotes.println("Set " + set + " model creation time: " + ((dateModelCreated - dateStart) / 1000));
                cvExpNotes.println("Set " + set + " training time: " + ((dateModelTrained - dateModelCreated) / 1000));
                cvExpNotes.println("Set " + set + " model saving time: " + ((dateModelSaved - dateModelTrained) / 1000));
                cvExpNotes.println("Set " + set + " evaluation time: " + ((dateModelEvaluated - dateModelSaved) / 2000)); //2 evaluations done at the same time
                cvExpNotes.flush();

                log.info(eval.stats());

                //save eavaluation statistics for last model
                evalResults.println(set + "\t"
                        + eval.trueNegatives().get(1) + "\t"
                        + eval.falseNegatives().get(1) + "\t"
                        + eval.truePositives().get(1) + "\t"
                        + eval.falsePositives().get(1) + "\t"
                        + eval.accuracy() + "\t"
                        + eval.precision() + "\t"
                        + eval.recall() + "\t"
                        + eval.f1() + "\t");
                evalResults.flush();

                //save evaluation statistics for best model
                evalResultsBest.println(set + "\t"
                        + evalBest.trueNegatives().get(1) + "\t"
                        + evalBest.falseNegatives().get(1) + "\t"
                        + evalBest.truePositives().get(1) + "\t"
                        + evalBest.falsePositives().get(1) + "\t"
                        + evalBest.accuracy() + "\t"
                        + evalBest.precision() + "\t"
                        + evalBest.recall() + "\t"
                        + evalBest.f1() + "\t");
                evalResultsBest.flush();

            } catch (Exception e) {
                log.error(e.getCause() + " : " + e.getMessage());
                evalResults.println("Error! in set: " + set + "\t" + e.toString());
                evalResults.flush();
                evalResultsBest.println("Error! in set: " + set + "\t" + e.toString());
                evalResultsBest.flush();
            }

        }
        evalResults.close();
        evalResultsBest.close();
        cvExpNotes.close();
    }

    /**
     * Creates a standard rocket shape Feed Forward neural network with 4 hidden
     * layers.
     *
     * @param webListeners adds a listener to the neural network so its training
     * can be monitorized in a web browser.
     * @return MutilLayer Neural Network with the predefined shape.
     */
    private MultiLayerNetwork createModel(boolean webListeners) {

        log.info("Creating model");
        //Multi layer configuration
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(randomSeed)
                .iterations(1)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(learningRate)
                .updater(Updater.NESTEROVS).momentum(0.9)
                .dropOut(dropout)
                .list()
                //input layer
                .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes)
                        .weightInit(WeightInit.XAVIER)
                        .activation("relu")
                        .build())
                //hidden Layers
                .layer(1, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes / 2)
                        .weightInit(WeightInit.XAVIER)
                        .activation("relu")
                        .build())
                .layer(2, new DenseLayer.Builder().nIn(numHiddenNodes / 2).nOut(numHiddenNodes / 4)
                        .weightInit(WeightInit.XAVIER)
                        .activation("relu")
                        .build())
                .layer(3, new DenseLayer.Builder().nIn(numHiddenNodes / 4).nOut(numHiddenNodes / 4)
                        .weightInit(WeightInit.XAVIER)
                        .activation("relu")
                        .build())
                //Ouput layer
                .layer(4, new OutputLayer.Builder(this.lossFunction)
                        .weightInit(WeightInit.XAVIER)
                        .activation("softmax")
                        .nIn(numHiddenNodes / 4).nOut(numOutputs).build())
                .pretrain(false).backprop(true).build();

        log.info("Initializing model");
        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();
        if (webListeners) {
            UIServer uiServer = UIServer.getInstance();
            //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
            StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for saving and loading later
            //Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to be visualized
            uiServer.attach(statsStorage);
    
            model.setListeners(new StatsListener(statsStorage));
            //model.setListeners(new ScoreIterationListener(500),new HistogramIterationListener(1));    //Print score every 10 parameter updates
        }
        return model;
    }

    /**
     * Creates a neural network following the number of nodes and layers defined
     * in the "shape" attribute.
     *
     * @param webListeners adds a listener to the neural network so its training
     * can be monitorized in a web browser.
     * @return MutilLayer Neural Network with the desired shape.
     */
    private MultiLayerNetwork createCustomModel(boolean webListeners) {

        log.info("Creating model");
        //log.info("Build model....");

        //Create empty model with agreed parameters;
        NeuralNetConfiguration.ListBuilder LB = new NeuralNetConfiguration.Builder()
                .seed(randomSeed)
                .iterations(1)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(learningRate)
                .updater(Updater.NESTEROVS).momentum(0.9)
                .dropOut(dropout)
                .list();

        //add firstLayer
        LB.layer(0, new DenseLayer.Builder().nIn(networkShape[0]).nOut(networkShape[1])
                .weightInit(WeightInit.XAVIER)
                .activation("relu")
                .build()).pretrain(false).
                backprop(true);
        log.debug("Input Layer " + networkShape[0] + " to " + networkShape[1]);

        //add hidden layers
        for (int i = 1; i < this.networkShape.length - 2; i++) {
            //for each layer, obtain the number of desired hidden nodes.
            log.debug("Hidden layer " + networkShape[i] + " to " + networkShape[i + 1]);
            LB.layer(i, new DenseLayer.Builder().nIn(networkShape[i]).nOut(networkShape[i + 1])
                    .weightInit(WeightInit.XAVIER)
                    .activation("relu")
                    .build()).pretrain(false).
                    backprop(true);
        }

        //add output layer
        LB.layer(networkShape.length - 2, new OutputLayer.Builder(this.lossFunction)
                .weightInit(WeightInit.XAVIER)
                .activation("softmax")
                .nIn(networkShape[networkShape.length - 2]).nOut(networkShape[networkShape.length - 1]).build())
                .pretrain(false).backprop(true);
        log.debug("Output Layer " + networkShape[networkShape.length - 2] + " to " + networkShape[networkShape.length - 1]);

        //build model
        log.info("Initializing model");
        MultiLayerConfiguration conf = LB.build();
        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        if (webListeners) {
            UIServer uiServer = UIServer.getInstance();
            //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
            StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for saving and loading later
            //Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to be visualized
            uiServer.attach(statsStorage);
    
            model.setListeners(new StatsListener(statsStorage));
            //model.setListeners(new ScoreIterationListener(500),new HistogramIterationListener(1));    //Print score every 10 parameter updates
        }

        return model;
    }

    /**
     * Determines the appropriate CV training method (based on the available
     * data) and trains the model neural network.
     *
     * @param model neural network to be trained
     * @param trainIter training data iterator
     * @param valIter validation data iterator (optional). If this is provided,
     * the Cross Validation will follow a train, test, validation approach
     * @param cvExpNotes location of the file containing notes generated during
     * training
     * @return An array with all the trained models (one per CV fold).
     */
    private MultiLayerNetwork[] trainModel(MultiLayerNetwork model, DataSetIterator trainIter, DataSetIterator valIter, PrintWriter cvExpNotes) {
        if (valIter == null) {
            log.info("Regular iterative trainning");
            return trainModel(model, trainIter, cvExpNotes);
        }
        log.info("Early stopping trainer from DL4J used for training");
        return trainModelWithEarlyStopping(model, trainIter, valIter, cvExpNotes);
    }

    /**
     * Trains the model neural network following a Cross Validation approach.
     * The training last nEpochs.
     *
     * @param model neural network to be trained
     * @param trainIter training data iterator
     * @param cvExpNotes location of the file containing notes generated during
     * training
     * @return An array with all the trained models (one per CV fold).
     */
    private MultiLayerNetwork[] trainModel(MultiLayerNetwork model, DataSetIterator trainIter, PrintWriter cvExpNotes) {
        log.info("Training model");
        double bestScore = 99999999;
        int bestScoreEpoch = 0;

        MultiLayerNetwork bestModel = model.clone();
        MultiLayerNetwork lastModel;

        for (int n = 0; n < nEpochs; n++) {
            log.info("Epoch " + n + "/" + nEpochs);
            model.fit(trainIter);

            cvExpNotes.println("Epoch " + n + " - score " + model.score());
            if (bestScore >= Math.abs(model.score())) {
                bestScoreEpoch = n;
                bestModel = model.clone();
                bestScore = Math.abs(model.score());
            }
            cvExpNotes.flush();
        }
        MultiLayerNetwork[] models = new MultiLayerNetwork[2];
        lastModel = model.clone();
        models[0] = lastModel;
        models[1] = bestModel;
        cvExpNotes.println("Best epoch- " + bestScoreEpoch + " Score: " + bestScore);
        cvExpNotes.flush();
        return models;

    }

    /**
     * Trains the neural network model following a train test validation
     * approach. If the network is not improving after "epochsWithoutChange",
     * the learning process is automatically stop. The validation dataset is
     * used to determine if the model is improving at each iteration and to
     * select the 'best model' generated during all the epochs.
     *
     * @param model neural network to be trained
     * @param trainIter training data iterator
     * @param valIter validation data iterator (optional). If this is provided,
     * the Cross Validation will follow a train, test, validation approach
     * @param cvExpNotes location of the file containing notes generated during
     * training
     * @return An array with all the trained models (one per CV fold).
     */
    private MultiLayerNetwork[] trainModelWithEarlyStopping(MultiLayerNetwork model, DataSetIterator trainIter, DataSetIterator valIter, PrintWriter cvExpNotes) {
        log.info("Training model");

        //Defining automatic stop training conditions
        EpochTerminationCondition[] terminationConditions = {new MaxEpochsTerminationCondition(nEpochs), new ScoreImprovementEpochTerminationCondition(this.epochsWithoutChange)};
        EarlyStoppingConfiguration esConf = new EarlyStoppingConfiguration.Builder()
                .epochTerminationConditions(terminationConditions)
                //.iterationTerminationConditions(new MaxTimeIterationTerminationCondition(20, TimeUnit.MINUTES))
                .scoreCalculator(new DataSetLossCalculator(valIter, true))
                .evaluateEveryNEpochs(1)
                .build();

        //creating DL4J trainer
        EarlyStoppingTrainer trainer = new EarlyStoppingTrainer(esConf, model, trainIter);

        //Start training
        EarlyStoppingResult<MultiLayerNetwork> trainingResult = trainer.fit();
        MultiLayerNetwork bestModel = trainingResult.getBestModel();

        MultiLayerNetwork[] models = new MultiLayerNetwork[2];
        models[0] = bestModel;
        models[1] = bestModel;
        cvExpNotes.println("Best Score " + trainingResult.getBestModelScore() + " in Epoch " + trainingResult.getBestModelEpoch() + " of " + trainingResult.getTotalEpochs());
        cvExpNotes.println("Termination condition: " + trainingResult.getTerminationDetails());
        cvExpNotes.flush();
        return models;
    }

    /**
     * Evaluates a neural network using a test dataset and returns statistics.
     *
     * @param model neural network to be evaluated
     * @param testIter test training dataset iterator.
     * @return
     */
    private Evaluation evaluateModel(MultiLayerNetwork model, DataSetIterator testIter) {
        log.info("Evaluate model....");
        Evaluation eval = new Evaluation(numOutputs);

        //use DL4J tools to evaluate the network.
        while (testIter.hasNext()) {
            DataSet t = testIter.next();
            INDArray features = t.getFeatureMatrix();
            INDArray labels = t.getLabels();
            INDArray predicted = model.output(features, false);
            eval.eval(labels, predicted);

        }

        return eval;
    }

    /**
     * Evaluates a neural network using a test dataset and returns the ROC curve
     *
     * @param model neural network to be evaluated
     * @param testIter test training dataset iterator.
     * @return ROC curve object
     */
    private ROC getROC(MultiLayerNetwork model, DataSetIterator testIter) {
        log.info("Evaluate model....");

        //use DL4J tools to evaluate the network.
        ROC rocCurve = new ROC(20);
        while (testIter.hasNext()) {
            DataSet t = testIter.next();
            INDArray features = t.getFeatureMatrix();
            INDArray labels = t.getLabels();
            INDArray predicted = model.output(features, false);

            rocCurve.eval(labels, predicted);

        }
        return rocCurve;
    }

    /**
     * Creates and initializes a print writer to save notes generated during
     * cross-validation.
     *
     * @param experimentDirectory location to save the notes.
     * @param dataSource String with the location of the CV data.
     * @return print writer where the notes will be saved.
     * @throws FileNotFoundException
     */
    private PrintWriter crossValidationNotes(String experimentDirectory, String dataSource) throws FileNotFoundException {
        PrintWriter cvExpNotes = new PrintWriter(experimentDirectory + "/notes.txt");

        cvExpNotes.println("========================Configuration========================");
        cvExpNotes.println("Nsets = " + nSets);
        cvExpNotes.println("Data = " + dataSource);
        cvExpNotes.println("Hidden layers = " + hiddenLayers);
        cvExpNotes.print("Configuration=");
        for (int l : networkShape) {
            cvExpNotes.print(l + ", ");
        }
        cvExpNotes.println();
        cvExpNotes.println("Epochs = " + nEpochs);
        cvExpNotes.println("Learning rate = " + learningRate);
        cvExpNotes.println("BatchSize = " + batchSize);
        cvExpNotes.println("Dropout rate= " + dropout);
        cvExpNotes.println("EpochsWithoutChangeStopper = " + epochsWithoutChange);

        cvExpNotes.println("========================Experimentation=======================");
        cvExpNotes.flush();

        return cvExpNotes;
    }

    /**
     * Creates and initializes a print writer to save the evaluation results
     * cross-validation.
     *
     * @param experimentDirectory location to save the evaluation.
     * @param dataSource String with the location of the CV data.
     * @return print writer where the notes will be saved.
     * @throws FileNotFoundException
     */
    private PrintWriter crossValidationEvaluationOutput(String experimentDirectory, String docName) throws FileNotFoundException {
        PrintWriter evalResults = new PrintWriter(experimentDirectory + "/" + docName);
        evalResults.println("Set\tTN\tFN\tTP\tFP\tAccuracy\tPrecision\tREcall\tF1Score\tAUC\t");
        evalResults.flush();
        return evalResults;
    }

    /*#########################################
    ############# Getters & Setters ###########
    #########################################*/
    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public int getRandomSeed() {
        return randomSeed;
    }

    public void setRandomSeed(int randomSeed) {
        this.randomSeed = randomSeed;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public int getnEpochs() {
        return nEpochs;
    }

    public void setnEpochs(int nEpochs) {
        this.nEpochs = nEpochs;
    }

    public int getNumInputs() {
        return numInputs;
    }

    public int getMiRNAInputs() {
        return miRNAInputs;
    }

    public void setMiRNAInputs(int miRNAInputs) {
        this.miRNAInputs = miRNAInputs;
        this.setNumInputs(this.miRNAInputs + this.seqInputs);
    }

    public int getSeqInputs() {
        return seqInputs;
    }

    public void setSeqInputs(int seqInputs) {
        this.seqInputs = seqInputs;
        this.setNumInputs(this.miRNAInputs + this.seqInputs);
    }

    public void setNumInputs(int miRNAInputs, int seqInputs) {
        this.miRNAInputs = miRNAInputs;
        this.seqInputs = seqInputs;
        this.setNumInputs(this.miRNAInputs + this.seqInputs);
    }

    private void setNumInputs(int numInputs) {
        this.numInputs = numInputs;
        this.networkShape = this.makeShape();
    }

    public int getNumOutputs() {
        return numOutputs;
    }

    public void setNumOutputs(int numOutputs) {
        this.numOutputs = numOutputs;
    }

    public int getNumHiddenNodes() {
        return numHiddenNodes;
    }

    public void setNumHiddenNodes(int numHiddenNodes) {
        this.numHiddenNodes = numHiddenNodes;
        this.networkShape = this.makeShape();
    }

    public int[] getNetworkShape() {
        return networkShape;
    }

    public void setNetworkShape(int[] networkShape) {
        int[] tempNetworkShape = networkShape;
        if (tempNetworkShape != null && tempNetworkShape.length > 1) {
            //checks if the number of input nodes is correct;
            if (tempNetworkShape[0] != this.numInputs) {
                log.warn("Incorrect number of input nodes; the network shape of the first layer will be corrected to" + numInputs);
                tempNetworkShape[0] = this.numInputs;
            }

            if (tempNetworkShape[tempNetworkShape.length - 1] != this.numOutputs) {
                log.warn("Incorrect number of output nodes; the network shape of the first layer will be corrected to" + numOutputs);
                tempNetworkShape[tempNetworkShape.length - 1] = this.numOutputs;
            }

            this.networkShape = tempNetworkShape;
        } else {
            log.warn("Empty or too small network shape, it won't be updated.");
        }

    }

    public int getHiddenLayers() {
        return hiddenLayers;
    }

    public void setHiddenLayers(int hiddenLayers) {
        this.hiddenLayers = hiddenLayers;
    }

    public int getnSets() {
        return nSets;
    }

    public void setnSets(int nSets) {
        this.nSets = nSets;
    }

    public double getDropout() {
        return dropout;
    }

    public void setDropout(double dropout) {
        this.dropout = dropout;
    }

    public int getEpochsWithoutChange() {
        return epochsWithoutChange;
    }

    public void setEpochsWithoutChange(int epochsWithoutChange) {
        this.epochsWithoutChange = epochsWithoutChange;
    }

    public int getLabelLocation() {
        return labelLocation;
    }

    public void setLabelLocation(int labelLocation) {
        this.labelLocation = labelLocation;
    }
    
    public void setLossFunction(LossFunctions.LossFunction lossF){
        this.lossFunction=lossF;
    }
    
    public void setLossFunction(String lossF){
        switch (lossF.toUpperCase()){
            case "XENT":
                this.lossFunction= LossFunctions.LossFunction.XENT;
                break;
            case "NEGATIVELOGLIKELIHOOD":
                this.lossFunction= LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD;
                break;
            case "SQUARED_LOSS":
                this.lossFunction=LossFunctions.LossFunction.SQUARED_LOSS;
                break;
            default:
                this.lossFunction= LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD;
        }
    }

    public LossFunctions.LossFunction getLossFunction() {
        return lossFunction;
    }
    
    

    /**
     * Method used during development to test/develop MLModel Learner.
     *
     * @param args
     * @throws FileNotFoundException
     */
    public static void mainA(String args[]) throws FileNotFoundException {
        String expName = "Filter2_CV";
        String experimentDirectory = "/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - GD/expRes/" + expName;

        String setPrefix = "set";

        MLModelLearner modelLearner = new MLModelLearner();

        modelLearner.setNumHiddenNodes(14);
        modelLearner.setNumInputs(7);
        int[] newNetworkShape = {modelLearner.getNumInputs(), 14, 12, 7, 4, 4, 3, modelLearner.getNumOutputs()};
        modelLearner.setNetworkShape(newNetworkShape);

        modelLearner.setDropout(0);
        modelLearner.setnEpochs(1000);
        modelLearner.setEpochsWithoutChange(200);
        modelLearner.setLabelLocation(7);
        modelLearner.setLearningRate(0.05); //experiment 2

        String dataFolder = "/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - GD/CVf";
        modelLearner.crossValidate(expName, experimentDirectory, dataFolder, setPrefix, false);

        //String dataFolder = "/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - RD/CVV";
        //modelLearner.crossValidateTVT(expName, experimentDirectory, dataFolder, setPrefix, false);
    }

    /**
     * Method used during development to test/develop MLModel Learner using non
     * miRNA data.
     *
     * @param args@throws FileNotFoundException
     */
    public static void main_sensistepRD(String args[]) throws FileNotFoundException {
        String expName = "RawData2_CV";
        String experimentDirectory = "/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - RD/expRes/" + expName;

        String setPrefix = "set";

        MLModelLearner modelLearner = new MLModelLearner();

        modelLearner.setNumHiddenNodes(200);
        modelLearner.setNumInputs(100);
        int[] newNetworkShape = {modelLearner.getNumInputs(), 200, 150, 100, 100, 50, 25, modelLearner.getNumOutputs()};
        modelLearner.setNetworkShape(newNetworkShape);

        modelLearner.setDropout(0);
        modelLearner.setnEpochs(1500);
        modelLearner.setEpochsWithoutChange(200);
        modelLearner.setLabelLocation(100);

        String dataFolder = "/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - RD/CV";
        modelLearner.crossValidate(expName, experimentDirectory, dataFolder, setPrefix, false);

        //String dataFolder = "/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - RD/CVV";
        //modelLearner.crossValidateTVT(expName, experimentDirectory, dataFolder, setPrefix, false);
    }

    /**
     * Method used during development to test/develop MLModel Learner.
     *
     * @param args
     * @throws FileNotFoundException
     * @throws InterruptedException
     */
    public static void main2(String args[]) throws FileNotFoundException, InterruptedException {
        int delayTime = 0 * 3600;
        log.debug("Waiting to start.");
        Thread.sleep(delayTime * 1000);
        log.debug("Waking up!");
        String expName = "Exp_161025_2575_Dropout01_200eBIS";
        String experimentDirectory = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/experimentResults/" + expName;
        String dataFolder = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal_WN/CV75-25/binary";
        String setPrefix = "set_";

        MLModelLearner modelLearner = new MLModelLearner();

        modelLearner.setNumHiddenNodes(800);
        int[] newNetworkShape = {modelLearner.getNumInputs(), 800, 800, 400, 400, 200, 200, modelLearner.getNumOutputs()};
        modelLearner.setNetworkShape(newNetworkShape);
        modelLearner.setDropout(0.1);
        modelLearner.setnEpochs(1);
        modelLearner.setEpochsWithoutChange(50);

        //modelLearner.crossValidateTVT(expName, experimentDirectory, dataFolder, setPrefix, false);
        modelLearner.crossValidate(expName, experimentDirectory, dataFolder, setPrefix, false);

    }

}
