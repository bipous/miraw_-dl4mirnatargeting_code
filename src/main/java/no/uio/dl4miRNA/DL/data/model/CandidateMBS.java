/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DL.data.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.uio.dl4miRNA.Data.gathering.EnsemblAccess;
import no.uio.dl4miRNA.SiteFinder.DnaFoldPrediction;
import no.uio.dl4miRNA.SiteFinder.RNAFolder;
import org.slf4j.LoggerFactory;

/**
 * Class modeling a candidate Mirna Binding Site
 *
 * @author apla
 */
public class CandidateMBS {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(CandidateMBS.class);

    /**
     * Mirna Binding Site Transcript
     */
    protected String siteTranscript;
    /**
     * Mature miRNA transcript
     */
    protected String matureMirnaTranscript;
    /**
     * Starting position of the MBS in the 3'UTR
     */
    protected int gene3UTRStart;
    /**
     * Ending position of the MBS in the 3'UTr
     */
    protected int gene3UTREnd;
    /**
     * Free energy of the MBS
     */
    protected double freeEnergy;
    /**
     * Number of base pairs in the seed region
     */
    protected int bindsInSeed;

    /**
     * Starting nucleotide of the seed region
     */
    protected int seedRegionStart;

    /**
     * ending nucleotide of the seed region
     */
    protected int seedRegionEnd;

    /**
     * Watson crick basepairs in the seed region
     */
    protected int WCpairsInSeed;

    /**
     * Wobble pairs in the seed region
     */
    protected int WobblePairsInSeed;

    /**
     * Total Watson crick basepairs in the MBS
     */
    protected int totalWCPairs;

    /**
     * Total Wobble pairs in the MBS
     */
    protected int totalWobblePairs;

    /**
     * The MBS follows a canonical binding
     */
    protected boolean isCanonical;

    /**
     * Full 3'UTR gene transcript
     */
    protected String full3UTRTranscript;

    /**
     * Free text comment
     */
    protected String comment;

    /**
     * Predicted class of the MBS.
     */
    protected double prediction = 0;

    protected int seedRegionStartInSequence;

    protected String chromosome;
    protected int chrRegionStart;
    protected int chrRegionEnd;
    protected char strand = '+';

    protected String structure = "";

    protected boolean filtered = false;

    protected String filteredReason = "";

    protected Map<String, Object> additionalProperties;
    
    protected String targetGene ="";
    protected String miRNA="";
    
    protected double accessibilityEnergy;
    

    /**
     * Empty constructor
     */
    public CandidateMBS() {
        comment = "";
        additionalProperties = new HashMap<>();
    }

    //#################################
    //####### Getters & Setters #######
    //#################################
    public String getSiteTranscript() {
        return siteTranscript;
    }

    public void setSiteTranscript(String siteTranscript) {
        this.siteTranscript = siteTranscript;
    }

    public String getMatureMirnaTranscript() {
        return matureMirnaTranscript;
    }

    public void setMatureMirnaTranscript(String matureMirnaTranscript) {
        this.matureMirnaTranscript = matureMirnaTranscript;
    }

    public String getFull3UTRTranscript() {
        return full3UTRTranscript;
    }

    public void setFull3UTRTranscript(String full3UTRTranscript) {
        this.full3UTRTranscript = full3UTRTranscript;
    }

    public int getGene3UTRStart() {
        return gene3UTRStart;
    }

    public void setGene3UTRStart(int gene3UTRStart) {
        this.gene3UTRStart = gene3UTRStart;
    }

    public int getGene3UTREnd() {
        return gene3UTREnd;
    }

    public void setGene3UTREnd(int gene3UTREnd) {
        this.gene3UTREnd = gene3UTREnd;
    }

    public double getFreeEnergy() {
        return freeEnergy;
    }

    public void setFreeEnergy(double freeEnergy) {
        this.freeEnergy = freeEnergy;
    }

    public double getPrediction() {
        return prediction;
    }

    public void setPrediction(double prediction) {
        this.prediction = prediction;
    }

    public int getBindsInSeed() {
        return bindsInSeed;
    }

    public void setBindsInSeed(int bindsInSeed) {
        this.bindsInSeed = bindsInSeed;
    }

    public int getSeedRegionStart() {
        return seedRegionStart;
    }

    public void setSeedRegionStart(int seedRegionStart) {
        this.seedRegionStart = seedRegionStart;
    }

    public int getSeedRegionEnd() {
        return seedRegionEnd;
    }

    public void setSeedRegionEnd(int seedRegionEnd) {
        this.seedRegionEnd = seedRegionEnd;
    }

    public int getWCpairsInSeed() {
        return WCpairsInSeed;
    }

    public void setWCpairsInSeed(int WCpairsInSeed) {
        this.WCpairsInSeed = WCpairsInSeed;
    }

    public int getWobblePairsInSeed() {
        return WobblePairsInSeed;
    }

    public void setWobblePairsInSeed(int WobblePairsInSeed) {
        this.WobblePairsInSeed = WobblePairsInSeed;
    }

    public int getTotalWCPairs() {
        return totalWCPairs;
    }

    public void setTotalWCPairs(int totalWCPairs) {
        this.totalWCPairs = totalWCPairs;
    }

    public int getTotalWobblePairs() {
        return totalWobblePairs;
    }

    public void setTotalWobblePairs(int totalWobblePairs) {
        this.totalWobblePairs = totalWobblePairs;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean getIsCanonical() {
        return isCanonical;
    }

    public void setIsCanonical(boolean isCanonical) {
        this.isCanonical = isCanonical;
    }

    public int getSeedRegionStartInSequence() {
        return seedRegionStartInSequence;
    }

    public void setSeedRegionStartInSequence(int seedRegionStartInSequence) {
        this.seedRegionStartInSequence = seedRegionStartInSequence;
    }

    public String getChromosome() {
        return chromosome;
    }

    public void setChromosome(String chromosome) {
        this.chromosome = chromosome;
    }

    public int getChrRegionStart() {
        return chrRegionStart;
    }

    public void setChrRegionStart(int chrRegionStart) {
        this.chrRegionStart = chrRegionStart;
    }

    public int getChrRegionEnd() {
        return chrRegionEnd;
    }

    public void setChrRegionEnd(int chrRegionEnd) {
        this.chrRegionEnd = chrRegionEnd;
    }

    public char getStrand() {
        return strand;
    }

    public void setStrand(char strand) {
        this.strand = strand;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public void addProperty(String property, Object value) {
        this.additionalProperties.put(property, value);
    }

    public Object getProperty(String property) {
        return this.additionalProperties.get(property);
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public boolean isFiltered() {
        return filtered;
    }

    public void setFiltered(boolean filtered) {
        this.filtered = filtered;
    }

    public String getFilteredReason() {
        return filteredReason;
    }

    public void setFilteredReason(String filteredReason) {
        this.filteredReason = filteredReason;
    }

    public String getTargetGene() {
        return targetGene;
    }

    public void setTargetGene(String targetGene) {
        this.targetGene = targetGene;
    }

    public String getMiRNA() {
        return miRNA;
    }

    public void setMiRNA(String miRNA) {
        this.miRNA = miRNA;
    }

    public double getAccessibilityEnergy() {
        return accessibilityEnergy;
    }

    public void setAccessibilityEnergy(double accessibilityEnergy) {
        this.accessibilityEnergy = accessibilityEnergy;
    }
    
    
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CandidateMBS other = (CandidateMBS) obj;
        if (this.gene3UTRStart != other.gene3UTRStart) {
            return false;
        }
        if (this.gene3UTREnd != other.gene3UTREnd) {
            return false;
        }
        if (Double.doubleToLongBits(this.freeEnergy) != Double.doubleToLongBits(other.freeEnergy)) {
            return false;
        }
        if (this.bindsInSeed != other.bindsInSeed) {
            return false;
        }
        if (this.seedRegionStart != other.seedRegionStart) {
            return false;
        }
        if (this.seedRegionEnd != other.seedRegionEnd) {
            return false;
        }
        if (this.WCpairsInSeed != other.WCpairsInSeed) {
            return false;
        }
        if (this.WobblePairsInSeed != other.WobblePairsInSeed) {
            return false;
        }
        if (this.totalWCPairs != other.totalWCPairs) {
            return false;
        }
        if (this.totalWobblePairs != other.totalWobblePairs) {
            return false;
        }
        if (this.isCanonical != other.isCanonical) {
            return false;
        }
        if (Double.doubleToLongBits(this.prediction) != Double.doubleToLongBits(other.prediction)) {
            return false;
        }
        if (!Objects.equals(this.siteTranscript, other.siteTranscript)) {
            return false;
        }
        if (!Objects.equals(this.matureMirnaTranscript, other.matureMirnaTranscript)) {
            return false;
        }
        if (!Objects.equals(this.full3UTRTranscript, other.full3UTRTranscript)) {
            return false;
        }
        if (!Objects.equals(this.comment, other.comment)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String summary = "";

        int can = 1;
        if (!this.isCanonical) {
            can = 0;
        }

        summary = summary + "3UTR Start: " + this.gene3UTRStart + "\t3UTR End: " + this.gene3UTREnd;
        summary = summary + "\tSeed region Start: " + this.seedRegionStart + "\t Seed Region End: " + this.seedRegionEnd;
        summary = summary + "\tPairs: " + this.bindsInSeed + "\tWC: " + this.WCpairsInSeed + "\t Wob: " + this.WobblePairsInSeed;
        summary = summary + "\tFree Energy: " + this.freeEnergy + "\tCannonical:" + can + "\tComment: " + this.comment;
        summary = summary + "\tSeed start in site:" + this.seedRegionStartInSequence;

        return summary;
    }

    /**
     * Transforms the object into a string. Suitable for tab separated files.
     *
     * @return information of the CandidateMBS separated by '\t'
     */
    public String toStringVal() {
        String summary = "";
        int can = 1;
        if (!this.isCanonical) {
            can = 0;
        }
        summary = summary + "" + this.gene3UTRStart + "\t" + this.gene3UTREnd;
        summary = summary + "\t" + this.seedRegionStart + "\t" + this.seedRegionEnd;
        summary = summary + "\t" + this.bindsInSeed + "\t" + this.WCpairsInSeed + "\t" + this.WobblePairsInSeed;
        summary = summary + "\t" + this.freeEnergy + "\t" + can + "\t" + this.comment + "\t";
        for (String property : this.additionalProperties.keySet()) {
            summary = summary + property + "=" + additionalProperties.get(property) + ";";
        }

        return summary;
    }

    /**
     * Generates a header suitable for tab separated files.
     *
     * @return header of a tab separated file.
     */
    public String toStringHead() {
        return "3UTR Start\t3UTR End\tSeed region Start\tSeed Region End\tPairs\tWC\tWob\tFree Energy\tCannonical\tComment";
    }

    /**
     * Aligns a mRNA target site in order to have the seed region starting at
     * seedStartIndex. The method shifts the transcript in order to make sure
     * that the seed region of the miRNA is starting to pair at the position
     * defined bye seedStartIndex. In case the shifting requires adding new
     * nucleotides in the site (of size siteLength), they are obtained from the
     * 3'UTR mRNATranscript. Note that the miRNA seed region is pairing from 3'
     * to 5' so seedSTartIndex will be the last paired nucleotide of the target
     * site.
     *
     * @param mRNATranscript transcript of the mRNA 3'UTR
     * @param siteLength size that the transcript site must have (fixed size)
     * @param seedStartIndex Nucleotide index where the seed region starts its
     * binding.
     */
    public void alignAndExtend(String mRNATranscript, int siteLength, int seedStartIndex) {
        
       
        

        if (this.gene3UTREnd == 6848) {
            int k = 27;
        }

        int shift = seedStartIndex - this.seedRegionStartInSequence;
        //int tail = siteLength - this.siteTranscript.length() - shift;

        String tempSiteTranscript;

        int newStart = this.gene3UTRStart - shift;
        int newEnd = newStart + siteLength;
        int auxLStart = 0;
        int auxLEnd = 0;

        if (newStart < 0) {
            auxLStart = -newStart;
            newStart = 0;
        }

        if (newEnd > mRNATranscript.length()) {
            auxLEnd = newEnd - mRNATranscript.length();
            newEnd = mRNATranscript.length();
        }
        String startaux = "";
        String endaux = "";
        for (int i = 0; i < auxLStart; i++) {
            startaux = startaux + "L";
        }
        for (int i = 0; i < auxLEnd; i++) {
            endaux = endaux + "L";
        }
        
        
        tempSiteTranscript = mRNATranscript.substring(newStart, newEnd);

        this.gene3UTRStart = newStart;
        this.gene3UTREnd = newEnd;
        this.seedRegionStartInSequence = this.seedRegionStartInSequence + shift;

        this.setSiteTranscript(startaux + tempSiteTranscript + endaux);

        

        //compute new free energy
        //DnaFoldPrediction pred = RNAFolder.getBestFoldWithSeedBinding(this.getSiteTranscript(), this.getMatureMirnaTranscript(), this.siteTranscript.length()-this.seedRegionStartInSequence);        
        //this.setFreeEnergy(pred.deltaG());
        //this.setStructure(pred.structure());
    }

    /**
     * Aligns a mRNA target site in order to have the seed region starting at
     * seedStartIndex. The method shifts the transcript in order to make sure
     * that the seed region of the miRNA is starting to pair at the position
     * defined bye seedStartIndex. In case the shifting requires adding new
     * nucleotides in the site (of size siteLength), they are obtained from the
     * chromosome region. Note that the miRNA seed region is pairing from 3' to
     * 5' so seedSTartIndex will be the last paired nucleotide of the target
     * site.
     *
     * @param siteLength size that the transcript site must have (fixed size)
     * @param seedStartIndex Nucleotide index where the seed region starts its
     * binding.
     */
    public void alignAndExtendUsingChromosomeInfo(int siteLength, int seedStartIndex) {
        int shift = seedStartIndex - this.seedRegionStartInSequence;
        //int tail = siteLength - this.siteTranscript.length() - shift - 1;

        String tempSiteTranscript;

        if (this.strand != '-') {
            try {
                int newStart = this.chrRegionStart - shift;
                int newEnd = newStart + siteLength-1; //we remove the last nucleotide as EnsemblAccess returns the sequence contained between nucleotides A, and B (both A and B included) 

                if (newStart > this.chrRegionStart && newStart < this.chrRegionEnd
                        && newEnd > this.chrRegionStart && newEnd < this.chrRegionEnd) {    //check if the new transcript is contained in the old one
                    //this avoids having to access online resource.
                    tempSiteTranscript = this.siteTranscript.substring((newStart - this.chrRegionStart), (newEnd - this.chrRegionStart));
                } else {
                    int auxLStart = 0;
                    if (newStart < 0) {                 //if the new stard is 'before' the chromosome start, we fill the space with Ls (not a nucleotide) and mark the new start as 0
                        auxLStart = -newStart;
                        newStart = 0;
                    }
                    String startaux = "";
                    for (int i = 0; i < auxLStart; i++) {
                        startaux = startaux + "L";
                    }

                    tempSiteTranscript = startaux + EnsemblAccess.getChromosomeTrasncript("human", chromosome, newStart, newEnd, this.strand, "GRCh37");
                }
                this.chrRegionStart = newStart;
                this.chrRegionEnd = newEnd;
                this.seedRegionStartInSequence = this.seedRegionStartInSequence + shift;

                this.setSiteTranscript(tempSiteTranscript);
            } catch (IOException ex) {
                if (chromosome.equalsIgnoreCase("chrM") || chromosome.equalsIgnoreCase("M")) {
                    log.error("Error, trying to access chrM in  ENSEMBL API. ChrM is not accessible from EMBL.\n" + ex.getMessage());
                } else {
                    log.error("Could not access ENSEMBL API.\n" + ex.getMessage());
                }

            } catch (Exception ex) {
                log.error("Problem with the ENSEMBL API.\n" + ex.getMessage());
            }
        } else {

            try {
                int newEnd = this.chrRegionEnd + shift;
                int newStart = newEnd-siteLength +1;  //the extra nucleotides now need to be obtained from the 'lower side' of the - strand.

                if (newStart > this.chrRegionStart && newStart < this.chrRegionEnd
                        && newEnd > this.chrRegionStart && newEnd < this.chrRegionEnd) {   //check if the new transcript is contained in the old one
                    //this avoids having to access online resource.
                    tempSiteTranscript = this.siteTranscript.substring((newStart - this.chrRegionStart), (newEnd - this.chrRegionStart));
                } else {
                    if (newStart < 0) {
                        newStart = 0;       //if the new starT is 'before' the chromosome start, we mark the new start as 0. In this case, there is no need to add 'empty nucleotides' as it will be reversed.
                    }

                    tempSiteTranscript = EnsemblAccess.getChromosomeTrasncript("human", chromosome, newStart, newEnd, this.strand, "GRCh37");
                    tempSiteTranscript = new StringBuilder(tempSiteTranscript).reverse().toString();    //As the transcript is retrieved from the - strand, it goes 3'->5'. THus, needs to be reversed.
                }
                this.chrRegionStart = newStart;
                this.chrRegionEnd = newEnd;
                this.seedRegionStartInSequence = this.seedRegionStartInSequence + (-shift);

                this.setSiteTranscript(tempSiteTranscript);
            } catch (IOException ex) {
                log.error("Could not access ENSEMBL API.\n" + ex.getMessage());
            } catch (Exception ex) {
                log.error("Problem with the ENSEMBL API.\n" + ex.getMessage());
            }
        }

    }

    public void alignAndFillWithL(int siteLength, int seedStartIndex) {
        int shift = seedStartIndex - this.seedRegionStartInSequence;
        int tail = siteLength - this.siteTranscript.length() - shift - 1;
    }

    public static void main(String[] args) {
        String mirna = "UAAAGUGCUUAUAGUGCAGGUAG";
        String seqB = "ATGTTTGATTTTATsCACTTTG";

        CandidateMBS cs = new CandidateMBS();
        cs.setMatureMirnaTranscript(mirna);
        cs.setFreeEnergy(-9.30);
        cs.setBindsInSeed(6);       //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        cs.setWCpairsInSeed(6);
        cs.setWobblePairsInSeed(0);
        cs.setSeedRegionStart(1);
        cs.setSeedRegionEnd(6);
        cs.setComment("TS:Cannonical");
        cs.setIsCanonical(true);
        cs.setSeedRegionStartInSequence(14);
        cs.setGene3UTREnd(39);
        cs.setGene3UTRStart(17);
        cs.setSiteTranscript(seqB);

        String full3UTR = "abcdefghijklmnopqATGTTTGATTTTATsCACTTTGabcdefghijklmnopq";
        System.out.println(full3UTR.substring(cs.getGene3UTRStart(), cs.getGene3UTREnd()));

        cs.setFull3UTRTranscript(full3UTR);

        System.out.println(cs.siteTranscript);
        System.out.println(cs.gene3UTRStart);
        System.out.println(cs.gene3UTREnd);
        System.out.println(cs.seedRegionStartInSequence);
        System.out.println("#########################");
        System.out.println("#########ALIGN###########");
        System.out.println("#########################");
        cs.alignAndExtend(full3UTR, 35, 25);
        System.out.println(cs.siteTranscript);
        System.out.println(cs.gene3UTRStart);
        System.out.println(cs.gene3UTREnd);
        System.out.println(cs.seedRegionStartInSequence);
        System.out.println(cs);

        String seqB2 = "ATGTTTGATTTTATGCACsTTG";
        String full3UTR2 = "abcdefghijklmnopqATGTTTGATTTTATGCACsTTGabcdefghijklmnopq";

        CandidateMBS cs2 = new CandidateMBS();
        cs2.setSeedRegionStartInSequence(18);
        cs2.setGene3UTREnd(39);
        cs2.setGene3UTRStart(17);
        cs2.setSiteTranscript(seqB2);
        cs2.alignAndExtend(full3UTR2, 35, 25);

        String seqB3 = "ATGTTTGATTTTATGCACsTTG";
        String full3UTR3 = "ATGTTTGATTTTATGCACsTTGabcdefghijklmnopq";

        CandidateMBS cs3 = new CandidateMBS();
        cs3.setSeedRegionStartInSequence(18);
        cs3.setGene3UTREnd(22);
        cs3.setGene3UTRStart(0);
        cs3.setSiteTranscript(seqB3);
        cs3.alignAndExtend(full3UTR3, 35, 25);

        String seqB4 = "ATGTTTGATTTTATGCACsTTG";
        String full3UTR4 = "ATGTTTGATTTTATGCACsTTG";

        CandidateMBS cs4 = new CandidateMBS();
        cs4.setSeedRegionStartInSequence(18);
        cs4.setGene3UTREnd(22);
        cs4.setGene3UTRStart(0);
        cs4.setSiteTranscript(seqB4);
        cs4.alignAndExtend(full3UTR4, 35, 25);

        System.out.println(cs.siteTranscript);
        System.out.println(cs2.siteTranscript);
        System.out.println(cs3.siteTranscript);
        System.out.println(cs4.siteTranscript);

    }

    public CandidateMBS(CandidateMBS another) {
        this.siteTranscript = another.siteTranscript;
        this.matureMirnaTranscript = another.matureMirnaTranscript;
        this.gene3UTRStart = another.gene3UTRStart;
        this.gene3UTREnd = another.gene3UTREnd;
        this.freeEnergy = another.freeEnergy;
        this.bindsInSeed = another.bindsInSeed;
        this.seedRegionStart = another.seedRegionStart;
        this.seedRegionEnd = another.seedRegionEnd;
        this.WCpairsInSeed = another.WCpairsInSeed;
        this.WobblePairsInSeed = another.WobblePairsInSeed;
        this.totalWCPairs = another.totalWCPairs;
        this.totalWobblePairs = another.totalWobblePairs;
        this.isCanonical = another.isCanonical;
        this.full3UTRTranscript = another.full3UTRTranscript;
        this.comment = another.comment;
        this.prediction = another.prediction;
        this.seedRegionStartInSequence = another.seedRegionStartInSequence;
        this.chromosome = another.chromosome;
        this.chrRegionStart = another.chrRegionStart;
        this.chrRegionEnd = another.chrRegionEnd;
        this.additionalProperties = new HashMap<>();
        this.structure = another.getStructure();
        this.filtered = another.filtered;
        this.filteredReason = another.filteredReason;
        for (String property : another.additionalProperties.keySet()) {
            Object val = another.additionalProperties.get(property);
            this.additionalProperties.put(property, val);
        }
    }

}
