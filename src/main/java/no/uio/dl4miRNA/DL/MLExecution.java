/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DL;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class controls the logic underneath the cross validation process used to 
 * train and evaluate miRAW's neural networks.
 * @author apla
 */
public class MLExecution {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MLExecution.class);
    
    /**
     * Performs a cross validation using the parameters defined in the configuration
     * file without activating any progress listener.
     * @param configurationFile location of the configuration file
     */
    public static void crossValidate(String configurationFile){
        crossValidate(configurationFile, false);
    }
    
    /**
     * Performs a cross validation using the parameters defined in the configuration
     * file. Optionally activates a web listener to show how the network is evolving.
     * @param configurationFile location of the configuration file
     * @param webListener boolean defining if web listeners should be activated or not.
     */
    public static void crossValidate(String configurationFile, boolean webListener) {

        try {
            //Loading configuration file properties
            Properties prop = new Properties();
            FileInputStream configInput = new FileInputStream(configurationFile);

            prop.load(configInput);

            String expName = prop.getProperty("ExperimentName");
            String experimentFolder = prop.getProperty("ExperimentFolder") + "/" + expName;
            String dataFolder = prop.getProperty("CrossValidation.Folder") + "/binary";
            String CVType = prop.getProperty("CrossValidation.Type");
            String setPrefix = "set_";
            

            //TODO: add site size / alignement parameters (number of inputs in the network).
            int maxSiteLength = 30;
            int seedAlignementOffset = 10;

            if (prop.getProperty("MaxSiteLength") != null) {
                maxSiteLength = Integer.parseInt(prop.getProperty("MaxSiteLength"));
            }
            if (prop.getProperty("SeedAlignementOffset") != null) {
                seedAlignementOffset = Integer.parseInt(prop.getProperty("SeedAlignementOffset"));
            }
            
            MLModelLearner networkLearner = new MLModelLearner();

            
            networkLearner.setMiRNAInputs(4*30);
            networkLearner.setSeqInputs(maxSiteLength*4);
            
            int inputNodes = networkLearner.getNumInputs();
            int outputNodes = networkLearner.getNumOutputs();

            float dropout = 0;
            float learningRate = (float) 0.005;
            int maxEpochs = 1500;
            int epochsNoChange = 500;
            int folds=10;

            try {
                dropout = Float.parseFloat(prop.getProperty("ANN.Dropout"));
            } catch (NumberFormatException|NullPointerException e) {
            }
            try {
                learningRate = Float.parseFloat(prop.getProperty("ANN.LearningRate"));
            } catch (NumberFormatException|NullPointerException e) {
            }
            try {
                maxEpochs = Integer.parseInt(prop.getProperty("ANN.MaxEpochs"));
            } catch (NumberFormatException|NullPointerException e) {
            }
            try {
                epochsNoChange = Integer.parseInt(prop.getProperty("ANN.EpochsNoChange"));
            } catch (NumberFormatException|NullPointerException e) {
            }
            try {
                folds = Integer.parseInt(prop.getProperty("CrossValidation.folds"));
            } catch (NumberFormatException|NullPointerException e) {
            }

            int layerNum = 1;
            ArrayList<Integer> nodesPerLayer = new ArrayList<>();
            nodesPerLayer.add(inputNodes);
            while (prop.getProperty("ANN.NodesLayer" + layerNum) != null) {
                nodesPerLayer.add(Integer.parseInt(prop.getProperty("ANN.NodesLayer" + layerNum)));
                layerNum++;
            }
            nodesPerLayer.add(outputNodes);
            int[] networkShape = new int[nodesPerLayer.size()];
            for (int i = 0; i < nodesPerLayer.size(); i++) {
                networkShape[i] = nodesPerLayer.get(i);
            }
            
            networkLearner.setDropout(dropout);
            networkLearner.setNetworkShape(networkShape);
            networkLearner.setnEpochs(maxEpochs);
            networkLearner.setEpochsWithoutChange(epochsNoChange);
            networkLearner.setLearningRate(learningRate);
            networkLearner.setnSets(folds);
            
            if (prop.getProperty("ANN.LossFunction")!=null){
                networkLearner.setLossFunction(prop.getProperty("ANN.LossFunction"));
            }
                      
            
            if (CVType==null){
                log.warn("Cross Validation Type not defined! ASSUMING: traintest");
                CVType="traintest";
            }
            switch (CVType) {

                case "testtrainval":
                case "testvaltrain":
                case "trainvaltest":
                case "traintestval":
                case "valtraintest":
                case "valtesttrain":
                    networkLearner.crossValidateTVT(expName, experimentFolder, dataFolder, setPrefix, webListener);
                    break;
                case "traintest":
                case "testtrain":
                default:
                    networkLearner.crossValidate(expName, experimentFolder, dataFolder, setPrefix, webListener);
                    break;
            }

        } catch (IOException ex) {
            Logger.getLogger(MLExecution.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
