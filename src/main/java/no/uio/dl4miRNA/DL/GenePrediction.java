/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DL;

//import Utils.SeqUtils;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import no.uio.dl4miRNA.SiteFinder.*;
import no.uio.dl4miRNA.SiteFinder.SiteFilters.AccessibilityEnergyFilter;
import no.uio.dl4miRNA.SiteFinder.SiteFilters.CandidateSiteFilter;
import no.uio.dl4miRNA.SiteFinder.SiteFilters.SpecificZonesFilter;
//import org.deeplearning4j.eval.Evaluation;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
//import org.nd4j.linalg.api.ndarray.INDArray;
//import org.nd4j.linalg.dataset.DataSet;

//import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.LoggerFactory;

/**
 * Class that predicts if a Gene is a target of a set of miRNAs. To do so, the
 * gene is scrolled using a CandidateSiteFinder object that returns an array of
 * potential MBSs. The MBSs are, then, analyzed using a CandidatePrediction
 * object.
 *
 * @author apla
 */
public class GenePrediction {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GenePrediction.class);

    protected String geneName;              //name of the gene
    protected String gene3UTRTranscript;    //transcript of the gene 3UTR
    protected String geneEnsemblId;         //ID of the gene in ensembl

    protected HashMap<String, String> matureMiRNATranscripts;       //trasncripts of the miRNAs to be tested
    private HashMap<String, Float> miRNAInteractions;               //maximum score obtained by each miRNA in the gene. 
    //The order MUST be the same that in matureMiRNATranscripts.            
    protected HashMap<String, ArrayList<CandidateMBS>> candidateMirnaSites;    //Array list containging the MBS corresponding to the different miRNA-gene pairs.
    //The order MUST be the same that in matureMiRNATranscripts.            

    private MultiLayerNetwork model;        //Deep neural network used to do the prediction

    private HashMap<String, Integer> numberOfPosSites;      //number of MBS classified as positive in each miRNA-gene pair
    private HashMap<String, Integer> numberOfNegSites;      //number of MBS classified as negative each miRNA-gene pair
    private HashMap<String, Integer> numberOfFilterRemovedSites;      //number of MBS classified as negative each miRNA-gene pair
    private HashMap<String, Float> maxVal;                  //maximum score obtained by each miRNA-gene pair
    private HashMap<String, Float> minVal;                  //minimum score obtained by each miRNA-gene pair

    private HashMap<String, Float> prediction;              //prediction corresponding to each miRNA-gene pair. 
    //The prediction corresponds to the maximum score obtained by the miRNA-gene pair.
    //A single positive MBS is enough to consider the gene is a target.

    private CandidateSiteFinder siteFinder;                 //Candidate Site finder object use to determine potential MBSs.
    //add site size / alignement parameters come with the CSF

    protected List<CandidateSiteFilter> aPosterioriFilters;

    /*###### Constructors getters and setters######*/
    /**
     * empty constructor
     */
    public GenePrediction() {
        this.matureMiRNATranscripts = new HashMap<>();
        this.miRNAInteractions = new HashMap<>();
        candidateMirnaSites = new HashMap<>();

        prediction = new HashMap<>();
        numberOfPosSites = new HashMap<>();
        numberOfNegSites = new HashMap<>();
        numberOfFilterRemovedSites = new HashMap<>();
        maxVal = new HashMap<>();
        minVal = new HashMap<>();
        siteFinder = new DefaultCandidateSiteFinder();

        aPosterioriFilters = new ArrayList<>();
    }

    /**
     * Constructor which includes the gene name and the Neural network model
     *
     * @param geneName Name of the gene
     * @param model Trained neural network to perform predictions.
     */
    public GenePrediction(String geneName, MultiLayerNetwork model) {
        this();
        this.model = model;
        this.geneName = geneName;
    }

    /**
     * Constructor which includes the gene name, the transcript, and the Neural
     * network model
     *
     * @param geneName Name of the gene
     * @param gene3UTRTranscript 3'UTR transcript of the gene
     * @param model Trained neural network model
     */
    public GenePrediction(String geneName, String gene3UTRTranscript, MultiLayerNetwork model) {
        this();
        this.model = model;
        this.gene3UTRTranscript = gene3UTRTranscript;
        this.geneName = geneName;
    }

    /**
     * Constructor which includes the gene name and the transcript
     *
     * @param geneName Name of the gene
     * @param gene3UTRTranscript 3'UTR transcript of the gene
     */
    public GenePrediction(String geneName, String gene3UTRTranscript) {
        this();
        this.geneName = geneName;
        this.gene3UTRTranscript = gene3UTRTranscript;
    }

    /*###### Getters, Setters & Adders ######*/
    /**
     * Set the candidate site finder to search for potential MBSs.
     *
     * @param siteFinder CandidateSiteFinder object.
     */
    public void setCandidateSiteFinder(CandidateSiteFinder siteFinder) {
        this.siteFinder = siteFinder;
    }

    /**
     * Creates a candidate site finder to search for potential MBSs.
     *
     * @param siteFinder name of the type of candidate site finder object.
     */
    public void setCandidateSiteFinder(String siteFinder, int maxSitelength, int seedAlignementOffset) {
        this.siteFinder = CandidateSiteFinderFactory.getSiteFinder(siteFinder);
        this.siteFinder.setMaxSiteLength(maxSitelength);
        this.siteFinder.setSeedAlignementOffset(seedAlignementOffset);
    }

    /**
     * Getter of the neural network
     *
     * @return neural network used for classification
     */
    public MultiLayerNetwork getModel() {
        return model.clone();
    }

    /**
     * Set the trained neural network used for classification
     *
     * @param model trained neural network
     */
    public void setModel(MultiLayerNetwork model) {
        this.model = model;
    }

    /**
     * Obtains the string defining 3'UTR transcript
     *
     * @return 3'UTR transcript
     */
    public String getGene3UTRTranscript() {
        return gene3UTRTranscript;
    }

    /**
     * Defines the string with the 3'UTR transcript
     *
     * @param gene3UTRTranscript 3'UTR transcript
     */
    public void setGene3UTRTranscript(String gene3UTRTranscript) {
        this.gene3UTRTranscript = gene3UTRTranscript;
    }

    /**
     * Obtain gene name
     *
     * @return gene name.
     */
    public String getGeneName() {
        return geneName;
    }

    /**
     * Define gene Name
     *
     * @param geneName
     */
    public void setGeneName(String geneName) {
        this.geneName = geneName;
    }

    /**
     * Gets the miRNA transcripts. The HashMap key is the name of the miRNTA,
     * the content is its transcript.
     *
     * @return HashMap with miRNA transcripts
     */
    public HashMap<String, String> getMiRNATranscripts() {
        return (HashMap<String, String>) matureMiRNATranscripts.clone();
    }

    /**
     * Sets the miRNA transcripts. The HashMap key is the name of the miRNTA,
     * the content is its transcript.
     *
     * @param miRNATranscripts HashMap with miRNA transcripts
     */
    public void setMiRNATranscripts(HashMap<String, String> miRNATranscripts) {
        this.matureMiRNATranscripts = miRNATranscripts;
    }

    /**
     * Gets the predicted interactions. The key of the HashMap is a the miRNA
     * name, and the value a float [-1,1].
     *
     * @return HashMap with predicted interactions
     */
    public HashMap<String, Float> getMiRNAInteractions() {
        return (HashMap<String, Float>) miRNAInteractions.clone();
    }

    /**
     * Set the gene Name and its 3'UTR transcript
     *
     * @param geneName gene name
     * @param gene3UTRTranscript transcript of the gene 3'UTR
     */
    public void setGene(String geneName, String gene3UTRTranscript) {
        this.setGeneName(geneName);
        this.setGene3UTRTranscript(gene3UTRTranscript);
    }

    /**
     * Deletes the save mature mirna transcripts resulting in an empty
     * matureMirnaTranscripts HashMap.
     */
    public void clearMiRNAs() {
        this.matureMiRNATranscripts = new HashMap<>();
    }

    /**
     * Adds a mirna transcript to the matureMirnaTrasncripts HashMap.
     *
     * @param miRNAName name of the miRNA (key)
     * @param miRNATranscript transcript of the mature mirna (value)
     */
    public void addMiRNA(String miRNAName, String miRNATranscript) {
        this.matureMiRNATranscripts.put(miRNAName, miRNATranscript);
    }

    /**
     * Returns the gene id corresponding to the Ensembl format.
     *
     * @return Ensembl gene id.
     */
    public String getGeneEnsemblId() {
        return geneEnsemblId;
    }

    /**
     * Sets the gene id corresponding to the Ensembl format.
     *
     * @param geneEnsemblId Ensembl gene id.
     */
    public void setGeneEnsemblId(String geneEnsemblId) {
        this.geneEnsemblId = geneEnsemblId;
    }

    /**
     * Erases all the data corresponding to previously performed predictions.
     */
    public void clearPredictions() {
        prediction = new HashMap<>();
        numberOfPosSites = new HashMap<>();
        numberOfNegSites = new HashMap<>();
        numberOfFilterRemovedSites = new HashMap<>();

        maxVal = new HashMap<>();
        minVal = new HashMap<>();
    }

    /**
     * For each miRNA, returns the potential mirna binding sites identified by
     * the candidate Site Finder Object.
     *
     * @return HashMap where the key is the name of the mirna, and the value an
     * ArrayList<> of potential MBSs.
     */
    public HashMap<String, ArrayList<CandidateMBS>> getCandidateMirnaSites() {
        return candidateMirnaSites;
    }

    /**
     * Returns a Map with the predictions done for each miRNA
     *
     * @return HashMap where the mirna name is the key, and the predicted value
     * the content.
     */
    public HashMap<String, Float> getPrediction() {
        return prediction;
    }

    /**
     * Returns a Map with the number of positive MBS predicted for each miRNA
     *
     * @return HashMap where the mirna name is the key, and the predicted
     * positive the content.
     */
    public HashMap<String, Integer> getNumberOfPosSites() {
        return numberOfPosSites;
    }

    /**
     * Returns a Map with the number of predicted negative sites for each mirna.
     *
     * @return HashMap where the mirna name is the key, and the number of
     * negative sites the content.
     */
    public HashMap<String, Integer> getNumborOfNegSites() {
        return numberOfNegSites;
    }

    public HashMap<String, Integer> getNumberOfFilterRemovedSites() {
        return this.numberOfFilterRemovedSites;
    }

    /**
     * Returns a Map with the maximum predicted value for each miRNA
     *
     * @return HashMap where the mirna name is the key, and the maximum
     * predicted value the content.
     */
    public HashMap<String, Float> getMaxVal() {
        return maxVal;
    }

    /**
     * Returns a Map with the minimum predicted for each miRNA
     *
     * @return HashMap where the mirna name is the key, and th minimum predicted
     * value the content.
     */
    public HashMap<String, Float> getMinVal() {
        return minVal;
    }

    /*##### Methods #####*/
    /**
     * Loads a saved trained neural network from a file. The network must have
     * been saved using the ModelSerializer class from DL4J and must have 240
     * inputs and 2 outputs. The order of the inputs must be consistent with the
     * one used in miRAW.
     *
     * @param modelLocation String with the path to the model
     * @throws IOException
     */
    public void loadLearnedModel(String modelLocation) throws IOException {
        if (modelLocation.endsWith(".bin")) {
            DataInputStream dis = new DataInputStream(new FileInputStream(modelLocation));
            model = ModelSerializer.restoreMultiLayerNetwork(dis);
            dis.close();
        } else {
            log.error("Wrong model file format: " + modelLocation + " should be a DL4J bin file.");
        }
    }

    /**
     * Loads a saved trained neural network object. The network must have 240
     * inputs and 2 outputs. The order of the inputs must be consistent with the
     * one used in miRAW.
     *
     * @param model trained neural network object
     * @throws IOException
     */
    public void loadLearnedModel(MultiLayerNetwork model) throws IOException {
        this.setModel(model);

    }

    /**
     * Predicts if the different miRNA target the gene.
     */
    public void doPrediction() {
        long predStart = System.currentTimeMillis();
        //clear the mirna Interactions table
        this.miRNAInteractions = new HashMap<>();

        //for each miRNA to be tested
        this.matureMiRNATranscripts.keySet().stream()
                .sequential()
                //.parallel()
                .forEach((miRNAKey) -> {
                    /*
                    //create the candidate predictor
                    CandidatePrediction candPredictor = new CandidatePrediction(); //It is created inside the loop to avoid concurrent modification
                    candPredictor.setModel(model);
                    //Set the mature mirna transcript into the candidate site predictor
                    candPredictor.setMatureMirna(matureMiRNATranscripts.get(miRNAKey));*/

                    //initialize candidateMirnaSites
                    candidateMirnaSites.put(miRNAKey, new ArrayList<>());

                    //find the candidate Mirna Binding Sites
                    long before = System.currentTimeMillis();
                    List<CandidateMBS> candidateSites = findCandidatesByMiRNA(miRNAKey);
                    long ellapsed = System.currentTimeMillis() - before;
                    log.debug(candidateSites.size() + " canidate sites obtained using " + siteFinder.getClass() + " Evaluation time = " + ellapsed);

                    //for each candidate Site
                    ArrayList<Float> siteEvaluation = new ArrayList<>();                //Array containing all the MBSs predictions for the mirna. The miRNA-Gene prediction will be teh maximum value of this array.
                    ArrayList<Boolean> removedList = new ArrayList<>();
                    int positives = 0;
                    int negatives = 0;
                    int filterRemoved = 0;
                    candidateSites.stream()         
                            .sequential() // slighlty more efficiency when not using filters
                            //.parallel()     // Significantly more efficient when using filters
                            .forEach((candidateSite) -> {
                                try {

                                    //create the candidate predictor
                                    CandidatePrediction candPredictor = new CandidatePrediction(); //It is created inside the loop to avoid concurrent modification
                                    candPredictor.setModel(model);
                                    //Set the mature mirna transcript into the candidate site predictor
                                    candPredictor.setMatureMirna(matureMiRNATranscripts.get(miRNAKey));

                                    candPredictor.setMrnaMBS(candidateSite.getSiteTranscript());    //set the transcript of the MBS to the ml predicto
                                    log.debug(candidateSite.toString());
                                    candPredictor.setSiteLength(siteFinder.getMaxSiteLength());
                                    float predictionCS = candPredictor.doPrediction();                //Obtain prediction for the MBS
                                    candidateSite.setPrediction(predictionCS);

                                    //Apply a posteriory filters for the evaluated candidate site   
                                    boolean removed = false;
                                    if (predictionCS > 0) {
                                        if (candidateSite.getFull3UTRTranscript() == null) {
                                            candidateSite.setFull3UTRTranscript(gene3UTRTranscript);
                                        }
                                        CandidateMBS candidateSiteAux = this.applyPosterioriIndividualFiltering(candidateSite);
                                        if (candidateSiteAux == null) {
                                            removed = true;
                                            predictionCS = 0;
                                            //candidateSite.setPrediction(predictionCS);
                                        };
                                    }
                                    removedList.add(removed);
                                    siteEvaluation.add(predictionCS);

                                    this.candidateMirnaSites.get(miRNAKey).add(candidateSite);
                                } catch (Exception e) {
                                    //TODO  Strange Error is produced here some times. null pointer. Is it related to parallelization?
                                    log.error("Strang error A: " + e.getClass() + ": " + e.getCause());
                                    e.printStackTrace();
                                }
                            });

                    for (Float se : siteEvaluation) {
                        try {
                            if (se > 0) {
                                positives++;
                            } else {
                                negatives++;
                            }
                        } catch (Exception e) {                                             //TODO  Strange Error is produced here some times. null pointer. Is it related to parallelization?
                            log.error("Strang error B: " + e.getClass() + ": " + e.getCause());
                        }
                    }
                    for (boolean r : removedList) {
                        try {
                            if (r) {
                                filterRemoved++;
                            }
                        } catch (Exception e) {                                             //TODO  Strange Error is produced here some times. null pointer. Is it related to parallelization?
                            log.error("Strang error C: " + e.getClass() + ": " + e.getCause());
                        }
                    }

                    //Save the result of the highest prediction.
                    log.debug("providePrediction");
                    String headTag = miRNAKey + ":" + this.geneName + "\t";
                    if (!siteEvaluation.isEmpty()) {    //the gene has at least one MBS.
                        try {
                            this.miRNAInteractions.put(miRNAKey, Collections.max(siteEvaluation));
                            log.debug(headTag + "Maximum prediction:" + Collections.max(siteEvaluation) + " / Minimum prediction:" + Collections.min(siteEvaluation));
                            if (filterRemoved>0){
                                log.debug("REMOVED ELEMENTS");
                            }                            
                            log.debug(headTag + filterRemoved + " of the original " + (positives + filterRemoved) + " positive sites have been filtered.");
                            log.debug(headTag + "Final prediction: " + positives + " sites identified as positive, " + negatives + " as negative");

                            this.prediction.put(miRNAKey, Collections.max(siteEvaluation));
                            this.maxVal.put(miRNAKey, Collections.max(siteEvaluation));
                            this.numberOfNegSites.put(miRNAKey, negatives);
                            this.numberOfPosSites.put(miRNAKey, positives);
                            this.numberOfFilterRemovedSites.put(miRNAKey, filterRemoved);
                            this.minVal.put(miRNAKey, Collections.min(siteEvaluation));
                        } catch (Exception e) {
                            //TODO strange null pointer error               has to do with parallelization?
                            log.debug(headTag + "Maximum prediction: 0 -- STRANGE NP ERROR");
                            //log.debug(positives + " sites identified as positive, " + negatives + " as negatie");

                            this.prediction.put(miRNAKey, (float) 0);
                            this.maxVal.put(miRNAKey, (float) 0);
                            this.numberOfNegSites.put(miRNAKey, 0);
                            this.numberOfPosSites.put(miRNAKey, 0);
                            this.numberOfFilterRemovedSites.put(miRNAKey, 0);
                            this.minVal.put(miRNAKey, (float) 0);
                        }
                    } else {    //The gene does not have MBSs for this particular miRNA.
                        log.debug(headTag + "Maximum prediction: 0 (no candidates found) /t Minimum prediction: 0 (no candidates found)");
                        log.debug(headTag + positives + " sites identified as positive, " + negatives + " as negative" + filterRemoved + " removed in a posteriori filtering");

                        this.prediction.put(miRNAKey, (float) 0);
                        this.maxVal.put(miRNAKey, (float) 0);
                        this.numberOfNegSites.put(miRNAKey, 0);
                        this.numberOfPosSites.put(miRNAKey, 0);
                        this.numberOfFilterRemovedSites.put(miRNAKey, 0);
                        this.minVal.put(miRNAKey, (float) 0);
                    }
                });

        /* NOT PARALLELized version
        //for each miRNA to be tested
        for (String miRNAKey : this.matureMiRNATranscripts.keySet()) {          //TODO: Think about paralelization.
        //initialize candidateMirnaSites
        candidateMirnaSites.put(miRNAKey, new ArrayList<>());
        //Set the mature mirna transcript into the candidate site predictor
        candPredictor.setMatureMirna(matureMiRNATranscripts.get(miRNAKey));
        //find the candidate Mirna Binding Sites
        List<CandidateMBS> candidateSites = findCandidatesByMiRNA(miRNAKey);
        log.debug(candidateSites.size() + " canidate sites obtained using " + siteFinder.getClass());
        //for each candidate Site
        ArrayList<Float> siteEvaluation = new ArrayList<>();                //Array containing all the MBSs predictions for the mirna. The miRNA-Gene prediction will be teh maximum value of this array.
        int positives = 0;
        int negatives = 0;
        candidateSites.parallelStream().forEachOrdered((candidateSite) -> {
        candPredictor.setMrnaMBS(candidateSite.getSiteTranscript());    //set the transcript of the MBS
        log.debug(candidateSite.toString());
        candPredictor.setSiteLength(siteFinder.getMaxSiteLength());
        float predictionCS = candPredictor.doPrediction();                //Obtain prediction for the MBS
        candidateSite.setPrediction(predictionCS);
        siteEvaluation.add(predictionCS);
        this.candidateMirnaSites.get(miRNAKey).add(candidateSite);
        });
        for (Float se : siteEvaluation) {
        if (se > 0) {
        positives++;
        } else {
        negatives++;
        }
        }
        //Save the result of the highest prediction.
        if (!siteEvaluation.isEmpty()) {    //the gene has at least one MBS.
        this.miRNAInteractions.put(miRNAKey, Collections.max(siteEvaluation));
        log.debug("Maximum prediction:" + Collections.max(siteEvaluation) + " / Minimum prediction:" + Collections.min(siteEvaluation));
        log.debug(positives + " sites identified as positive, " + negatives + " as negatie");
        this.prediction.put(miRNAKey, Collections.max(siteEvaluation));
        this.maxVal.put(miRNAKey, Collections.max(siteEvaluation));
        this.numberOfNegSites.put(miRNAKey, negatives);
        this.numberOfPosSites.put(miRNAKey, positives);
        this.minVal.put(miRNAKey, Collections.min(siteEvaluation));
        } else {    //The gene does not have MBSs for this particular miRNA.
        log.debug("Maximum prediction: 0 (no candidates found) /t Minimum prediction: 0 (no candidates found)");
        log.debug(positives + " sites identified as positive, " + negatives + " as negatie");
        this.prediction.put(miRNAKey, (float) 0);
        this.maxVal.put(miRNAKey, (float) 0);
        this.numberOfNegSites.put(miRNAKey, 0);
        this.numberOfPosSites.put(miRNAKey, 0);
        this.minVal.put(miRNAKey, (float) 0);
        }
        }
         */
        long predEnd = System.currentTimeMillis();
        log.debug("Prediction time = " + (predEnd - predStart) + " miliseconds");
    }

    /**
     * Searches the candidate MBSs for the the desired miRNA.
     *
     * @param miRNA name of the miRNa
     * @return ArrayList containing all the potential MBSs according to the
     * CandidateSiteFinder object.
     */
    protected ArrayList<CandidateMBS> findCandidatesByMiRNA(String miRNA) {
        if (!this.matureMiRNATranscripts.containsKey(miRNA)) {
            log.warn("There is not an available transcript for miRNA " + miRNA);
            return null;
        }
        return findCandidatesByMatureTranscript(this.matureMiRNATranscripts.get(miRNA));

    }

    /**
     * Searches the candidate MBSs for a specific trasncript of a mature miRNA.
     *
     * @param transcript name of the miRNa
     * @return ArrayList containing all the potential MBSs according to the
     * CandidateSiteFinder object.
     */
    protected ArrayList<CandidateMBS> findCandidatesByMatureTranscript(String transcript) {
        if (transcript.length() > 30) {
            log.warn("The lenght of the transcript (" + transcript.length() + ") is too long to be a mature miRNA");
            return null;
        }
        ArrayList<CandidateMBS> candidateSites = this.siteFinder.getCandidateSites(transcript, this.gene3UTRTranscript);
        return candidateSites;
    }

    /**
     * Saves the MBSs predicted as positive into an external TSV file.
     *
     * @param outputFile location of the output file
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected void savePositiveCandidateSitesToFile(String outputFile) throws FileNotFoundException, IOException {
        PrintWriter resultFileWriter;

        if (!new File(outputFile).exists()) {
            resultFileWriter = new PrintWriter(outputFile);
            resultFileWriter.println("GeneName\tmiRNA\tSiteStart\tSiteEnd\tPrediction\tPairsInSeed\tFreeEnergy\tSiteTranscript\tMatureMiRNATranscript\tAdditionalProperties");
        } else {
            FileWriter fw = new FileWriter(outputFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            resultFileWriter = new PrintWriter(bw);
        }

        for (String mirna : this.candidateMirnaSites.keySet()) {
            ArrayList<CandidateMBS> candidateSites = this.candidateMirnaSites.get(mirna);
            for (CandidateMBS cs : candidateSites) {
                if (cs.getPrediction() > 0 && !cs.isFiltered()) { //if it is predicted as true and it is not filtered
                    resultFileWriter.print(this.geneName + "\t");
                    resultFileWriter.print(mirna + "\t");
                    resultFileWriter.print(cs.getGene3UTRStart() + "\t");
                    resultFileWriter.print(cs.getGene3UTREnd() + "\t");
                    resultFileWriter.print(cs.getPrediction() + "\t");
                    resultFileWriter.print(cs.getBindsInSeed() + "\t");
                    resultFileWriter.print(cs.getFreeEnergy() + "\t");
                    resultFileWriter.print(cs.getSiteTranscript() + "\t");
                    resultFileWriter.print(cs.getMatureMirnaTranscript() + "\t");
                    for (String property : cs.getAdditionalProperties().keySet()) {
                        resultFileWriter.print(property + "=" + cs.getProperty(property) + ";");
                    }
                    resultFileWriter.println();
                    resultFileWriter.flush();
                }
            }
        }
        resultFileWriter.close();
    }

    /**
     * Saves the MBSs and their prediction into an external TSV file. Both the
     * positive and the negative predicted
     *
     * @param outputFile location of the output file
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected void saveCandidateSitesToFile(String outputFile, boolean append) throws FileNotFoundException, IOException {
        PrintWriter resultFileWriter;

        if (!append) {
            resultFileWriter = new PrintWriter(outputFile);
            resultFileWriter.println("GeneName\tmiRNA\tSiteStart\tSiteEnd\tPrediction\tFiltered\tPostfilterPrediction\tPairsInSeed\tFreeEnergy\tSiteTranscript\tMatureMiRNATranscript\tFiltering Reason\tCanonical\tAdditional properties");
        } else {
            FileWriter fw = new FileWriter(outputFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            resultFileWriter = new PrintWriter(bw);
        }
         //resultFileWriter = new PrintWriter(outputFile);
         //resultFileWriter.println("GeneName\tmiRNA\tSiteStart\tSiteEnd\tPrediction\tFiltered\tPostfilterPrediction\tPairsInSeed\tFreeEnergy\tSiteTranscript\tMatureMiRNATranscript\tFiltering Reason\tAdditional properties");

        for (String mirna : this.candidateMirnaSites.keySet()) {
            ArrayList<CandidateMBS> candidateSites = this.candidateMirnaSites.get(mirna);
            for (CandidateMBS cs : candidateSites) {
                resultFileWriter.print(this.geneName + "\t");
                resultFileWriter.print(mirna + "\t");
                resultFileWriter.print(cs.getGene3UTRStart() + "\t");
                resultFileWriter.print(cs.getGene3UTREnd() + "\t");
                resultFileWriter.print(cs.getPrediction() + "\t");
                if (cs.isFiltered()) {
                    resultFileWriter.print(1 + "\t");
                    resultFileWriter.print(0 + "\t");
                } else {
                    resultFileWriter.print(0 + "\t");
                    resultFileWriter.print(cs.getPrediction() + "\t");
                }
                resultFileWriter.print(cs.getBindsInSeed() + "\t");
                resultFileWriter.print(cs.getFreeEnergy() + "\t");
                resultFileWriter.print(cs.getSiteTranscript() + "\t");
                resultFileWriter.print(cs.getMatureMirnaTranscript() + "\t");
                resultFileWriter.print(cs.getFilteredReason() + "\t");
                
                if(cs.getIsCanonical()){resultFileWriter.print("1\t");}
                else{resultFileWriter.print("0\t");}
                
                for (String property : cs.getAdditionalProperties().keySet()) {
                    resultFileWriter.print(property + "=" + cs.getProperty(property) + ";");
                }
                resultFileWriter.println();
                resultFileWriter.flush();
            }
        }
        resultFileWriter.close();
    }

    /**
     * Saves the MBSs predicted as positive into an external TSV file. In order
     * to reduce the file weight, only the name of the gene, the name of the
     * mirna and the miRNA trasncript are saved.
     *
     * @param outputFile location of the output file
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected void savePositiveCandidateSitesToFileLight(String outputFile) throws FileNotFoundException, IOException {
        PrintWriter resultFileWriter;

        if (!new File(outputFile).exists()) {
            resultFileWriter = new PrintWriter(outputFile);
            resultFileWriter.println(new CandidateMBS().toStringHead());

        } else {
            FileWriter fw = new FileWriter(outputFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            resultFileWriter = new PrintWriter(bw);
        }

        for (String mirna : this.candidateMirnaSites.keySet()) {
            int i = 0;

            ArrayList<CandidateMBS> candidateSites = this.candidateMirnaSites.get(mirna);
            for (CandidateMBS cs : candidateSites) {

                if (cs.getPrediction() > 0 && !cs.isFiltered()) {

                    if (i == 0) {
                        resultFileWriter.print(this.geneName + "\t" + mirna);
                        resultFileWriter.println("\t" + cs.getMatureMirnaTranscript());
                        i++;
                    }
                    resultFileWriter.println(cs.toStringVal());
                    resultFileWriter.flush();
                }
            }
        }
        resultFileWriter.close();
    }

    protected CandidateMBS applyPosterioriIndividualFiltering(CandidateMBS candidateSite) {
        CandidateMBS auxCS = new CandidateMBS(candidateSite);
        for (CandidateSiteFilter csf : aPosterioriFilters) {
            auxCS = csf.evaluateSite(candidateSite);
            if (auxCS == null) {
                break;
            }
        }
        return auxCS;
    }

    protected List<CandidateMBS> applyPosterioriFiltering(ArrayList<CandidateMBS> candidateSites) {
        List<CandidateMBS> filteredSites = new ArrayList<>(candidateSites);

        for (CandidateSiteFilter csf : aPosterioriFilters) {
            filteredSites = csf.filter(filteredSites);
        }
        return filteredSites;
    }

    public void addAPosterioriFilter(CandidateSiteFilter csf) {
        if (this.aPosterioriFilters == null) {
            this.aPosterioriFilters = new ArrayList<>();
        }
        this.aPosterioriFilters.add(csf);
    }

    /**
     * Example of how to use Gene Prediction to predict miRNA-Gene interactions
     * given a miRNA and a mRNA transcript.
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {

        //Create Gene prediction object
        GenePrediction gp = new GenePrediction();

        CandidateSiteFilter csf = new AccessibilityEnergyFilter();
        gp.addAPosterioriFilter(csf);

        SpecificZonesFilter csf2 = new SpecificZonesFilter();
        //csf2.addSpecificSiteRestriction(0, 3000);
        csf2.addPercentileRestriction(0, 50);
        //gp.addAPosterioriFilter(csf2);

        //Load the DeepLearning model (previously trained).
        gp.loadLearnedModel("/home/albertpp/miRAW/eccb/Repeating/DNNModel/bestModel.bin");

        //Define the 'target gene'
        gp.setGeneEnsemblId("ENSG00000106351");
        gp.setGene("AGFG2", "CACTGTGTTTTTGGGGGGCCTCTTCCCTGCCTTCTGGGGCCCCTCTGCTCCCTAGAGCTC"
                + "TGGTGACCACTTGCCTGTGGGCATTTCTATGGGCCTTGGGGATGGTGGAGGTGCTAATGC"
                + "TTTGCTTGGGGCCTACAGGTGAAAGGTGGCTGCCCTCAGATTCCACAAAGCCTCTCTCCC"
                + "CTCCCTCGTCCCACCCCCACCCAGGCAGGAAGCCCAGGAGGAGTGCGGGCAGGGCCTGAC"
                + "CTGGAGGAGTGATGGTTGAGGGGGAGGGATTTTTTTCAAATGATCAGTCCCCTGTGGAAC"
                + "AGCTCTTCCCTGGGCCTAGCTCGCCAGCGCTGTGCTCCTCATGGACGGGAGCGCAGTGGG"
                + "GAAGGTAGGGGAAAGATGAGGCACAGTGTTGATGGGGCAGTGACCAGACAGTCCTGAGAC"
                + "CCAGAAGGCCCAGGTGGCCCAAGTGCTCTTGGTGGCCCAGCTTGGCCAGCACACACTTTC"
                + "CTCCTGCAGCACCCCCTGCTGAAGGGGTCCCCCATTGTTTTGCTTCCCCAGCGCCCAGGA"
                + "TGAGCGGTTTACCATCGAGCTTACCCGGGCCCGGCTCCTGTCAAGCACTTTAGTTAGCTG"
                + "TTGGTGTCATGTTTGGATACCAGTGTTTTATATTTATACATAGAGAGGATTTTCTAATAT"
                + "AGCCTATTATATATAAATAGATCTATCTATATGCACATACATACATATATACACACACCC"
                + "AGCCGATCTCCCGCTCCCAGACTGCTCTCGTATGTGGGGAATGGGATGAATCCCCATCTG"
                + "TGCCTCGACCATCAAAGCCGGAGCTCCAGGGCTGGAGGGGGGGCAGCCATGTGTCCTGAC"
                + "CCCCACCATCCCCAGAGCCTGGGTTTTCCAGGGAAAGGCTTTGTGCAATTGCCTGGTCTT"
                + "TTTTTTTTTTTTTTTTTCTCCATGTGCCTCCCTGTCCCCAAATCTCTGCACCAAAGCTCA"
                + "TTGCAGGCAATGTTGGAAAAGAACTGCATTTGAACGAAAGAGAAAACGGCGCCTGTGGTT"
                + "TTGCTTCAGGCCACTTTGAGGGGTTGCAGGTCAGTCAACATGACTGATCCTTGACCTGGC"
                + "TCTGCTGGGCTGGACTGCCTGGGCCAGTGATAATGCTGCCATTGGCCAGAGACAGGGCAG"
                + "AGCCCGTCCCTGTCTGAACAAGAGTCCCGCATTAGCAATGAGAAGAAGCGTGGCGGGAGA"
                + "GGAGCCACCACTGCCGGGGACTGGAGTTTGTTGGATGTGAGGGGAATGCCCACTGGGGCC"
                + "AGGGTGGGCAAAGGAGGTTGGTGCCAATGTGGTGCTGGAAGTAGACCCCTCCTGGGTCTG"
                + "GAACATTCTCTGCCCCAGGCTCTTTCCACCTGGGGATGTTGTGACAACATCCAGCTGTGG"
                + "TGTCTGGGCTATTTGTGTGGTGACAAGACAGACCTGGTCAGGGGGTGGGTGGCAAACACC"
                + "TCACCTGGCTGGGCAGAGGCCACCTTGCTGTTTGGCCCAAGGCCAGGTGAGGCCCCTCCC"
                + "TTTCCACCATTGCCCTGGCCCCGTGGCCATTCATTCTTGGGGTTTGTCTGAGTCTTCCAC"
                + "AGAGAGCAGCAAAGAGATATGGGAGTTGGGTGATCAGTGTATTTACTTTGCAGATTTTTC"
                + "TGGAAATAACCTCTAGCTGCTGCTATTTGCAAGGCTGGGATTGGCCTCCCTGTCTTTTCC"
                + "AACAATCAGGACCCCATCCTGTCTGTTCCTGTTCTTCTCCCAGCCTCTGCCCTCCTTCTG"
                + "TCTCCTGGGTGGTCTCCCGCTTCGCTCCTGGCCTTTGCATGTTCCATCTCTTCTCTTTCT"
                + "CCCCTCTTCGCCACCCTAGATTATCTCTGTGTCCCTCTACCTAATTCTCTTACCAGTTCT"
                + "TTCCTTTCTGCTTAGAGCCGGAGGGAGGGAAGCTGGAATCTGTTATGGTGTATTAGGGGG"
                + "AGGAGCCTTTCTGTTATTTCCCCCCAGCTTCCATCACCCTTCCCTTAATGCCAGGAGCGC"
                + "CGAGGTTTGGAACTGAGGCTAAACCGAAGGGCTTTCTCCTGCTTCTGTCACCCTGTGCTG"
                + "TCTCCCGGCATCTCCTCACTATCTGGGTCCAGGCCCGGGTCCTCCCTGCAGACTCCCTCC"
                + "CCTCCAGCCCACAAGGACTCCAGCCTGCTGGCTGGTGAGAGCTGGATGAGCAGTAAGAAG"
                + "TTAACGCCAGGTCAGCCTCGGGCCCCAGGGTTCCCTGGGCACAGGCTGACACTAGGAGCT"
                + "GCTCTCCTTGGCCGGGGACAGCTCCAGCAGCTCAGGCTCCAGCCCTCCAGTGAGCCGCCT"
                + "CTCCTGGAGAGTAGCAAAGAATGGAGAAAGGCCTGGCTTGAGAGGAAATGGAGAAAACAT"
                + "TGCCTGGTAGCTGCAGATTCAAACCCACCACACACACCACGGGGCTAGAGTGACTCACCT"
                + "GGGCTCACACCAACTTCTGGCCAATGCAGGGACCATGCTCTGCAGACAGCAGTCCTCTAG"
                + "CATGGCCCCAGCATTCAGCCAGTCGGTCACCTGTGGGCCATCCCCATGTGTGAGGAAGCA"
                + "GGAGTCAGCCACCAAGTGTCTCCACATCGGAGCTTGCGGGTCAGACCTGTGGGCCAGGGG"
                + "ATGGGAGCAGGGCTGAGGGTAGGGGCTGAATGTGTGGCTCTGTCCCTGTGTTGCCTTCCA"
                + "CAGAGGAGCAAGGCCTCAGGCTGAGGAAGGAGGGGCACGCTGGAACAGCCTAGTCTCCTC"
                + "CCCGTGGATTCCCCCAAACCCATAACATTCTTCCATAGGGGCTGAGAACGCAGTGCCCCG"
                + "TCCCTGACAGGGATGAAAAGTGAACCCCTCAGGTCAGGAGAGGCAGAGTTGAGGTTCTGC"
                + "CACTTCCTGTCCCTGGGGAGCCACTCAAGTTACCAGGGCTACCGGCTGAAATAAATCTTT"
                + "TCCGGGTAGGGTCAAGGGCAGTGTGTTGCCAAGGCAACTGATGTAGGCCAGTTGCGTGAC"
                + "TCCAGGTTTGTCCTGGTACTCAGTGGGTCCAATCACCTGGCATTGATCACCTGGCATTGA"
                + "TCAGCACCCACCCCACCCCTGAGGCTTGCCCAGCCCCCAGGCCCTCAGATCCCTGCTCTT"
                + "CCTGCCTTTCCTGCCCATGTGTCACCCAGCACCCAAGGTTCAGTGACACAGGGTGGTTTG"
                + "GAGCTGGTCACTGTCATAGCAGCTGTGATTTCACAAGGAAGGGTGCTGCAGGGGGACCTG"
                + "GTTGATGGGGAGTGGGAAGGGGAAGGAATAAAGAGATCTTCCTCAGGT");

        //Add miRNAs to evaluate
        gp.addMiRNA("hsa-miR-941", "CAUCAAAGCGGUGGUUGAUGUG");
        gp.addMiRNA("hsa-let-7a-1", "ugagguaguagguuguauaguu");
        gp.addMiRNA("hsa-mir-492", "aggaccugcgggacaagauucuu");

        //
        //OPTIONAL: Define the type of Mirna Canidate Site Finder. By default
        //it uses miRAW's own Candidate Site Finder
        //log.info("Prediction with TargetScan");
        //gp.setCandidateSiteFinder(new TargetScanSiteFinder());
        //gp.setCandidateSiteFinder(new PitaCandidateSiteFinder());
        gp.setCandidateSiteFinder(new DefaultCandidateSiteFinder());

        //Perform prediction
        log.info("Doing Prediction");
        gp.doPrediction();

        //Obtain results of the prediction
        log.info("Obtaining results");
        HashMap<String, Float> predictions = gp.getMiRNAInteractions(); //Hashtable with the predictions for the tested miRNAS

        //Obtain a particular prediction        
        float predVal = (float) 0.0;
        String miRNAName = "hsa-miR-941";
        if (predictions.containsKey(miRNAName)) {
            predVal = predictions.get(miRNAName);
        }
        log.info("PredVal=" + predVal);

        //gp.evaluateModel(testIter);
    }

    /**
     * Method used during development to test/develop gene prediction.
     *
     * @param args
     */
    public static void main3(String[] args) throws IOException {
        String expName = "testA";
        String expFolder = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/wholeGeneResults";
        String unifiedFile = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/tarBaseValidTranscript.csv";
        String modelLocation = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/experimentResults/Exp_161017_2575_A/Set1/lastModel.bin";

//        GenePredictionExecution.evaluateUnifiedFile(unifiedFile, modelLocation, expName, "default", expFolder, 30, 10);

    }

    //Functions called in the main file
}
