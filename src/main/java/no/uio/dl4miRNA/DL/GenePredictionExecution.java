/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DL;

import com.google.common.io.Files;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.uio.dl4miRNA.SiteFinder.CandidateSiteFinder;
import no.uio.dl4miRNA.SiteFinder.CandidateSiteFinderFactory;
import no.uio.dl4miRNA.SiteFinder.SiteFilters.CandidateSiteFilter;
import no.uio.dl4miRNA.SiteFinder.SiteFilters.FilterFactory;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.slf4j.LoggerFactory;

/**
 * This class controls the logic underneath miRAW's miRNA target prediction.
 *
 * @author apla
 */
public class GenePredictionExecution {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GenePredictionExecution.class);

    /**
     * Predicts new miRNA targets using the parameters defined in the
     * configuration file.
     *
     * @param configurationFile location of the configuration file
     */
    public static void genePrediction(String configurationFile) {
        try {

            Properties prop = new Properties();
            FileInputStream configInput = new FileInputStream(configurationFile);

            //Read basic properties for executing the prediction
            prop.load(configInput);

            String expName = prop.getProperty("ExperimentName");
            String expFolder = prop.getProperty("ExperimentFolder");
            String unifiedFile = prop.getProperty("UnifiedFile");
            String modelLocation = prop.getProperty("DLModel");
            String sitePredictor = prop.getProperty("CandidateSiteFinder.Type");

            int maxSiteLength = 30;
            int seedAlignementOffset = 0;

            if (prop.getProperty("MaxSiteLength") != null) {
                maxSiteLength = Integer.parseInt(prop.getProperty("MaxSiteLength"));
            }
            if (prop.getProperty("SeedAlignementOffset") != null) {
                seedAlignementOffset = Integer.parseInt(prop.getProperty("SeedAlignementOffset"));
            }

            int i = 0;
            int nFilters = 0;
            while (prop.getProperty("Filter." + i) != null) {
                nFilters++;
                i++;
            }
            i=0;
            String[] filters = new String[nFilters];
            while (prop.getProperty("Filter." + i) != null) {
                filters[i] = prop.getProperty("Filter." + i);
                i++;
            }

            //perform prediction
            predictGenes(unifiedFile, modelLocation, expName, expFolder, sitePredictor, filters, maxSiteLength, seedAlignementOffset, false, configurationFile);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenePredictionExecution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GenePredictionExecution.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Classify miRNA targets using the parameters defined in the configuration
     * file, results are then compared with the real values in order to assess
     * classification accuracy.
     *
     * @param configurationFile location of the configuration file
     */
    public static void genePredictionEvaluation(String configurationFile) {

        try {
            FileInputStream configInput = null;
            //Read basic properties for executing the prediction
            Properties prop = new Properties();
            configInput = new FileInputStream(configurationFile);

            prop.load(configInput);

            String expName = prop.getProperty("ExperimentName");
            String expFolder = prop.getProperty("ExperimentFolder");
            String unifiedFile = prop.getProperty("UnifiedFile");
            String modelLocation = prop.getProperty("DLModel");
            String sitePredictor = prop.getProperty("CandidateSiteFinder.Type");

            int maxSiteLength = 30;
            int seedAlignementOffset = 10;

            if (prop.getProperty("MaxSiteLength") != null) {
                maxSiteLength = Integer.parseInt(prop.getProperty("MaxSiteLength"));
            }
            if (prop.getProperty("SeedAlignementOffset") != null) {
                seedAlignementOffset = Integer.parseInt(prop.getProperty("SeedAlignementOffset"));
            }

            int i = 0;
            int nFilters = 0;
            while (prop.getProperty("Filter." + i) != null) {
                nFilters++;
                i++;
            }
            String[] filters = new String[nFilters];
            i=0;
            while (prop.getProperty("Filter." + i) != null) {
                filters[i] = prop.getProperty("Filter." + i);
                i++;
            }

            //perform classification and prediction
            evaluateUnifiedFile(unifiedFile, modelLocation, expName, sitePredictor, filters, expFolder, maxSiteLength, seedAlignementOffset, configurationFile);

        } catch (FileNotFoundException ex) {
            log.error("File not found exception! " + ex.getMessage());
        } catch (IOException ex) {
            log.error("An error occured while reading configuration file: " + configurationFile);
            java.util.logging.Logger.getLogger(GenePrediction.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }

    /**
     * Predicts the miRNA-Gene pairs defined in a unified file using the default
     * parameters, if the real class of the targets is known, the predictions
     * can also be evaluated.
     *
     * @param inputFile location of the unified file
     * @param dLModelLocation location of the Neural network to be used for
     * classification
     * @param expName name of the experiment
     * @param outputFolder location of the output folder, where all the new data
     * will be saved.
     * @param candidateSiteFinder type of candidate site finder method to be
     * used
     * @param evaluate defines if the results need to be compared with the real
     * class of the targets.
     * @throws IOException
     */
    protected static void predictGenes(String inputFile, String dLModelLocation, String expName, String outputFolder, String candidateSiteFinder, String[] aPosterioriFilters, int maxSiteLength, int seedAlignementOffset, boolean evaluate) throws IOException {
        predictGenes(inputFile, dLModelLocation, expName, outputFolder, candidateSiteFinder, aPosterioriFilters, maxSiteLength, seedAlignementOffset, evaluate, null);
    }

    /**
     * Predicts the miRNA-Gene pairs defined in a unified file using the
     * parameters defined in the configuration file, if the real class of the
     * targets is known, the predictions can also be evaluated.
     *
     * @param inputFile location of the unified file
     * @param dLModelLocation location of the Neural network to be used for
     * classification
     * @param expName name of the experiment
     * @param outputFolder location of the output folder, where all the new data
     * will be saved.
     * @param candidateSiteFinder type of candidate site finder method to be
     * used
     * @param evaluate defines if the results need to be compared with the real
     * class of the targets.
     * @param configurationFile location of the configuration file.
     * @throws IOException
     */
    protected static void predictGenes(String inputFile, String dLModelLocation, String expName, String outputFolder, String candidateSiteFinder, String[] aPosterioriFilters, int maxSiteLength, int seedAlignementOffset, boolean evaluate, String configurationFile) throws IOException {

        //Create output folder
        if (!new File(outputFolder + "/" + expName).exists()) {
            boolean res = new File(outputFolder + "/" + expName).mkdir();
            if (!res) {
                log.error("Experimentation destination " + outputFolder + "/" + expName + " could not be created.");
            }
        }

        //saves a copy of the used Neural Network, for reproducibility
        Files.copy(new File(dLModelLocation), new File(outputFolder + "/" + expName + "/modelUsed.bin"));
        if (configurationFile != null) {
            Files.copy(new File(configurationFile), new File(outputFolder + "/" + expName + "/confFile.properties"));
        }

        //Initialize output files
        PrintWriter resultFileWriter = new PrintWriter(outputFolder + "/" + expName + "/targetPredictionOutput.csv");
        PrintWriter notesFileWriter = new PrintWriter(outputFolder + "/" + expName + "/notes.txt");

        //Header of the output file. If 'evaluate' is set to true, the Real Class information is also saved.
        if (evaluate) {
            resultFileWriter.println("GeneName\tGeneId\tmiRNA\tPrediction\tRealClass\tHighestPredVal\tLowestPredVal\tPosSites\tNegSites\tRemovedSites");
        } else {
            resultFileWriter.println("GeneName\tGeneId\tmiRNA\tPrediction\tHighestPredVal\tLowestPredVal\tPosSites\tNegSites\tRemovedSites");
        }

        //Initialize reading of the unified file
        BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));

        //Determine the indexes of the different input file attributes
        String firstLine = fileReader.readLine();
        while (firstLine.length() == 0) {
            firstLine = fileReader.readLine();
        }
        String[] vals = firstLine.split("\t");
        ArrayList<String> fieldIndex = new ArrayList<>(Arrays.asList(vals));
        int MIRNAME = fieldIndex.indexOf("miRNA");
        int GENENAME = fieldIndex.indexOf("gene_name");
        int ENSEMBLID = fieldIndex.indexOf("EnsemblId");
        int MIRTRANSCRIPT = fieldIndex.indexOf("Mature_mirna_transcript");
        int UTRTRANSCRIPT = fieldIndex.indexOf("3UTR_transcript");
        int VALIDATION = fieldIndex.indexOf("Positive_Negative");

        //Initialize targetPredictor
        GenePrediction targetPredictor;

        //Intialize counting of true positives, true negatives, etc. for evaluation purposes
        int TP = 0;
        int TN = 0;
        int FP = 0;
        int FN = 0;

        //load the neural network
        MultiLayerNetwork dnnModel = ModelSerializer.restoreMultiLayerNetwork(dLModelLocation);
        int analyzed = 0;

        //for each line in the unified file
        boolean append = false;
        for (String line; (line = fileReader.readLine()) != null;) {
            //initialize target predictor to get rid of previousp redictions.
            targetPredictor = new GenePrediction();
            targetPredictor.loadLearnedModel(dnnModel);
            CandidateSiteFinder siteFinder = CandidateSiteFinderFactory.getSiteFinder(candidateSiteFinder);
            siteFinder.setMaxSiteLength(maxSiteLength);
            siteFinder.setSeedAlignementOffset(seedAlignementOffset);
            targetPredictor.setCandidateSiteFinder(candidateSiteFinder, maxSiteLength, seedAlignementOffset);
            for (String filter : aPosterioriFilters) {
                log.debug(filter);
                CandidateSiteFilter csf = FilterFactory.getFilter(filter);
                if (csf != null) {
                    targetPredictor.addAPosterioriFilter(csf);
                }
            }

            //get mirna and gene values
            vals = line.split("\t");
            String mirname = vals[MIRNAME];
            String geneName = vals[GENENAME];
            String ensemblId = vals[ENSEMBLID];
            String validation = vals[VALIDATION];
            String mirTranscript = vals[MIRTRANSCRIPT];
            String utrTranscript = vals[UTRTRANSCRIPT];

            log.info("Evaluating " + mirname + " / " + geneName);

            //send information to the target predictor and perform prediction
            targetPredictor.setGeneEnsemblId(ensemblId);
            targetPredictor.setGene3UTRTranscript(utrTranscript);
            targetPredictor.setGeneName(geneName);
            targetPredictor.clearMiRNAs();
            targetPredictor.addMiRNA(mirname, mirTranscript);
            targetPredictor.doPrediction();
            float prediction = targetPredictor.getPrediction().get(mirname);

            //Save results into a file
            // GeneName\tGeneId\tmiRNA\tPrediction\tRealVal\tHighestPredVal\tLowestPredVal\tPosSites\tNegSites
            resultFileWriter.print(geneName + "\t" + ensemblId + "\t" + mirname + "\t");
            resultFileWriter.print(prediction + "\t");
            if (evaluate) {
                resultFileWriter.print(validation + "\t");
            }
            resultFileWriter.print(targetPredictor.getMaxVal().get(mirname) + "\t");
            resultFileWriter.print(targetPredictor.getMinVal().get(mirname) + "\t");
            resultFileWriter.print(targetPredictor.getNumberOfPosSites().get(mirname) + "\t");
            resultFileWriter.print(targetPredictor.getNumborOfNegSites().get(mirname) + "\t");
            resultFileWriter.print(targetPredictor.getNumberOfFilterRemovedSites().get(mirname) + "\t");
            resultFileWriter.println();
            resultFileWriter.flush();

            //Save the specific MBSs. Can be commented if generate files are too big.
            targetPredictor.savePositiveCandidateSitesToFileLight(outputFolder + "/" + expName + "/positiveTargetSites.csv");
            targetPredictor.saveCandidateSitesToFile(outputFolder + "/" + expName + "/allTargetSites.csv",append);
            append = true;

            //If data needs to be evaluated, count if the prediction is a TP, TN, FP or FN.
            if (evaluate) {
                if (Integer.parseInt(validation) > 0) {
                    if (prediction > 0) {
                        TP++;
                    } else {
                        FN++;
                    }
                } else if (prediction > 0) {
                    FP++;
                } else {
                    TN++;
                }
                //Show results of the evaluation
                log.info("Predicted as " + prediction + "\t\tReal Class = " + validation);
                log.info("Maximum prediction:" + prediction + " / Minimum prediction:" + targetPredictor.getMinVal().get(mirname));
                log.debug(targetPredictor.getNumberOfPosSites().get(mirname) + " sites identified as positive, " + targetPredictor.getNumborOfNegSites().get(mirname) + " as negatie");
                //save results of the evaluation
                if (analyzed == 0) {
                    notesFileWriter.println("AnalyzedPairs\tTP\tFP\tTN\tFN");
                    notesFileWriter.flush();
                } else if (analyzed % 500 == 0) {
                    notesFileWriter.println(analyzed + "\t" + TP + "\t" + FP + "\t" + TN + "\t" + FN);
                    notesFileWriter.flush();
                }
            } else {
                //If data does not needs to be evaluated
                //Show results of the prediction
                log.info("Predicted as " + prediction);
                log.info("Maximum prediction:" + prediction + " / Minimum prediction:" + targetPredictor.getMinVal().get(mirname));
                log.debug(targetPredictor.getNumberOfPosSites().get(mirname) + " sites identified as positive, " + targetPredictor.getNumborOfNegSites().get(mirname) + " as negatie");
            }

            analyzed++; //increas counter
        }
        //Save final evaluation results
        if (evaluate) {
            notesFileWriter.println(analyzed + "\t" + TP + "\t" + FP + "\t" + TN + "\t" + FN);
        }

        //close files
        notesFileWriter.flush();
        notesFileWriter.close();
        resultFileWriter.close();
    }

    /**
     * Classifies and evaluates the miRNA-Gene pairs defined in a unified file
     * using the default parameters.
     *
     * @param inputFile location of the unified file
     * @param dLModelLocation location of the Neural network to be used for
     * classification
     * @param expName name of the experiment
     * @param candidateSiteFinder type of candidate site finder method to be
     * used
     * @param outputFolder location of the output folder, where all the new data
     * will be saved.
     * @throws IOException
     */
    protected static void evaluateUnifiedFile(String inputFile, String dLModelLocation, String expName, String candidateSiteFinder, String[] aPosterioriFilters, String outputFolder, int maxSiteLength, int seedAlignementOffset) throws IOException {
        evaluateUnifiedFile(inputFile, dLModelLocation, expName, candidateSiteFinder, aPosterioriFilters, outputFolder, maxSiteLength, seedAlignementOffset, null);
    }

    /**
     * Classifies and evaluates the miRNA-Gene pairs defined in a unified file
     * using the parameters defined in the configuration file.
     *
     * @param inputFile location of the unified file
     * @param dLModelLocation location of the Neural network to be used for
     * classification
     * @param expName name of the experiment
     * @param candidateSiteFinder type of candidate site finder method to be
     * used
     * @param outputFolder location of the output folder, where all the new data
     * will be saved.
     * @param configurationFile location of the configuration file
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected static void evaluateUnifiedFile(String inputFile, String dLModelLocation, String expName, String candidateSiteFinder, String[] aPosterioriFilters, String outputFolder, int maxSiteLength, int seedAlignementOffset, String configurationFile) throws FileNotFoundException, IOException {
        predictGenes(inputFile, dLModelLocation, expName, outputFolder, candidateSiteFinder, aPosterioriFilters, maxSiteLength, seedAlignementOffset, true, configurationFile);
    }

}
