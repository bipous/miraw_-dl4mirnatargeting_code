/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DL;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import no.uio.dl4miRNA.DCP.Binarization;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.LoggerFactory;

/**
 * Class used to predict if a candidate site is a positive or negative target.
 *
 * @author apla
 */
public class CandidatePrediction {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(CandidatePrediction.class);

    private int mirnaLength = 30;                         //max length of the transcript
    private int siteLength = 30;                          //max length of the transcript

    private int mirnaBinLength = mirnaLength * 4;           //length of the binarized transcript (4*30)
    private int siteBinLength = siteLength * 4;             //length of the binarized transcript (4*30)
    private String mrnaMBS;                             //transcript of the candidate Mirna Binding Site in the mRNA 3'UTR
    private String matureMirna;                         //transcript of the mature miRNA
    private int[] binMBS = new int[siteBinLength];       //binarized transcript of the MBS
    private int[] binMiRNA = new int[mirnaBinLength];      //binarized transcript of the miRNA
    private INDArray featureVector;                     //feature vector containing the binMBS and binMiRNA that will be feed into the DL4J neural network

    private Binarization bin;

    private MultiLayerNetwork model;

    /*###### Constructors ######*/
    /**
     * Empty constructor.
     */
    public CandidatePrediction() {
        for (int i = 0; i < mirnaBinLength; i++) {
            binMiRNA[i] = 0;
        }
        for (int i = 0; i < siteBinLength; i++) {
            binMBS[i] = 0;
        }

        mrnaMBS = "";
        matureMirna = "";
        bin = new Binarization();
        bin.setMaxMirnaLength(mirnaLength);
        bin.setMaxSiteLength(siteLength);

    }

    /**
     * Constructor that includes a trained neural network.
     *
     * @param model Neural network previously trained.
     */
    public CandidatePrediction(MultiLayerNetwork model) {
        this();
        this.model = model;
    }

    /*###### Getters & Setters ######*/

    public int getMirnaLength() {
        return mirnaLength;
    }

    public void setMirnaLength(int mirnaLength) {
        this.mirnaLength = mirnaLength;
        this.mirnaBinLength= this.mirnaLength*4;
        for (int i = 0; i < this.mirnaBinLength; i++) {
            this.binMiRNA[i] = 0;
        }
        this.bin = new Binarization();
        this.bin.setMaxMirnaLength(this.mirnaLength);
        this.bin.setMaxSiteLength(this.siteLength);
    }

    public int getSiteLength() {
        return siteLength;
    }

    public void setSiteLength(int siteLength) {
        this.siteLength = siteLength;
        this.siteBinLength= this.siteLength*4;
        binMBS = new int[siteBinLength];
        for (int i = 0; i < this.siteBinLength; i++) {
            this.binMBS[i] = 0;
        }
        this.bin = new Binarization();
        this.bin.setMaxMirnaLength(this.mirnaLength);
        this.bin.setMaxSiteLength(this.siteLength);
    }
    
    
    
    /**
     * Get the MBS transcript.
     *
     * @return MBS transcript
     */
    public String getMrnaMBS() {
        return mrnaMBS;
    }

    /**
     * Sets the MBS transcript
     *
     * @param mrnaMBS MBS transcript
     */
    public void setMrnaMBS(String mrnaMBS) {
        this.mrnaMBS = mrnaMBS;
    }

    /**
     * Gets the transcript of the Mature Mirna
     *
     * @return trasncript of the mature miRNA
     */
    public String getMatureMirna() {
        return matureMirna;
    }

    /**
     * Sets the transcript of the Mature miRNA
     *
     * @param matureMirna transcript of the mature miRNA
     */
    public void setMatureMirna(String matureMirna) {
        this.matureMirna = matureMirna;
    }

    /**
     * Gets a copy of the neural network used for the prediction.
     *
     * @return copy of the neural network used for the prediction.
     */
    public MultiLayerNetwork getModel() {
        return model.clone();
    }

    /**
     * Sets the model of the neural network to be used for prediction.
     *
     * @param model
     */
    public void setModel(MultiLayerNetwork model) {
        this.model = model.clone();
    }

    /*##### Methods #####*/
    /**
     * Predicts if the mature miRNA and the candidate MBS form a functional
     * target or not. Returns a score indicating if it is a positive target (>0)
     * or a negative one (less than 0). The closer to 0, the less confident the
     * prediction is.
     *
     * @return Score representing the class of the target.
     */
    public float doPrediction() {
        binarize();         //binarize transcripts
        buildIndArray();    //build feature vector
        INDArray result = model.output(featureVector, false);    //obtain output values of the network (two ouputs)
        float prediction = -result.getFloat(0, 0) + result.getFloat(0, 1);  //compare the two outputs. Neuron 0 corresponds to negative class, Neuron 1 to positive.
        log.debug("Val 0:" + result.getFloat(0, 0) + " Val 1:" + result.getFloat(0, 1));
        return prediction;
    }

    /**
     * Binarizes the MBS and miRNA transcripts so they can be understood by a
     * neural network.
     */
    private void binarize() {
        //this.bin3UTR=binarizeTranscript(this.mrna3UTR);
        this.binMBS = binarizeSiteTranscript(new StringBuilder(this.mrnaMBS).reverse().toString()); //PATCH: THIS IS A TEMPORAL FIX FOR A BUG IN THE BINARIZATION (LINE 189, marked as bug).
        //       once the bug is solved: - all the training and test data needs to be generated again
        //                               - the neural nets needs to be trained again
        //                               - the first line of this method should be uncommented and the second deleted.
        this.binMiRNA = binarizeMirnaTranscript(this.matureMirna);
    }

    /**
     * Binarizes a string containing a miRNA transcript. Creates a binarization
     * object in order to to perform the binarization
     *
     * @param transcript String containing the transcript to be binarized.
     * @return Array containing the binary values for the trasncript.
     */
    private int[] binarizeMirnaTranscript(String transcript) {

        return bin.binarizeMiRNA(transcript);
    }
    
    /**
     * Binarizes a string containing a site transcript. Creates a binarization
     * object in order to to perform the binarization
     *
     * @param transcript String containing the transcript to be binarized.
     * @return Array containing the binary values for the trasncript.
     */
    private int[] binarizeSiteTranscript(String transcript) {

        return bin.binarizeSite(transcript);
    }

    /**
     * Transforms the binarized miRNA and MBS transcripts and creates a feature
     * vector suitable for the DL4J neural networks.
     */
    private void buildIndArray() {
        log.debug("Build feature vector");
        //Create an empty feature vector (Using ND4J).
        this.featureVector = Nd4j.zeros(1, this.binMiRNA.length + this.binMBS.length);
        int c = 0;

        //fill the first part of the vector using the miRNA binarized trasncript
        for (int i = 0; i < this.binMiRNA.length; i++) {
            featureVector.put(0, c, this.binMiRNA[i]);
            c++;
        }
        //fill the second part of the vector using the MBS binarized trasncript
        for (int i = 0; i < this.binMBS.length; i++) {
            featureVector.put(0, c, this.binMBS[i]);
            c++;
        }
        /*log.debug("Feature vector");
        for (int i =0; i<featureVector.size(0);i++){
            log.debug("i = "+i);
        }*/
    }

    /**
     * Method used during development to test/develop candidate prediction.
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        CandidatePrediction cp = new CandidatePrediction();
        cp.setMatureMirna("UGGCCCUGACUGAAGACCAGCAGU");
        cp.setMrnaMBS("GGCGCTGCGCGGACTCGCCCCCAGGAACGC");
        cp.binarize();
        cp.buildIndArray();
        
        DataInputStream dis = new DataInputStream(new FileInputStream("/home/albertpp/miRAW/eccb/Repeating/DNNModel/bestModel.bin"));
        cp.setModel(ModelSerializer.restoreMultiLayerNetwork(dis));
        dis.close();
        
        float res = cp.doPrediction();
        System.out.println(res);
        
        
        System.out.println();
        /*cp.setMatureMirna("UGUCAGUUUGUCAAAUACCCCA");
        cp.setMrna3UTR("TTCCTCTCCACTGCCTTCGATACACCGGGC");
        cp.binarize();
        cp.buildIndArray();*/
    }

}
