/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DCP;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.LoggerFactory;

/**
 * Transformation of regular MBSs files to binarized values -that can be fed 
 * into a DL4J neural network- and to Cross Validation datasets.
 * 
 * @author apla
 */ // Suggestion: Split the class in binarization, Cross Validation and CSV utils.
public class Binarization {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Binarization.class);

    protected int maxSiteLength = 30; //maximum length for a miRNA and, thus, the maximum 'paring size';
    protected int maxMirnaLength = 30; //maximum length for a miRNA and, thus, the maximum 'paring size';

    private HashMap<String, String> nucleotideTranslation;
    //cyanine           c   1000
    //adenine           a   0100
    //guanine           g   0010
    //uracil/thymine    u/t 0001

    protected char sep = '\t';

    /**
     * Constructor.
     * Default output CSV separator set to "\t".
     * nucleotideTranslation HashMap initialized.
     */
    public Binarization() {
        setSeparator('\t');
    }

    public int getMaxSiteLength() {
        return maxSiteLength;
    }

    public void setMaxSiteLength(int maxSiteLength) {
        this.maxSiteLength = maxSiteLength;
    }

    public int getMaxMirnaLength() {
        return maxMirnaLength;
    }

    public void setMaxMirnaLength(int maxMirnaLength) {
        this.maxMirnaLength = maxMirnaLength;
    }

    
    
    /**
     * Set the separator to "separator" for the output file and updates the translation
     * hashmap to include the new separator.
     * @param separator separation character
     * @return true
     */
    public boolean setSeparator(char separator) {
        sep = separator;
        nucleotideTranslation = new HashMap<>();
        nucleotideTranslation.put("c", "1" + sep + "0" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("a", "0" + sep + "1" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("g", "0" + sep + "0" + sep + "1" + sep + "0" + sep);
        nucleotideTranslation.put("u", "0" + sep + "0" + sep + "0" + sep + "1" + sep);
        nucleotideTranslation.put("t", "0" + sep + "0" + sep + "0" + sep + "1" + sep);
        nucleotideTranslation.put("C", "1" + sep + "0" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("A", "0" + sep + "1" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("G", "0" + sep + "0" + sep + "1" + sep + "0" + sep);
        nucleotideTranslation.put("U", "0" + sep + "0" + sep + "0" + sep + "1" + sep);
        nucleotideTranslation.put("T", "0" + sep + "0" + sep + "0" + sep + "1" + sep);
        nucleotideTranslation.put("cyanine", "1" + sep + "0" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("adenine", "0" + sep + "1" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("guanine", "0" + sep + "0" + sep + "1" + sep + "0" + sep);
        nucleotideTranslation.put("uracil", "0" + sep + "0" + sep + "0" + sep + "1" + sep);
        nucleotideTranslation.put("thymine", "0" + sep + "0" + sep + "0" + sep + "1" + sep);
        nucleotideTranslation.put(" ", "0" + sep + "0" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("l", "0" + sep + "0" + sep + "0" + sep + "0" + sep);
        nucleotideTranslation.put("L", "0" + sep + "0" + sep + "0" + sep + "0" + sep);
        return true;

    }

    /**
     * Returns the separator character of the output csv.
     * @return separation character
     */
    public char getSeparator() {
        return sep;
    }

    /**
     * Returns the translation HashMap Table.
     * @return  translation hashmap.
     */
    public HashMap<String, String> getNucleoTideTranslation() {
        return (HashMap<String, String>) this.nucleotideTranslation.clone();
    }

    /**
     * Transform the inputCSV miRNA and mRNA transcripts to binarized trasncripts. 
     * Deprecated method, it has been replaced by {@link transformCSV2DL4MiRNACSV(String inputCSV, char inputCSVSeparator, String outputCSV, String classColumn, ArrayList<String> positiveCaseTerms, ArrayList<String> negativeCaseTerms, String maturemiRNATranscriptColumn, String mRNAtranscriptColumn, String geneIdColumn, String mirnaColumn) transformCSV2DL4MiRNACSV}
     * 
     * @param inputCSV input file location
     * @param inputCSVSeparator separation character of the output file
     * @param outputCSV output file location
     * @param validationColumn Name of the column containing the 'class' of the target 
     * @param positiveCaseTerms Values that the positive class can take
     * @param negativeCaseTerms Values that the negative class can take
     * @param maturemiRNATranscriptColumn Name of the miRNA transcript column
     * @param mRNAtranscriptColumn name of the mRNA transcript column
     * @throws IOException
     * @deprecated
     */
    @Deprecated
    public void transformCSV2DL4MiRNACSV(String inputCSV, char inputCSVSeparator, String outputCSV, String validationColumn, ArrayList<String> positiveCaseTerms, ArrayList<String> negativeCaseTerms, String maturemiRNATranscriptColumn, String mRNAtranscriptColumn) throws IOException {
        transformCSV2DL4MiRNACSV(inputCSV, inputCSVSeparator, outputCSV, validationColumn, positiveCaseTerms, negativeCaseTerms, maturemiRNATranscriptColumn, mRNAtranscriptColumn, null, null);
    }

    /**
     *  Transforms the inputCSV miRNA and mRNA transcripts to binarized transcripts.
     * @param inputCSV input file location
     * @param inputCSVSeparator separation character of the output file
     * @param outputCSV output file location
     * @param classColumn Name of the column containing the 'class' of the target 
     * @param positiveCaseTerms Values that the positive class can take
     * @param negativeCaseTerms Values that the negative class can take
     * @param maturemiRNATranscriptColumn Name of the miRNA transcript column
     * @param mRNAtranscriptColumn name of the mRNA transcript column
     * @param geneIdColumn name of the column containing the id of the Gene
     * @param mirnaColumn name of the column containing the name of the miRNA
     * @throws IOException 
     */
    public void transformCSV2DL4MiRNACSV(String inputCSV, char inputCSVSeparator, String outputCSV, String classColumn, ArrayList<String> positiveCaseTerms, ArrayList<String> negativeCaseTerms, String maturemiRNATranscriptColumn, String mRNAtranscriptColumn, String geneIdColumn, String mirnaColumn) throws IOException {

        //Write output file headers
        String header = "target" + sep;
        for (int i = 0; i < maxMirnaLength; i++) {
            header = header + "maturemiRNA.nc" + i + ".C" + sep;
            header = header + "maturemiRNA.nc" + i + ".A" + sep;
            header = header + "maturemiRNA.nc" + i + ".G" + sep;
            header = header + "maturemiRNA.nc" + i + ".U" + sep;
        }
        
        for (int i=0; i<maxSiteLength; i++) {
            header = header + "mbsRNA.nc" + i + ".C" + sep;
            header = header + "mbsRNA.nc" + i + ".A" + sep;
            header = header + "mbsRNA.nc" + i + ".G" + sep;
            header = header + "mbsRNA.nc" + i + ".U" + sep;
        }

        if (geneIdColumn != null && mirnaColumn != null) {
            //ADD GENE, MIRNA AND SPECIES - REMOVE?
            header = header + "geneId" + sep + "mirna" + sep;
        }

        //load and parse input file
        File f = new File(inputCSV);
        log.debug("Exists: " + f.exists());
        log.debug("Size: " + f.length());

        BufferedReader csvReader = new BufferedReader(new FileReader(inputCSV));

        
        CSVParser parser;
        CSVFormat csvFileFormat = CSVFormat.EXCEL.withDelimiter(inputCSVSeparator).withIgnoreEmptyLines().withHeader();
        parser = csvFileFormat.parse(csvReader);
        Map<String, Integer> X = parser.getHeaderMap();
        List<CSVRecord> records = parser.getRecords();
        log.debug("Number of records: " + records.size());
        PrintWriter writer = new PrintWriter(outputCSV);
        writer.println(header);
        int nRecord = 0;
        for (CSVRecord csvRecord : records) {
            //Get the target (positive or negative)
            String line = "";
            try {

                String valType = csvRecord.get(classColumn);
                int target = -1;
                if (positiveCaseTerms.contains(valType)) { //if positive
                    target = 1;
                }
                if (negativeCaseTerms.contains(valType)) { //if negative
                    target = 0;
                }

                //Get the mirna
                String matureMirna = csvRecord.get(maturemiRNATranscriptColumn);
                String matMirnaTranscriptCSV = ""; 
                for (int i = 0; i < maxMirnaLength; i++) { //translation character by character
                    String translation = "0" + sep + "0" + sep + "0" + sep + "0" + sep; //default empty
                    if (matureMirna.length() > i) {
                        String auxTranslation = this.nucleotideTranslation.get(matureMirna.charAt(i) + ""); //get translation
                        if (auxTranslation != null) {
                            translation = auxTranslation;
                        }
                    }
                    matMirnaTranscriptCSV = matMirnaTranscriptCSV + translation;
                }
                //this could be simplified using "this.binarizeTranscriptcsvRecord.get(maturemiRNATranscriptColumn);"
                
                
                //Get the mRNA transcript (assume the begining is the 5p and the end the 3p. Mirna binds in 3p
                String firstMRNAtranscript = csvRecord.get(mRNAtranscriptColumn);
                String mRNATranscriptCSV = "";
                for (int i = 0; i < this.maxSiteLength; i++) {  //translation character by character
                    String translation = "0" + sep + "0" + sep + "0" + sep + "0" + sep; //default empty
                    if (firstMRNAtranscript.length() - 1 - i >= 0) {    //BUG: THIS SHOULD BE CHANGED TO AN INCREASING LOOP, NOT A DECREASING ONE. ONCE FIXED, PATCH IN CANDIDATEPREDICTION "BINARIZATION METHOD" SHOULD BE CHANGED.
                                                                        //All the training data was generated with a decreasing loop, thus, the binarized mRNA transcript of the binarized test and training data is upside down.
                                                                        //To be consistent wit this, data feed into the network must be 'reversed' (candidateprediciton line 90) and has the following format:
                                                                        //
                                                                        //      Neural Network inputs:        miRNA Transcript Nucleotides   mRNA Transcript nucleotides
                                                                        //          nucleotide number:        0 1 2 3 .......... 28 29 30    30 29 28 .......... 3 2 1 0
                                                                        // 
                                                                        //If the loop is changed to an increasing loop, all the data generation steps and the network training needs to be redone. 
                                                                        //If the loop is changed to an increasing loop, CANDIDATEPREDICTION "BINARIZATION METHOD" (marked with "PATCH") should be changed to: 
                                                                        //                                                                             "this.bin3UTR=binarizeTranscript(this.mrna3UTR);"
                                                                        
                        
                        
                        String auxTranslation = this.nucleotideTranslation.get(firstMRNAtranscript.charAt(firstMRNAtranscript.length() - 1 - i) + "");
                        if (auxTranslation != null) {
                            translation = auxTranslation;                            
                        }
                    }
                    mRNATranscriptCSV = mRNATranscriptCSV + translation;
                    //this could be simplified using "this.binarizeTranscript(firstMRNAtranscript);"
                }

                line = line + target + sep + matMirnaTranscriptCSV + mRNATranscriptCSV;

                if (geneIdColumn != null && mirnaColumn != null) {
                    //ADD GENE, MIRNA AND SPECIES At the end of the line - left for informative purposes. Should be removed?
                    header = header + "geneId" + sep + "mirna" + sep;
                    line = line + csvRecord.get(geneIdColumn) + sep + csvRecord.get(mirnaColumn) + sep;
                }

                writer.println(line);
                writer.flush();
            } catch (Exception e) {
                log.error("Exception at record" + nRecord, e);                
            }
            nRecord++;
        }
        //close output file
        writer.close();
        removeUnnecesaryCommaFromCSV(outputCSV);
    }

    /**
     * Combine two different csvs in an output file. The two csvs must have the 
     * same header
     * @param csv1 Location of the csv 1
     * @param csv2 Lcoation of the csv 2
     * @param outputCSV Location of the output csv
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void mergeCSVs(String csv1, String csv2, String outputCSV) throws FileNotFoundException, IOException {
        BufferedReader reader1 = new BufferedReader(new FileReader(csv1));
        BufferedReader reader2 = new BufferedReader(new FileReader(csv2));

        String line1 = reader1.readLine();
        String line2 = reader2.readLine();

        //if the CSVS are not null and they have the same header
        if (line1 != null && line2 != null && line1.equalsIgnoreCase(line2)) {
            //print in the output csv
            PrintWriter writer = new PrintWriter(outputCSV);
            
            //the csv header
            writer.println(line1);
            //the cases from the first and second csv s
            while ((line1 = reader1.readLine()) != null) {
                writer.println(line1);
            }
            while ((line2 = reader2.readLine()) != null) {
                writer.println(line2);
            }
            writer.flush();
            writer.close();

        }
        reader1.close();
        reader2.close();
        //check that there are not any unnecessary floating separator at the end of the CSV
        removeUnnecesaryCommaFromCSV(outputCSV);

    }

    /**
     * Creates a new CSV with an additional column and filled with a particular value.
     * 
     * @param csv1 location of the original csv
     * @param columnName name of the new column
     * @param columnValue default value of the column
     * @param separator separator of the csv (, ; \t ...)
     * @param outputCSV locaiton of the output CSV
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void addColumnAndValue2CSV(String csv1, String columnName, String columnValue, String separator, String outputCSV) throws FileNotFoundException, IOException {
        BufferedReader reader1 = new BufferedReader(new FileReader(csv1));

        String line1 = reader1.readLine();

        int i = 0;
        if (line1 != null) {
            PrintWriter writer = new PrintWriter(outputCSV);
            writer.println(line1 + separator + columnName);
            while ((line1 = reader1.readLine()) != null) {

                writer.println(line1 + separator + columnValue);

            }

            writer.flush();
            writer.close();
        }

        reader1.close();
    }

    /**
     * Takes an inputCSV and generates a copy of it with its order shuffled.
     * 
     * @param inputCSV location of the original csv
     * @param outputCSV location of the output shuffled csv
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void shuffleCSV(String inputCSV, String outputCSV) throws FileNotFoundException, IOException {
        BufferedReader reader0 = new BufferedReader(new FileReader(inputCSV));
        String line;
        
        //read original csv content
        ArrayList<String> originalCSVContent = new ArrayList<>();
        boolean first = true;
        int nFields = 0;
        while ((line = reader0.readLine()) != null) {
            if (first) {
                nFields = line.split(",").length;
                first = false;
            }
            originalCSVContent.add(line);
        }
        reader0.close();

        //create new csv
        PrintWriter csvWriter = new PrintWriter(outputCSV);

        //randomly copy (and delete) lines from the original csv to the new ones
        while (originalCSVContent.size() > 0) {
            line = originalCSVContent.remove((int) (Math.floor(Math.random() * originalCSVContent.size())));
            if (line.split(",").length == nFields) { //make sure that the line fits the csv format
                csvWriter.println(line);
            } else {
                log.warn("There is an error in line \""+line+"\". It won't be saved to file");
            }
        }
        csvWriter.flush();
        csvWriter.close();

        removeUnnecesaryCommaFromCSV(outputCSV);
        
    }

    /**
     * Splits a CSV in two parts, using 'index' column as the separation position.
     * The CSV is separated vertically, not horizontally: left fields (including
     * index) will be saved in csv1, right ones in csv2.
     * 
     * @param inputCSV location of the original csv
     * @param index place to split
     * @param separator separation character of the csv
     * @param outputCSV1 location of the first split csv
     * @param outputCSV2 location of the second split csv
     * @return 0 if everything is fine, -1 in case of an error.
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static int splitCSV(String inputCSV, int index, String separator, String outputCSV1, String outputCSV2) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(inputCSV));
        String line;
        //create new csvs
        PrintWriter csv1 = new PrintWriter(outputCSV1);
        PrintWriter csv2 = new PrintWriter(outputCSV2);
        
        //for each line
        while ((line = br.readLine()) != null && line.length() > 1) {
            String[] fields = line.split(separator);
            if (fields.length < index) {
                return -1;
            }
            String line1 = "";
            String line2 = "";
            
            //save left fields in csv 1
            for (int i = 1; i <= index; i++) {
                line1 = line1 + fields[i] + separator;
            }
            //save right fields in csv2
            for (int i = index + 1; i < fields.length; i++) {
                line2 = line2 + fields[i] + separator;
            }
            csv1.println(fields[0] + separator + line1);
            csv2.println(fields[0] + separator + line2);
            csv1.flush();
            csv2.flush();
        }
        csv1.close();
        csv2.close();
        br.close();
        return 0;

    }

    /**
     * Check that a csv does not have unnecessary separator characters at the end of the lines.
     * If there are unnecessary separator characters ',', they are removed.
     * 
     * @param source csv file location
     * @throws IOException 
     */
    public static void removeUnnecesaryCommaFromCSV(String source) throws IOException {
        BufferedReader reader1 = new BufferedReader(new FileReader(source));
        PrintWriter auxwriter = new PrintWriter(new File(source + "aux"));

        boolean isOk = false;
        String line;
        while (!isOk && (line = reader1.readLine()) != null) {
            if (!line.endsWith(",")) {
                isOk = true;
            } else {
                line = line.substring(0, line.length() - 1);
                auxwriter.println(line);
            }
        }
        reader1.close();
        auxwriter.flush();
        auxwriter.close();
        if (!isOk) {
            new File(source).delete();
            new File(source + "aux").renameTo(new File(source));
        } else if (new File(source + "aux").exists()) {
            new File(source + "aux").delete();
        }

    }

    /**
     * Check that a tsv does not have unnecessary separator characters at the end of the lines.
     * If there are unnecessary separator characters '\t', they are removed.
     * 
     * @param source tsv file location
     * @throws IOException 
     */
    public static void removeUnnecesaryTABFromTSV(String source) throws IOException {
        BufferedReader reader1 = new BufferedReader(new FileReader(source));
        PrintWriter auxwriter = new PrintWriter(new File(source + "aux"));

        boolean isOk = false;
        String line;
        while (!isOk && (line = reader1.readLine()) != null) {
            if (!line.endsWith("\t")) {
                isOk = true;
            } else {
                line = line.substring(0, line.length() - 1);
                auxwriter.println(line);
            }
        }
        reader1.close();
        auxwriter.flush();
        auxwriter.close();
        if (!isOk) {
            new File(source).delete();
            new File(source + "aux").renameTo(new File(source));
        } else if (new File(source + "aux").exists()) {
            new File(source + "aux").delete();
        }

    }

    
    public int[] binarizeMiRNA(String transcript){
        return this.binarizeTranscript(transcript, this.maxMirnaLength);
    }
    
    public int[] binarizeSite(String transcript){
        return this.binarizeTranscript(transcript, this.maxSiteLength);
    }
    
    /**
     * Transform a transcript into a binary sequence.
     * 
     * @param transcript String definining the transcript
     * @param maxLength integer defining the maximum size of a transcript;
     * @return an array with the binarized transcript values.
     */
    public int[] binarizeTranscript(String transcript, int maxLength) {
        //initialize array
        int[] binArray = new int[maxLength * 4];
        for (int i=0; i<binArray.length;i++){
            binArray[i]=0;
        }

        int i = 0;
        int binIndex = 0;
        //for each nucleotide in the transcript
        while (i < maxLength && i < transcript.length()) {
            
            //transform to binarized nucleotide
            if (nucleotideTranslation.containsKey(transcript.charAt(i) + "")) {
                String[] bin4 = nucleotideTranslation.get(transcript.charAt(i) + "").split("" + sep);
                for (int j = 0; j < bin4.length; j++) {
                    binArray[binIndex] = Integer.parseInt(bin4[j]);
                    binIndex++;
                }
                
            }
            else{
                log.warn(transcript.charAt(i)+" is not a valid nucleotide. Position "+i+" of transcript:\n"+transcript+"\nTreated as empty nucleotide");
                String[] bin4 = nucleotideTranslation.get(" ").split("" + sep);
                for (int j = 0; j < bin4.length; j++) {
                    binArray[binIndex] = Integer.parseInt(bin4[j]);
                    binIndex++;
                }
            }
            i++;
        }
        return binArray;
    }

   /**
    * Creates a stratified Random-Subsampling Cross validation (train-test) with
    * a fixed number of train and test cases.
    * 
    * @param inputFile location of the original data
    * @param classColumn column of the class label
    * @param positiveVal value of positive label
    * @param negativeVal value of negative label
    * @param outputFilesName location of the output file
    * @param nTrain number of train instances per fold
    * @param nTest number of test instances per fold
    * @param nSets number of folds/sets in the CV
    * @param inputDelimiter separator character of the input csv
    * @param outputDelimiter separator character of the ouput csvs
    * @return 0 if everything is fine, -1 in case of an error.
    * @throws FileNotFoundException
    * @throws IOException 
    */
    public static int createRSSamplingCV(String inputFile, int classColumn, String positiveVal, String negativeVal, String outputFilesName, int nTrain, int nTest, int nSets, String inputDelimiter, String outputDelimiter) throws FileNotFoundException, IOException {

        PrintWriter targetIndex = new PrintWriter(outputFilesName + "TargetIndexes.csv");
        //file storing the indexes of the targets used for train and test. In this way, the CV files can be mapped to its original source.

        File binCV = new File(new File(outputFilesName + "TargetIndexes.csv").getParentFile().getPath() + "/binary");
        File pairCV = new File(new File(outputFilesName + "TargetIndexes.csv").getParentFile().getPath() + "/names");

        binCV.mkdir();
        pairCV.mkdir();

        BufferedReader reader0 = new BufferedReader(new FileReader(inputFile));

        //Counting the number of lines at each file. 
        //file not loaded into memory to save memory.
        ArrayList<Integer> originalPositiveLines = new ArrayList<>();
        ArrayList<Integer> originalNegativeLines = new ArrayList<>();

        String line;
        int cLine = 0;
        ArrayList<String> originalData = new ArrayList<>();
        
        int splitIndex=240;        
        boolean columnsCounted=false;
        while ((line = reader0.readLine()) != null) {
            if(!columnsCounted){
                splitIndex=line.split(inputDelimiter).length-3;
            }
            //separatin positive and negative cases
            if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(positiveVal)) {
                originalPositiveLines.add(cLine);
            } else if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(negativeVal)) {
                originalNegativeLines.add(cLine);
            }
            cLine++;
            originalData.add(line);
        }

        if (originalPositiveLines.size() < (nTrain + nTest) / 2 || originalNegativeLines.size() < (nTrain + nTest) / 2) {
            return -1;
        }
        reader0.close();

        //for each dataset that we want to create
        for (int i = 0; i < nSets; i++) {
            log.info("Generating Set " + i + " of " + nSets);

            ArrayList<Integer> positiveLines = new ArrayList<>(originalPositiveLines);
            ArrayList<Integer> negativeLines = new ArrayList<>(originalNegativeLines);

            ArrayList<String> trainData = new ArrayList<>();
            ArrayList<String> testData = new ArrayList<>();
            ArrayList<Integer> trainCases0 = new ArrayList<>();
            ArrayList<Integer> trainCases1 = new ArrayList<>();
            ArrayList<Integer> testCases0 = new ArrayList<>();
            ArrayList<Integer> testCases1 = new ArrayList<>();

            //Create outputfiles (without header);
            PrintWriter test_pr = new PrintWriter(outputFilesName + i + "_test.csv");
            PrintWriter train_pr = new PrintWriter(outputFilesName + i + "_train.csv");

            //Train
            for (int j = 0; j < nTrain / 2; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * positiveLines.size());

                trainCases1.add(positiveLines.remove(ind1));

                //same for the negative case
                int ind0 = (int) Math.floor(Math.random() * negativeLines.size());
                trainCases0.add(negativeLines.remove(ind0));
            }
            //test Same as in train
            for (int j = 0; j < nTest / 2; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * positiveLines.size());
                testCases1.add(positiveLines.remove(ind1));

                //same for the negative case
                int ind0 = (int) Math.floor(Math.random() * negativeLines.size());
                testCases0.add(negativeLines.remove(ind0));

            }

            //fill train and test
            line = null;
            int c = 0;
            String train0Index = "Set " + i + " negativeTrain";
            String train1Index = "Set " + i + " positiveTrain";
            String test0Index = "Set " + i + " negativeTest";
            String test1Index = "Set " + i + " positiveTest";

            //train
            int j = 0;
            while (trainCases0.size() > j) {
                String auxLine;
                auxLine = originalData.get(trainCases0.get(j)).replace(inputDelimiter, outputDelimiter);
                train_pr.println(auxLine);
                train0Index = train0Index + outputDelimiter + trainCases0.get(j);
                auxLine = originalData.get(trainCases1.get(j)).replace(inputDelimiter, outputDelimiter);
                train_pr.println(auxLine);
                train1Index = train1Index + outputDelimiter + trainCases1.get(j);
                j++;
                train_pr.flush();
            }

            //test
            j = 0;
            while (testCases0.size() > j) {
                String auxLine;
                auxLine = originalData.get(testCases0.get(j)).replace(inputDelimiter, outputDelimiter);
                test_pr.println(auxLine);
                test0Index = test0Index + outputDelimiter + testCases0.get(j);
                auxLine = originalData.get(testCases1.get(j)).replace(inputDelimiter, outputDelimiter);
                test_pr.println(auxLine);
                test1Index = test1Index + outputDelimiter + testCases1.get(j);
                j++;
                test_pr.flush();
            }

            test_pr.close();
            train_pr.close();
            targetIndex.println(train0Index);
            targetIndex.println(train1Index);
            targetIndex.println(test0Index);
            targetIndex.println(test1Index);

            splitCSV(outputFilesName + i + "_test.csv", splitIndex, outputDelimiter, binCV.getPath() + "/set_" + i + "_test.csv", pairCV.getPath() + "/set_" + i + "_test.csv");
            splitCSV(outputFilesName + i + "_train.csv", splitIndex, outputDelimiter, binCV.getPath() + "/set_" + i + "_train.csv", pairCV.getPath() + "/set_" + i + "_train.csv");

            removeUnnecesaryCommaFromCSV(outputFilesName + i + "_test.csv");
            removeUnnecesaryCommaFromCSV(outputFilesName + i + "_train.csv");
            removeUnnecesaryCommaFromCSV(binCV.getPath() + "/set_" + i + "_test.csv");
            removeUnnecesaryCommaFromCSV(binCV.getPath() + "/set_" + i + "_train.csv");
            removeUnnecesaryCommaFromCSV(pairCV.getPath() + "/set_" + i + "_train.csv");
            removeUnnecesaryCommaFromCSV(pairCV.getPath() + "/set_" + i + "_test.csv");

        }

        targetIndex.close();

        return 0;
    }

    
    /**
    * Creates a stratified Random-Subsampling Cross validation (train-test) with
    * a determined proportion of train and test cases.
    * 
    * @param inputFile location of the original data
    * @param classColumn column of the class label
    * @param positiveVal value of positive label
    * @param negativeVal value of negative label
    * @param outputFilesName location of the output file
    * @param pTrain number of train instances per fold
    * @param pTest proportion of test instances per fold
    * @param nSets proportion of folds/sets in the CV
    * @param inputDelimiter separator character of the input csv
    * @param outputDelimiter separator character of the ouput csvs
    * @return 0 if everything is fine, -1 in case of an error.
    * @throws FileNotFoundException
    * @throws IOException 
    */
    public static int createRSSamplingCVProportion(String inputFile, int classColumn, String positiveVal, String negativeVal, String outputFilesName, float pTrain, float pTest, int nSets, String inputDelimiter, String outputDelimiter) throws FileNotFoundException, IOException {
        if (pTrain + pTest > 100 || pTrain > 100 || pTest > 100 || pTrain < 0 || pTest < 0) {
            return -1;
        }

        BufferedReader reader0 = new BufferedReader(new FileReader(inputFile));

        //Counting the number of lines at each file. 
        //file not loaded into memory to save memory.
        int posLines, negLines;
        posLines = negLines = 0;

        String line;

        while ((line = reader0.readLine()) != null) {
            if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(positiveVal)) {
                posLines++;
            } else if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(negativeVal)) {
                negLines++;
            }
        }

        int minLines = posLines;
        if (minLines > negLines) {
            minLines = negLines;
        }

        int nTrain = 2 * (int) Math.floor(minLines * (pTrain / (float) 100));
        int nTest = 2 * (int) Math.floor(minLines * (pTest / (float) 100));

        return createRSSamplingCV(inputFile, classColumn, positiveVal, negativeVal, outputFilesName, nTrain, nTest, nSets, inputDelimiter, outputDelimiter);
    }

    /**
    * Creates a stratified Random-Subsampling Cross validation (train-test-validation) with
    * a fixed number of train, test and validation cases.
    * 
    * @param inputFile location of the original data
    * @param classColumn column of the class label
    * @param positiveVal value of positive label
    * @param negativeVal value of negative label
    * @param outputFilesName location of the output file
    * @param nTrain number of train instances per fold
    * @param nTest number of test instances per fold
    * @param nVal number of validation instances per fold
    * @param nSets number of folds/sets in the CV
    * @param inputDelimiter separator character of the input csv
    * @param outputDelimiter separator character of the ouput csvs
    * @return 0 if everything is fine, -1 in case of an error.
    * @throws FileNotFoundException
    * @throws IOException 
    */
    public static int createRSSampling3CV(String inputFile, int classColumn, String positiveVal, String negativeVal, String outputFilesName, int nTrain, int nVal, int nTest, int nSets, String inputDelimiter, String outputDelimiter) throws FileNotFoundException, IOException {

        PrintWriter targetIndex = new PrintWriter(outputFilesName + "TargetIndexes.csv");
        //file storing the indexes of the targets used for train and test. In this way, the CV files can be mapped to its original source.

        File binCV = new File(new File(outputFilesName + "TargetIndexes.csv").getParentFile().getPath() + "/binary");
        File pairCV = new File(new File(outputFilesName + "TargetIndexes.csv").getParentFile().getPath() + "/names");

        binCV.mkdir();
        pairCV.mkdir();

        BufferedReader reader0 = new BufferedReader(new FileReader(inputFile));

        //Counting the number of lines at each file. 
        //file not loaded into memory to save memory.
        ArrayList<Integer> originalPositiveLines = new ArrayList<>();
        ArrayList<Integer> originalNegativeLines = new ArrayList<>();

        String line;
        int cLine = 0;
        ArrayList<String> originalData = new ArrayList<>();

        int splitIndex=240;        
        boolean columnsCounted=false;
        while ((line = reader0.readLine()) != null) {
            if(!columnsCounted){
                splitIndex=line.split(inputDelimiter).length-3;
            }
            if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(positiveVal)) {
                originalPositiveLines.add(cLine);
            } else if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(negativeVal)) {
                originalNegativeLines.add(cLine);
            }
            originalData.add(line);
            cLine++;
        }

        if (originalPositiveLines.size() < (nTrain + nVal + nTest) / 2 || originalNegativeLines.size() < (nTrain + nVal + nTest) / 2) {
            return -1;
        }
        reader0.close();

        //for each dataset that we want to create
        for (int i = 0; i < nSets; i++) {
            log.info("Generating Set " + i + " of " + nSets);

            ArrayList<Integer> positiveLines = new ArrayList<>(originalPositiveLines);
            ArrayList<Integer> negativeLines = new ArrayList<>(originalNegativeLines);

            ArrayList<Integer> trainCases0 = new ArrayList<>();
            ArrayList<Integer> trainCases1 = new ArrayList<>();
            ArrayList<Integer> testCases0 = new ArrayList<>();
            ArrayList<Integer> testCases1 = new ArrayList<>();
            ArrayList<Integer> valCases0 = new ArrayList<>();
            ArrayList<Integer> valCases1 = new ArrayList<>();

            //Train
            for (int j = 0; j < nTrain / 2; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * positiveLines.size());
                trainCases1.add(positiveLines.remove(ind1));

                //same for the negative case
                int ind0 = (int) Math.floor(Math.random() * negativeLines.size());
                trainCases0.add(negativeLines.remove(ind0));

            }

            //validation Same as in train
            for (int j = 0; j < nTest / 2; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * positiveLines.size());
                valCases1.add(positiveLines.remove(ind1));

                //same for the negative case
                int ind0 = (int) Math.floor(Math.random() * negativeLines.size());
                valCases0.add(negativeLines.remove(ind0));

            }

            //test Same as in train
            for (int j = 0; j < nTest / 2; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * positiveLines.size());
                testCases1.add(positiveLines.remove(ind1));

                //same for the negative case
                int ind0 = (int) Math.floor(Math.random() * negativeLines.size());
                testCases0.add(negativeLines.remove(ind0));

            }

            //Create outputfiles (without header);
            PrintWriter test_pr = new PrintWriter(outputFilesName + i + "_test.csv");
            PrintWriter val_pr = new PrintWriter(outputFilesName + i + "_val.csv");
            PrintWriter train_pr = new PrintWriter(outputFilesName + i + "_train.csv");

            //reopen the input files
            reader0 = new BufferedReader(new FileReader(inputFile));

            //fill train and test
            line = null;
            int c = 0;
            String train0Index = "Set " + i + " negativeTrain";
            String train1Index = "Set " + i + " positiveTrain";
            String test0Index = "Set " + i + " negativeTest";
            String test1Index = "Set " + i + " positiveTest";
            String val0Index = "Set " + i + " negativeVal";
            String val1Index = "Set " + i + " positiveVal";

            //train
            int j = 0;
            while (trainCases0.size() > j) {
                String auxLine;
                auxLine = originalData.get(trainCases0.get(j)).replace(inputDelimiter, outputDelimiter);
                train_pr.println(auxLine);
                train0Index = train0Index + outputDelimiter + trainCases0.get(j);
                auxLine = originalData.get(trainCases1.get(j)).replace(inputDelimiter, outputDelimiter);
                train_pr.println(auxLine);
                train1Index = train1Index + outputDelimiter + trainCases1.get(j);
                j++;
                train_pr.flush();
            }

            //test
            j = 0;
            while (testCases0.size() > j) {
                String auxLine;
                auxLine = originalData.get(testCases0.get(j)).replace(inputDelimiter, outputDelimiter);
                test_pr.println(auxLine);
                test0Index = test0Index + outputDelimiter + testCases0.get(j);
                auxLine = originalData.get(testCases1.get(j)).replace(inputDelimiter, outputDelimiter);
                test_pr.println(auxLine);
                test1Index = test1Index + outputDelimiter + testCases1.get(j);
                j++;
                test_pr.flush();
            }

            //val
            j = 0;
            while (valCases0.size() > j) {
                String auxLine;
                auxLine = originalData.get(valCases0.get(j)).replace(inputDelimiter, outputDelimiter);
                val_pr.println(auxLine);
                val0Index = val0Index + outputDelimiter + valCases0.get(j);
                auxLine = originalData.get(valCases1.get(j)).replace(inputDelimiter, outputDelimiter);
                val_pr.println(auxLine);
                val1Index = val1Index + outputDelimiter + valCases1.get(j);
                j++;
                val_pr.flush();
            }

            test_pr.close();
            train_pr.close();
            targetIndex.println(train0Index);
            targetIndex.println(train1Index);
            targetIndex.println(val0Index);
            targetIndex.println(val1Index);
            targetIndex.println(test0Index);
            targetIndex.println(test1Index);

            splitCSV(outputFilesName + i + "_test.csv", splitIndex, outputDelimiter, binCV.getPath() + "/set_" + i + "_test.csv", pairCV.getPath() + "/set_" + i + "_test.csv");
            splitCSV(outputFilesName + i + "_train.csv", splitIndex, outputDelimiter, binCV.getPath() + "/set_" + i + "_train.csv", pairCV.getPath() + "/set_" + i + "_train.csv");
            splitCSV(outputFilesName + i + "_val.csv", splitIndex, outputDelimiter, binCV.getPath() + "/set_" + i + "_val.csv", pairCV.getPath() + "/set_" + i + "_val.csv");

            removeUnnecesaryCommaFromCSV(outputFilesName + i + "_test.csv");
            removeUnnecesaryCommaFromCSV(outputFilesName + i + "_train.csv");
            removeUnnecesaryCommaFromCSV(binCV.getPath() + "/set_" + i + "_test.csv");
            removeUnnecesaryCommaFromCSV(binCV.getPath() + "/set_" + i + "_train.csv");
            removeUnnecesaryCommaFromCSV(binCV.getPath() + "/set_" + i + "_val.csv");
            removeUnnecesaryCommaFromCSV(pairCV.getPath() + "/set_" + i + "_train.csv");
            removeUnnecesaryCommaFromCSV(pairCV.getPath() + "/set_" + i + "_test.csv");
            removeUnnecesaryCommaFromCSV(pairCV.getPath() + "/set_" + i + "_val.csv");

        }

        targetIndex.close();

        return 0;
    }

    /**
    * Creates a stratified Random-Subsampling Cross validation (train-test) with
    * a fixed number of train and test cases.
    * 
    * @param inputFile location of the original data
    * @param classColumn column of the class label
    * @param positiveVal value of positive label
    * @param negativeVal value of negative label
    * @param outputFilesName location of the output file
    * @param pTrain proportion of train instances per fold
    * @param pTest proportion of test instances per fold
    * @param pVal proportion of valdiation instances per fold
    * @param nSets number of folds/sets in the CV
    * @param inputDelimiter separator character of the input csv
    * @param outputDelimiter separator character of the ouput csvs
    * @return 0 if everything is fine, -1 in case of an error.
    * @throws FileNotFoundException
    * @throws IOException 
    */
    public static int createRSSampling3CVProportion(String inputFile, int classColumn, String positiveVal, String negativeVal, String outputFilesName, float pTrain, float pVal, float pTest, int nSets, String inputDelimiter, String outputDelimiter) throws FileNotFoundException, IOException {
        if (pTrain + pTest > 100 || pTrain > 100 || pTest > 100 || pTrain < 0 || pTest < 0) {
            return -1;
        }

        BufferedReader reader0 = new BufferedReader(new FileReader(inputFile));

        //Counting the number of lines at each file. 
        //file not loaded into memory to save memory.
        int posLines, negLines;
        posLines = negLines = 0;

        String line;

        while ((line = reader0.readLine()) != null) {
            if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(positiveVal)) {
                posLines++;
            } else if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(negativeVal)) {
                negLines++;
            }
        }

        int minLines = posLines;
        if (minLines > negLines) {
            minLines = negLines;
        }

        int nTrain = 2 * (int) Math.floor(minLines * (pTrain / (float) 100));
        int nVal = 2 * (int) Math.floor(minLines * (pVal / (float) 100));
        int nTest = 2 * (int) Math.floor(minLines * (pTest / (float) 100));

        return createRSSampling3CV(inputFile, classColumn, positiveVal, negativeVal, outputFilesName, nTrain, nVal, nTest, nSets, inputDelimiter, outputDelimiter);
    }

    /**
     * Creates a cross validation with a predefined number of postive and 
     * negative train and test instances.
     * 
     * @param inputFile location of the csv contianing the data
     * @param classColumn column with the class label
     * @param positiveVal value of positive label 
     * @param negativeVal value of teh negative label
     * @param outputFilesName name of the ouput folder
     * @param nTrainPos number of positive train instances
     * @param nTestPos number of positive test instances
     * @param nTrainNeg number of negative train instances
     * @param nTestNeg number of negative test instances
     * @param nSets number of sets/folds in the CV
     * @param inputDelimiter separation character of the input csv
     * @param outputDelimiter separation character of the output csv
     * @return 0 if everything is fine, -1 in case of an error.
     * @throws FileNotFoundException
     * @throws IOException
     * @deprecated
     */
     @Deprecated
    public static int createUnbalancedRSSamplingCV(String inputFile, int classColumn, String positiveVal, String negativeVal, String outputFilesName, int nTrainPos, int nTestPos, int nTrainNeg, int nTestNeg, int nSets, String inputDelimiter, String outputDelimiter) throws FileNotFoundException, IOException {

        PrintWriter targetIndex = new PrintWriter(outputFilesName + "TargetIndexes.csv");
        //file storing the indexes of the targets used for train and test. In this way, the CV files can be mapped to its original source.

        BufferedReader reader0 = new BufferedReader(new FileReader(inputFile));

        //Counting the number of lines at each file. 
        //file not loaded into memory to save memory.
        ArrayList<Integer> originalPositiveLines = new ArrayList<>();
        ArrayList<Integer> originalNegativeLines = new ArrayList<>();

        String line;
        int cLine = 0;
        while ((line = reader0.readLine()) != null) {
            if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(positiveVal)) {
                originalPositiveLines.add(cLine);
            } else if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(negativeVal)) {
                originalNegativeLines.add(cLine);
            }
            cLine++;
        }

        if (originalPositiveLines.size() < (nTrainPos + nTestPos) || originalNegativeLines.size() < (nTrainNeg + nTestNeg)) {
            return -1;
        }
        reader0.close();

        //for each dataset that we want to create
        for (int i = 0; i < nSets; i++) {
            log.debug("Generating Set " + i + " of " + nSets);

            ArrayList<Integer> positiveLines = new ArrayList<>(originalPositiveLines);
            ArrayList<Integer> negativeLines = new ArrayList<>(originalNegativeLines);

            ArrayList<Integer> trainCases0 = new ArrayList<>();
            ArrayList<Integer> trainCases1 = new ArrayList<>();
            ArrayList<Integer> testCases0 = new ArrayList<>();
            ArrayList<Integer> testCases1 = new ArrayList<>();

            //Train
            for (int j = 0; j < nTrainPos; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * positiveLines.size());
                trainCases1.add(positiveLines.remove(ind1));
            }

            //same for the negative case
            for (int j = 0; j < nTrainNeg; j++) {
                int ind0 = (int) Math.floor(Math.random() * negativeLines.size());
                trainCases0.add(negativeLines.remove(ind0));
            }

            //test Same as in train
            for (int j = 0; j < nTestPos; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * positiveLines.size());
                testCases1.add(positiveLines.remove(ind1));
            }

            for (int j = 0; j < nTestNeg; j++) {
                //same for the negative case
                int ind0 = (int) Math.floor(Math.random() * negativeLines.size());
                testCases0.add(negativeLines.remove(ind0));
            }

            //Create outputfiles (without header);
            PrintWriter test_pr = new PrintWriter(outputFilesName + i + "_test.csv");
            PrintWriter train_pr = new PrintWriter(outputFilesName + i + "_train.csv");

            //reopen the input files
            reader0 = new BufferedReader(new FileReader(inputFile));

            //fill train and test
            line = null;
            int c = 0;
            String train0Index = "Set " + i + " negativeTrain";
            String train1Index = "Set " + i + " positiveTrain";
            String test0Index = "Set " + i + " negativeTest";
            String test1Index = "Set " + i + " positiveTest";

            while ((line = reader0.readLine()) != null) {
                if (testCases0.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    test_pr.println(auxLine);
                    test0Index = test0Index + "\t" + c;
                }
                if (trainCases0.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    train_pr.println(auxLine);
                    train0Index = train0Index + "\t" + c;
                }
                if (testCases1.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    test_pr.println(auxLine);
                    test1Index = test1Index + "\t" + c;
                }
                if (trainCases1.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    train_pr.println(auxLine);
                    train1Index = train1Index + "\t" + c;
                }
                c++;
            }

            reader0.close();
            test_pr.close();
            train_pr.close();
            targetIndex.println(train0Index);
            targetIndex.println(train1Index);
            targetIndex.println(test0Index);
            targetIndex.println(test1Index);
        }

        targetIndex.close();

        return 0;
    }

    /**
     * Creates a cross validation with a predefined proportion of postive and 
     * negative train and test instances.
     * 
     * @param inputFile location of the csv contianing the data
     * @param classColumn column with the class label
     * @param positiveVal value of positive label 
     * @param negativeVal value of teh negative label
     * @param outputFilesName name of the ouput folder
     * @param pTrainPos proportion of positive train instances
     * @param pTestPos proportion of positive test instances
     * @param pTrainNeg proportion of negative train instances
     * @param pTestNeg proportion of negative test instances
     * @param nSets number of sets/folds in the CV
     * @param inputDelimiter separation character of the input csv
     * @param outputDelimiter separation character of the output csv
     * @return 0 if everything is fine, -1 in case of an error.
     * @throws FileNotFoundException
     * @throws IOException
     * @deprecated
     */
    @Deprecated
    public static int createUnbalancedRSSamplingCVProportion(String inputFile, int classColumn, String positiveVal, String negativeVal, String outputFilesName, float pTrainPos, float pTestPos, float pTrainNeg, float pTestNeg, int nSets, String inputDelimiter, String outputDelimiter) throws FileNotFoundException, IOException {
        /*if (pTrainPos + pTestPos + pTrainNeg + pTestNeg > 100 
                || pTrainPos > 100 || pTestPos > 100 || pTrainPos < 0 || pTestPos < 0
                || pTrainNeg > 100 || pTestNeg > 100 || pTrainNeg < 0 || pTestNeg < 0) {
            return -1;
        }*/

        BufferedReader reader0 = new BufferedReader(new FileReader(inputFile));

        //Counting the number of lines at each file. 
        //file not loaded into memory to save memory.
        int posLines, negLines;
        posLines = negLines = 0;

        String line;

        while ((line = reader0.readLine()) != null) {
            if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(positiveVal)) {
                posLines++;
            } else if (line.split(inputDelimiter)[classColumn].equalsIgnoreCase(negativeVal)) {
                negLines++;
            }
        }

        int minLines = posLines;
        if (minLines > negLines) {
            minLines = negLines;
        }

        int nTrainPos = 2 * (int) Math.floor(minLines * (pTrainPos / (float) 100));
        int nTestPos = 2 * (int) Math.floor(minLines * (pTestPos / (float) 100));
        int nTrainNeg = 2 * (int) Math.floor(minLines * (pTrainNeg / (float) 100));
        int nTestNeg = 2 * (int) Math.floor(minLines * (pTestNeg / (float) 100));
        //multiply by 2 because the % is based on the class with the least cases.

        return createUnbalancedRSSamplingCV(inputFile, classColumn, positiveVal, negativeVal, outputFilesName, nTrainPos, nTestPos, nTrainNeg, nTestNeg, nSets, inputDelimiter, outputDelimiter);
    }

   /**
    * Creates a stratified Random-Subsampling Cross validation (train-test) with
    * a fixed number of train and test cases. The negative and positive instances are
    * stored in a different file
    * 
    * @param positiveFile location of the original positive data
     * @param negativeFile location of the original negative data
    * @param outputFilesName location of the output file
    * @param inputsContainHeader indicates if the input files contain the csv header at the first line
    * @param nTrain number of train instances per fold
    * @param nTest number of test instances per fold
    * @param nSets number of folds/sets in the CV
    * @param inputDelimiter separator character of the input csv
    * @param outputDelimiter separator character of the ouput csvs
    * @return 0 if everything is fine, -1 in case of an error.
    * @throws FileNotFoundException
    * @throws IOException 
    */
    @Deprecated
    public static int createRSSamplingCVfrom2Files(String positiveFile, String negativeFile, String outputFilesName, Boolean inputsContainHeader, int nTrain, int nTest, int nSets, String inputDelimiter, String outputDelimiter) throws FileNotFoundException, IOException {

        PrintWriter targetIndex = new PrintWriter(outputFilesName + "TargetIndexes.csv");
        //file storing the indexes of the targets used for train and test. In this way, the CV files can be mapped to its original source.

        BufferedReader reader0 = new BufferedReader(new FileReader(negativeFile));
        BufferedReader reader1 = new BufferedReader(new FileReader(positiveFile));

        //Counting the number of lines at each file. 
        //file not loaded into memory to save memory.
        int nLines0 = 0;
        int nLines1 = 0;
        while (reader0.readLine() != null) {
            nLines0++;
        }
        while (reader1.readLine() != null) {
            nLines1++;
        }

        if (inputsContainHeader) {
            nLines0--;
            nLines1--;
        }

        if (nLines0 < (nTrain + nTest) || nLines1 < (nTrain + nTest)) {
            return -1;
        }
        reader0.close();
        reader1.close();
        //for each dataset that we want to create
        for (int i = 0; i < nSets; i++) {

            int offset = 0;
            if (inputsContainHeader) {
                offset++;
            }

            //List of cases not used in a test or train set.
            ArrayList<Integer> available1 = new ArrayList<>();
            ArrayList<Integer> available0 = new ArrayList<>();
            for (int j = offset; j < nLines0; j++) {
                available0.add(j);
            }
            for (int j = offset; j < nLines1; j++) {
                available1.add(j);
            }

            ArrayList<Integer> trainCases0 = new ArrayList<>();
            ArrayList<Integer> trainCases1 = new ArrayList<>();
            ArrayList<Integer> testCases0 = new ArrayList<>();
            ArrayList<Integer> testCases1 = new ArrayList<>();

            //Train
            for (int j = 0; j < nTrain / 2; j++) {
                //we select a case index, and we take its line. Then remove it from the list of not used cases.
                int ind1 = (int) Math.floor(Math.random() * available1.size());
                trainCases1.add(available1.get(ind1));
                available1.remove(ind1);

                //same for the negative case
                int ind0 = (int) Math.floor(Math.random() * available0.size());
                trainCases0.add(available0.get(ind0));
                available0.remove(ind0);

            }
            //test Same as in train
            for (int j = 0; j < nTest / 2; j++) {
                int ind1 = (int) Math.floor(Math.random() * available1.size());
                testCases1.add(available1.get(ind1));
                available1.remove(ind1);
                int ind0 = (int) Math.floor(Math.random() * available0.size());
                testCases0.add(available0.get(ind0));
                available0.remove(ind0);

            }

            //Create outputfiles (without header);
            PrintWriter test_pr = new PrintWriter(outputFilesName + i + "_test.csv");
            PrintWriter train_pr = new PrintWriter(outputFilesName + i + "_train.csv");

            //reopen the input files
            reader0 = new BufferedReader(new FileReader(negativeFile));
            reader1 = new BufferedReader(new FileReader(positiveFile));

            //fill negative cases
            String line;
            int c = 0;
            String train0Index = "Set " + i + " negativeTrain";
            String train1Index = "Set " + i + " positiveTrain";
            String test0Index = "Set " + i + " negativeTest";
            String test1Index = "Set " + i + " positiveTest";

            while ((line = reader0.readLine()) != null) {
                if (testCases0.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    test_pr.println(auxLine);
                    test0Index = test0Index + "\t" + c;
                }
                if (trainCases0.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    train_pr.println(auxLine);
                    train0Index = train0Index + "\t" + c;
                }
                c++;
            }
            reader0.close();
            //fill positive cases
            c = 0;
            while ((line = reader1.readLine()) != null) {
                if (testCases1.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    test_pr.println(auxLine);
                    test1Index = test1Index + "\t" + c;
                }
                if (trainCases1.contains(c)) {
                    String auxLine = line.replace(inputDelimiter, outputDelimiter);
                    train_pr.println(auxLine);
                    train1Index = train1Index + "\t" + c;
                }
                c++;
            }
            reader1.close();
            test_pr.close();
            train_pr.close();
            targetIndex.println(train0Index);
            targetIndex.println(train1Index);
            targetIndex.println(test0Index);
            targetIndex.println(test1Index);
        }

        targetIndex.close();

        return 0;
    }

    /**
     * main method used to perform tests during development. TO DELETE
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException{
        Binarization bin = new Binarization();
        bin.setSeparator('\t');
        
        String inputFile="/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - GD/csvFiltered.csv";
        String outputFilesName = "/Users/apla/Dropbox/Papers/Sensistep Survey/Results/ANN - GD/CVf/set";
        
        Binarization.createRSSamplingCV(inputFile, 7, "1", "0", outputFilesName, 2438, 346, 10, ",", ",");
        //Binarization.createRSSamplingCV(inputFile, 7, "'1'", "'0'", outputFilesName, 2438, 346, 10, ",", ",");

        
        
    }
    
    /*public static void main(String[] args) throws IOException {
        String originalFolder = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/CV75-25";
        String shuffledFolder = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/CV75-25Shuffled";

        new File(shuffledFolder).mkdir();

        String[] files = new File(originalFolder).list();
        for (String f : files) {
            System.out.println(f);
            if (f.endsWith("csv")) {
                shuffleCSV(originalFolder + "/" + f, shuffledFolder + "/" + f);
            } else {
                Files.copy(new File(originalFolder + "/" + f), new FileOutputStream(shuffledFolder + "/" + f));
            }
        }

        originalFolder = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/unbalancedCVV_37i5-50-12i5-12i5";
        shuffledFolder = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/unbalancedSHCVV_37i5-50-12i5-12i5";

        new File(shuffledFolder).mkdir();

        files = new File(originalFolder).list();
        for (String f : files) {
            if (f.endsWith("csv")) {
                shuffleCSV(originalFolder + "/" + f, shuffledFolder + "/" + f);
            } else {
                Files.copy(new File(originalFolder + "/" + f), new FileOutputStream(shuffledFolder + "/" + f));
            }
        }
    }*/
}
