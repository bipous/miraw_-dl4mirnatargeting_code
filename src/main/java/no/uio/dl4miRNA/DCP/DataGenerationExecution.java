/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DCP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class controls the logic underneath the generation of miRAW's datasets.
 * @author apla
 */
public class DataGenerationExecution {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(DataGenerationExecution.class);  

 

    /**
     * Creates a UnifiedFile containing the information from TarBase, miRBASe, 
     * and Ensembl.
     * Information regarding the location of the input/output files, and other
     * properties is loaded from the configuration file.
     * @param configurationFile Location of the configuration file.
     */
    public static void buildUnifiedFile(String configurationFile) {

        try {
            FileInputStream configInput = null;
            Properties prop = new Properties();
            configInput = new FileInputStream(configurationFile);
            prop.load(configInput);
            
            //input file locations
            String tarBaseDBFile = prop.getProperty("TarBaseDBFile");       //tarbase
            String threeUTRFile = prop.getProperty("Fasta3UTR");            //ensembl 
            String maturemiRNAFile = prop.getProperty("FastaMatureMirna");  //mirBAse
            String stemLoopFile = "";                                       //stemLoop --> not used
            if (prop.getProperty("fastaStemLoopFile") != null) {
                maturemiRNAFile = prop.getProperty("fastaStemLoopFile");
            }
            
            //ouput file locations
            String unifiedDestination = prop.getProperty("UnifiedFile");
            
            //parameters
            String species = "homo sapiens";    
            if (prop.getProperty("Species") != null) {
                species = prop.getProperty("Species");
            }
            
            //create the unified file
            dataConstruction.buildUnifiedFile(tarBaseDBFile, threeUTRFile, maturemiRNAFile, stemLoopFile, species, unifiedDestination);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Given a UnifiedFile containing validated miRNA-mRNA targets, it searches 
     * for positives and/or negative miRNA binding sites.
     * 
     * @param configurationFile location of the configuration file containing the parameters.
     * @param siteType type of site to search (positive, negative, all).
     */
    public static void findValidatedBindingSites(String configurationFile, String siteType) {
        try {
            FileInputStream configInput = null;
            Properties prop = new Properties();
            configInput = new FileInputStream(configurationFile);
            prop.load(configInput);

            String unifiedFile = prop.getProperty("UnifiedFile");
            String negativeCandidatesDest = prop.getProperty("NegativeCandidatesLocation");
            String positiveCandidatesDest = prop.getProperty("PositiveCandidatesLocation");
            int maxSiteLength = 30;
            int seedAlignementOffset = 10;
            
            if (prop.getProperty("MaxSiteLength")!=null){
                maxSiteLength=Integer.parseInt(prop.getProperty("MaxSiteLength"));
            }
            if (prop.getProperty("SeedAlignementOffset")!=null){
                seedAlignementOffset=Integer.parseInt(prop.getProperty("SeedAlignementOffset"));
            }

            //###############################################################################
            //############GENERATE THE CANDIDATE SITES FROM VALIDATED EXPERIMENTS############
            //###############################################################################
            log.info("Generating Candidates");

            //if the method does not restrict the site selectio to positives, negative sites are searched:
            if (!siteType.equalsIgnoreCase("positive") && !siteType.equalsIgnoreCase("1")) {
                //GENERATE NEGATIVE CANDIDATES.
                log.info("Generate negative validated candidates");

                //parameters for the candidateSite method
                int winOff;
                float mFE;
                String windowOffset = prop.getProperty("CandidateWindowOffset");
                try {
                    winOff = Integer.parseInt(windowOffset);
                } catch (NumberFormatException | NullPointerException e) {
                    winOff = 5;
                }
                String maxFreeEnergy = prop.getProperty("CandidateMaxFreeEnergy");
                try {
                    mFE = (float) Double.parseDouble(maxFreeEnergy);
                } catch (NumberFormatException | NullPointerException ne) {
                    mFE = 0;
                }
                
                //generate negative sites
                dataConstruction.obtainNegativeMBS(unifiedFile, negativeCandidatesDest, winOff, mFE,maxSiteLength,seedAlignementOffset);  
                log.info("negativeCandidates generated");
            }

            //if the method does not restrict the site selectio to negatives, positive sites are searched:
            if (!siteType.equalsIgnoreCase("negative") && !siteType.equalsIgnoreCase("0")) {
                String threeUTRFile = prop.getProperty("Fasta3UTR");            //ensembl 
                log.info("Generating positive validated candidates");
                String positiveMethod = "";// 
                switch (positiveMethod.toLowerCase()) {
                    default:
                        log.info("Default method used for detecting postivie verified sites (targetScan)");
                        String targetScanFile = prop.getProperty("PositiveSelectionFile");
                        //dataConstruction.generateTargetScanMBSfromEMBL(unifiedFile, targetScanFile, true, positiveCandidatesDest, true);
                        dataConstruction.generateTargetScanMBSfrom3UTRFile(unifiedFile, targetScanFile, threeUTRFile, true, positiveCandidatesDest, true,maxSiteLength,seedAlignementOffset); 
                        break;
                }
            }
            configInput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

     /**
     * Given a UnifiedFile containing validated miRNA-mRNA targets, it searches 
     * for positives and negative miRNA binding sites.
     * 
     * @param configurationFile location of the configuration file containing the parameters.
     */
    public static void findValidatedBindingSites(String configurationFile) {
        log.warn("No type has been specified. Sites will be searched for both positive and negative sites");
        DataGenerationExecution.findValidatedBindingSites(configurationFile, "all");

    }
    
    /**
     * Merges a CSV file containing positive MBS with a CSV file containing 
     * negative MBS.
     * The location of the input and output files is defined in the configuration file.
     * 
     * @param configurationFile location of the configuration file.
     */
    public static void mergePositiveNegativeSiteS(String configurationFile) {
        try {
            //NegativeCandidatesLocation (mandatory for negative sites)
            //PositiveCandidatesLocation (mandatory for positive sites)
            //CompleteCandidateDatasetLocation

            FileInputStream configInput = null;
            Properties prop = new Properties();
            configInput = new FileInputStream(configurationFile);
            prop.load(configInput);

            //negative validated sites
            String negativeSitesCsv = prop.getProperty("NegativeCandidatesLocation");
            //positive validated sites
            String positiveSitesCsv = prop.getProperty("PositiveCandidatesLocation");
            //output files
            String positiveNegativeCsv = prop.getProperty("CompleteCandidateDatasetLocation");
            
            Binarization bin = new Binarization();
            
            log.info("Create final dataset.");
            log.info("Merging");
            Binarization.mergeCSVs(negativeSitesCsv, positiveSitesCsv, positiveNegativeCsv);
            configInput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Binarizes the transcripts of a CSV file contianing aligned trasncripts of miRNAS 
     * and MBSs. 
     * The output is a CSV where the first column correspond to the 
     * class of the target, the following ones to the miRNA transcript, and the 
     * last ones to the mRNA MBS transcript.
     * It is assumed that the transcripts have been previously aligned (all the 
     * sites contain the seed region start at the same position).
     * 
     * C = 1 0 0 0
     * A = 0 1 0 0
     * G = 0 0 1 0
     * U = 0 0 0 1
     * T = 0 0 0 1
     * Empty = 0 0 0 0
     * 
     * @param configurationFile location of the configuration file
     */
    public static void binarizeDataSet(String configurationFile) {
        try {
            FileInputStream configInput = null;
            Properties prop = new Properties();
            configInput = new FileInputStream(configurationFile);
            prop.load(configInput);

            String positiveNegativeDataset = prop.getProperty("CompleteCandidateDatasetLocation");
            String binarizedDataset = prop.getProperty("BinarizedDatasetLocation");

            Binarization bin = new Binarization();
            bin.setSeparator('\t');
            
            //size of the transcript
            int maxSiteLength = 30;
            if (prop.getProperty("MaxSiteLength")!=null){
                maxSiteLength=Integer.parseInt(prop.getProperty("MaxSiteLength"));
            }
            bin.setMaxSiteLength(maxSiteLength);

            //define values for the positive (1) and negative (0) validations
            ArrayList<String> positiveCaseTerms = new ArrayList<>();
            ArrayList<String> negativeCaseTerms = new ArrayList<>();
            positiveCaseTerms.add("1");
            negativeCaseTerms.add("0");

            log.info("binarizing:");
            bin.transformCSV2DL4MiRNACSV(positiveNegativeDataset, '\t', binarizedDataset, "validation", positiveCaseTerms, negativeCaseTerms, "mature_miRNA_Transcript", "mRNA_Site_Transcript", "EnsemblId", "miRNA"); //contains the mirna and gene names
            log.info("Final dataset generated");
            configInput.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Generates a Cross Validation dataset using a file containing positive and 
     * negative MBSs. The cross validation properties are defined in the 
     * configuration file. The CV can be generated in the form of a train and 
     * test CV, or in the form of a train test and validation CV.
     * 
     * The CV results are saved in a folder that contains the test and train 
     * files for the different CV folds in their binarized format. The "names" 
     * folder contains the relation of miRNA identifier and mRNA identifier 
     * corresponding to each CV file.
     * In addition, a TargetIndexes contains information that allows relating the 
     * CV data with its original source.
     * 
     * @param configurationFile location of the configuration file
     */
    public static void generateCrossValidation(String configurationFile) {
        try {
            FileInputStream configInput = null;
            Properties prop = new Properties();
            configInput = new FileInputStream(configurationFile);
            prop.load(configInput);
            String binarizedDataset = prop.getProperty("BinarizedDatasetLocation");

            String CVType = prop.getProperty("CrossValidation.Type");
            if (CVType == null) {
                CVType = "traintest";
            }
            CVType=CVType.toLowerCase();
            File CVFolder = new File(prop.getProperty("CrossValidation.Folder"));
            CVFolder.mkdir();

            int classColumn = 0;
            float trainProp = 90;
            float testProp = 10;
            int folds = 10;

            String positiveVal = "1";
            String negativeVal = "0";
            String inputDelimiter = "\t";
            String outputDelimiter = ",";
            try {
                classColumn = Integer.parseInt(prop.getProperty("CrossValidation.ClassColumn"));
            } catch (NumberFormatException | NullPointerException e) {
            }
            try {
                trainProp = Float.parseFloat(prop.getProperty("CrossValidation.TrainProportion"));
            } catch (NumberFormatException | NullPointerException e) {
            }
            try {
                testProp = Float.parseFloat(prop.getProperty("CrossValidation.TestProportion"));
            } catch (NumberFormatException | NullPointerException e) {
            }
            try {
                folds = Integer.parseInt(prop.getProperty("CrossValidation.folds"));
            } catch (NumberFormatException | NullPointerException e) {
            }

            switch (CVType) {

                case "testtrainval":
                case "testvaltrain":
                case "trainvaltest":
                case "traintestval":
                case "valtraintest":
                case "valtesttrain":
                    float valProp = 10;
                    try {
                        valProp = Float.parseFloat(prop.getProperty("CrossValidation.ValidationProportion"));
                    } catch (NumberFormatException | NullPointerException e) {
                    }
                    ;
                    try {
                        trainProp = Float.parseFloat(prop.getProperty("CrossValidation.TrainProportion"));
                    } catch (NumberFormatException | NullPointerException e) {
                        trainProp = 80;
                    }

                    Binarization.createRSSampling3CVProportion(binarizedDataset, classColumn, positiveVal, negativeVal, CVFolder.getPath() + "/Set_", trainProp, valProp, testProp, folds, inputDelimiter, outputDelimiter);
                    break;
                case "traintest":
                case "testtrain":
                default:
                    Binarization.createRSSamplingCVProportion(binarizedDataset, classColumn, positiveVal, negativeVal, CVFolder.getPath() + "/Set_", trainProp, testProp, folds, inputDelimiter, outputDelimiter);
                    break;
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataGenerationExecution.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Calls the method defined in the command line arguments.
     * @param args String[] containing the arguments of the command line:
     *      - buildunifiedfile  confFileLocation
     *      - gneeratecandidatesites confFileLocation TypeOfSyte
     *      - unifyposneg confFileLocation
     *      - binarize confFileLocation
     *      - generatecrossval confFileLocation
     *      - all confFileLocation
     */
    public static void run(String args[]) {
        switch (args[0].toLowerCase()) {
            case "builduinfiedfile": //BuildUnifiedFile
                DataGenerationExecution.buildUnifiedFile(args[1]);
                break;
            case "generatecandidatesites": //"GenerateCandidateSites"
                if (args.length == 3) {
                    DataGenerationExecution.findValidatedBindingSites(args[1], args[2]);
                } else {
                    log.warn("Wrong number of parameters: 3 expected.\nGenerateCandidateSites\tConfigurationFile\tTypeOfSite");
                }
                break;
            case "unifyposneg": //"UnifyPosNeg"
                if (args.length == 2) {
                    DataGenerationExecution.mergePositiveNegativeSiteS(args[1]);
                } else {
                    log.warn("Wrong number of parameters: 2 expected.\nUnifyPosNeg\tConfigurationFile");
                }
                //unify posNeg
                break;
            case "binarize": //"binarize"
                if (args.length == 2) {
                    DataGenerationExecution.binarizeDataSet(args[1]);
                } else {
                    log.warn("Wrong number of parameters: 2 expected.\nBinarize\tConfigurationFile");
                }
                //binarize
                break;
            case "generatecrossval": //"generatecrossval"
                if (args.length == 2) {
                    DataGenerationExecution.generateCrossValidation(args[1]);
                } else {
                    log.warn("Wrong number of parameters: 2 expected.\nGenerateCrossVal\tConfigurationFile\t");
                }
                break;
            case "all":
                DataGenerationExecution.buildUnifiedFile(args[1]);
                DataGenerationExecution.findValidatedBindingSites(args[1]);
                DataGenerationExecution.mergePositiveNegativeSiteS(args[1]);
                DataGenerationExecution.binarizeDataSet(args[1]);
                DataGenerationExecution.generateCrossValidation(args[1]);
                break;
            default:
                System.out.println("Unkown parameter " + args[0]);
                System.out.println("Showing help");
                generateDataSetsHelp();
        }
    }
    
    /** 
     * Shows a help message explaining how to execute the Data Generation process.
     */
    private static void generateDataSetsHelp() {
        System.out.println("miRAW CreateDataSets creates and generates the necessary files to train and crossvalidate miRAW's deep Neural Network");
        System.out.println();
        System.out.println("Usage: miRaw CreateDataSets option configurationFile (parameters)");
        System.out.println("\tOption list:");
        System.out.println("\tBuildUnifiedFile: Creates a file containing the transcripts of the miRNA-mRNa pairs.");
        System.out.println("\tGenerateCandidateSites (typeOfSite): Searches for the candidate sites in a validated targets file. TypeOfSite can be 'positive' 'negative' or 'any'");
        System.out.println("\tUnifyPosNeg: Merges a positive sites and a negative sites file.");
        System.out.println("\tBinarize: Binarizes the candidate sites so they can be feed to a Neural Network.");
        System.out.println("\tGenerateCrossVal: Generates cross validation files so a Neural Network can be trained and tested.");
        System.out.println("\tAll: Performs all the previous steps: "
                + "\n\t\t1. GenerateCandidateSites any "
                + "\n\t\t2. UnifyPosNeg"
                + "\n\t\t3. Binarize"
                + "\n\t\t4. GenerateCrossVal");
        System.out.println("\thelp: Shows a help message");
        System.out.println("\n\nParameters in the configurationFile:");

        System.out.println("\tFor buildUnifiedfile (* means mandatory parameter):\n"
                + "\t\ttarBaseDBFile*:csv from DIANA Tarbase containing a list of positive/negative targets \n"
                + "\t\tFasta3UTR*: fasta file containing the transcripts of the 3'UTR region for all the genes of a given species\n"
                + "\t\tfastaMatureMirna*:asta file (from mirBase) containing the transcripts of mature mirnas\n"
                + "\t\tfastaStemLoopFile: fasta file (from mirBase) containing the estimated stemLoops for mirnas (not used right now)\n"
                + "\t\tSpecies: Species used to generate the unified file (Deault = homo sapiens). \n"
                + "\t\tUnifiedFile*: Location of the file containing the experimentally verified (postitive and negative) miRNa-mRNA targets.\n"
                + "\n"
                + "\tFor generateCandidateSites \n"
                + "\t\tUnifiedFile* \n"
                + "\t\tNegativeCandidatesLocation*: Location where the identified negative sites are (will be) stored\n"
                + "\t\tPositiveCandidatesLocation*: Location where the identified positive sites are (will be) stored\n"
                + "\t\tWindowOffset: minimum separation between sites (default = 5)\n"
                + "\t\tmaxFreeEnergy: maximum free energy in a site. It is a non-positive value. (default = 0)\n"
                + "\t\tPositiveSelectionMethod: method used to determine positive candidates (deault = targetScan)\n"
                + "\t\tPositiveSelectionFile*: location of the file containing the positive candidate sites\n"
                + "\n"
                + "\tFor UnifyPosNeg:\n"
                + "\t\tPositiveCandidatesLocation*\n"
                + "\t\tNegativeCandidatesLocation*\n"
                + "\t\tCompleteCandidateDatasetLocation*: location of the file containing both the positive and negative candiate sites\n"
                + "\n"
                + "\tFor Binarize:\n"
                + "\t\tCompleteCandidateDatasetLocation*\n"
                + "\t\tBinarizedDatasetLocation*: location of the binarized file containing both the positive and negative candiate sites\n"
                + "\n"
                + "\tFor GenerateCrossVal:\n"
                + "\t\tBinarizedDatasetLocation*\n"
                + "\t\tCrossValidation.Folder*: Folder where the cross validation data will be saved.\n"
                + "\t\tCrossValidation.Type: Type of Cross Validation (testtrain, testtrainval, etc. Default = testtrain)\n"
                + "\t\t\n"
                + "\t\tCrossValidation.ClassColumn: Column where the labels are (default 0)\n"
                + "\t\tCrossValidation.TrainProportion: Train proportion for the CV (default = 90)\n"
                + "\t\tCrossValidation.TestProportion: Test proportion for the CV (Default = 10)\n"
                + "\t\tCrossValidation.ValProportion: Validation proportion for the CV (Default = 10. when validation is used, Train is set to 80 by default)");

    }

}
