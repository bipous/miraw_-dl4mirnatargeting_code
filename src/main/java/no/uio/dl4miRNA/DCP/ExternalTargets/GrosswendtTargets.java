/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DCP.ExternalTargets;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;
import no.uio.dl4miRNA.SiteFinder.CandidateSiteFinder;
import no.uio.dl4miRNA.SiteFinder.DnaFoldPrediction;
import no.uio.dl4miRNA.SiteFinder.RNAFolder;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.LoggerFactory;

/**
 *
 * @author apla
 */
public class GrosswendtTargets {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GrosswendtTargets.class);

    /**
     * Checks if the targets determined by ClipSeq can form a folded structure
     * according to the Vienna package. Targets that can form a suboptimal
     * structure where the miRNA seed region is binded to the target site and
     * with negative free energy are saved at the destination File.
     *
     * The folding is done using RNASubfold and it requires that at least four
     * nucleotides of the seed region form a binding with the target site.
     *
     * that can be binded
     *
     * @param originalFile
     * @param destinationFile
     */
    public static void filterTargets(String originalFile, String destinationFile) throws FileNotFoundException, IOException {
        final String[] FILE_HEADER = {"chromosome", "start", "end", "interaction ID",
            "alignment score of target part", "strand", "miRNA ID", "miRNA sequence",
            "target sequence", "number of reads", "number of unique reads",
            "read IDs", "read sequences"};

        FileReader fileReader = new FileReader(originalFile);
        CSVFormat csvFileFormat = CSVFormat.RFC4180.withHeader(FILE_HEADER).withDelimiter(';');
        CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);

        //Create the CSVFormat object with the header mapping
        List<CSVRecord> csvRecords = csvFileParser.getRecords();
        //remove header
        csvRecords.remove(0);

        FileWriter fileWriter = new FileWriter(destinationFile);
        CSVFormat csvFileFormat2 = CSVFormat.RFC4180.withHeader(FILE_HEADER).withDelimiter('\t');
        CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat2);

        FileWriter fileWriter3 = new FileWriter(destinationFile + "_unfoldable.csv");
        CSVFormat csvFileFormat3 = CSVFormat.RFC4180.withHeader(FILE_HEADER).withDelimiter('\t');
        CSVPrinter csvFilePrinter3 = new CSVPrinter(fileWriter3, csvFileFormat3);

        int nTargets = csvRecords.size();
        int evalTargets = 0;

        for (CSVRecord record : csvRecords) {
            DnaFoldPrediction foldPrediction = RNAFolder.getBestFoldWithSeedBinding(record.get("target sequence"), record.get("miRNA sequence"));
            if (foldPrediction == null) {
                log.warn("Site-Target cannot be folded : " + record.get("target sequence") + "-" + record.get("miRNA sequence"));
                csvFilePrinter3.printRecord(record);

            } else if (foldPrediction.deltaG() >= 0) {
                log.warn("Site-Target cannot be folded with negative energy: " + record.get("target sequence") + "-" + record.get("miRNA sequence"));
                csvFilePrinter3.printRecord(record);
            } else {
                csvFilePrinter.printRecord(record);
            }
            if (evalTargets % (nTargets / 100) == 0) {
                double analyzed = evalTargets / (nTargets / 100);
                log.debug(analyzed + "% of targets analyzed (" + evalTargets + ")");
            }
            evalTargets++;
        }
        csvFilePrinter.close();
        csvFileParser.close();
        csvFilePrinter3.close();
        fileWriter.close();
        fileReader.close();
        fileWriter3.close();
    }

    public static void obtainTargetSites(String grosswendtFile, String miRbaseFIle, String outputFile, int targetSiteLength, int seedStartIndex) throws FileNotFoundException, IOException {
        final String[] FILE_HEADER = {"chromosome", "start", "end", "interaction ID",
            "alignment score of target part", "strand", "miRNA ID", "miRNA sequence",
            "target sequence", "number of reads", "number of unique reads",
            "read IDs", "read sequences"};

        final String[] OUTPUT_FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};

        //READING ORIGINAL TARGETS
        FileReader fileReader = new FileReader(grosswendtFile);
        CSVFormat csvFileFormat = CSVFormat.RFC4180.withHeader(FILE_HEADER).withDelimiter('\t');
        CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);

        List<CSVRecord> records = csvFileParser.getRecords();
        //remove header
        records.remove(0);

        //Preparing writer for  ALIGNED TARGETS
        FileWriter fileWriter = new FileWriter(outputFile);
        CSVFormat csvFileFormatOut = CSVFormat.RFC4180.withHeader(OUTPUT_FILE_HEADER).withDelimiter('\t');
        CSVPrinter csvFileWriter = new CSVPrinter(fileWriter, csvFileFormatOut);

        //READ MIRNA TRANSCRIPTS
        HashMap<String, String> mirnaTranscripts = FastaAccess.obtainNamesAndSequences(miRbaseFIle, "hsa");

        int nTargets = records.size();
        int evalTargets = 0;
        
        //for each record
        for (CSVRecord record : records) {
            String chromosome = record.get("chromosome");
            int chrStart = Integer.parseInt(record.get("start"));
            int chrEnd = Integer.parseInt(record.get("end"));
            String siteTranscript = record.get("target sequence");
            String[] miRNAsIDs = record.get("miRNA ID").split(",");
            char strand = record.get("strand").charAt(0);

            //for each mirnaID
            for (String mirnaID : miRNAsIDs) {
                String mirnaIdAux = mirnaID;
                String mirnaTranscript = mirnaTranscripts.get(mirnaID.toLowerCase());
                if (mirnaTranscript == null) {

                    String mirnaID5p = mirnaID + "-5p";
                    String mirnaID3p = mirnaID + "-3p";
                    String grosswendtTranscriptStart = record.get("miRNA sequence").substring(0, 5).replace("T", "U");
                    String transcript3 = mirnaTranscripts.get(mirnaID3p.toLowerCase());
                    String transcript5 = mirnaTranscripts.get(mirnaID5p.toLowerCase());
                    if (transcript3 != null && transcript3.startsWith(grosswendtTranscriptStart)) {
                        mirnaTranscript = transcript3;
                        mirnaIdAux = mirnaID3p;

                    } else if (transcript5 != null && transcript5.startsWith(grosswendtTranscriptStart)) {
                        mirnaTranscript = transcript5;
                        mirnaIdAux = mirnaID5p;

                    } else {
                        //System.out.println(mirnaID + " could not be found in miRbase. Transcript from Grosswend will be used");
                        mirnaTranscript = record.get("miRNA sequence").replace("T", "U").replace("X", "L");
                        //use grosswendt transcript. 
                        //Some grosswendt transcripts have an X meaning it does not matter the nucleotide. 
                        //For that purpose we use "L" as in ViennRNA
                    }
                }
                if (mirnaTranscript != null && siteTranscript != null) {

                    DnaFoldPrediction folding = RNAFolder.getBestFoldWithSeedBinding(siteTranscript, mirnaTranscript);
                    if (RNAFolder.checkBindingFromStructure(folding.structure())) {
                        CandidateMBS mbs = new CandidateMBS();
                        mbs.setChromosome(chromosome);
                        mbs.setChrRegionStart(chrStart);
                        mbs.setChrRegionEnd(chrEnd);
                        mbs.setStrand(strand);
                        mbs.setMatureMirnaTranscript(mirnaTranscript);
                        mbs.setSiteTranscript(siteTranscript);
                        mbs.setSeedRegionStartInSequence(CandidateSiteFinder.findBindingStartInSequence(folding.structure()));
                        mbs.alignAndExtendUsingChromosomeInfo(targetSiteLength, seedStartIndex);
                        int x = 0; //SAVE TRANSCRIPT

                        //"miRNA","gene_name","EnsemblId","mature_miRNA_Transcript","mRNA_Site_Transcript","validation"
                        ArrayList<String> newRecord = new ArrayList<>();
                        newRecord.add(mirnaIdAux);
                        newRecord.add(chromosome + ":" + chrStart + ".." + chrEnd + strand);
                        newRecord.add(chromosome + ":" + chrStart + ".." + chrEnd + strand);
                        newRecord.add(mbs.getMatureMirnaTranscript());
                        newRecord.add(mbs.getSiteTranscript());
                        newRecord.add("" + 1);

                        csvFileWriter.printRecord(newRecord);
                        csvFileWriter.flush();
                    } else {
                        System.out.println("WARNING: NO SUBOPTIMAL FOLDING AVAILABLE");
                    }
                };

            }
            if (evalTargets % (nTargets / 100) == 0) {
                double analyzed = evalTargets / (nTargets / 100);
                log.debug(analyzed + "% of targets analyzed (" + evalTargets + ")");
            }
            evalTargets++;
        }

        csvFileWriter.flush();
        csvFileWriter.close();
        csvFileParser.close();
        fileReader.close();
        fileWriter.close();

    }

    public static void main(String args[]) throws IOException {
        //GrosswendtTargets.filterTargets("/Users/apla/Documents/MirnaTargetDatasets/grosswendt/humanTargets.csv", "/Users/apla/Documents/MirnaTargetDatasets/grosswendt/foldableTargets.csv");
        GrosswendtTargets.obtainTargetSites("/home/albertpp/miRAW/mirnaDatasets/grosswendt/foldableTargets.csv", "/home/albertpp/miRAW/mirnaDatasets/mirBase/20/mature.fa", "/home/albertpp/miRAW/mirnaDatasets/miRAW2/TrainningSites/positiveGrosswendtTrainning.csv", 60, 10);
    }
}
