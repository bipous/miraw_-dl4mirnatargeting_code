/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DCP.ExternalTargets;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;

/**
 *
 * @author apla
 */
public class ChimiricTarget {

    String miRNAName;
    String miRNATranscript;
    String geneName;
    String geneId;
    String geneTranscript;
    String referenceTranscriptDB;
    char type; //'+', '-';

    String upstream;
    String downstream;
    String seed;
    String miRNAFamily;

    /*####################################
    ###############Cosntructor############
    ######################################
     */
    public ChimiricTarget() {

    }

    public ChimiricTarget(String miRNAName, String miRNATranscript, String geneName, String geneId, String geneTranscript, String referenceTranscriptDB, char type, String upstream, String downstream, String seed, String miRNAFamily) {
        this.miRNAName = miRNAName;
        this.miRNATranscript = miRNATranscript;
        this.geneName = geneName;
        this.geneId = geneId;
        this.geneTranscript = geneTranscript;
        this.referenceTranscriptDB = referenceTranscriptDB;
        this.type = type;
        this.upstream = upstream;
        this.downstream = downstream;
        this.seed = seed;
        this.miRNAFamily= miRNAFamily;
    }

    public ChimiricTarget(String miRNAName, String miRNATranscript, String geneName, String geneTranscript, char type) {
        this.miRNAName = miRNAName;
        this.miRNATranscript = miRNATranscript;
        this.geneName = geneName;
        this.geneTranscript = geneTranscript;
        this.type = type;
    }

    /*####################################
    ######### Getters and Setters ########
    ######################################
     */
    public String getMiRNAName() {
        return miRNAName;
    }

    public void setMiRNAName(String miRNAName) {
        this.miRNAName = miRNAName;
    }

    public String getMiRNATranscript() {
        return miRNATranscript;
    }

    public void setMiRNATranscript(String miRNATranscript) {
        this.miRNATranscript = miRNATranscript;
    }

    public String getGeneName() {
        return geneName;
    }

    public void setGeneName(String geneName) {
        this.geneName = geneName;
    }

    public String getGeneId() {
        return geneId;
    }

    public void setGeneId(String geneId) {
        this.geneId = geneId;
    }

    public String getGeneTranscript() {
        return geneTranscript;
    }

    public void setGeneTranscript(String geneTranscript) {
        this.geneTranscript = geneTranscript;
    }

    public String getReferenceTranscriptDB() {
        return referenceTranscriptDB;
    }

    public void setReferenceTranscriptDB(String referenceTranscriptDB) {
        this.referenceTranscriptDB = referenceTranscriptDB;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public String getUpstream() {
        return upstream;
    }

    public void setUpstream(String upstream) {
        this.upstream = upstream;
    }

    public String getDownstream() {
        return downstream;
    }

    public void setDownstream(String downstream) {
        this.downstream = downstream;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    public String getMiRNAFamily() {
        return miRNAFamily;
    }

    public void setMiRNAFamily(String miRNAFamily) {
        this.miRNAFamily = miRNAFamily;
    }
    
    

    /*#####################################
    ######### Load from file methods ########
    ########################################
     */
    public static ArrayList<ChimiricTarget> readFromFasta(String fastaFile, String miRBaseFile) {
        //Load matureMirnaFile in memory
        HashMap<String, String> matureMirnas = FastaAccess.obtainNamesAndSequences(miRBaseFile, null);
        ArrayList<String> targets=new ArrayList<>();
        
       int count=0;
        //Load data from chimric
        HashMap<String, String> chimricData = FastaAccess.obtainDataAndSequences(fastaFile);
        for (String header: chimricData.keySet()){
            String tscpt= chimricData.get(header);
            String[] parts = header.split("\\|");
            String geneName = parts[0];
            String geneID = parts[1];
            String chromosome  = parts[2];
            String chromosomeStrand = parts[3]; //+ are Ok (5' to 3'), - needs to be reversed (they are 3' to 5').
            String seedGenStart  = parts[4];
            String seedGenEnd  = parts[5];
            String idk3  = parts[6];
            String idk4  = parts[7];
            String idk5 = parts[8];
            String mirnaList = parts[9];
            ArrayList<String> miRNAs = new ArrayList<>(Arrays.asList(mirnaList.split(";")));

            for (String miRNA: miRNAs){
                count++;
                
                    targets.add(geneName+"-"+miRNA);
                
            }
        }
        
        
        return null;
    }

    
    
    public static void main(String[] args) throws IOException {
        
        //Runtime rt = Runtime.getRuntime();
        //Process proc = rt.exec("/usr/local/bin/RNAfold AAAAAAAAAAAAALLLLLUUUUUUUUUUUUU");
        
        String chimiricFasta = "/Users/apla/Documents/Code/chimiric/data/hek293_clip_pos_new.66nt.fa";
        String miRBaseFile = "/Users/apla/Documents/MirnaTargetDatasets/mirBase/mature.fa";
        ChimiricTarget.readFromFasta(chimiricFasta, miRBaseFile);
    }


}
