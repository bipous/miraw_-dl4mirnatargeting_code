/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DCP;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import no.uio.dl4miRNA.DCP.Binarization;
import static no.uio.dl4miRNA.DCP.Binarization.splitCSV;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;
import no.uio.dl4miRNA.SiteFinder.CandidateSiteFinder;
import no.uio.dl4miRNA.SiteFinder.DnaFoldPrediction;
import no.uio.dl4miRNA.SiteFinder.RNAFolder;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.LoggerFactory;

/**
 * This class is used to generate datasets considering only sites that have
 * been identified by Grosswendt / Hayek and 
 * that also are present in tarBase or mirTarBase
 * this crossreferencing has been implemented with R due to the need of using
 * biomaRt package. See the "Additional scripts" of the data repository.
 * @author albertpp
 */
public class RobustDataset {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(RobustDataset.class);

    
    
    /**
     * Function that generates a test/train dataset using CLIP and SEQ data 
     * appearing in TarBase or mirTarBase. The training dataset contains ONLY
     * examples of positive and negative targets using the same miRNA.
     * Example: if the positive examples of the train set contain 3 intances of 
     * hsa-miR-21, the negative eamples of the train will also contain 3 instances
     * of hsa-miR-21
     * @param crossReferencedPositiveCasesFile Location of the file containing miRNA targets (Crossreferenced betwen TarBase/mirTarBase/CLASH/CLIP datasets
     * @param negativeCasesFile Location of the file containing negative miRNA targets 
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param destinationFolder destination of the train/test file
     * @param set number (id) of the generated set
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void generatePairWiseTrainingSet(String crossReferencedPositiveCasesFile, String negativeCasesFile, int train, int test, String destinationFolder, int set) throws FileNotFoundException, IOException {
        //change to 
        //   generateRelaxedPairWiseTrainingSet(crossReferencedPositiveCasesFile, negativeCasesFile, train, test, 0, destinationFolder, set);

        //Cases file header
        //final String[] CASE_FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};
        int posCases = 0;
        int negCases = 0;

        //READING ORIGINAL TARGETS
        FileReader positiveFileReader = new FileReader(crossReferencedPositiveCasesFile);
        CSVFormat positvieCsvFileFormat = CSVFormat.RFC4180.withHeader().withDelimiter('\t');
        CSVParser positiveCsvFileParser = new CSVParser(positiveFileReader, positvieCsvFileFormat);
        List<CSVRecord> positiveRecords = positiveCsvFileParser.getRecords();

        //READING ORIGINAL TARGETS
        FileReader negativeFileReader = new FileReader(negativeCasesFile);
        CSVFormat negativeCsvFileFormat = CSVFormat.RFC4180.withHeader().withDelimiter('\t');
        CSVParser negativeCsvFileParser = new CSVParser(negativeFileReader, negativeCsvFileFormat);
        List<CSVRecord> negativeRecords = negativeCsvFileParser.getRecords();

        //List of mirnas appearing on positive case
        HashMap<String, ArrayList<CSVRecord>> positiveMirnas = new HashMap<>();
        //list of mirnas appearing on negativeCases        
        HashMap<String, ArrayList<CSVRecord>> negativeMirnas = new HashMap<>();
        ///List of mirnas appearing in both places        
        HashMap<String, Integer> sharedMirnas = new HashMap<>();
        //List of only positive mirnas
        HashMap<String, Integer> onlyPos = new HashMap<>();
        //list of only negative mirnas
        HashMap<String, Integer> onlyNeg = new HashMap<>();

        for (CSVRecord posRecord : positiveRecords) {
            String miRNA = posRecord.get("mirna");
            if (!positiveMirnas.containsKey(miRNA)) {
                positiveMirnas.put(miRNA, new ArrayList<>());
            }
            positiveMirnas.get(miRNA).add(posRecord);
            posCases++;
        }

        for (CSVRecord negRecord : negativeRecords) {
            String miRNA = negRecord.get("mirna");
            if (!negativeMirnas.containsKey(miRNA)) {
                negativeMirnas.put(miRNA, new ArrayList<>());
            }
            negativeMirnas.get(miRNA).add(negRecord);
            negCases++;
        }

        System.out.println("Positive mirnas");
        for (String pk : positiveMirnas.keySet()) {
            System.out.println(pk);
        }

        System.out.println("Negative mirnas");
        for (String nk : negativeMirnas.keySet()) {
            System.out.println(nk);
        }

        int shared = 0;
        for (String pos : positiveMirnas.keySet()) {
            if (negativeMirnas.containsKey(pos)) {
                sharedMirnas.put(pos, Math.min(negativeMirnas.get(pos).size(), positiveMirnas.get(pos).size()));
                shared = shared + 2 * Math.min(negativeMirnas.get(pos).size(), positiveMirnas.get(pos).size());
            }
        }

        boolean error = false;
        //System.out.println(shared);
        if ((test + train) / 2 > posCases || (test + train) / 2 > negCases) {
            log.error("There are not enough cases in the files. Train:{} Test:{} PositiveCases:{} NegativeCases:{}", train, test, posCases, negCases);
            error = true;
        }

        if (train > shared) {
            log.error("There are not eneugh miRNAs appearing in the positive and in the negative case files to build a pair-wise trainning dataset. Train: {} Shared mirnas: {}", train, shared);
            error = true;
        }

        ArrayList<CSVRecord> testSites = new ArrayList<>();
        ArrayList<CSVRecord> trainSites = new ArrayList<>();
        if (!error) {
            ArrayList<ArrayList<CSVRecord>> sets = selectTestAndTrainPairwiseSites(negativeMirnas, positiveMirnas, sharedMirnas, train, test);
            trainSites = sets.get(0);
            testSites = sets.get(1);
        }

        //Write test and train
        BufferedWriter testWriter = new BufferedWriter(new FileWriter(destinationFolder + "/set_" + set + "_test.csv"));
        CSVPrinter testPrinter = new CSVPrinter(testWriter, CSVFormat.RFC4180.withDelimiter(','));

        BufferedWriter trainWriter = new BufferedWriter(new FileWriter(destinationFolder + "/set_" + set + "_train.csv"));
        CSVPrinter trainPrinter = new CSVPrinter(trainWriter, CSVFormat.RFC4180.withDelimiter(','));

        for (CSVRecord testRecord : testSites) {
            testPrinter.printRecord(testRecord);
            testPrinter.flush();
        }
        for (CSVRecord trainRecord : trainSites) {
            trainPrinter.printRecord(trainRecord);
            trainPrinter.flush();

        }
        trainPrinter.close();
        testPrinter.close();
        new File(destinationFolder + "/binary").mkdir();
        new File(destinationFolder + "/names").mkdir();

        int splitIndex = positiveCsvFileParser.getHeaderMap().size() - 3;

        splitCSV(destinationFolder + "/set_" + set + "_test.csv", splitIndex, ",", destinationFolder + "/binary/set_" + set + "_test.csv", destinationFolder + "/names/set_" + set + "_test.csv");
        splitCSV(destinationFolder + "/set_" + set + "_train.csv", splitIndex, ",", destinationFolder + "/binary/set_" + set + "_train.csv", destinationFolder + "/names/set_" + set + "_train.csv");

    }

    /**
     * Function that generates a test/train dataset using CLIP and SEQ data 
     * appearing in TarBase or mirTarBase. The training dataset contains ONLY
     * examples of positive and negative targets using the same miRNA. 
     * The original dataset from which the Train/test is generated  must be binarized
     * Example: if the positive examples of the train set contain 3 intances of 
     * hsa-miR-21, the negative eamples of the train will also contain 3 instances
     * of hsa-miR-21
     * 
     * @param binarizedFile Location of the  binarized file containing miRNA targets (Crossreferenced betwen TarBase/mirTarBase/CLASH/CLIP datasets
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param destinationFolder destination of the train/test set
     * @param set number (id) of the generated set
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void generatePairWiseTrainingSet(String binarizedFile, int train, int test, String destinationFolder, int set) throws FileNotFoundException, IOException {
        generateRelaxedPairWiseTrainingSet(binarizedFile, train, test, 0, destinationFolder, set);
    }

    /**
     * Generates train and test lists of data containing a "pairwise" mirna
     * relation.
     * @param negativeMirnas List of negative mirnas present in the data.
     * @param positiveMirnas List of positive mirnas present in the data.
     * @param sharedMirnas List of mirnas that are both in the positive and negative list.
     * @param nTrain Number of train cases to generate
     * @param nTest Number of test cases to generate
     * @return Array[0] corresponds to the train, Array[1] test.
     */
    private static ArrayList<ArrayList<CSVRecord>> selectTestAndTrainPairwiseSites(HashMap<String, ArrayList<CSVRecord>> negativeMirnas, HashMap<String, ArrayList<CSVRecord>> positiveMirnas, HashMap<String, Integer> sharedMirnas, int nTrain, int nTest) {
        ArrayList<CSVRecord> trainList = new ArrayList<>();
        ArrayList<CSVRecord> testList = new ArrayList<>();

        int trainAdded = 0;
        int testAdded = 0;

        //create training dataset
        while (trainAdded < nTrain) {
            //only use mirnas that are both in the negative and positive cases
            int ind = (int) Math.floor(Math.random() * sharedMirnas.size());
            String selectedMiRNA = new ArrayList<>(sharedMirnas.keySet()).get(ind);

            int cases = sharedMirnas.get(selectedMiRNA);

            //recover the negative case
            ArrayList<CSVRecord> negativeRecords = negativeMirnas.get(selectedMiRNA);
            CSVRecord negRecord = negativeRecords.remove((int) Math.floor(Math.random() * negativeRecords.size()));    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (negativeRecords.size() > 0) { //if it has content, update
                negativeMirnas.put(selectedMiRNA, negativeRecords);
            } else { //if it is empty, remove reference
                negativeMirnas.remove(selectedMiRNA);
            }

            //recover the positive case                        
            ArrayList<CSVRecord> positiveRecords = positiveMirnas.get(selectedMiRNA);
            CSVRecord posRecord = positiveRecords.remove((int) Math.floor(Math.random() * positiveRecords.size()));    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (positiveRecords.size() > 0) { //if it has content, update
                positiveMirnas.put(selectedMiRNA, positiveRecords);
            } else { //if it is empty, remove reference
                positiveMirnas.remove(selectedMiRNA);
            }

            cases--;
            //update the Shared table mirnas, removing the one that has been used.
            if (cases == 0) {
                sharedMirnas.remove(selectedMiRNA);
            } else {
                sharedMirnas.put(selectedMiRNA, cases);
            }

            trainList.add(posRecord);
            trainAdded++;
            trainList.add(negRecord);
            trainAdded++;
        }

        //create test datset
        while (testAdded < nTest) {
            //pick random positive and random negative mirna
            int indPos = (int) Math.floor(Math.random() * positiveMirnas.size());
            String selectedMiRNA = new ArrayList<>(positiveMirnas.keySet()).get(indPos);

            //recover the positive case                        
            ArrayList<CSVRecord> positiveRecords = positiveMirnas.get(selectedMiRNA);
            CSVRecord posRecord = positiveRecords.remove((int) Math.floor(Math.random() * positiveRecords.size()));    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (positiveRecords.size() > 0) { //if it has content, update
                positiveMirnas.put(selectedMiRNA, positiveRecords);
            } else { //if it is empty, remove reference
                positiveMirnas.remove(selectedMiRNA);
            }
            testList.add(posRecord);
            testAdded++;

            //pick random positive and random negative mirna
            int indNeg = (int) Math.floor(Math.random() * negativeMirnas.size());
            selectedMiRNA = new ArrayList<>(negativeMirnas.keySet()).get(indNeg);

            //recover the negative case                        
            ArrayList<CSVRecord> negativeRecords = negativeMirnas.get(selectedMiRNA);
            int negIndex = (int) Math.floor(Math.random() * negativeRecords.size());
            //log.debug("Index {} Size {}", negIndex, negativeRecords.size());
            CSVRecord negRecord = negativeRecords.remove(negIndex);    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (negativeRecords.size() > 0) { //if it has content, update
                negativeMirnas.put(selectedMiRNA, negativeRecords);
            } else { //if it is empty, remove reference
                negativeMirnas.remove(selectedMiRNA);
            }
            testList.add(negRecord);
            testAdded++;

        }

        ArrayList<ArrayList<CSVRecord>> ret = new ArrayList<>();
        ret.add(trainList);
        ret.add(testList);
        return ret;
    }

    /**
     * Function that generates a relaxed pairwise test/train dataset using CLIP and SEQ data 
     * appearing in TarBase or mirTarBase. A % of the training dataset contains ONLY
     * examples of positive and negative targets using the same miRNA.
     * Example: if the positive examples of the train set contain 3 intances of 
     * hsa-miR-21, the negative eamples of the train will also contain 3 instances
     * of hsa-miR-21
     * @param crossReferencedPositiveCasesFile Location of the file containing miRNA targets (Crossreferenced betwen TarBase/mirTarBase/CLASH/CLIP datasets
     * @param negativeCasesFile Location of the file containing negative miRNA targets 
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param percentagePairWise percentage of the set that needs to be "pairwise"
     * @param destinationFolder destination of the train/test file
     * @param set number (id) of the generated set
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void generateRelaxedPairWiseTrainingSet(String crossReferencedPositiveCasesFile, String negativeCasesFile, int train, int test, float percentagePairWise, String destinationFolder, int set) throws FileNotFoundException, IOException {
        //Cases file header
        //final String[] CASE_FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};
        log.debug("Starting: {} for train, {} for test", train, test);

        int posCases = 0;
        int negCases = 0;

        //READING ORIGINAL TARGETS
        FileReader positiveFileReader = new FileReader(crossReferencedPositiveCasesFile);
        CSVFormat positvieCsvFileFormat = CSVFormat.RFC4180.withHeader().withDelimiter('\t');
        CSVParser positiveCsvFileParser = new CSVParser(positiveFileReader, positvieCsvFileFormat);
        List<CSVRecord> positiveRecords = positiveCsvFileParser.getRecords();

        //READING ORIGINAL TARGETS
        FileReader negativeFileReader = new FileReader(negativeCasesFile);
        CSVFormat negativeCsvFileFormat = CSVFormat.RFC4180.withHeader().withDelimiter('\t');
        CSVParser negativeCsvFileParser = new CSVParser(negativeFileReader, negativeCsvFileFormat);
        List<CSVRecord> negativeRecords = negativeCsvFileParser.getRecords();

        //List of mirnas appearing on positive case
        HashMap<String, ArrayList<CSVRecord>> positiveMirnas = new HashMap<>();
        //list of mirnas appearing on negativeCases        
        HashMap<String, ArrayList<CSVRecord>> negativeMirnas = new HashMap<>();

        for (CSVRecord posRecord : positiveRecords) {
            String miRNA = posRecord.get("mirna");
            if (!positiveMirnas.containsKey(miRNA)) {
                positiveMirnas.put(miRNA, new ArrayList<>());
            }
            positiveMirnas.get(miRNA).add(posRecord);
            posCases++;
        }

        for (CSVRecord negRecord : negativeRecords) {
            String miRNA = negRecord.get("mirna");
            if (!negativeMirnas.containsKey(miRNA)) {
                negativeMirnas.put(miRNA, new ArrayList<>());
            }
            negativeMirnas.get(miRNA).add(negRecord);
            negCases++;
        }

        int splitIndex = positiveCsvFileParser.getHeaderMap().size() - 3;

        //System.out.println("Positive mirnas");
        //   for (String pk : positiveMirnas.keySet()) {
        //     System.out.println(pk);
        //  }
        //    System.out.println("Negative mirnas");
        //  for (String nk : negativeMirnas.keySet()) {
        //      System.out.println(nk);
        //   }
        performRelaxedPairWiseLogic(positiveMirnas, negativeMirnas, posCases, negCases, test, train, percentagePairWise, destinationFolder, set, splitIndex);

    }

    /**
     * Function that generates a relaxed pairwise test/train dataset using a binarized file
     * . A % of the training dataset contains ONLY
     * examples of positive and negative targets using the same miRNA.
     * Example: if the positive examples of the train set contain 3 intances of 
     * hsa-miR-21, the negative eamples of the train will also contain 3 instances
     * of hsa-miR-21
     * @param binarizedFile Location of the file containing miRNA targets (Crossreferenced betwen TarBase/mirTarBase/CLASH/CLIP datasets
     * @param negativeCasesFile Location of the file containing negative miRNA targets 
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param percentagePairWise percentage of the set that needs to be "pairwise"
     * @param destinationFolder destination of the train/test file
     * @param set number (id) of the generated set
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void generateRelaxedPairWiseTrainingSet(String binarizedFile, int train, int test, float percentagePairWise, String destinationFolder, int set) throws FileNotFoundException, IOException {

        //Cases file header
        //final String[] CASE_FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};
        log.debug("Starting: {} for train, {} for test", train, test);

        int posCases = 0;
        int negCases = 0;

        //READING ORIGINAL TARGETS
        FileReader fileReader = new FileReader(binarizedFile);
        CSVFormat csvFileFormat = CSVFormat.RFC4180.withHeader().withDelimiter('\t');
        CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);
        List<CSVRecord> records = csvFileParser.getRecords();

        //List of mirnas appearing on positive case
        HashMap<String, ArrayList<CSVRecord>> positiveMirnas = new HashMap<>();
        //list of mirnas appearing on negativeCases        
        HashMap<String, ArrayList<CSVRecord>> negativeMirnas = new HashMap<>();

        for (CSVRecord record : records) {
            if (record.get("target").equalsIgnoreCase("1")) {
                String miRNA = record.get("mirna");
                if (!positiveMirnas.containsKey(miRNA)) {
                    positiveMirnas.put(miRNA, new ArrayList<>());
                }
                positiveMirnas.get(miRNA).add(record);
                posCases++;
            } else if (record.get("target").equalsIgnoreCase("0")) {
                String miRNA = record.get("mirna");
                if (!negativeMirnas.containsKey(miRNA)) {
                    negativeMirnas.put(miRNA, new ArrayList<>());
                }
                negativeMirnas.get(miRNA).add(record);
                negCases++;
            }
        }
        int splitIndex = csvFileParser.getHeaderMap().size() - 3;

        performRelaxedPairWiseLogic(positiveMirnas, negativeMirnas, posCases, negCases, test, train, percentagePairWise, destinationFolder, set, splitIndex);

    }

    
    protected static void performRelaxedPairWiseLogic(HashMap<String, ArrayList<CSVRecord>> positiveMirnas, HashMap<String, ArrayList<CSVRecord>> negativeMirnas, int posCases, int negCases, int test, int train, float percentagePairWise, String destinationFolder, int set, int splitIndex) throws IOException {
        ///List of mirnas appearing in both places        
        HashMap<String, Integer> sharedMirnas = new HashMap<>();
        //List of only positive mirnas
        HashMap<String, Integer> onlyPos = new HashMap<>();
        //list of only negative mirnas
        HashMap<String, Integer> onlyNeg = new HashMap<>();

        int shared = 0;
        for (String pos : positiveMirnas.keySet()) {
            if (negativeMirnas.containsKey(pos)) {
                sharedMirnas.put(pos, Math.min(negativeMirnas.get(pos).size(), positiveMirnas.get(pos).size()));
                shared = shared + 2 * Math.min(negativeMirnas.get(pos).size(), positiveMirnas.get(pos).size());
            }
        }

        boolean error = false;
        //System.out.println(shared);
        log.debug("{} Shared miRNAS", shared);
        if ((test + train) / 2 > posCases || (test + train) / 2 > negCases) {
            log.error("There are not enough cases in the files. Train:{} Test:{} PositiveCases:{} NegativeCases:{}", train, test, posCases, negCases);
            error = true;
        }

        if ((train - Math.round(train * percentagePairWise / 100)) > shared) {
            log.error("There are not eneugh miRNAs appearing in the positive and in the negative case files to build a pair-wise trainning dataset. Train: {} Shared mirnas: {}", train, shared);
            error = true;
        }

        ArrayList<CSVRecord> testSites = new ArrayList<>();
        ArrayList<CSVRecord> trainSites = new ArrayList<>();
        if (!error) {
            ArrayList<ArrayList<CSVRecord>> sets = selectTestAndTrainRelaxedPairwiseSites(negativeMirnas, positiveMirnas, sharedMirnas, train, test, percentagePairWise);
            trainSites = sets.get(0);
            testSites = sets.get(1);
        }

        //Write test and train
        BufferedWriter testWriter = new BufferedWriter(new FileWriter(destinationFolder + "/set_" + set + "_test.csv"));
        CSVPrinter testPrinter = new CSVPrinter(testWriter, CSVFormat.RFC4180.withDelimiter(','));

        BufferedWriter trainWriter = new BufferedWriter(new FileWriter(destinationFolder + "/set_" + set + "_train.csv"));
        CSVPrinter trainPrinter = new CSVPrinter(trainWriter, CSVFormat.RFC4180.withDelimiter(','));

        for (CSVRecord testRecord : testSites) {
            testPrinter.printRecord(testRecord);
            testPrinter.flush();
        }
        for (CSVRecord trainRecord : trainSites) {
            trainPrinter.printRecord(trainRecord);
            trainPrinter.flush();

        }
        trainPrinter.close();
        testPrinter.close();
        new File(destinationFolder + "/binary").mkdir();
        new File(destinationFolder + "/names").mkdir();

        splitCSV(destinationFolder + "/set_" + set + "_test.csv", splitIndex, ",", destinationFolder + "/binary/set_" + set + "_test.csv", destinationFolder + "/names/set_" + set + "_test.csv");
        splitCSV(destinationFolder + "/set_" + set + "_train.csv", splitIndex, ",", destinationFolder + "/binary/set_" + set + "_train.csv", destinationFolder + "/names/set_" + set + "_train.csv");
    }

    private static ArrayList<ArrayList<CSVRecord>> selectTestAndTrainRelaxedPairwiseSites(HashMap<String, ArrayList<CSVRecord>> negativeMirnas, HashMap<String, ArrayList<CSVRecord>> positiveMirnas, HashMap<String, Integer> sharedMirnas, int nTrain, int nTest, float percentagePairwise) {
        log.debug("Starting: {} for train, {} for test", nTrain, nTest);

        ArrayList<CSVRecord> trainList = new ArrayList<>();
        ArrayList<CSVRecord> testList = new ArrayList<>();

        int trainAdded = 0;
        int testAdded = 0;

        int nPairWise = Math.round(nTrain * percentagePairwise / (float) 100);

        //create training dataset
        while (trainAdded < nTrain & trainAdded < nPairWise) {
            //only use mirnas that are both in the negative and positive cases
            int ind = (int) Math.floor(Math.random() * sharedMirnas.size());
            String selectedMiRNA = new ArrayList<>(sharedMirnas.keySet()).get(ind);

            int cases = sharedMirnas.get(selectedMiRNA);

            //recover the negative case
            ArrayList<CSVRecord> negativeRecords = negativeMirnas.get(selectedMiRNA);
            CSVRecord negRecord = negativeRecords.remove((int) Math.floor(Math.random() * negativeRecords.size()));    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (negativeRecords.size() > 0) { //if it has content, update
                negativeMirnas.put(selectedMiRNA, negativeRecords);
            } else { //if it is empty, remove reference
                negativeMirnas.remove(selectedMiRNA);
            }

            //recover the positive case                        
            ArrayList<CSVRecord> positiveRecords = positiveMirnas.get(selectedMiRNA);
            CSVRecord posRecord = positiveRecords.remove((int) Math.floor(Math.random() * positiveRecords.size()));    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (positiveRecords.size() > 0) { //if it has content, update
                positiveMirnas.put(selectedMiRNA, positiveRecords);
            } else { //if it is empty, remove reference
                positiveMirnas.remove(selectedMiRNA);
            }

            cases--;
            //update the Shared table mirnas, removing the one that has been used.
            if (cases == 0) {
                sharedMirnas.remove(selectedMiRNA);
            } else {
                sharedMirnas.put(selectedMiRNA, cases);
            }

            trainList.add(posRecord);
            trainAdded++;
            trainList.add(negRecord);
            trainAdded++;
        }
        log.debug("{} (pairwise) train cases added", trainAdded);
        while (trainAdded < nTrain) {
            //pick random positive and random negative mirna
            int indPos = (int) Math.floor(Math.random() * positiveMirnas.size());
            String selectedMiRNA = new ArrayList<>(positiveMirnas.keySet()).get(indPos);

            //recover the positive case                        
            ArrayList<CSVRecord> positiveRecords = positiveMirnas.get(selectedMiRNA);
            CSVRecord posRecord = positiveRecords.remove((int) Math.floor(Math.random() * positiveRecords.size()));    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (positiveRecords.size() > 0) { //if it has content, update
                positiveMirnas.put(selectedMiRNA, positiveRecords);
            } else { //if it is empty, remove reference
                positiveMirnas.remove(selectedMiRNA);
            }
            trainList.add(posRecord);
            trainAdded++;

            //pick random positive and random negative mirna
            int indNeg = (int) Math.floor(Math.random() * negativeMirnas.size());
            selectedMiRNA = new ArrayList<>(negativeMirnas.keySet()).get(indNeg);

            //recover the negative case                        
            ArrayList<CSVRecord> negativeRecords = negativeMirnas.get(selectedMiRNA);
            int negIndex = (int) Math.floor(Math.random() * negativeRecords.size());
            //log.debug("Index {} Size {}", negIndex, negativeRecords.size());
            CSVRecord negRecord = negativeRecords.remove(negIndex);    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (negativeRecords.size() > 0) { //if it has content, update
                negativeMirnas.put(selectedMiRNA, negativeRecords);
            } else { //if it is empty, remove reference
                negativeMirnas.remove(selectedMiRNA);
            }
            trainList.add(negRecord);
            trainAdded++;
        }
        log.debug("{} train cases added", trainAdded);

        //create test datset
        while (testAdded < nTest) {
            //pick random positive and random negative mirna
            int indPos = (int) Math.floor(Math.random() * positiveMirnas.size());
            String selectedMiRNA = new ArrayList<>(positiveMirnas.keySet()).get(indPos);

            //recover the positive case                        
            ArrayList<CSVRecord> positiveRecords = positiveMirnas.get(selectedMiRNA);
            CSVRecord posRecord = positiveRecords.remove((int) Math.floor(Math.random() * positiveRecords.size()));    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (positiveRecords.size() > 0) { //if it has content, update
                positiveMirnas.put(selectedMiRNA, positiveRecords);
            } else { //if it is empty, remove reference
                positiveMirnas.remove(selectedMiRNA);
            }
            testList.add(posRecord);
            testAdded++;

            //pick random positive and random negative mirna
            int indNeg = (int) Math.floor(Math.random() * negativeMirnas.size());
            selectedMiRNA = new ArrayList<>(negativeMirnas.keySet()).get(indNeg);

            //recover the negative case                        
            ArrayList<CSVRecord> negativeRecords = negativeMirnas.get(selectedMiRNA);
            int negIndex = (int) Math.floor(Math.random() * negativeRecords.size());
            //log.debug("Index {} Size {}", negIndex, negativeRecords.size());
            CSVRecord negRecord = negativeRecords.remove(negIndex);    //obtain the record, and remove it from the list.
            //return the array to the hash map
            if (negativeRecords.size() > 0) { //if it has content, update
                negativeMirnas.put(selectedMiRNA, negativeRecords);
            } else { //if it is empty, remove reference
                negativeMirnas.remove(selectedMiRNA);
            }
            testList.add(negRecord);
            testAdded++;

        }
        log.debug("{} test cases added", testAdded);

        ArrayList<ArrayList<CSVRecord>> ret = new ArrayList<>();
        ret.add(trainList);
        ret.add(testList);
        return ret;
    }

    /**
     * Generates a binarized file containing validated positive targets (from CLIP data)
     * @param inputGrosswendtValidated Grosswendt parCLIP dataset location
     * @param inputHayekValidated Helwak CLASH dataset location
     * @param mirBaseFile location of mirBaseFile 
     * @param outputFolder location of the output folder
     * @param targetSiteLength maximum length of the cosidered target sites
     * @param seedStartIndex position in the mRNA target site where the seed region
     * starts its binding (from 5'e to 3'e)
     * @throws IOException 
     */
    public static void obtainPositiveSitesFromCrossReferenced(String inputGrosswendtValidated, String inputHayekValidated, String mirBaseFile, String outputFolder, int targetSiteLength, int seedStartIndex) throws IOException {
        obtainAndAlignSitesFromGWCrossReferencedSite(inputGrosswendtValidated, mirBaseFile, outputFolder + "/alignedGrosswendt.csv", targetSiteLength, seedStartIndex);
        obtainAndAlignSitesFromHWCrossReferencedSite(inputHayekValidated, mirBaseFile, outputFolder + "/alignedHayek.csv", targetSiteLength, seedStartIndex);
        Binarization.mergeCSVs(outputFolder + "/alignedGrosswendt.csv", outputFolder + "/alignedHayek.csv", outputFolder + "/alignedPositiveSites.csv");
    }

    /** 
     * Aligns all the validated and cross referenced target sites from a Grosswendt dataset 
     * so they have the starting of the mirna seed binding in the same nucleotided
     * @param crossReferencedFile Grosswednt file location
     * @param miRbaseFIle mirBase file location
     * @param outputFile Where to save the results
     * @param targetSiteLength maximum length of the cosidered target sites
     * @param seedStartIndex position in the mRNA target site where the seed region
     * starts its binding (from 5'e to 3'e)
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private static void obtainAndAlignSitesFromGWCrossReferencedSite(String crossReferencedFile, String miRbaseFIle, String outputFile, int targetSiteLength, int seedStartIndex) throws FileNotFoundException, IOException {
        final String[] FILE_HEADER = {"GeneId", "GeneName", "mirnaName", "species", "targetSequence", "mirnaSequence", "chromosome", "regionStart", "regionEnd", "strand", "entrezGeneId", "dataset", "validationSet"};
        final String[] OUTPUT_FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};

        //READING ORIGINAL TARGETS
        FileReader fileReader = new FileReader(crossReferencedFile);
        CSVFormat csvFileFormat = CSVFormat.RFC4180.withHeader(FILE_HEADER).withDelimiter('\t');
        CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);
        List<CSVRecord> records = csvFileParser.getRecords();
        //remove header
        records.remove(0);

        //Preparing writer for  ALIGNED TARGETS
        FileWriter fileWriter = new FileWriter(outputFile);
        CSVFormat csvFileFormatOut = CSVFormat.RFC4180.withHeader(OUTPUT_FILE_HEADER).withDelimiter('\t');
        CSVPrinter csvFileWriter = new CSVPrinter(fileWriter, csvFileFormatOut);

        //READ MIRNA TRANSCRIPTS from TarBase
        HashMap<String, String> mirnaTranscripts = FastaAccess.obtainNamesAndSequences(miRbaseFIle, "hsa");

        //for each record
        //for each record
        for (CSVRecord record : records) {
            String chromosome = record.get("chromosome");
            int chrStart = Integer.parseInt(record.get("regionStart"));
            int chrEnd = Integer.parseInt(record.get("regionEnd"));
            String siteTranscript = record.get("targetSequence");
            String mirnaID = record.get("mirnaName");
            //remove the * 
            mirnaID = mirnaID.split("\\*")[0];

            char strand = record.get("strand").charAt(0);
            String providedMirnaSequence = record.get("mirnaSequence");

            String mirnaIdAux = mirnaID;

            //if the mirna is in MirBase, we get it from there
            String mirnaTranscript = mirnaTranscripts.get(mirnaID.toLowerCase());

            //if its not in mirBase
            if (mirnaTranscript == null && providedMirnaSequence.length() >= 8) {
                String mirnaID5p = mirnaID + "-5p";
                String mirnaID3p = mirnaID + "-3p";
                String providedTranscriptStart = providedMirnaSequence.substring(0, 7).replace("T", "U"); //we get the beggining of the provided transcript
                String transcript3 = mirnaTranscripts.get(mirnaID3p.toLowerCase()); //we check if mirbase has the 3p transcript
                String transcript5 = mirnaTranscripts.get(mirnaID5p.toLowerCase()); //we check if mirbase has teh 5p transcript

                //check if the provided transcript can be mapped with the 3p or 5p version of the mature mirna
                if (transcript3 != null && transcript3.startsWith(providedTranscriptStart)) {    //matches with 3p
                    mirnaTranscript = transcript3;
                    mirnaIdAux = mirnaID3p;

                } else if (transcript5 != null && transcript5.startsWith(providedTranscriptStart)) {     //matches with 5p
                    mirnaTranscript = transcript5;
                    mirnaIdAux = mirnaID5p;

                } else {
                    //it does not map with mirbase. We use the provided one
                    mirnaTranscript = record.get("mirnaSequence").replace("T", "U").replace("X", "L");
                    //use the provided transcript. 
                    //Some notations have an X meaning it does not matter the nucleotide. 
                    //For that purpose we use "L" as in ViennRNA
                }
            } //now we have obtained the mirna transcript

            //if we have a trasncript and a target site
            if (mirnaTranscript != null && siteTranscript != null) {

                //We make sure that they can bind suboptimally
                DnaFoldPrediction folding = RNAFolder.getBestFoldWithSeedBinding(siteTranscript, mirnaTranscript);
                if (folding != null && RNAFolder.checkBindingFromStructure(folding.structure())) {
                    //Get the position of the seed region
                    int seedStartInBind = CandidateSiteFinder.findBindingStartInSequence(folding.structure());

                    //if they bind, we align and extend (if necessary) the binding site according to targetSiteLength and seed start Index                    
                    CandidateMBS mbs = new CandidateMBS();
                    mbs.setChromosome(chromosome);
                    mbs.setChrRegionStart(chrStart);
                    mbs.setChrRegionEnd(chrEnd);
                    mbs.setStrand(strand);
                    mbs.setMatureMirnaTranscript(mirnaTranscript);
                    mbs.setSiteTranscript(siteTranscript);
                    mbs.setSeedRegionStartInSequence(seedStartInBind);
                    mbs.alignAndExtendUsingChromosomeInfo(targetSiteLength, seedStartIndex);

                    //We save the positive candidate site 
                    //"miRNA","gene_name","EnsemblId","mature_miRNA_Transcript","mRNA_Site_Transcript","validation"
                    ArrayList<String> newRecord = new ArrayList<>();
                    newRecord.add(mirnaIdAux);
                    newRecord.add(record.get("GeneId"));
                    newRecord.add(record.get("GeneName"));
                    newRecord.add(mbs.getMatureMirnaTranscript());
                    newRecord.add(mbs.getSiteTranscript());
                    newRecord.add("" + 1);

                    csvFileWriter.printRecord(newRecord);
                    csvFileWriter.flush();
                } else {
                    System.out.println("WARNING: NO SUBOPTIMAL FOLDING AVAILABLE");
                }
            };
        }

    }

    /** 
     * Aligns all the validated and cross referenced target sites from a Helwak dataset 
     * so they have the starting of the mirna seed binding in the same nucleotided
     * @param crossReferencedFile Grosswednt file location
     * @param miRbaseFIle mirBase file location
     * @param outputFile Where to save the results
     * @param targetSiteLength maximum length of the cosidered target sites
     * @param seedStartIndex position in the mRNA target site where the seed region
     * starts its binding (from 5'e to 3'e)
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private static void obtainAndAlignSitesFromHWCrossReferencedSite(String crossReferencedFile, String miRbaseFIle, String outputFile, int targetSiteLength, int seedStartIndex) throws FileNotFoundException, IOException {
        final String[] FILE_HEADER = {"GeneId", "GeneName", "mirnaName", "species", "targetSequence", "mirnaSequence", "siteStart", "siteEnd", "mRNAFullTranscript", "entrezGeneId", "dataset", "validationSet"};
        final String[] OUTPUT_FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};

        //READING ORIGINAL TARGETS
        FileReader fileReader = new FileReader(crossReferencedFile);
        CSVFormat csvFileFormat = CSVFormat.RFC4180.withHeader(FILE_HEADER).withDelimiter('\t');
        CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);
        List<CSVRecord> records = csvFileParser.getRecords();
        //remove header
        records.remove(0);

        //Preparing writer for  ALIGNED TARGETS
        FileWriter fileWriter = new FileWriter(outputFile);
        CSVFormat csvFileFormatOut = CSVFormat.RFC4180.withHeader(OUTPUT_FILE_HEADER).withDelimiter('\t');
        CSVPrinter csvFileWriter = new CSVPrinter(fileWriter, csvFileFormatOut);

        //READ MIRNA TRANSCRIPTS from TarBase
        HashMap<String, String> mirnaTranscripts = FastaAccess.obtainNamesAndSequences(miRbaseFIle, "hsa");

        //for each record
        //for each record
        for (CSVRecord record : records) {
            if (record.get("siteStart") != null && !record.get("siteStart").equalsIgnoreCase("")) {
                int siteStart = Integer.parseInt(record.get("siteStart")) - 1; //-1 Is because we start counting at 0, not at 1
                int siteEnd = Integer.parseInt(record.get("siteEnd")) - 1; //-1 Is because we start counting at 0, not at 1
                String siteTranscript = record.get("targetSequence");
                String fullTargetTranscript = record.get("mRNAFullTranscript");
                String mirnaID = record.get("mirnaName");
                mirnaID = mirnaID.split("\\*")[0];

                String providedMirnaSequence = record.get("mirnaSequence");

                String mirnaIdAux = mirnaID;

                //if the mirna is in MirBase, we get it from there
                String mirnaTranscript = mirnaTranscripts.get(mirnaID.toLowerCase());

                //if its not in mirBase
                if (mirnaTranscript == null && providedMirnaSequence.length() >= 8) {
                    String mirnaID5p = mirnaID + "-5p";
                    String mirnaID3p = mirnaID + "-3p";
                    String providedTranscriptStart = providedMirnaSequence.substring(0, 7).replace("T", "U"); //we get the beggining of the provided transcript
                    String transcript3 = mirnaTranscripts.get(mirnaID3p.toLowerCase()); //we check if mirbase has the 3p transcript
                    String transcript5 = mirnaTranscripts.get(mirnaID5p.toLowerCase()); //we check if mirbase has teh 5p transcript

                    //check if the provided transcript can be mapped with the 3p or 5p version of the mature mirna
                    if (transcript3 != null && transcript3.startsWith(providedTranscriptStart)) {    //matches with 3p
                        mirnaTranscript = transcript3;
                        mirnaIdAux = mirnaID3p;

                    } else if (transcript5 != null && transcript5.startsWith(providedTranscriptStart)) {     //matches with 5p
                        mirnaTranscript = transcript5;
                        mirnaIdAux = mirnaID5p;

                    } else {
                        //it does not map with mirbase. We use the provided one
                        mirnaTranscript = record.get("mirnaSequence").replace("T", "U").replace("X", "L");
                        //use the provided transcript. 
                        //Some notations have an X meaning it does not matter the nucleotide. 
                        //For that purpose we use "L" as in ViennRNA
                    }
                } //now we have obtained the mirna transcript

                //if we have a trasncript and a target site
                if (mirnaTranscript != null && siteTranscript != null) {

                    if (siteEnd == 6848) {
                        int k = 27;
                    }

                    //We make sure that they can bind suboptimally
                    DnaFoldPrediction folding = RNAFolder.getBestFoldWithSeedBinding(siteTranscript, mirnaTranscript);
                    if (folding != null && RNAFolder.checkBindingFromStructure(folding.structure())) {
                        if (siteStart > fullTargetTranscript.length() || siteEnd > fullTargetTranscript.length()) {
                            log.warn("The recovered mRNA transcript is shorter than expected:");
                            log.warn("SiteStart:{}\t SiteEnd:{} \tTrasncriptLength:{}\tTranscript:{}", siteStart, siteEnd, fullTargetTranscript.length(), record.get("GeneName"));
                        } else {
                            //Get the position of the seed region
                            int seedStartInBind = CandidateSiteFinder.findBindingStartInSequence(folding.structure());

                            //if they bind, we align and extend (if necessary) the binding site according to targetSiteLength and seed start Index                    
                            CandidateMBS mbs = new CandidateMBS();
                            mbs.setMatureMirnaTranscript(mirnaTranscript);
                            mbs.setSiteTranscript(siteTranscript);
                            mbs.setSeedRegionStartInSequence(seedStartInBind);
                            mbs.setFull3UTRTranscript(fullTargetTranscript); //it is not actually the 3'UTR, but as the start and end of the site are referrenced according to this sequence, it will work.
                            mbs.setGene3UTRStart(siteStart);
                            mbs.setGene3UTREnd(siteEnd);
                            mbs.setComment("what is marked as 3UTR is actually the full mRNA trasncript");
                            //log.debug("BEFORE");
                            //log.debug("Gene3UTR start:" + mbs.getGene3UTRStart());
                            //log.debug("Gene3UTR end:" + mbs.getGene3UTREnd());
                            //log.debug("Bind in site:" + mbs.getSeedRegionStartInSequence());
                            //log.debug("Site transcript:" + mbs.getSiteTranscript());
                            mbs.alignAndExtend(fullTargetTranscript, targetSiteLength, seedStartIndex);
                            //log.debug("After");
                            //log.debug("Gene3UTR start:" + mbs.getGene3UTRStart());
                            //log.debug("Gene3UTR end:" + mbs.getGene3UTREnd());
                            //log.debug("Bind in site:" + mbs.getSeedRegionStartInSequence());
                            //log.debug("Site transcript:" + mbs.getSiteTranscript());

                            //We save the positive candidate site 
                            //"miRNA","gene_name","EnsemblId","mature_miRNA_Transcript","mRNA_Site_Transcript","validation"
                            ArrayList<String> newRecord = new ArrayList<>();
                            newRecord.add(mirnaIdAux);
                            newRecord.add(record.get("GeneId"));
                            newRecord.add(record.get("GeneName"));
                            newRecord.add(mbs.getMatureMirnaTranscript());
                            newRecord.add(mbs.getSiteTranscript());
                            newRecord.add("" + 1);

                            csvFileWriter.printRecord(newRecord);
                            csvFileWriter.flush();
                        }
                    } else {
                        System.out.println("WARNING: NO SUBOPTIMAL FOLDING AVAILABLE");
                    }
                };
            }
        }

    }

    /**
     * Generates a stratified cross validation. The training dataset contains similar
     * miRNAS for both the positive and negative data. 
     * @param crossReferencedPositiveCasesFile File containing the positive (aligned) sites
     * @param negativeCasesFile File containing the negative sites
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param nSets number of train/test cases to generate
     * @param destinationFolder
     * @throws IOException 
     */
    public static void generatePairWiseCrossValidation(String crossReferencedPositiveCasesFile, String negativeCasesFile, int train, int test, int nSets, String destinationFolder) throws IOException {
        Binarization.removeUnnecesaryTABFromTSV(crossReferencedPositiveCasesFile);
        Binarization.removeUnnecesaryTABFromTSV(negativeCasesFile);
        File dir = new File(destinationFolder);
        if (!dir.exists()) {
            dir.mkdir();
        }

        for (int set = 0; set < nSets; set++) {
            RobustDataset.generatePairWiseTrainingSet(crossReferencedPositiveCasesFile, negativeCasesFile, train, test, destinationFolder, set);
        }

    }

    /**
     * Generates a stratified cross validation. The training dataset contains similar
     * miRNAS for both the positive and negative data.
     * 
     * @param casesFile location of the original data
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param nSets number of train/test cases to generate
     * @param destinationFolder
     * @throws IOException 
     */
    public static void generatePairWiseCrossValidation(String casesFile, int train, int test, int nSets, String destinationFolder) throws IOException {
        Binarization.removeUnnecesaryTABFromTSV(casesFile);
        File dir = new File(destinationFolder);
        if (!dir.exists()) {
            dir.mkdir();
        }

        for (int set = 0; set < nSets; set++) {
            RobustDataset.generatePairWiseTrainingSet(casesFile, train, test, destinationFolder, set);
        }

    }
    
    /**
     * Generates a stratified cross validation. The training dataset contains similar
     * miRNAS for both the positive and negative data. The stratification  strictness can be graduated [0 to 100].
     * @param crossReferencedPositiveCasesFile File containing the positive (aligned) sites
     * @param negativeCasesFile File containing the negative sites
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param percentagePairwise strictness of the stratification (Percentage [0,100] of cases that will be forced to be stratified). Recommended value = 60.
     * @param nSets number of train/test cases to generate
     * @param destinationFolder 
     * @throws IOException 
     */
    public static void generateRelaxedPairWiseCrossValidation(String crossReferencedPositiveCasesFile, String negativeCasesFile, int train, int test, float percentagePairwise, int nSets, String destinationFolder) throws IOException {
        log.debug("Starting: {} for train, {} for test", train, test);
        Binarization.removeUnnecesaryTABFromTSV(crossReferencedPositiveCasesFile);
        Binarization.removeUnnecesaryTABFromTSV(negativeCasesFile);
        File dir = new File(destinationFolder);
        if (!dir.exists()) {
            dir.mkdir();
        }

        for (int set = 0; set < nSets; set++) {

            RobustDataset.generateRelaxedPairWiseTrainingSet(crossReferencedPositiveCasesFile, negativeCasesFile, train, test, percentagePairwise, destinationFolder, set);
        }

    }

    /**
     * Generates a stratified cross validation. The training dataset contains similar
     * miRNAS for both the positive and negative data. The stratification  strictness can be graduated [0 to 100].
     * @param casesFile file containing the data
     * @param train number of train cases to generate
     * @param test number of test cases to generate
     * @param percentagePairwise strictness of the stratification (Percentage [0,100] of cases that will be forced to be stratified). Recommended value = 60.
     * @param nSets number of train/test cases to generate
     * @param destinationFolder 
     * @throws IOException 
     */
    public static void generateRelaxedPairWiseCrossValidation(String casesFile, int train, int test, float percentagePairwise, int nSets, String destinationFolder) throws IOException {
        log.debug("Starting: {} for train, {} for test", train, test);
        Binarization.removeUnnecesaryTABFromTSV(casesFile);
        File dir = new File(destinationFolder);
        if (!dir.exists()) {
            dir.mkdir();
        }

        for (int set = 0; set < nSets; set++) {
            RobustDataset.generateRelaxedPairWiseTrainingSet(casesFile, train, test, percentagePairwise, destinationFolder, set);
        }

    }

    /** 
     * Generate dataset for PLOScomb article
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        String folderMac = "/Users/apla/Documents/MirnaTargetDatasets/RobustTraining/";
        String folderLinux = "/home/albertpp/miRAW/Oxford";
        String folder = folderLinux;

        //Define auxiliary data locations
        String mirBaseLocation21 = "/home/albertpp/miRAW/mirnaDatasets/mirBase/r21/mature.fa";

        //Define characteristics of the sites 
        int maxSiteLength = 40;
        int seedBindingStart = 35;
        int siteAlignmentOffset = maxSiteLength - seedBindingStart;

        //Define location of positive CLIP / CLASH / Putative sites
        String grosswendtCRef = folder + "/Data/CLASH_CLIP_CrossReferenced/GrosswendtValidated_FirstTarBase.txt";
        String hayekCRef = folder + "/Data/CLASH_CLIP_CrossReferenced/HelwakValidated_FirstDiana_ExtendedInfo.txt";

        //Define miRNA / RNA data location
        String unifiedTrainFile = folder + "/Data/DIANA DataSet/randomTrainSplit.csv";
        String targetScanFile = folder + "/Data/TargetScan Sites/Conserved_Site_Context_Scores.txt";
        String threeUTRFile = "/home/albertpp/miRAW/Oxford/Data/ensembl/87/ensembl_hsa_3utrs.fa";

        //Define outputs
        String putativeValidSites = folder + "/Data/ValidTargetSites/putativePositiveSites.txt";
        String negativeSitesOutput = folder + "/Data/ValidTargetSites/negativeSites.txt";
        String positiveSites = folder + "/Data/ValidTargetSites/positiveSites.txt";
        String allTrainingSites = folder + "/Data/ValidTargetSites/allTrainingSites.txt";
        String allTrainingSitesBinarized = folder + "/Data/ValidTargetSites/allTrainingSitesBinarized.txt";

        //Define location of positive and negative sites
        //Define location of aligned CLIP / CLASH sites
        String outputCLIPFolder = folder + "/Data/AlignedCLASHCLIP";
        File outDir = new File(outputCLIPFolder);
        if (!outDir.exists()) {
            outDir.mkdir();
        }
        log.info("Aligning CLIP/CLASH data");
        //Align and complement information from positive CLIP/CLASH sites                
        obtainAndAlignSitesFromGWCrossReferencedSite(grosswendtCRef, mirBaseLocation21, outputCLIPFolder + "/alignedGrosswendt.csv", maxSiteLength, seedBindingStart);
        
        obtainAndAlignSitesFromHWCrossReferencedSite(hayekCRef, mirBaseLocation21, outputCLIPFolder + "/alignedHayek.csv", maxSiteLength, seedBindingStart);
        log.info("CLIP/CLASH aligned");
        Binarization.mergeCSVs(outputCLIPFolder + "/alignedGrosswendt.csv", outputCLIPFolder + "/alignedHayek.csv", outputCLIPFolder + "/alignedCrossReferencedSites.csv");
        
        
         
        //Generate positive sites from targetScan //todo sembla que hi ha un error amb el metode
        log.info("Obtain putative sites");
        dataConstruction.generateTargetScanMBSfrom3UTRFile(unifiedTrainFile, targetScanFile, threeUTRFile, true, putativeValidSites, true, maxSiteLength, siteAlignmentOffset);
        log.info("Putative sites obtained");
        
        //Generate negative sites 
        int windowOffset = 5;
        int maxFreeEnergy = 0;
        log.info("obtaining negative sites");
        dataConstruction.obtainNegativeMBS(unifiedTrainFile, negativeSitesOutput, windowOffset, maxFreeEnergy, maxSiteLength, siteAlignmentOffset);
        log.info("negative sites obtained");
        
        log.info("merging positive");
        Binarization.mergeCSVs(outputCLIPFolder + "/alignedCrossReferencedSites.csv", putativeValidSites, positiveSites);
        log.info("positive merged");
        log.info("merging positive/negative");
        Binarization.mergeCSVs(positiveSites, negativeSitesOutput, allTrainingSites);
        log.info("merged all");

        //Binarize files and generate cross validation
        log.info("configure binarization");
        Binarization bin = new Binarization();
        //define values for the positive (1) and negative (0) validations
        ArrayList<String> positiveCaseTerms = new ArrayList<>();
        ArrayList<String> negativeCaseTerms = new ArrayList<>();
        positiveCaseTerms.add("1");
        negativeCaseTerms.add("0");
        bin.setMaxSiteLength(maxSiteLength);
        //binarizenegative

        log.info("binarize negative CSV");
        bin.transformCSV2DL4MiRNACSV(allTrainingSites, '\t',
                allTrainingSitesBinarized,
                "validation", positiveCaseTerms, negativeCaseTerms,
                "mature_miRNA_Transcript", "mRNA_Site_Transcript", "EnsemblId",
                "miRNA"); //contains the mirna and gene destinations
                
        
        
        log.info("Generating pairwise dataset");
        int trainCases = 14000;
        int testCases = 3500;
        float pairWisePercentage = 60;
        int nSets = 10;

        RobustDataset.generateRelaxedPairWiseCrossValidation(allTrainingSitesBinarized, trainCases, testCases, pairWisePercentage, nSets, "/home/albertpp/miRAW/Oxford/Data/CrossVal/RelaxedPW50_15K_4K");
        log.info("Generated pairwise dataset");    
    }

}
