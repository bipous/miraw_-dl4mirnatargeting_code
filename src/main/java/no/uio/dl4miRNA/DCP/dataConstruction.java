
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.DCP;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import no.uio.dl4miRNA.SiteFinder.CandidateSiteFinder;
import no.uio.dl4miRNA.SiteFinder.CandidateSiteFinderFactory;
import no.uio.dl4miRNA.Data.gathering.EnsemblAccess;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;
import no.uio.dl4miRNA.SiteFinder.DnaFoldPrediction;
import no.uio.dl4miRNA.SiteFinder.RNAFolder;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author apla
 */
public class dataConstruction {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(dataConstruction.class);

    /**
     * buildUnifiedFiles gathers information from files from different sources
     * and creates a single CSV file containing all the required miRNA/Gene
     * information for miRAW. Note that the resulting file still cannot used to
     * train/test models as it lacks the (candidate) target binding sites. The
     * output file contains the following headers:
     * miRNA","gene_name","EnsemblId","Positive_Negative","Mature_mirna_transcript","3UTR_transcript"
     *
     * The method ignores targets that have missing information (3UTR
     * transcript, gene name or mature mirna transcript)
     *
     * @param tarBaseDBFile csv from DIANA Tarbase containing a list of
     * positive/negative targets
     * @param threeUTRFile fasta file from ensembl containing the transcripts of
     * the 3'UTR region for all the genes of a given species.
     * @param maturemiRNAFile fasta file from mirBase containing the transcripts
     * of mature mirnas
     * @param stemLoopFile fasta file from mirBase containing the estimated
     * stemLoops for mirnas (not used right now)
     * @param species Species of interested. It must match the species defined
     * in Diana Tools (e.g. "Homo sapiens")
     * @param unifiedFileDestination CSV file that will contain the output of
     * the method.
     * @throws IOException if any of the files is not found.
     */
    public static void buildUnifiedFile(String tarBaseDBFile, String threeUTRFile, String maturemiRNAFile, String stemLoopFile, String species, String unifiedFileDestination) throws IOException {
        //Assume we can use huge amount of memory

        //Load matureMirnaFile in memory
        HashMap<String, String> matureMirnas = FastaAccess.obtainNamesAndSequences(maturemiRNAFile, null);

        //Load stemLoopFile in memory -- NOT USED
        //HashMap<String, String> stemLoops = FastaAccess.obtainNamesAndSequences(stemLoopFile, null);
        //Load 3utrfile in memory
        HashMap<String, String> threeUTR = FastaAccess.obtainNamesAndSequences(threeUTRFile, null);

        //Load secondary information from the 3UTRfile (name of gene, 3utr start, 3 utr end)
        HashMap<String, String> ensemble2gene = new HashMap<>();
        HashMap<String, Integer> ensemble2utrStart = new HashMap<>();
        HashMap<String, Integer> ensemble2utrEnd = new HashMap<>();
        ArrayList<ArrayList<String>> threeUTRHeaders = FastaAccess.obtainHeaderParts(threeUTRFile);

        for (ArrayList<String> header : threeUTRHeaders) {
            if (!ensemble2gene.containsKey(header.get(0)) && header.size() > 2) {
                ensemble2gene.put(header.get(0), header.get(2));
                if (header.size() > 4) {
                    try {
                        ensemble2utrStart.put(header.get(0), Integer.parseInt(header.get(3)));
                        ensemble2utrEnd.put(header.get(0), Integer.parseInt(header.get(4)));
                    } catch (NumberFormatException e) {
                        ensemble2utrStart.put(header.get(0), Integer.parseInt(header.get(3).split(";")[0]));
                        ensemble2utrEnd.put(header.get(0), Integer.parseInt(header.get(4).split(";")[0]));
                    } finally {
                    }
                }
            }
        }

        //Go through tarBaseDBFile (Csv parser)
        //Create CSV parser
        BufferedReader csvReader = new BufferedReader(new FileReader(tarBaseDBFile));
        CSVFormat csvFileFormat = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        CSVParser parser = csvFileFormat.parse(csvReader);
        Map<String, Integer> X = parser.getHeaderMap();
        List<CSVRecord> records = parser.getRecords();

        //List of the mirna-targets included in tarbase that will be considered
        //1 = positive
        //0 = negative
        //-1 = inconsistent (Validated as positive and negative) - won't be used.
        HashMap<String, Integer> mirnaTarget = new HashMap<>();

        int rec, neg, pos, inc;
        neg = pos = rec = inc = 0;

        for (CSVRecord csvRecord : records) {

            String geneId = csvRecord.get("geneId");
            String geneName = csvRecord.get("geneName");
            String mirna = csvRecord.get("mirna");
            String targetspecies = csvRecord.get("species");
            String tissue = csvRecord.get("tissue");
            String category = csvRecord.get("category");
            String method = csvRecord.get("method");
            int positive = (csvRecord.get("positive_negative").equalsIgnoreCase("positive")) ? 1 : 0;

            String code = geneId + "|" + mirna;
            if (targetspecies.equalsIgnoreCase(species)) {
                rec++;
                if (mirnaTarget.containsKey(code)) {
                    if (mirnaTarget.get(code) != positive) {
                        mirnaTarget.put(code, -1); //There is an inconsistency in the validation.
                        inc++;
                    }

                } else {
                    mirnaTarget.put(code, positive);
                    if (positive == 0) {
                        neg++;
                    } else {
                        pos++;
                    }

                }
            }

        }

        log.info("DATACONSTRUCTION:buildUnifiedFiles - Parsing complete");
        log.info(String.format("Inside the %d available %s records the following targets have been found:\n", rec, species));
        log.info(String.format("%d positives\t%d negatives\t%d inconsistent\n", pos, neg, inc));

        //Write each "not inconsistent" target to a CSV file (targets with a value of 0 or 1).
        //define header
        String[] FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "Positive_Negative", "Mature_mirna_transcript", "3UTR_transcript"};
        csvFileFormat = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines();

        //initialize file writer
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        fileWriter = new FileWriter(unifiedFileDestination);
        csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

        csvFilePrinter.printRecord(FILE_HEADER);

        //initialize information to generate statistics
        int anomalies = 0;
        int correctTargets = 0;
        inc = neg = pos = 0;
        for (String mirtar : mirnaTarget.keySet()) {
            //if the target is consistent
            int myValidation = mirnaTarget.get(mirtar);
            if (myValidation >= 0) {
                //get gene - microRNA infomration (from memory)            
                String myTranscriptId = mirtar.split(("\\|"))[0];
                String myMirna = mirtar.split(("\\|"))[1];
                String my3utr = threeUTR.get(myTranscriptId);
                String myGeneName = ensemble2gene.get(myTranscriptId);
                String myMatureMirna = matureMirnas.get(myMirna);

                //if there is missing information, the target is ignored
                if (my3utr == null || myGeneName == null || myMatureMirna == null) {
                    //System.out.println("Problem in " + mirtar + ": " + myValidation);
                    //System.out.println(myGeneName + ":" + my3utr);
                    //System.out.println(myMirna + ":" + my3utr);
                    anomalies++;
                } else {
                    //statistics
                    correctTargets++;
                    if (myValidation == 1) {
                        pos++;
                    } else {
                        neg++;
                    }
                    //Add to unified file
                    Object[] targetInfo = {myMirna, myGeneName, myTranscriptId, myValidation,
                        myMatureMirna, my3utr};
                    csvFilePrinter.printRecord(targetInfo);

                }
            } else { //if it is inconsistent
                inc++; //increase inconsistent counter
            }
        }

        //Save and close unified file
        csvFilePrinter.close();
        fileWriter.close();

        log.info("DATACONSTRUCTION:buildUnifiedFiles - Unified file created");
        log.info("Correct targets: " + correctTargets + "\tAnomalies: " + anomalies);
        log.info(String.format("%d positives\t%d negatives\t%d inconsistencies\n", pos, neg, inc));
    }

    public static void buildUnifiedFile(String matureMiRNAFile, String ensemblThreeUTRFile, String unifiedFileDestination, String speciesPrefix) throws IOException {
        HashMap<String, String> threePrimes = FastaAccess.obtainDataAndSequences(ensemblThreeUTRFile);
        HashMap<String, String> mirnas = FastaAccess.obtainNamesAndSequences(matureMiRNAFile, speciesPrefix);

        String[] FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "Positive_Negative", "Mature_mirna_transcript", "3UTR_transcript"};
        CSVFormat csvFileFormat = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines();

        //initialize file writer
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        fileWriter = new FileWriter(unifiedFileDestination);
        csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

        csvFilePrinter.printRecord((Object[]) FILE_HEADER);

        for (String geneHeader : threePrimes.keySet()) {
            String ensemblId = geneHeader.split("\\|")[0];
            String ensemblTransId = geneHeader.split("\\|")[1];
            String ensemblName = geneHeader.split("\\|")[2];

            String ensemblTranscript = threePrimes.get(geneHeader);

            for (String miRNAName : mirnas.keySet()) {
                String miRNATranscript = mirnas.get(miRNAName);
                Object[] targetInfo = {miRNAName, ensemblName, ensemblTransId, "?",
                    miRNATranscript, ensemblTranscript};
                csvFilePrinter.printRecord(targetInfo);
            }
        }
        //Save and close unified file
        csvFilePrinter.close();
        fileWriter.close();

        log.info("DATACONSTRUCTION:buildUnifiedFiles - Unified file created");

    }

    public static void obtainNegativeMBS(String unifiedFile, String outputFile, int windowOffset, float maxFreeEnergy) throws FileNotFoundException, IOException {
        obtainNegativeMBS(unifiedFile, outputFile, windowOffset, maxFreeEnergy, 30, 10);
    }

    /**
     * Generates a file with the transcript of negative Mirna Binding Sites in
     * negative validated miRNA-Gene targets. For each negative validated
     * miRNA-Gene pair in the unified file, the method checks for the candidate
     * areas and save the transcripts to the outputFile.
     *
     * @param unifiedFile CSV file containing information regarding the miRNA,
     * 3UTR region, ensembleID, positive/negative validation
     * @param outputFile CSV file containing the name of the miRNA, the genName,
     * the ensemblID, the miRNA transcript and the transcript of the candidate
     * site.
     * @param windowOffset Number of nucleotides that the sliding windowOffset
     * is advancing after finding a target
     * @param maxFreeEnergy Maximum free energy accepted in a binding site
     * (maximum value = 0)
     */
    public static void obtainNegativeMBS(String unifiedFile, String outputFile, int windowOffset, float maxFreeEnergy, int maxTargetSiteLength, int seedAlignementOffset) throws FileNotFoundException, IOException {
        //Go through unified file (Csv parser)
        //Create CSV parser
        if (maxFreeEnergy > 0) {
            maxFreeEnergy = 0;
        }

        BufferedReader csvReader = new BufferedReader(new FileReader(unifiedFile));
        CSVFormat csvFileFormat = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        CSVParser parser = csvFileFormat.parse(csvReader);
        List<CSVRecord> records = parser.getRecords();

        //Output file
        //initialize file writer
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat2 = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines();
        fileWriter = new FileWriter(outputFile);
        csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat2);
        String[] FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};
        csvFilePrinter.printRecord(FILE_HEADER);

        int count = 0;
        int totalSites = 0;
        int negCount = 0;
        //for each record
        int percentageAux = -1;
        for (CSVRecord csvRecord : records) {
            int candSize = 0;

            //Show progress
            int percentage = Math.round((float) count / (float) records.size() * 100);
            if (percentageAux != percentage) {
                log.info("###############" + percentage + "% accomplished- " + count + "/" + records.size());
            }
            count++;
            percentageAux = percentage;

            //if it is a negative validated case
            if (csvRecord.get("Positive_Negative").equalsIgnoreCase("0")) {
                //obtain candidate binding sites of the miRNA mRNA pair
                String miRNAName = csvRecord.get("miRNA");
                String miRNATranscript = csvRecord.get("Mature_mirna_transcript");
                String mRNA = csvRecord.get("3UTR_transcript");
                String geneName = csvRecord.get("gene_name");
                String ensemblId = csvRecord.get("EnsemblId");

                CandidateSiteFinder siteFinder = CandidateSiteFinderFactory.getSiteFinder("Training");
                siteFinder.setMaxFreeEnergy(maxFreeEnergy);
                siteFinder.setWinStep(windowOffset);
                siteFinder.setMinBindsSeed(5);
                siteFinder.setMaxSiteLength(maxTargetSiteLength);
                siteFinder.setSeedAlignementOffset(seedAlignementOffset);
                ArrayList<CandidateMBS> candidatesList = siteFinder.getCandidateSites(miRNATranscript, mRNA);
                for (CandidateMBS candidateSite : candidatesList) {
                    Object[] candidateRecord = {miRNAName, geneName, ensemblId,
                        miRNATranscript, candidateSite.getSiteTranscript(), "0"};
                    csvFilePrinter.printRecord(candidateRecord);
                }
                candSize = candidatesList.size();
                log.info(count + "/" + records.size() + " --> " + candSize + " candidate sites");
                totalSites = candSize + totalSites;
                csvFilePrinter.flush();
                negCount++;
            }

        }
        float avg = (float) totalSites / (float) negCount;
        log.info(totalSites + " negative sites have been identified. (Approx " + avg + " per miRNA-gene pair");

        csvFilePrinter.close();
        fileWriter.close();
    }

    /**
     * Old version of generateTargetScanMBS that uses Apache CSV to parse the
     * unified file. It collapses if the unified file is too big.
     *
     * @param unifiedFile CSV file containing information regarding the miRNA,
     * 3UTR region, ensembleID, positive/negative validation
     * @param targetScanSourceFile CSV (tab separated) file containing the
     * targetScan targets. Example: Conserved_Site_Context_Scores.txt from
     * targetscan
     * @param positiveNegative defines if it searches for positive or for
     * negative targets (true, positive. false, negative)
     * @param outputFile CSV file containing the name of the miRNA, the genName,
     * the ensemblID, the miRNA transcript and the transcript of the candidate
     * site.
     * @param utrStatic defines if we are assuming that the 3UTR zones of the
     * gene are preserved in the different ensemble regions. If false, when
     * retrieving transcripts from ensembl.org it uses the format
     * ENST.GeneCode.version, if true it uses the format ENST.GeneCode and skips
     * the version.
     * @throws FileNotFoundException
     * @throws IOException
     * @deprecated
     */
    @Deprecated
    public static void generateTargetScanCandidatesBasedOnApacheCSV(String unifiedFile, String targetScanSourceFile, boolean positiveNegative, String outputFile, boolean utrStatic) throws FileNotFoundException, IOException {
        String validation = "";
        if (positiveNegative) {
            validation = "1";
        } else {
            validation = "0";
        }

        //assume that 3UTR does not change in Ensemblversions?
        //boolean utrStatic=false;
        //load information from targetScan into memory
        //Predicted_Targets_Context_Scores.default_predictions.txt
        BufferedReader csvReaderTS = new BufferedReader(new FileReader(targetScanSourceFile));
        CSVFormat csvFileFormatTS = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        CSVParser parserTS = csvFileFormatTS.parse(csvReaderTS);
        List<CSVRecord> recordsTS = parserTS.getRecords();

        //1 pair Mirna-Gene can have multiple sites (in an ArrayList). Each element of the arrayList is a tuple where 0 is the transcriptID and 1 the UTR_start.
        //Hashtag = miRNA|ensemblID (without the .).
        HashMap<String, ArrayList<String[]>> targetScanData = new HashMap<>();

        for (CSVRecord recordTS : recordsTS) {
            String myEnsemblId = recordTS.get("Gene ID").split("\\.")[0];
            String myMirna = recordTS.get("miRNA");
            String myUTRstart = recordTS.get("UTR_start");
            String myTranscriptID = recordTS.get("Transcript ID");
            if (!targetScanData.keySet().contains(myMirna + "|" + myEnsemblId)) {
                targetScanData.put(myMirna + "|" + myEnsemblId, new ArrayList<>());
            }
            String[] tuple = {myTranscriptID, myUTRstart};
            targetScanData.get(myMirna + "|" + myEnsemblId).add(tuple);

        }
        csvReaderTS.close();
        parserTS.close();

        //Create a hasmap to store required trasncripts (maybe we should use 3UTR already available?)
        HashMap<String, String> ensemblTranscript = new HashMap<>();

        //Go through unified file (Csv parser)
        //Create CSV parser
        //CSV parser gets out of memory so we need to go manually line by line.
        BufferedReader br = new BufferedReader(new FileReader(unifiedFile));
        br.readLine();

        //Output file
        //initialize file writer
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat2 = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines();
        fileWriter = new FileWriter(outputFile);
        csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat2);
        String[] FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};
        csvFilePrinter.printRecord(FILE_HEADER);

        int count = 0;
        int countp = 0;
        int sitef = 0;
        int sitenf = 0;
        //for each record in the unified file
        int percentageAux = -1;
        for (String record; (record = br.readLine()) != null;) {
            count++;
            String[] csvRecord = record.split("\t");
            if (csvRecord[3].equalsIgnoreCase(validation)) {
                countp++;
                //obtain candidate binding sites of the miRNA mRNA pair
                String miRNAName = csvRecord[0];
                String miRNATranscript = csvRecord[4];
                //String mRNA = csvRecord.get("3UTR_transcript");
                String geneName = csvRecord[1];
                String ensemblId = csvRecord[2];

                //check if it is in the TargetScanData
                ArrayList<String[]> targetScanPairs = targetScanData.remove(miRNAName + "|" + ensemblId); // .remove --> removes from the hashmap whilst returning the removed object. Good procedure to free memory.
                if (targetScanPairs != null) {
                    for (String[] pair : targetScanPairs) {
                        //if transcript is not already stored, obtain it from ensembl
                        String transcript = null;
                        int utrStart = Integer.parseInt(pair[1]);
                        String transcriptID = pair[0];
                        if (utrStatic) {
                            transcriptID = pair[0].split("\\.")[0];
                        }

                        if (!ensemblTranscript.containsKey(transcriptID)) {
                            //getItfromTheWeb
                            try {
                                transcript = EnsemblAccess.getTranscriptOfEnsembleId(transcriptID);
                            } catch (ParserConfigurationException ex) {
                                Logger.getLogger(dataConstruction.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (MalformedURLException ex) {
                                Logger.getLogger(dataConstruction.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (SAXException ex) {
                                Logger.getLogger(dataConstruction.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception e) {
                                log.info("TRANSCRIPT NOT FOUND:");
                                transcript = null;
                                sitenf++;
                            }
                        } else {
                            transcript = ensemblTranscript.get(transcriptID);
                        }
                        String candidate = null;
                        if (transcript != null) {
                            sitef++;
                            try {
                                candidate = transcript.substring(utrStart - 1, utrStart - 1 + 30);
                            } catch (IndexOutOfBoundsException e) {
                                if (utrStart < transcript.length() && utrStart > 0) {
                                    candidate = transcript.substring(utrStart - 1);
                                }

                            }
                            if (candidate != null) {
                                Object[] candidateRecord = {miRNAName, geneName, ensemblId,
                                    miRNATranscript, candidate, validation};
                                csvFilePrinter.printRecord(candidateRecord);
                                csvFilePrinter.flush();
                            }
                        }

                    }
                }

            }

        }
        csvFilePrinter.close();
        fileWriter.close();

        log.info(countp + "/" + count + " records analyzed");
        log.info((sitef + sitenf) + " candidate sites found in the targetScan file");
        log.info(sitef + " sites considered\t\t" + sitenf + " missed its transcript");
    }

    /**
     * Generates a file with the transcript of MiRNA Binding Sites in validated
     * miRNA-Gene targets using targetscan predictions as a source. When looking
     * for positive sites: For each positive validated miRNA-Gene pair in the
     * unified file, the method uses targetScan to predict probable binding
     * sites. The transcripts are then saved to the outputFile.
     *
     * When looking for negative sites: For each negative validated miRNA-Gene
     * pair in the unified file, the method uses targetScan to see which binding
     * sites where wrongly predicted. The transcripts are then saved to the
     * outputFile and marked as negative.
     *
     * Note that the method connects to the internet to obtain the transcript
     * that was used for the targetScan prediction. This overrides the 3'UTR
     * transcriptions defined in the Unified file
     *
     * @param unifiedFile CSV file containing information regarding the miRNA,
     * 3UTR region, ensembleID, positive/negative validation
     * @param targetScanSourceFile CSV (tab separated) file containing the
     * targetScan targets. Example: Conserved_Site_Context_Scores.txt from
     * targetscan
     * @param positiveNegative defines if it searches for positive or for
     * negative targets (true, positive. false, negative)
     * @param outputFile CSV file containing the name of the miRNA, the genName,
     * the ensemblID, the miRNA transcript and the transcript of the candidate
     * site.
     * @param utrStatic defines if we are assuming that the 3UTR zones of the
     * gene are preserved in the different ensemble regions. If false, when
     * retrieving transcripts from ensembl.org it uses the format
     * ENST.GeneCode.version, if true it uses the format ENST.GeneCode and skips
     * the version.
     * @throws FileNotFoundException
     * @throws IOException
     */
    @Deprecated
    public static void generateTargetScanMBSfromEMBL(String unifiedFile, String targetScanSourceFile, boolean positiveNegative, String outputFile, boolean utrStatic) throws FileNotFoundException, IOException {
        String validation = "";
        if (positiveNegative) {
            validation = "1";
        } else {
            validation = "0";
        }

        int batchSize = 1000000;

        //assume that 3UTR does not change in Ensemblversions
        //boolean utrStatic=false;
        //load information from targetScan into memory
        //Predicted_Targets_Context_Scores.default_predictions.txt
        BufferedReader csvReaderTS = new BufferedReader(new FileReader(targetScanSourceFile));

        CSVFormat csvFileFormatTS = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        //CSVParser parserTS = csvFileFormatTS.parse(csvReaderTS);
        //List<CSVRecord> recordsTS = parserTS.getRecords();

        //Get headers
        ArrayList<String> header = new ArrayList<>(Arrays.asList(csvReaderTS.readLine().split("\t")));
        int geneIDindex = header.indexOf("Gene ID");
        int miRNAindex = header.indexOf("miRNA");
        int utrStartIndex = header.indexOf("UTR_start");
        int transcriptIdIndex = header.indexOf("Transcript ID");

        //Output file
        //initialize file writer
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat2 = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines();
        fileWriter = new FileWriter(outputFile);
        csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat2);
        String[] FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};
        csvFilePrinter.printRecord(FILE_HEADER);

        //STATISTICS DATA
        int count = 0;
        int countp = 0;
        int sitef = 0;
        int sitenf = 0;

        //1 pair Mirna-Gene can have multiple sites (in an ArrayList). Each element of the arrayList is a tuple where 0 is the transcriptID and 1 the UTR_start.
        //Hashtag = miRNA|ensemblID (without the .).
        String csvLine = csvReaderTS.readLine();
        while (csvLine != null) {
            int iBatch = 0;
            HashMap<String, ArrayList<String[]>> targetScanData = new HashMap<>();
            while (csvLine != null && iBatch < batchSize) {

                String fields[] = csvLine.split("\t");

                String myEnsemblId = fields[geneIDindex].split("\\.")[0];
                String myMirna = fields[miRNAindex];
                String myUTRstart = fields[utrStartIndex];
                String myTranscriptID = fields[transcriptIdIndex];
                if (!targetScanData.keySet().contains(myMirna + "|" + myEnsemblId)) {
                    targetScanData.put(myMirna + "|" + myEnsemblId, new ArrayList<>());
                }
                String[] tuple = {myTranscriptID, myUTRstart};
                targetScanData.get(myMirna + "|" + myEnsemblId).add(tuple);
                iBatch++;

                csvLine = csvReaderTS.readLine();
            }

            //Create a hasmap to store required trasncripts 
            HashMap<String, String> ensemblTranscript = new HashMap<>();

            //Go through unified file (Csv parser)
            //Create CSV parser
            //CSV parser gets out of memory so we need to go manually line by line.
            BufferedReader br = new BufferedReader(new FileReader(unifiedFile));
            br.readLine();

            //for each record in the unified file
            int percentageAux = -1;
            for (String record; (record = br.readLine()) != null;) {
                count++;
                String[] csvRecord = record.split("\t");
                if (csvRecord[3].equalsIgnoreCase(validation)) {
                    //obtain candidate binding sites of the miRNA mRNA pair

                    countp++;

                    String miRNAName = csvRecord[0];
                    String miRNATranscript = csvRecord[4];
                    //String mRNA = csvRecord.get("3UTR_transcript"); <-- online transcript is used
                    String geneName = csvRecord[1];
                    String ensemblId = csvRecord[2];

                    //check if the miRNA-gene is in the TargetScanData
                    ArrayList<String[]> targetScanPairs = targetScanData.remove(miRNAName + "|" + ensemblId); // .remove --> removes from the hashmap whilst returning the removed object. Good procedure to free memory.
                    if (targetScanPairs != null) { //is in the data
                        for (String[] pair : targetScanPairs) {

                            //if transcript is not already stored, obtain it from ensembl
                            String transcript = null;

                            String transcriptID = pair[0];
                            if (utrStatic) {
                                transcriptID = pair[0].split("\\.")[0];
                            }

                            if (!ensemblTranscript.containsKey(transcriptID)) {
                                //getItfromTheWeb
                                try {
                                    transcript = EnsemblAccess.getTranscriptOfEnsembleId(transcriptID);

                                } catch (MalformedURLException | ParserConfigurationException | SAXException ex) {
                                    Logger.getLogger(dataConstruction.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (Exception e) {
                                    log.info("EXCEPTION TRANSCRIPT NOT FOUND: " + transcriptID);
                                    transcript = null;
                                    sitenf++;
                                    ensemblTranscript.put(transcriptID, transcript);
                                }
                            } else {
                                transcript = ensemblTranscript.get(transcriptID);
                            }

                            //Transcript has been retrieved from the web, if available.
                            String candidate = null;

                            //if we have the transcript 
                            if (transcript != null) {
                                //use the targetScanData to retrieve the mirna binding site.
                                int utrStart = Integer.parseInt(pair[1]);
                                sitef++;
                                try { //save the MBS and its surroundings (total 30 nucleotides).
                                    candidate = transcript.substring(utrStart - 1, utrStart - 1 + 30);
                                } catch (IndexOutOfBoundsException e) {
                                    if (utrStart < transcript.length() && utrStart > 0) {
                                        candidate = transcript.substring(utrStart - 1);
                                    }

                                }
                                if (candidate != null) { //save the MBS.
                                    Object[] candidateRecord = {miRNAName, geneName, ensemblId,
                                        miRNATranscript, candidate, validation};
                                    csvFilePrinter.printRecord(candidateRecord);
                                    csvFilePrinter.flush();
                                }
                            }

                        }
                    }

                }

            }
        }
        csvFilePrinter.close();
        fileWriter.close();

        csvReaderTS.close();

        log.info(countp + "/" + count + " records analyzed");
        log.info((sitef + sitenf) + " candidate sites found in the targetScan file");
        log.info(sitef + " sites considered\t\t" + sitenf + " missed its transcript");
    }

    public static void generateTargetScanMBSfrom3UTRFile(String unifiedFile, String targetScanSourceFile, String threePrimeUTRFile, boolean positiveNegative, String outputFile, boolean utrStatic) throws FileNotFoundException, IOException {
        generateTargetScanMBSfrom3UTRFile(unifiedFile, targetScanSourceFile, threePrimeUTRFile, positiveNegative, outputFile, utrStatic, 30, 10);
    }

    /**
     * Generates a file with the transcript of MiRNA Binding Sites in validated
     * miRNA-Gene targets using targetscan predictions as a source. When looking
     * for positive sites: For each positive validated miRNA-Gene pair in the
     * unified file, the method uses targetScan to predict probable binding
     * sites. The transcripts are then saved to the outputFile.
     *
     * When looking for negative sites: For each negative validated miRNA-Gene
     * pair in the unified file, the method uses targetScan to see which binding
     * sites where wrongly predicted. The transcripts are then saved to the
     * outputFile and marked as negative.
     *
     * Note that the method connects to the internet to obtain the transcript
     * that was used for the targetScan prediction. This overrides the 3'UTR
     * transcriptions defined in the Unified file
     *
     * @param unifiedFile CSV file containing information regarding the miRNA,
     * 3UTR region, ensembleID, positive/negative validation
     * @param targetScanSourceFile CSV (tab separated) file containing the
     * targetScan targets. Example: Conserved_Site_Context_Scores.txt from
     * targetscan
     * @param positiveNegative defines if it searches for positive or for
     * negative targets (true, positive. false, negative)
     * @param outputFile CSV file containing the name of the miRNA, the genName,
     * the ensemblID, the miRNA transcript and the transcript of the candidate
     * site.
     * @param utrStatic defines if we are assuming that the 3UTR zones of the
     * gene are preserved in the different ensemble regions. If false, when
     * retrieving transcripts from ensembl.org it uses the format
     * ENST.GeneCode.version, if true it uses the format ENST.GeneCode and skips
     * the version.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void generateTargetScanMBSfrom3UTRFile(String unifiedFile, String targetScanSourceFile, String threePrimeUTRFile, boolean positiveNegative, String outputFile, boolean utrStatic, int maxTargetSiteLength, int seedAlignementOffset) throws FileNotFoundException, IOException {
        
        
        
        String validation = "";
        if (positiveNegative) {
            validation = "1";
        } else {
            validation = "0";
        }

        int batchSize = 1000000;

        //assume that 3UTR does not change in Ensemblversions
        //boolean utrStatic=false;
        //load information from targetScan into memory
        //Predicted_Targets_Context_Scores.default_predictions.txt
        BufferedReader csvReaderTS = new BufferedReader(new FileReader(targetScanSourceFile));

        CSVFormat csvFileFormatTS = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        //CSVParser parserTS = csvFileFormatTS.parse(csvReaderTS);
        //List<CSVRecord> recordsTS = parserTS.getRecords();

        //Get headers
        ArrayList<String> header = new ArrayList<>(Arrays.asList(csvReaderTS.readLine().split("\t")));
        int geneIDindex = header.indexOf("Gene ID");
        int miRNAindex = header.indexOf("miRNA");
        int utrStartIndex = header.indexOf("UTR_start");
        if (utrStartIndex < 0) {
            utrStartIndex = header.indexOf("UTR start");
        }
        int utrEndIndex = header.indexOf("UTR end");
        int transcriptIdIndex = header.indexOf("Transcript ID");

        //Output file
        //initialize file writer
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat2 = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines();
        fileWriter = new FileWriter(outputFile);
        csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat2);
        String[] FILE_HEADER = {"miRNA", "gene_name", "EnsemblId", "mature_miRNA_Transcript", "mRNA_Site_Transcript", "validation"};
        csvFilePrinter.printRecord(FILE_HEADER);

        //STATISTICS DATA
        int count = 0;
        int countp = 0;
        int sitef = 0;
        int sitenf = 0;

        //Create a hasmap to store 3'UTR trasncripts 
        //load transcripts from file
        HashMap<String, String> ensemblTranscript = new HashMap<>();
        HashMap<String, String> tempTranscripts = FastaAccess.obtainDataAndSequences(threePrimeUTRFile);

        for (String key : tempTranscripts.keySet()) {
            String tempTranscript = tempTranscripts.get(key);
            String tcId = key.split("\\|")[1];
            ensemblTranscript.put(tcId, tempTranscript);
        }

        //1 pair Mirna-Gene can have multiple sites (in an ArrayList). Each element of the arrayList is a tuple where 0 is the transcriptID and 1 the seed region binding site start at the 3'UTR.
        //Hashtag = miRNA|ensemblID (without the "." in the transcript).
        String csvLine = csvReaderTS.readLine();

        while (csvLine != null) {
            int iBatch = 0;
            HashMap<String, ArrayList<String[]>> targetScanData = new HashMap<>();
            while (csvLine != null && iBatch < batchSize) {

                String fields[] = csvLine.split("\t");

                String myEnsemblId = fields[geneIDindex].split("\\.")[0];
                String myMirna = fields[miRNAindex];
                String myUTRstart = fields[utrStartIndex];
                String myUTRend = fields[utrEndIndex];
                String myTranscriptID = fields[transcriptIdIndex];
                if (!targetScanData.keySet().contains(myMirna + "|" + myEnsemblId)) {
                    targetScanData.put(myMirna + "|" + myEnsemblId, new ArrayList<>());
                }
                String[] tuple = {myTranscriptID, myUTRend};
                targetScanData.get(myMirna + "|" + myEnsemblId).add(tuple);
                iBatch++;

                csvLine = csvReaderTS.readLine();
            }

            //Go through unified file (Csv parser)
            //Create CSV parser
            //CSV parser gets out of memory so we need to go manually line by line.
            BufferedReader br = new BufferedReader(new FileReader(unifiedFile));
            br.readLine();

            //for each record in the unified file
            int percentageAux = -1;
            for (String record; (record = br.readLine()) != null;) {

                count++;

                String[] csvRecord = record.split("\t");
                if (csvRecord[3].equalsIgnoreCase(validation)) {
                    //obtain candidate binding sites of the miRNA mRNA pair

                    countp++;

                    String miRNAName = csvRecord[0];
                    String miRNATranscript = csvRecord[4];
                    //String mRNA = csvRecord.get("3UTR_transcript"); <-- online transcript is used
                    String geneName = csvRecord[1];
                    String ensemblId = csvRecord[2];

                    //check if the miRNA-gene is in the TargetScanData
                    ArrayList<String[]> targetScanPairs = targetScanData.remove(miRNAName + "|" + ensemblId); // .remove --> removes from the hashmap whilst returning the removed object. Good procedure to free memory.
                    if (targetScanPairs != null) { //is in the data
                        for (String[] pair : targetScanPairs) {

                            //if transcript is not already stored, obtain it from ensembl
                            String transcript3UTR = null;
                            String transcriptID = pair[0];
                            if (utrStatic) {
                                transcriptID = transcriptID.split("\\.")[0];
                            }

                            transcript3UTR = ensemblTranscript.get(transcriptID);

                            //Transcript has been retrieved from the document if available
                            String candidateSiteTranscript = null;

                            //if we have the transcript 
                            if (transcript3UTR != null) {
                                //use the targetScanData to retrieve the mirna binding site.
                                int bindingUtrEnd = Integer.parseInt(pair[1]);
                                sitef++;

                                int seedStartIndex = maxTargetSiteLength - seedAlignementOffset;

                                int subSiteStart = bindingUtrEnd + 1 - maxTargetSiteLength + seedAlignementOffset;
                                int subSiteEnd = bindingUtrEnd + 1 + seedAlignementOffset;
                                //log.debug("UTRSize: {}\t bindingUtrEnd: {}\t SiteStart: {}\t SiteEnd: {}", transcript3UTR.length(), bindingUtrEnd, subSiteStart, subSiteEnd);
                                if (subSiteStart < 0) {
                                    subSiteStart = 0;
                                    log.debug("SubsiteStart updated to {}", subSiteStart);
                                };
                                if (subSiteEnd >= transcript3UTR.length()) {
                                    subSiteEnd = bindingUtrEnd - 1;
                                    log.debug("SubsiteEnd updated to {}", subSiteEnd);
                                };

                                try { //save the MBS and its surroundings (total maxTargetSiteLength nucleotides).
                                    candidateSiteTranscript = transcript3UTR.substring(subSiteStart, subSiteEnd);
                                } catch (IndexOutOfBoundsException e) {
                                    if (bindingUtrEnd < transcript3UTR.length()) {
                                        candidateSiteTranscript = transcript3UTR.substring(bindingUtrEnd + 1);
                                    }
                                }
                                if (candidateSiteTranscript != null) { //save the MBS.
                                    //See where the miRNA and the target site fold, and align to the desired offset/siteLength
                                    CandidateMBS site = new CandidateMBS();
                                    site.setMatureMirnaTranscript(miRNATranscript);
                                    site.setSiteTranscript(candidateSiteTranscript);
                                    site.setGene3UTREnd(subSiteEnd);
                                    site.setGene3UTRStart(subSiteStart);
                                    DnaFoldPrediction folding = RNAFolder.getBestFoldWithSeedBinding(candidateSiteTranscript, miRNATranscript);
                                    if (folding != null) {
                                        if (RNAFolder.checkBindingFromStructure(folding.structure())) {
                                            site.setSeedRegionStartInSequence(
                                                    CandidateSiteFinder.findBindingStartInSequence(folding.structure())
                                            );
                                            
                                            site.alignAndExtend(transcript3UTR, maxTargetSiteLength, maxTargetSiteLength-seedAlignementOffset);
                                            
                                        }

                                        Object[] candidateRecord = {miRNAName, geneName, ensemblId,
                                            site.getMatureMirnaTranscript(), site.getSiteTranscript(),
                                            validation};
                                        csvFilePrinter.printRecord(candidateRecord);
                                        csvFilePrinter.flush();                                        
                                    }
                                }
                            }

                        }
                    }

                }

            }

        }
        csvFilePrinter.close();
        fileWriter.close();

        csvReaderTS.close();

        log.info(countp + "/" + count + " records analyzed");
        log.info((sitef + sitenf) + " candidate sites found in the targetScan file");
        log.info(sitef + " sites considered\t\t" + sitenf + " missed its transcript");
    }

    /**
     * Method used during development to test/develop dataConstruction.
     *
     * @param args
     * @throws IOException
     */
    public static void main2(String[] args) throws IOException {

        //FILES TO GENERATE THE DATA
        String tarBaseDBFile = "/Users/apla/Documents/MirnaTargetDatasets/diana tarBase/tarbase_data/tarbase_data.csv";
        String threeUTRFile = "/Users/apla/Documents/MirnaTargetDatasets/ensembl/ensembl_hsa_3utrs.fa";
        String maturemiRNAFile = "/Users/apla/Documents/MirnaTargetDatasets/mirBase/mature.fa";
        String stemLoopFile = "/Users/apla/Documents/MirnaTargetDatasets/mirBase/predictedStemLoops.fa";
        //String unifiedDestination = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/tarBaseValidTranscript.csv";
        String unifiedDestination = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/tarBaseShortUnified.csv";

        //String negativeCandidates = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/RNAFold/negativeCandidates_min6BindsInSeed_SHORT.csv";
        String negativeCandidates = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/RNAFold/negativeCandidates_min6BindsInSeed_SHORT.csv";
        //String negativeCandidatesTS = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/TargetScan ConservedSites/negativeCandidates_TS_STATICUTR.csv";
        //String positiveCandidates = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/TargetScan ConservedSites/positiveTSCandidates_STATICUTR.csv";
        String positiveCandidates = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/TargetScan ConservedSites/positiveTSCandidates_STATICUTR_SHORT.csv";

        String targetScanFile = "/Users/apla/Documents/MirnaTargetDatasets/targetscanData/AllPredictions4RepresentativeScripts/Conserved_Site_Context_Scores.txt";
        //String binarizedDataSet = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/Original/binarizedDataset.csv";
        String binarizedDataSet = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/Original/shortBinarizedDataset.csv";

        long date1 = new Date().getTime();

        //###############################################################################
        //############GENERATE THE CANDIDATE SITES FROM VALIDATED EXPERIMENTS############
        //###############################################################################
        log.info("Create the unified file");
        buildUnifiedFile(tarBaseDBFile, threeUTRFile, maturemiRNAFile, stemLoopFile, "homo sapiens", unifiedDestination);
        log.info("Unified file created");
        //###############################################################################
        //############GENERATE THE CANDIDATE SITES FROM VALIDATED EXPERIMENTS############
        //###############################################################################
        log.info("Generating Candidates");
        //GENERATE NEGATIVE CANDIDATES.
        log.info("Generate negative candidates");
        dataConstruction.obtainNegativeMBS(unifiedDestination, negativeCandidates, 5, -2);
        log.info("negativeCandidates generated");
        //GENERATE POSITIVE CANDIDATES BASED ON TARGETSCAN PREDICTIONS
        log.info("Generate positive candidates with TS");
        dataConstruction.generateTargetScanMBSfromEMBL(unifiedDestination, targetScanFile, true, positiveCandidates, true);
        log.info("Positive TS Candidates generated");
        //GENERATE Negative CANDIDATES BASED ON TARGETSCAN PREDICTIONS  //IGNORED 

        log.info("Generate negative candidates with TS");
        //dataConstruction.generateTargetScanCandidates(unifiedDestination, targetScanFile, false, negativeCandidatesTS, true);
        log.info("Negative TS Candidates generated");
        //#####################################################################################################
        //############GENERATE THE FINAL DATASET, THE BINARIZED FILE FOR D4L & THE CROSSVALIDATIONS############
        //#####################################################################################################
        log.info("Create final dataset:");
        //negative validated sites
        String negativeSitesCsv = negativeCandidates;
        //positive validated sites
        String positiveSitesCsv = positiveCandidates;
        //output files
        //String positiveNegativeCsv = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/Original/positiveNegative.csv";
        String positiveNegativeCsv = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/Original/positiveNegativeSHORT.csv";

        Binarization bin = new Binarization();
        log.info("Create final dataset:");
        log.info("meging");
        Binarization.mergeCSVs(negativeSitesCsv, positiveSitesCsv, positiveNegativeCsv);
        //Binarization.mergeCSVs(positiveNegativeCsv+".aux",negativeCandidatesTS,positiveNegativeCsv);
        bin.setSeparator('\t');

        //define values for the positive (1) and negative (0) validations
        ArrayList<String> positiveCaseTerms = new ArrayList<>();
        ArrayList<String> negativeCaseTerms = new ArrayList<>();
        positiveCaseTerms.add("1");
        negativeCaseTerms.add("0");
        log.info("binarizing:");
        //generate the binarized file
        //bin.transformCSV2DL4MiRNACSV(positiveNegativeCsv,'\t',binarizedDataSet, "validation", positiveCaseTerms, negativeCaseTerms, "mature_miRNA_Transcript", "mRNA_Site_Transcript"); //contains the mirna and gene names
        bin.transformCSV2DL4MiRNACSV(positiveNegativeCsv, '\t', binarizedDataSet, "validation", positiveCaseTerms, negativeCaseTerms, "mature_miRNA_Transcript", "mRNA_Site_Transcript", "EnsemblId", "miRNA"); //contains the mirna and gene names
        log.info("Final dataset generated");
    }

    public static void main3(String[] args) throws IOException {
        String unifiedFile = "/home/albertpp/miRAW/eccb/Repeating 2/DataSet/Original/regularTrainSplit.csv";
        String targetScanSourceFile = "/home/albertpp/miRAW/eccb/Repeating 2/ExternalData/targetscanData/Conserved_Site_Context_Scores.txt";
        String threePrimeUtrfile = "/home/albertpp/miRAW/eccb/Repeating 2/ExternalData/ensembl/ensembl_hsa_3utrs.fa";
        String outputFile = "/home/albertpp/miRAW/eccb/Repeating 2/DataSet/Original/test.csv";
        dataConstruction.generateTargetScanMBSfrom3UTRFile(unifiedFile, targetScanSourceFile, threePrimeUtrfile, true, outputFile, true);
    }

    public static String getCustomTrans() {
        return "CCCAAGACCCACCCGCCTCAGCCCAGCCCAGGCAGCGGGGTGGTGGTTGTGGGAGGTAGAAACCTGTGTGTGGGAGGGGGCCGGAACGGGGAGGGCGAGTGGCCCCCATACTTGCCCTCCCTTGCTCCCCCTCCCTGGCAAACCCTACCCAAAGCCAGTGGGCCCCATTCCTAGGGCTGGGCTCCCCTTCTGGCTCCAGCTTCCCTCCAGCCACTCCCCATTTACCATCAGCTCAGCCCCTGGGAAGGGCGTGGCAGGGGCTCTGCATGCCCGTGACAGTGTTAGGTGTCAGCGCGTGCTACAGTGTTTTTGTGATGTTCTGAACTGCTCCCTTCCCTCCGTTCCTTTCGGACCCTTTTAGCTGGGGTTGGGGGACGGGAAGAGCCGTGCCCCCTTGGGCGCACTCTTCAGCGTCTCCTCCTCCTGCGCCCCCACTGCGTCTGCCCAGGAACAGCATCCTGGGTAGCAGAACAGGAGTCAACCTTGGCGGGGCGGGGGCTGCGTCCAACCTGGAGATTGCCCTTCCCTATGCCACGGTTCCCACCCTCCCTCACCAGTTTGGACAATTTGAAATTACCTATTGCTGCTACTTGTTCTGTCCTCTGACCTTGGGGCAAAGGAGCCCCAGGCCCTGTCTCCCCAGCATCCTCCCTGGTGGCCCTGGGCAGGTGCACTGACACCCCCACCTTCCCATCCCCTGCTGAACCAGGCCCTGTTACACACAGCCGCCTAAGGCCCGCGGCTCATGTGCTGCCCGCCCCCATATTTATTCACTGATAGAGAATCTTGGGGATGCTGGGGTCTGGAGTGAACATCTCCTCCCCTTCATGCCCTAGCCTGTGTTCTAGCTGTCCTGGCGAGACTTCTGTGAGTGAAGAGGAAGGGGTCTCTGGTCAAACCCAGCCCCCAGGGCCTAGGGTTGAAAGCCTTCCCCGGCTCCGGGCATTATTTGGGTTTAATCTCGGAGCCTCACTCCTGGACTGAAGTCCGGTGCCTCTGCCTTATCCCTGGTGGAGATGGAATGTGGCCCATTGCCTCCTCCCTCTCCTGTCAAAAACCCTGATCAGGTAGATTTGGAGGCGGCCACGATTTCCTGTTTGGCCCCTGTTCACCCCAGTGCACTGGCCCTGACTCCAGGCGTGAGTATGGGGAAGGATACGGGTTCTTCTGACGGGGAGCAAGGGCCTCCGTCTTCCCTTCCTTAACTCTCCCCCTTTGCCCTCCGCCCTGAAAAAGGTGTCCTTGAAGTCCCTTCCACCTCTATGCCACTGTCTGCTTAGCCCAGCTCAGGGGTGGGGAAGAGGCGAAAGCGTGGGGGAGGTGAGCGCAGCGGCAGTTCTGCCTCGGAGCTGATTGCAGGGCCCTGTGTGGTCTCCGGACAGCTGCGGGAAGGCTGCCGCAGCTGAAGCTGAAGAGGCGGCTACGTGCGGTTTGTCAGGGGGATTGGGTTGAAAACTGGCCAGTCGGGATGACTGGGTGAAAGAGGAGTAGCTCCTGCCACTGGCGTTTTGAGTGTTGGCAATTTGGGATGCCTCCTGGGGAAGGTTTCCGGGCGTTTGGTGAGTCTCTAGATTTTTCCTTGCTTTCTGTGTTTATTGGTTTTTGATGTTGTAAAAGCAATGAATCCCCTTTACAAGAAAATCGAAAACACAGAAGAATGAAGGACATGCCAGTCCCCGATCGCTGCTGTGAGCACCTCAGTGGCTCCCTCAGACCAGATCCCGTAGGCAGCCCCACAGACCGACCCTGACCCCACTCACAGCCACCCTGAAGATAGACTATAGGAACGGGCCCATACCACACAGACTGCTCTCCAATCCCTGAGTCTCAGATGTTTCATTTATTTCCTACTTTTCCACTACTAAAAAACAGTGTGGAATAGACATTATTGGCAAAATTGCTCATCCCTAATCCTGAAAAACAGGCCAGAATGGGTAAAGACTTGTCAAAGCTTGCAACATAGCTACATGGTGCACCCGGACCTGTACCCCCTCCCCCCAACACAAAACCAGTGTCTGGGAGGTTCATTTTCCTTTAAACTGATCCAGCTGGCCCTGAACCAATTGTTTTTGACTGAGTATCTAGGAGAGCAGTAAGTGGAACTTCAGACAAGCCCACTGGGTCTGGTCCAGGTGAGGGGCAGGGGGCATGGGGCTGGGAGGTCTCAGGGGCCTTCCCTGGGGGTGGCCAGCCTGGTAGGGGGCAGAGAAGGAAAAGCTGAGGGGGGTCCCTGTGAGGGAGGAAAGAAGGATCATTTGCCCCGCTGGGTCTCAAAGGCAGTGAGAAGAGAGCTGAAGAAAGCTCTGGCTGGCTGACAGGATCCCTGTGTTGTAATTGGTCCCTCCTTTCAGCTCTCTAGTGAGATGCCCGTGTCTGTGCGTGTGCGTGTGTGTTTCATACAGCTAGCATTAGATGGGTGATGTTTCTTACTTATCATCCCTAACTATTGCAACTTGACCTTAAAAAGACAAAACCCCACAAAACTCTTCCTGCCACGGGCTTGCAGATTGAAGCACTTTCGATGTTGGGCGCTGGCGTTTGTGTTCTGGGCACCACCGTGACCCTGCCCAGATGGCTATAATATTATTTTATACACAAACCTTTTTTTTCATAAATGTTATAATTTTGTGTCTGTCTTTATAAACTATTATAAGTACTATTTTTGTTATAATTCAAAATAGATATTTAGTATAAAGTTTTTGCTGTTAAATATTTGTTATTTAGTAAAATATGAATTTTGCTCTATTGTAAACATGGTTCAAAATATTAATATGTTTTTATCACAGTCGTTTTAATATTGAAAAAGCACTTGTGTGTTTTGTTTTGATATGAAACTGGTACCGTGTGAGTGTTTTTGCTGTCGTGGTTTTAATCTGTATATAATATTCCATGTTGCATATTAAAAACATGAATGTTGTGCATTTTGTGATTTTGGAAATACTCAATGTGGCTCTTCTATAGGCTTCTAGAATAAACCGTGGGGACCCGC";
    }

    public static void main(String[] args) throws IOException {
        //String unifiedFile ="/home/albertpp/miRAW/eccb/Repeating 2/DataSet/Original/regularTrainSplit.csv";
        //String outputFile ="/home/albertpp/miRAW/mirnaDatasets/miRAW2/TrainningSites/negativeSites.csv";
        //dataConstruction.obtainNegativeMBS(unifiedFile, outputFile, 10, 0, 60, 10);
        String folder = "/home/albertpp/miRAW/mirnaDatasets/miRAW2/TrainningSites/";
        String inputPos = folder + "positiveGrosswendtTrainning.csv";
        String inputNeg = folder + "negativeSites.csv";
        String outputPosNeg = folder + "allSites.csv";

        String outputBin = "/home/albertpp/miRAW/mirnaDatasets/miRAW2/BinarizedSites/BinarizedTranscripts.csv";
        String cvFolderTT = "/home/albertpp/miRAW/mirnaDatasets/miRAW2/CrossVal/TT-80-20";
        String cvFolderTTV = "/home/albertpp/miRAW/mirnaDatasets/miRAW2/CrossVal/TTV-80-10-10";

        //Binarization.mergeCSVs(inputPos, inputNeg, outputPosNeg);
        /*
        Binarization bin = new Binarization();
        bin.setSeparator('\t');

        //size of the transcript
        int maxSiteLength = 60;
        bin.setMaxSiteLength(maxSiteLength);

        //define values for the positive (1) and negative (0) validations
        ArrayList<String> positiveCaseTerms = new ArrayList<>();
        ArrayList<String> negativeCaseTerms = new ArrayList<>();
        positiveCaseTerms.add("1");
        negativeCaseTerms.add("0");

        bin.transformCSV2DL4MiRNACSV(outputPosNeg, '\t', outputBin, "validation", positiveCaseTerms, negativeCaseTerms, "mature_miRNA_Transcript", "mRNA_Site_Transcript", "EnsemblId", "miRNA"); //contains the mirna and gene names
         */
        Binarization.createRSSampling3CVProportion(outputBin, 0, "1", "0", cvFolderTTV + "/Set_", 80, 10, 10, 10, "\t", ",");
        Binarization.createRSSamplingCVProportion(outputBin, 0, "1", "0", cvFolderTT + "/Set_", 80, 20, 10, "\t", ",");

    }

}
