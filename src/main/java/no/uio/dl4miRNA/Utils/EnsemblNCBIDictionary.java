/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.uio.dl4miRNA.DCP.Binarization;
import org.slf4j.LoggerFactory;

/**
 *
 * @author apla
 */
public class EnsemblNCBIDictionary {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(EnsemblNCBIDictionary.class);

    protected HashMap<String, ArrayList<String>> Ensembl2NCBI;
    protected HashMap<String, ArrayList<String>> NCBI2Ensembl;

    private int autoSaveInterval = 999999999;
    private int unsavedWords;

    protected String dictionaryLocations = "./";

    public EnsemblNCBIDictionary() {
        String defaultDictionaryLocation;
        try {
            defaultDictionaryLocation = EnsemblNCBIDictionary.class.getProtectionDomain().getCodeSource().get‌​Location().toURI().getPath();
        } catch (URISyntaxException ex) {

            defaultDictionaryLocation = System.getProperty("user.dir");
            log.warn("Jar location has an expected format, execution directory will be used: " + defaultDictionaryLocation);
        }
        if (!new File(defaultDictionaryLocation).exists()) {
            String unr = defaultDictionaryLocation;
            defaultDictionaryLocation = System.getProperty("user.dir");
            log.warn("Jar location \"" + unr + "\" appears to be unreachable, execution directory will be used: " + defaultDictionaryLocation);
        }
        dictionaryLocations = defaultDictionaryLocation;
        Ensembl2NCBI = new HashMap<>();
        NCBI2Ensembl = new HashMap<>();
        loadDictionaries();
        unsavedWords = 0;
    }
    
    public void clearDictionaries(){
        Ensembl2NCBI = new HashMap<>();
        NCBI2Ensembl = new HashMap<>();
        unsavedWords = 0;
    }

    public void setDictionaryLocations(String location) {
        if (new File(location).exists()) {
            dictionaryLocations = location;
        }
    }

    public String getDictionaryLocations() {
        return dictionaryLocations;
    }

    private void saveDictionaries() throws FileNotFoundException, IOException {
        FileOutputStream fileOut = new FileOutputStream(dictionaryLocations + "ncbi2ensembl.bin");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(this.NCBI2Ensembl);
        out.close();

        FileOutputStream fileOut2 = new FileOutputStream(dictionaryLocations + "ensembl2ncbi.bin");
        ObjectOutputStream out2 = new ObjectOutputStream(fileOut2);
        out2.writeObject(this.Ensembl2NCBI);
        out2.close();
    }

    private void loadDictionaries() {
        NCBI2Ensembl = new HashMap<>();
        Ensembl2NCBI = new HashMap<>();

        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream(dictionaryLocations + "ncbi2ensembl.bin");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            NCBI2Ensembl = (HashMap<String, ArrayList<String>>) in.readObject();
            in.close();
            fileIn.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        fileIn = null;
        try {
            fileIn = new FileInputStream(dictionaryLocations + "ensembl2ncbi.bin");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Ensembl2NCBI = (HashMap<String, ArrayList<String>>) in.readObject();
            in.close();
            fileIn.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void saveDictionariesTo(String folder) throws FileNotFoundException, IOException {
        FileOutputStream fileOut = new FileOutputStream(folder + "/ncbi2ensembl.bin");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(this.NCBI2Ensembl);
        out.close();

        FileOutputStream fileOut2 = new FileOutputStream(folder + "/ensembl2ncbi.bin");
        ObjectOutputStream out2 = new ObjectOutputStream(fileOut2);
        out2.writeObject(this.Ensembl2NCBI);
        out2.close();
    }

    public void loadDictionariesFrom(String folder) {
        NCBI2Ensembl = new HashMap<>();
        Ensembl2NCBI = new HashMap<>();

        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream(folder + "/ncbi2ensembl.bin");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            NCBI2Ensembl = (HashMap<String, ArrayList<String>>) in.readObject();
            in.close();
            fileIn.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        fileIn = null;
        try {
            fileIn = new FileInputStream(folder + "/ensembl2ncbi.bin");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Ensembl2NCBI = (HashMap<String, ArrayList<String>>) in.readObject();
            in.close();
            fileIn.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void putNcbiKey(String ncbiWord, ArrayList<String> EnsemblNames) {
        if (EnsemblNames.size() > 0) {
            this.NCBI2Ensembl.put(ncbiWord, EnsemblNames);

            for (String ensKey : EnsemblNames) {
                if (Ensembl2NCBI.containsKey(ensKey)) {
                    this.Ensembl2NCBI.get(ensKey).add(ncbiWord);
                } else {
                    ArrayList<String> ncbiWords = new ArrayList<>();
                    ncbiWords.add(ncbiWord);
                    this.Ensembl2NCBI.put(ensKey, ncbiWords);
                }
            }

        } else {
            log.info("The list of ensembl names is empty.");
        }

        unsavedWords++;
        if (unsavedWords < this.autoSaveInterval) {
            try {
                this.saveDictionaries();
                unsavedWords = 0;
            } catch (IOException ex) {
                Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void putEnsemblKey(String ensemblWord, ArrayList<String> NcbiNames) {
        if (NcbiNames.size() > 0) {
            this.Ensembl2NCBI.put(ensemblWord, NcbiNames);

            for (String ncbiKey : NcbiNames) {
                if (NCBI2Ensembl.containsKey(ncbiKey)) {
                    this.NCBI2Ensembl.get(ncbiKey).add(ensemblWord);
                } else {
                    ArrayList<String> ensemblWords = new ArrayList<>();
                    ensemblWords.add(ensemblWord);
                    this.NCBI2Ensembl.put(ncbiKey, ensemblWords);
                }
            }

        } else {
            log.info("The list of ensembl names is empty.");
        }
        unsavedWords++;
        if (unsavedWords < this.autoSaveInterval) {
            try {
                this.saveDictionaries();
                unsavedWords = 0;
            } catch (IOException ex) {
                Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void addNcbiKey(String ncbiWord, String ensemblWord) {
        if (this.NCBI2Ensembl.containsKey(ncbiWord)) {
            if (!NCBI2Ensembl.get(ncbiWord).contains(ensemblWord)) {
                NCBI2Ensembl.get(ncbiWord).add(ensemblWord);

                unsavedWords++;
                if (unsavedWords < this.autoSaveInterval) {
                    try {
                        this.saveDictionaries();
                        unsavedWords = 0;
                    } catch (IOException ex) {
                        Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                this.addEnsemblKey(ensemblWord, ncbiWord);
            }
        } else {
            ArrayList<String> ensemblList = new ArrayList<>();
            ensemblList.add(ensemblWord);
            this.putNcbiKey(ncbiWord, ensemblList);
        }
    }

    public void addEnsemblKey(String ensemblWord, String ncbiWord) {
        if (this.Ensembl2NCBI.containsKey(ensemblWord)) {
            if (!Ensembl2NCBI.get(ensemblWord).contains(ncbiWord)) {
                Ensembl2NCBI.get(ensemblWord).add(ncbiWord);

                unsavedWords++;
                if (unsavedWords < this.autoSaveInterval) {
                    try {
                        this.saveDictionaries();
                        unsavedWords = 0;
                    } catch (IOException ex) {
                        Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                this.addNcbiKey(ncbiWord, ensemblWord);
            }
        } else {
            ArrayList<String> ncbiList = new ArrayList<>();
            ncbiList.add(ncbiWord);
            this.putEnsemblKey(ensemblWord, ncbiList);
        }
    }

    public String getEnsembl2Ncbi(String ensemblWord) {
        if (this.Ensembl2NCBI.get(ensemblWord) != null) {
            return this.Ensembl2NCBI.get(ensemblWord).get(0);
        }
        return null;
    }

    public String getNcbi2Ensembl(String ncbiWord) {
        if (this.NCBI2Ensembl.get(ncbiWord) != null) {
            return this.NCBI2Ensembl.get(ncbiWord).get(0);
        }
        return (null);
    }

    public ArrayList<String> getAllEnsembl2NcbiOptions(String ensemblWord) {
        return this.Ensembl2NCBI.get(ensemblWord);
    }

    public ArrayList<String> getAllNcbi2EnsemblOptions(String ncbiWord) {
        return this.NCBI2Ensembl.get(ncbiWord);
    }

    public void loadG2ELookUpTable(String location, String ncbiPrefix, String ensemblPrefix) {
        String nprefix = "", eprefix = "";
        if (ncbiPrefix != null) {
            nprefix = ncbiPrefix;
        }
        if (ensemblPrefix != null) {
            eprefix = ensemblPrefix;
        }

       
        try {
            BufferedReader br = new BufferedReader(new FileReader(location));
            StringBuilder sb = new StringBuilder();
            String[] attributes = br.readLine().split("\t");
            int indGene=0;
            int indEnsembl=0;
            int ind = 0;
            for (String attri:attributes){
                if (attri.equalsIgnoreCase("RNA_nucleotide_accession.version")){
                    indGene=ind;                    
                }
                if (attri.equalsIgnoreCase("Ensembl_gene_identifier")){
                    indEnsembl=ind;                    
                }
                ind++;
            }
            
            String line = br.readLine();
            while (line != null) {
                String[] elements = line.split("\t");
                if (elements[indGene].startsWith(nprefix) && elements[indEnsembl].startsWith(eprefix)){
                    this.addNcbiKey(elements[indGene].split("\\.")[0],elements[indEnsembl]);
                }
                line = br.readLine();
            }
            
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        } 
        try {
            this.saveDictionaries();
        } catch (IOException ex) {
            Logger.getLogger(EnsemblNCBIDictionary.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public static void main2(String args[]) throws IOException {
        EnsemblNCBIDictionary dic = new EnsemblNCBIDictionary();
        System.out.println(dic.dictionaryLocations);

        ArrayList<String> ensemblWords1 = new ArrayList<>();
        ensemblWords1.add("Ens1A");
        ensemblWords1.add("Ens1B");

        String ensemblWord2 = "Ens2A";

        ArrayList<String> ncbiWords2 = new ArrayList<>();
        ncbiWords2.add("Ncbi2A");
        String ncbiWord1 = "Ncbi1A";

        dic.putNcbiKey(ncbiWord1, ensemblWords1);

        dic.putEnsemblKey(ensemblWord2, ncbiWords2);

        dic.saveDictionaries();
    }

    public static void main3(String args[]) throws IOException {
        EnsemblNCBIDictionary dic = new EnsemblNCBIDictionary();
        dic.loadDictionaries();
        System.out.println(dic.getEnsembl2Ncbi("Ens2A"));
        System.out.println(dic.getEnsembl2Ncbi("Ens1A"));
        System.out.println(dic.getAllNcbi2EnsemblOptions("Ncbi1A"));

        dic.addNcbiKey("NCBI3", "ENSEMBL3A");
        System.out.println(dic.getEnsembl2Ncbi("ENSEMBL3A"));
    }
    
    public static void main(String args[]) throws IOException{
        EnsemblNCBIDictionary dic = new EnsemblNCBIDictionary();
        dic.clearDictionaries();
        dic.loadG2ELookUpTable("/Users/apla/Downloads/gene2ensembl", "", "ENSG0");
        new File(dic.dictionaryLocations+"/ensmbl_ncbiTranscript").mkdir();
        dic.saveDictionariesTo(dic.dictionaryLocations+"/ensmbl_ncbiTranscript");
        System.out.println("");
    }

}
