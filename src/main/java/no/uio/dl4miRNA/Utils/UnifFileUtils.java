/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.LoggerFactory;

/**
 * This class contains some adHoc methods that were used to generate some training/testing files
 * for the bioinformatics paper/ECCB conference paper datasets.
 * 
 * It is not actually used in the miRAW core but is kept as it might be useful or
 * might be reused in future.
 * 
 * TODO: move to no.uio.hotchpotch?
 * 
 * @author apla
 */
public class UnifFileUtils {
 private static final org.slf4j.Logger log = LoggerFactory.getLogger(UnifFileUtils.class);
   
    /**
     * Splits a unifiedFile in two, the first file contains {@link proportionA} 
     * of the original data whilst the second contains the (@link proportionB} 
     * of the original data. The ratio between positive and negative instances
     * in the original file is kept in the two new files. The spliting can be
     * done in a sequenetial way (first instances go to first file, while the 
     * lasts to the second), or in a randomized way.
     * 
     * @param unifiedFile location of the original unified file
     * @param proportionA proportion of instances that must go to the first file (0.0,1.0)
     * @param proportionB proportion of instances that must go to the second file (0.0,1.0)
     * @param randomized boolean that defines if the instance selection must 
     * follow a randomized procedure
     * @param outputFileA location of the first output file
     * @param outputFileB location of the second output file
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void splitUnifiedFile(String unifiedFile, double proportionA, double proportionB, boolean randomized, String outputFileA, String outputFileB) throws FileNotFoundException, IOException {
        //reader for the unified file
        BufferedReader csvReader = new BufferedReader(new FileReader(unifiedFile));
        CSVFormat csvFileFormat = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        CSVParser parser = csvFileFormat.parse(csvReader);
        List<CSVRecord> records = parser.getRecords();

        //create output writers, they will have the same header than the unified file
        FileWriter fileWriterA = new FileWriter(outputFileA);
        CSVPrinter csvFilePrinterA = new CSVPrinter(fileWriterA, csvFileFormat);
        csvFilePrinterA.printRecord(parser.getHeaderMap().keySet());

        
        FileWriter fileWriterB = new FileWriter(outputFileB);
        CSVPrinter csvFilePrinterB = new CSVPrinter(fileWriterB, csvFileFormat);
        csvFilePrinterB.printRecord(parser.getHeaderMap().keySet());

        //count number of positive/negative records in the unified file
        int positiveRecords = 0;
        int negativeRecords = 0;
        for (CSVRecord csvRecord : records) {
            if (csvRecord.get("Positive_Negative").equalsIgnoreCase("1")) {
                positiveRecords++;
            } else {
                negativeRecords++;
            }
        }
        
        //computes positive/negative records in the outputfile
        int positivesInFileA = (int) Math.floor(positiveRecords * proportionA);
        int positivesInFileB = (int) Math.floor(positiveRecords * proportionB);

        int negativesInFileA = (int) Math.floor(negativeRecords * proportionA);
        int negativesInFileB = (int) Math.floor(negativeRecords * proportionB);

        //if the selection needs to be randomized, suffle the  instances of the unified file.
        if (randomized) {
            long seed = System.nanoTime();
            Collections.shuffle(records, new Random(seed));
        }

        int curPosA, curPosB, curNegA, curNegB;
        curPosA = curPosB = curNegA = curNegB = 0;

        //save the records to the two new output files.
        for (CSVRecord csvRecord : records) {
            //for positive records
            if (csvRecord.get("Positive_Negative").equalsIgnoreCase("1")) {
                if (curPosA < positivesInFileA) {   //check if it goes to the first file
                    csvFilePrinterA.printRecord(csvRecord);
                    curPosA++;
                } else if (curPosB < positivesInFileB) { //check if it goes to the second
                    csvFilePrinterB.printRecord(csvRecord);
                    curPosB++;
                }
                
            //for negative recordds 
            } else if (curNegA < negativesInFileA) { //check if it goes to first file
                csvFilePrinterA.printRecord(csvRecord);
                curNegA++;
            } else if (curNegB < negativesInFileB) { //chec if it goes to the second
                csvFilePrinterB.printRecord(csvRecord);
                curNegB++;
            }
        }

        log.info("File A: " + curPosA + "+," + curNegA + "-");
        log.info("File B: " + curPosA + "+," + curNegA + "-");

        //close files
        csvFilePrinterA.flush();
        csvFilePrinterB.flush();
        csvFilePrinterA.close();
        csvFilePrinterB.close();

    }

    /**
     * Given a unified file, it generates a new one that contains the same number
     * of positive and negative instances (it is 'leveraged').
     * The new file can be created following a sequential selection (instances 
     * will be selected following a sequential order), or randomized 
     * (the instances will be selected in a random order).
     * 
     * @param unifiedFile location of the original file.
     * @param outputFileA loction of the output leveraged file.
     * @param randomized determines if the selection must be randomized.
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void leverageUnifiedFile(String unifiedFile, String outputFileA, boolean randomized) throws FileNotFoundException, IOException {
        //read original unified file
        BufferedReader csvReader = new BufferedReader(new FileReader(unifiedFile));
        CSVFormat csvFileFormat = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        CSVParser parser = csvFileFormat.parse(csvReader);
        List<CSVRecord> records = parser.getRecords();

        //creates writer for the output file
        FileWriter fileWriterA = new FileWriter(outputFileA);
        CSVPrinter csvFilePrinterA = new CSVPrinter(fileWriterA, csvFileFormat);
        csvFilePrinterA.printRecord(parser.getHeaderMap().keySet());

        //count positive negative records
        int positiveRecords = 0;
        int negativeRecords = 0;
        for (CSVRecord csvRecord : records) {
            if (csvRecord.get("Positive_Negative").equalsIgnoreCase("1")) {
                positiveRecords++;
            } else {
                negativeRecords++;
            }
        }

        //if randomized, shuffle the order in the records in order to ensure
        //the resulting order is random
        if (randomized) {
            long seed = System.nanoTime();
            Collections.shuffle(records, new Random(seed));
        }

        //check if there are less positive or negative cases.
        int minCases = Math.min(positiveRecords, negativeRecords);

        int pos, negs;
        pos = negs = 0;

        //save 'minCases' of each tipe to the new file.
        for (CSVRecord csvRecord : records) {
            if (csvRecord.get("Positive_Negative").equalsIgnoreCase("1")) {
                if (pos < minCases) {
                    csvFilePrinterA.printRecord(csvRecord);
                    pos++;
                }
            } else if (negs < minCases) {
                csvFilePrinterA.printRecord(csvRecord);
                negs++;
            }
        }

        csvFilePrinterA.close();

        log.info("Leveraged File : " + pos + "+," + negs + "-");

    }

    /**
     * Creates a 'reduced version' of a unified file with the desired number of
     * positive and negative cases.
     * @param unifiedFile Locaiton of the original unified file
     * @param outputFileA location of the second unified file
     * @param posCases number of positive instances
     * @param negCases number of negativei nstances
     * @param randomized determines if the selecion of instnaces must be randomized.
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void smallUnifiedFile(String unifiedFile, String outputFileA, int posCases, int negCases, boolean randomized) throws FileNotFoundException, IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader(unifiedFile));
        CSVFormat csvFileFormat = CSVFormat.TDF.withDelimiter('\t').withIgnoreEmptyLines().withHeader();
        CSVParser parser = csvFileFormat.parse(csvReader);
        List<CSVRecord> records = parser.getRecords();

        FileWriter fileWriterA = new FileWriter(outputFileA);
        CSVPrinter csvFilePrinterA = new CSVPrinter(fileWriterA, csvFileFormat);
        csvFilePrinterA.printRecord(parser.getHeaderMap().keySet());

        int positiveRecords = 0;
        int negativeRecords = 0;
        for (CSVRecord csvRecord : records) {
            if (csvRecord.get("Positive_Negative").equalsIgnoreCase("1")) {
                positiveRecords++;
            } else {
                negativeRecords++;
            }
        }
        
        int posAmount = posCases;
        int negAmount = negCases;

        if (positiveRecords <posAmount) {
            posAmount = positiveRecords;
        }
        if (positiveRecords < negAmount) {
            negAmount = negativeRecords;
        }

        if (randomized) {
            long seed = System.nanoTime();
            Collections.shuffle(records, new Random(seed));
        }

        int pos, negs;
        pos = negs = 0;

        for (CSVRecord csvRecord : records) {
            if (csvRecord.get("Positive_Negative").equalsIgnoreCase("1")) {
                if (pos < posAmount) {
                    csvFilePrinterA.printRecord(csvRecord);
                    pos++;
                }
            } else if (negs < negAmount) {
                csvFilePrinterA.printRecord(csvRecord);
                negs++;
            }
        }

        csvFilePrinterA.close();

        log.info("Leveraged File : " + pos + "+," + negs + "-");

    }

    
    public static void main(String[] args) throws IOException {
        /*
        splitUnifiedFile("/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/tarBaseValidTranscript.csv",
                0.50,0.50,
                false,
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/regularTrainSplit.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/regularTestSplit.csv");
        
        splitUnifiedFile("/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/tarBaseValidTranscript.csv",
                0.50,0.50,
                true,
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/randomTrainSplit.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/randomTestSplit.csv");
         
        for  (int i=0;i<10;i++){
        leverageUnifiedFile("/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/randomTestSplit.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/BalancedDataSets/NOPE randomLeveragedTestSplit_"+i+".csv",
                true);
        
       
    }*/
        smallUnifiedFile("/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/randomTestSplit.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/UnbalancedDataSets/unbalancedTestSplit.csv", 16496, 548,
                true);

        /*
        leverageUnifiedFile("/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/regularTestSplit.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/regularLeveragedTestSplit.csv",
                false); 
        leverageUnifiedFile("/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/randomTrainSplit.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/randomLeveragedTrainSplit.csv",
                false);
        leverageUnifiedFile("/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/regularTrainSplit.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/regularLeveragedTrainSplit.csv",
                false);*/
    }

    
}
