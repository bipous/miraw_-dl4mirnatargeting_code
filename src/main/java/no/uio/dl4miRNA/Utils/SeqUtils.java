/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Utils;

import no.uio.dl4miRNA.SiteFinder.RNAFolder;
import no.uio.dl4miRNA.SiteFinder.DnaFoldPrediction;
import java.util.ArrayList;
import java.util.HashMap;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import static org.apache.commons.lang3.CharSetUtils.count;

/**
 *
 * @author apla
 */
public class SeqUtils {
    
    /**
     * This class was used to identify miRNA-mRNA potential MBSs. It is no longer
     * uses as it was replaced by the SiteFinder package. It will be deleted in
     * the short future.
     */

    public SeqUtils() {
    }

    /**
     * Checks if a microRNA can bind to a 30er sequence. Returns a tuple
     * containing the size of the seed region ( 0 if the binding in the seed
     * region is smaller than 6) and the free energy of the binding ( 0 if the
     * binding in the seed region is smaller than 6).
     *
     * @param mirna microRNA sequence
     * @param seqB RNA sequence of the target. Returns false if it's longer than
     * 30
     * @param minBindsSeed minimum number of required linked nucleotides in the
     * seed region
     *
     * @return double[0] is the size of the seed region. double[1] the free
     * energy of the folding.
     */
    public double[] isCandidate(String mirna, String seqB, int minBindsSeed) {
        int Lsize = 7;

        int seedRegionBinds = 0;
        double[] result = {0, 0};

        //if the sequence is too long, returns false
        if (seqB.length() > 30) {
            return result;
        }

        DnaFoldPrediction fold = RNAFolder.foldSeqs(seqB, mirna, Lsize);

        //if the squence cannot be fold returns false
        if (fold.deltaG() == 0) {
            return result;
        }

        //The structure has a size of |seqB|+7+|mirna|
        //the seed region starts at structure.length - |mirna|+1
        int seedStart = fold.structure().length() - mirna.length() + 1;
        int seedIndex = seedStart;
        while (seedIndex < fold.structure().length() && seedIndex < (seedStart + 9)) {
            if (fold.structure().charAt(seedIndex) == ')') {
                seedRegionBinds++;
            }
            seedIndex++;
        };

        if (seedRegionBinds >= minBindsSeed) {
            result[0] = seedRegionBinds;
            result[1] = fold.deltaG();
            //result[1] = lastCharacter(fold.structure(), ')') - Lsize - seqB.length(); //position of the last binding size in the mirna
            //System.out.println(fold.sequence());
            //System.out.println(fold.structure());
            return result;

        }
        return result;
    }

    /**
     * Searches for all the target candidates of a mRNA given a miRNA. Returns a
     * Hashmap<Integer,doulbe[]> where the integer corresponds to the position
     * in the mRNA and the double[] to the size of the seed region [0] and its
     * free energy[1];
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * @param minBindsSeed minimum number of required linked nucleotides in the
     * seed region
     * @return HashMap<Integer, double[]> with all the candidate sites. the
     * index corresponds to the position in the mRNA whilst the double
     * corresponds to the number of pairs in the seed region [0] and the free
     * energy of the site [1]
     */
    @Deprecated
    public HashMap<Integer, double[]> findCandidatesHM(String mirna, String mRNA, int minBindsSeed) {
        //public static ArrayList<CandidateSite> findCandidatesHM(String mirna, String mRNA, int minBindsSeed) {
        HashMap<Integer, double[]> bindings = new HashMap<>();
        for (int i = mRNA.length() - mirna.length(); i >= 0; i--) {
            int bindSize = 30;
            if (mRNA.length() - i < 30) {
                bindSize = mRNA.length() - i;
            }
            double[] num;
            num = isCandidate(mirna, mRNA.substring(i, i + bindSize), minBindsSeed);
            if (num[0] > 0) {
                bindings.put(i, num);
            }
            //i = i - num[1];
        }
        return bindings;
    }

    /**
     * Searches for all the target candidates of a mRNA given a miRNA. Returns a
     * ArrayList<> with the Candidate Sites;
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * @param minBindsSeed minimum number of required linked nucleotides in the
     * seed region
     * @return ArrayList<CandidateSite> with all the candidate sites.
     */
    @Deprecated
    public ArrayList<CandidateMBS> findCandidates(String mirna, String mRNA, int minBindsSeed) {
        ArrayList<CandidateMBS> candidateList = new ArrayList<>();
        for (int i = mRNA.length() - mirna.length(); i >= 0; i--) {
            int bindSize = 30;
            if (mRNA.length() - i < 30) {
                bindSize = mRNA.length() - i;
            }
            double[] num;
            num = isCandidate(mirna, mRNA.substring(i, i + bindSize), minBindsSeed);
            if (num[0] > 0) {
                CandidateMBS cs = new CandidateMBS();

                cs.setBindsInSeed((int) num[0]);
                cs.setFreeEnergy(num[1]);

                cs.setMatureMirnaTranscript(mirna);

                cs.setGene3UTRStart(i);
                cs.setGene3UTREnd(i + bindSize);

                cs.setSiteTranscript(mRNA.substring(i, i + bindSize));

                candidateList.add(cs);
            }

        }
        return candidateList;
    }

    /**
     * Returns the transcript of the candidate sites for a miRNA within a mRNA.
     * Given a miRNA returns the transcripts of the candidate binding sites in a
     * mRNA. The SeqUtils parameter can be used to regulate the number of
     * adjacent candidate sites: only the candidate with the minimum free energy
     * within a window of size winSize is returned.
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * @param maxFreeEnergy maximum free energy that a binding site can have.
     * @param winSize size of the window that will be used to select the
     * candidate with the best free energy. If SeqUtils is higher than the size
     * of the miRNA, it will be changed to the miRNA size.
     * @param minBindsSeed minimum number of required linked nucleotides in the
     * seed region
     * @return ArrayList<String> with the transcript of all the candidate sites.
     */
    @Deprecated
    public ArrayList<String> getCandidateSequences(String mirna, String mRNA, double maxFreeEnergy, int winSize, int minBindsSeed) {
        if (winSize < 0) {
            winSize = 1;
        }
        if (winSize > mirna.length()) {
            winSize = mirna.length();
        }
        HashMap<Integer, double[]> candidates = findCandidatesHM(mirna, mRNA, minBindsSeed);
        ArrayList<String> candidateSequenceTranscripts = new ArrayList<>();
        int currentWindow = 0;
        double minFreeEnergy = 999999;
        int minFreeIndex = 0;
        boolean pending = false;
        for (int ind : candidates.keySet()) {
            double seedSize = candidates.get(ind)[0];
            double freeEnergy = candidates.get(ind)[1];
            if (seedSize > 0 && maxFreeEnergy > freeEnergy) {

                if (freeEnergy < minFreeEnergy) {
                    minFreeEnergy = freeEnergy;
                    minFreeIndex = ind;
                }
                pending = true;
                currentWindow++;
                if (currentWindow == winSize) {
                    if (minFreeIndex < mRNA.length() - 30) {
                        candidateSequenceTranscripts.add(mRNA.substring(minFreeIndex, minFreeIndex + 30));
                    } else {
                        candidateSequenceTranscripts.add(mRNA.substring(minFreeIndex));
                    }
                    //System.out.printf("%04d \t%s \t%f\n", minFreeIndex, candidateSequenceSites.get(candidateSequenceSites.size() - 1), minFreeEnergy);
                    pending = false;
                    minFreeEnergy = 999999;
                    currentWindow = 0;
                }
            } else {

                if (pending) {
                    if (minFreeIndex < mRNA.length() - 30) {
                        candidateSequenceTranscripts.add(mRNA.substring(minFreeIndex, minFreeIndex + 30));
                    } else {
                        candidateSequenceTranscripts.add(mRNA.substring(minFreeIndex));
                    }
                    //System.out.printf("%04d \t%s \t%f\n", minFreeIndex, candidateSequenceSites.get(candidateSequenceSites.size() - 1), minFreeEnergy);
                    pending = false;
                }
                currentWindow = 0;
                minFreeIndex = 0;
                minFreeEnergy = 999999;
            }
        }
        return candidateSequenceTranscripts;
    }

    /**
     * Returns the transcript of the candidate sites in a mRNA given a miRNA.
     * Given a miRNA returns the transcripts of the candidate binding sites in a
     * mRNA. The SeqUtils parameter can be used to regulate the number of
     * adjacent candidate sites: only the candidate with the minimum free energy
     * within a window of size winSize is returned.
     *
     * @param mirna transcript of the miRNA target
     * @param mRNA transcript of the mRNA
     * @param maxFreeEnergy maximum free energy that a binding site can have.
     * @param winSize size of the window that will be used to select the
     * candidate with the best free energy. If winSize is higher than the size
     * of the miRNA, it will be changed to the miRNA size.
     * @param minBindsSeed minimum number of required linked nucleotides in the
     * seed region
     * @return ArrayList<CandidateSite> with the the candidate sites.
     */
    @Deprecated
    public ArrayList<CandidateMBS> getCandidateSites(String mirna, String mRNA, double maxFreeEnergy, int winSize, int minBindsSeed) {
        if (winSize < 0) {
            winSize = 1;
        }
        if (winSize > mirna.length()) {
            winSize = mirna.length();
        }
        //HashMap<Integer, double[]> candidates = findCandidates(mirna, mRNA, minBindsSeed);
        ArrayList<CandidateMBS> candidates = findCandidates(mirna, mRNA, minBindsSeed);

        ArrayList<CandidateMBS> candidateSequenceSites = new ArrayList<>();

        for (CandidateMBS cs : candidates) {
            cs.setMatureMirnaTranscript(mirna);
            int seedSize = cs.getBindsInSeed();
            double freeEnergy = cs.getFreeEnergy();

            if (seedSize > 0 && maxFreeEnergy > freeEnergy) {
                if (candidateSequenceSites.isEmpty()) {
                    candidateSequenceSites.add(cs);
                } else {
                    CandidateMBS tempCS = candidateSequenceSites.get(candidateSequenceSites.size() - 1);
                    if (tempCS.getGene3UTRStart() - cs.getGene3UTRStart() < winSize) {
                        if (tempCS.getFreeEnergy() > cs.getFreeEnergy()) {
                            candidateSequenceSites.remove(candidateSequenceSites.size() - 1);
                            candidateSequenceSites.add(cs);
                        }
                    } else {
                        candidateSequenceSites.add(cs);
                    }
                }

                //System.out.printf("%04d \t%s \t%f\n", minFreeIndex, candidateSequenceSites.get(candidateSequenceSites.size() - 1), minFreeEnergy);                    
            }
        }
        return candidateSequenceSites;
    }

    /**
     * Checks if two sequences bind together
     *
     * @param seqA
     * @param seqB
     * @return true if they bind.
     */
    public boolean checkBinding(String seqA, String seqB) {
        //Returns true if the freeenergy of the folding is lower than 0.
        return RNAFolder.checkBinding(seqA, seqB);
    }

    /**
     * Counts the number of links in a binding between two sequences
     *
     * @param seqA
     * @param seqB
     * @return int number of pairs in a candidate site
     */
    public int countPairs(String seqA, String seqB) {
        DnaFoldPrediction res = RNAFolder.foldSeqs(seqA, seqB);
        return count(res.structure(), "({[");
    }

    public static void main(String[] args) {
        /*
        try {
        foldSeq("AAAGCTALLLLGCGATU");
        } catch (IOException ex) {
        Logger.getLogger(SeqUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
        Logger.getLogger(SeqUtils.class.getName()).log(Level.SEVERE, null, ex);
        }*//*
        long date = new Date().getTime();
        DnaFoldPrediction result = SeqUtils.foldSeqs("GUACGCCCG", "CGGCCUAG");
        result = SeqUtils.foldSeqs("GUACGCCCG", "CGGCCUAG");

        long date2 = new Date().getTime();
        System.out.println("Computed in:" + (date2 - date) + " miliseconds");
        System.out.println(result.sequence());
        System.out.println(result.structure());
        System.out.println(result.deltaG());

        System.out.println(countPairs("GUACGCCGGUACGCCGGGAAGAFGGAAGAF", "CGGCCUAGCGGGUACGCCGGGAAGAFCCUAG"));

        DnaFoldPrediction x = foldSeqs("GUACGCCGGUACGCCGGGAAGAFGGAAGAF", "CGGCCUAGCGGGUACGCCGGGAAGAFCCUAG");
        System.out.println(x.sequence());
        System.out.println(x.structure());

        System.out.println(count(x.structure(), "({["));*/

 /*long date = new Date().getTime();
        findCandidates("UAAAUCCCAUGGUGCCUUCUCCU", "AGACCCAAGAAAACCTGGAACTTTGGATCAATTTCTTTTTCATAGGGGTGGAACTTGCACAGCAAAAACAAACAAACGCAAGAAGAGATTTGGGCTTTAACACACTGGGTACTTTGTGGGTCTCTCTTTCGTCGGTGGCTTAAAGTAACATCTATTTCCATTGATCCTAGGTTCTTCCTGACTGCTTTCTCCAACTGTTCACAGCAAATGCTTGGATTTTATGCAGTAGGCATTACTACAGTACATGGCTAATCTTCCCAAAAACTAGCTCATTAAAGATGAAATAGACCAGCTCTCTTCAGTGAAGAGGACAAATAGTTTATTTAAAGCATTTGTTCCAATAAAATAAATAGAGGGAAACTTGGATGCTAAAATTACATGAATAGGAATCTTCCTGGCACTTAGTGTTTCTATGTTATTGAAAAATGATGTTCCAGAAAGATTACTTTTTTCCTCTTATTTTTACTGCCATTGTCGACCTATTGTGGGACATTTTTATATATTGAATCTGGGTTCTTTTTTGACTTTTTTTTTTTCCCAATCCAACAGCATCCTTTTTTTTAAAAGAGAGAATTAGAAAATATTAAATCCTGCATGTAATATATCTGCTGTCATCTTAGTTGGACCAACTTCCCATTTATTTATCTTAAAACTATACAGTTACATCTTAATTCCATCCAAAGAAGATACAGTTTGAAGACAGAAGTGTACTCTCTACAATGCAATTTACTGTACAGTTAGAAAGCAAAGTGTTAAATGGAGAAGATACTTGTTTTTATTAAACATTTTGAGATTTAGATAAACTACATTTTAACTGAATGTCTAAAGTGATTATCTTTTTTCCCCCCAAGTTAGTCTTAAATCTTTTGGGTTTGAATGAAGGTTTTACATAAGAAATTATTAAAAACAAGGGGGGTGGGTAATAAATGTATATAACATTAAATAATGTAACGTAGGTGTAGATTCCCAAATGCATTTGGATGTACAGATCGACTACAGAGTACTTTTTTCTTATGATGATTGGTGTAGAAATGTGTGATTTGGGTGGGCTTTTACATCTTGCCTACCATTGCATGAAACATTGGGGTTTCTTCAAAATGTGTGTGTCATACTTCTTTTGGGAGGGGGGTTGTTTTCTTCTGTTTATTTTCTGAGACTCCTACAGGAGCCAAATTTGTAATTTAGAGACACTTAATTTTGTTAATCCTGTCTGGGACACTTAAGTAACATCTAAAGCATTATTGCTTTAGAATGTTCAAATAAAATTTCCTGACCAAATTGTTTTGTGGAAATAGATGTGTTTGCAATTTGAAGATATCTTTCTGTCCAGAAGGCAAAATTACCGAATGCCATTTTTAAAAGTATGCTATAAACTATGCTACTCTCATACAGGGGACCCGTATTTTAAAATCTCCAGACTTGCTTACATCTAGATTATCCAGCACAATCATAAAGTGAATGACAAACCCTTTGAATGAAATTGTGGCACAAAATCTGTTCAGGTTGGTGTACCGTGTAAAGTGGGGATGGGGTAAAAGTGGTTAACGTACTGTTGGATCAACAAATAAAGGTTACAGTTTTGTAAGAGAAGTGATTTGAATACATTTTTCTGGAACTATTCATAATATGAAGTTTTCCTAGAACCACTGAGTTTCTAGTTTAATAGTTTGCTATGCAAATGACCACCTAAAACAATACTTTATATTGTTATTTTTAGAAAGACTCAAAACACCTGTATTTAAACCTTAATATGAAAATCATGCAATTAATAGTTACACAAGATGTTTTCATTACAAAATATGTACCTATCTATTGATGGACTCTACATCCTATATTGTGACATGTAAGTCCTTTAAAAGGTGAAAAGTATGATTTCTTACCACTTAAGTATGATTGATATGATCCAACAAATTTGATCAGAAGCTGTAGGTAAATCCTCTTCTGAAGCCAAAATGGTATATTAAATATAATTTATTGGTACTTCCATTTTCTCTTCCTTCTTACTTGCCTTTAAGATCTTATAAAAAAGAAACTAAAAGTTAATATTTAGTTGCCTATATTATGTAACCTTTTAACTATATATAAAGTACTTTTTTGGTTTCTTTCTCACCACTTTTATTCAAAAGTACTTTTAACATACCAATACATAGTCTGTCTGATGGGAGTATAAATTGGACAGTAAGGTTTTGTCTTAATAAAATGAAATTTGTTTCTCATGATATGAATCTTGCAGGTAAGATGTAGGGTTTATTGAAAATGTGTGGGTTAAATGCTTTCAGGTACACCAATTCTTTCTACTAAATTGAGCTCTATTTGAAGTTCTTTGGAATCTGTGGTGAAAAATAATTTTCTGATTTCCAAATACATTAAGAGCATTAAATGAATATTAATCACCTTTAAAGTCTTTTAGAAAAGGACTTGTATTGGTTTTTGGCTGCATAGAGGGGTTGAATAAGTGTATGTATGTGTGTGCGTGTGTGTGTGTCTTCTTAAAGAAGATGTAATTCACAAATAGTTTAGCTCCCTAGCGCTCAGTTGTAGAATAGAAAATAGAACATTATTCAAGTTAATTGAAAGGTGAGGTTTTTATACCCCCACTAATGCTGTGTATCTGTCTTTCGTTTGTTAACATTATTTGCTTAATTTCTTTCAACTCACACTTTGGATAATACTATCAAAAACTAAGGCTAAACATTCCTTGTGTATCTTTAAGCATGCTTCTCCTGAAATTTAACTACATTAGTAGTTGACATTTGTATACATATATCCTAATACAAGAGTAGGATAAGGTGGAAATGTAATGGCCTGAGGGATGGTGAAGCATTCTTTTAGTATTTTTCATCATGTTGGGCTCCTAGATTGTACTGGGGTTGCCCATAAATCAAACCCCATACTCTTAGAATTCATTATATTATGGTGATATCCGAACCTAGTGAATGGTATGCTTGGGTGTTTTCCATTGAGAGTGGATGGACCTCTTTATAAAGTTGGTTGCTGCAAAATCCAGTTCTTCCAAAAGCCACTTTATTTAGGGTTTATTCACAAGTCATATCCATTTTGGTACAGTGTTTGTTTCCTAATATTTATTAACCACCTTATACCAAATGTCTTGCAAAGAAATGTTATTAAAACCTTGAATTTTTACAAATGTAAAAAACAAAAAGTGTATTAATGTATTTGTTCAGGAAAAGCTACATACCGAAGGGCTTTTGTATATGAATTCTGTGGTGGGGAGACCCATTTGTAATCTATATGGCAGTTCCATCTGGGTTTTAAGTTTAGATTTCACCGTGTCTTAGTGCTTCATTCTATTGGTTTATTGGAACATGTAATAAATAGGAGTAGTGATGTATTAAAACACAAGTATTCATTAATGTTTTATATCTTCACTAAAATTCTATAGTTATGAAACTATCAATCAAGGTGTTATATTTCAGTCAGAAGTGAAAATTTATGAAGAGTATTTGGAAGTGTGTACAGAAATAAACTAGACTTACAGGTAGGCTAGATCAGAACGTTAACATATGAACCTGCAGAAATCTGGTAAGACTTAAATTCAGTGTGAGGAATAACTCTAGTTCTCTCCTATGAGCATTTCCTAAAAGCCATCTGATTTGGCATTCTTACTGGAGCTGCAGACAGAAATCTACAAAGACAAAAGTAAACAAAATTAAGTTATTATTCCACTGTTAGGAATGGAAATAAACTTGTGAAGTCTGTTTATTTTGAAGTATTGGTGAACTAGGCTTGCTAATTGATAACTGCAGCAGTTTGTGTTTACTCCAGTTCATCAGCTTAGGTCATTTGAAAGATATAAGAGCTTAAGGCAAGAAAGAAATAACATGGAATTCTATTTGAAGGACAACAGAACATTCTTGGAAAAGCAGCTCCAGTTGGTTTTTCAACTGTCAAACTTGAATGTGTAAGTCCCCACAGAGCATGGACAGTCGGTGCAGAGTTCCAAGGAAACAATTATTGCCTGATGACCACTTCCATTTTGTATACACTCTTTGGTTCGTATAGGCCATATTCCAACTGGCTTTTTAGTAATAGAAATCCAGTATATAATGTATCAAATACAATTGAGGTTCTAACCTAGTGTGTTAATTTATCTGAATTTGGATTTTTAAAAAGTAATAAAAAGTTAAATGTAC");
        long date2 = new Date().getTime();
        System.out.println(date2-date+" ms elepased");*/
        for (String s : new SeqUtils().getCandidateSequences("UAAAUCCCAUGGUGCCUUCUCCU", "AGACCCAAGAAAACCTGGAACTTTGGATCAATTTCTTTTTCATAGGGGTGGAACTTGCACAGCAAAAACAAACAAACGCAAGAAGAGATTTGGGCTTTAACACACTGGGTACTTTGTGGGTCTCTCTTTCGTCGGTGGCTTAAAGTAACATCTATTTCCATTGATCCTAGGTTCTTCCTGACTGCTTTCTCCAACTGTTCACAGCAAATGCTTGGATTTTATGCAGTAGGCATTACTACAGTACATGGCTAATCTTCCCAAAAACTAGCTCATTAAAGATGAAATAGACCAGCTCTCTTCAGTGAAGAGGACAAATAGTTTATTTAAAGCATTTGTTCCAATAAAATAAATAGAGGGAAACTTGGATGCTAAAATTACATGAATAGGAATCTTCCTGGCACTTAGTGTTTCTATGTTATTGAAAAATGATGTTCCAGAAAGATTACTTTTTTCCTCTTATTTTTACTGCCATTGTCGACCTATTGTGGGACATTTTTATATATTGAATCTGGGTTCTTTTTTGACTTTTTTTTTTTCCCAATCCAACAGCATCCTTTTTTTTAAAAGAGAGAATTAGAAAATATTAAATCCTGCATGTAATATATCTGCTGTCATCTTAGTTGGACCAACTTCCCATTTATTTATCTTAAAACTATACAGTTACATCTTAATTCCATCCAAAGAAGATACAGTTTGAAGACAGAAGTGTACTCTCTACAATGCAATTTACTGTACAGTTAGAAAGCAAAGTGTTAAATGGAGAAGATACTTGTTTTTATTAAACATTTTGAGATTTAGATAAACTACATTTTAACTGAATGTCTAAAGTGATTATCTTTTTTCCCCCCAAGTTAGTCTTAAATCTTTTGGGTTTGAATGAAGGTTTTACATAAGAAATTATTAAAAACAAGGGGGGTGGGTAATAAATGTATATAACATTAAATAATGTAACGTAGGTGTAGATTCCCAAATGCATTTGGATGTACAGATCGACTACAGAGTACTTTTTTCTTATGATGATTGGTGTAGAAATGTGTGATTTGGGTGGGCTTTTACATCTTGCCTACCATTGCATGAAACATTGGGGTTTCTTCAAAATGTGTGTGTCATACTTCTTTTGGGAGGGGGGTTGTTTTCTTCTGTTTATTTTCTGAGACTCCTACAGGAGCCAAATTTGTAATTTAGAGACACTTAATTTTGTTAATCCTGTCTGGGACACTTAAGTAACATCTAAAGCATTATTGCTTTAGAATGTTCAAATAAAATTTCCTGACCAAATTGTTTTGTGGAAATAGATGTGTTTGCAATTTGAAGATATCTTTCTGTCCAGAAGGCAAAATTACCGAATGCCATTTTTAAAAGTATGCTATAAACTATGCTACTCTCATACAGGGGACCCGTATTTTAAAATCTCCAGACTTGCTTACATCTAGATTATCCAGCACAATCATAAAGTGAATGACAAACCCTTTGAATGAAATTGTGGCACAAAATCTGTTCAGGTTGGTGTACCGTGTAAAGTGGGGATGGGGTAAAAGTGGTTAACGTACTGTTGGATCAACAAATAAAGGTTACAGTTTTGTAAGAGAAGTGATTTGAATACATTTTTCTGGAACTATTCATAATATGAAGTTTTCCTAGAACCACTGAGTTTCTAGTTTAATAGTTTGCTATGCAAATGACCACCTAAAACAATACTTTATATTGTTATTTTTAGAAAGACTCAAAACACCTGTATTTAAACCTTAATATGAAAATCATGCAATTAATAGTTACACAAGATGTTTTCATTACAAAATATGTACCTATCTATTGATGGACTCTACATCCTATATTGTGACATGTAAGTCCTTTAAAAGGTGAAAAGTATGATTTCTTACCACTTAAGTATGATTGATATGATCCAACAAATTTGATCAGAAGCTGTAGGTAAATCCTCTTCTGAAGCCAAAATGGTATATTAAATATAATTTATTGGTACTTCCATTTTCTCTTCCTTCTTACTTGCCTTTAAGATCTTATAAAAAAGAAACTAAAAGTTAATATTTAGTTGCCTATATTATGTAACCTTTTAACTATATATAAAGTACTTTTTTGGTTTCTTTCTCACCACTTTTATTCAAAAGTACTTTTAACATACCAATACATAGTCTGTCTGATGGGAGTATAAATTGGACAGTAAGGTTTTGTCTTAATAAAATGAAATTTGTTTCTCATGATATGAATCTTGCAGGTAAGATGTAGGGTTTATTGAAAATGTGTGGGTTAAATGCTTTCAGGTACACCAATTCTTTCTACTAAATTGAGCTCTATTTGAAGTTCTTTGGAATCTGTGGTGAAAAATAATTTTCTGATTTCCAAATACATTAAGAGCATTAAATGAATATTAATCACCTTTAAAGTCTTTTAGAAAAGGACTTGTATTGGTTTTTGGCTGCATAGAGGGGTTGAATAAGTGTATGTATGTGTGTGCGTGTGTGTGTGTCTTCTTAAAGAAGATGTAATTCACAAATAGTTTAGCTCCCTAGCGCTCAGTTGTAGAATAGAAAATAGAACATTATTCAAGTTAATTGAAAGGTGAGGTTTTTATACCCCCACTAATGCTGTGTATCTGTCTTTCGTTTGTTAACATTATTTGCTTAATTTCTTTCAACTCACACTTTGGATAATACTATCAAAAACTAAGGCTAAACATTCCTTGTGTATCTTTAAGCATGCTTCTCCTGAAATTTAACTACATTAGTAGTTGACATTTGTATACATATATCCTAATACAAGAGTAGGATAAGGTGGAAATGTAATGGCCTGAGGGATGGTGAAGCATTCTTTTAGTATTTTTCATCATGTTGGGCTCCTAGATTGTACTGGGGTTGCCCATAAATCAAACCCCATACTCTTAGAATTCATTATATTATGGTGATATCCGAACCTAGTGAATGGTATGCTTGGGTGTTTTCCATTGAGAGTGGATGGACCTCTTTATAAAGTTGGTTGCTGCAAAATCCAGTTCTTCCAAAAGCCACTTTATTTAGGGTTTATTCACAAGTCATATCCATTTTGGTACAGTGTTTGTTTCCTAATATTTATTAACCACCTTATACCAAATGTCTTGCAAAGAAATGTTATTAAAACCTTGAATTTTTACAAATGTAAAAAACAAAAAGTGTATTAATGTATTTGTTCAGGAAAAGCTACATACCGAAGGGCTTTTGTATATGAATTCTGTGGTGGGGAGACCCATTTGTAATCTATATGGCAGTTCCATCTGGGTTTTAAGTTTAGATTTCACCGTGTCTTAGTGCTTCATTCTATTGGTTTATTGGAACATGTAATAAATAGGAGTAGTGATGTATTAAAACACAAGTATTCATTAATGTTTTATATCTTCACTAAAATTCTATAGTTATGAAACTATCAATCAAGGTGTTATATTTCAGTCAGAAGTGAAAATTTATGAAGAGTATTTGGAAGTGTGTACAGAAATAAACTAGACTTACAGGTAGGCTAGATCAGAACGTTAACATATGAACCTGCAGAAATCTGGTAAGACTTAAATTCAGTGTGAGGAATAACTCTAGTTCTCTCCTATGAGCATTTCCTAAAAGCCATCTGATTTGGCATTCTTACTGGAGCTGCAGACAGAAATCTACAAAGACAAAAGTAAACAAAATTAAGTTATTATTCCACTGTTAGGAATGGAAATAAACTTGTGAAGTCTGTTTATTTTGAAGTATTGGTGAACTAGGCTTGCTAATTGATAACTGCAGCAGTTTGTGTTTACTCCAGTTCATCAGCTTAGGTCATTTGAAAGATATAAGAGCTTAAGGCAAGAAAGAAATAACATGGAATTCTATTTGAAGGACAACAGAACATTCTTGGAAAAGCAGCTCCAGTTGGTTTTTCAACTGTCAAACTTGAATGTGTAAGTCCCCACAGAGCATGGACAGTCGGTGCAGAGTTCCAAGGAAACAATTATTGCCTGATGACCACTTCCATTTTGTATACACTCTTTGGTTCGTATAGGCCATATTCCAACTGGCTTTTTAGTAATAGAAATCCAGTATATAATGTATCAAATACAATTGAGGTTCTAACCTAGTGTGTTAATTTATCTGAATTTGGATTTTTAAAAAGTAATAAAAAGTTAAATGTAC",
                0, 20, 6)) {

        }
    }

    public int lastCharacter(String str, Character c) {
        int position = -1;
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) == c) {
                position = i;
                break;
            }
        }
        return position;
    }

}
