/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.Utils;

import java.util.ArrayList;

/**
 *
 * @author apla
 */
public class Translator {

    protected EnsemblNCBIDictionary dictionary;
    
    protected String gene2ensemblLU;

    public Translator() {
        dictionary = new EnsemblNCBIDictionary();

    }

    public String getGene2ensemblLU() {
        return gene2ensemblLU;
    }
       
    public EnsemblNCBIDictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(EnsemblNCBIDictionary dictionary) {
        this.dictionary = dictionary;
    }

    public ArrayList<String> NCBI2EnsemblList(String ncbiWord) {
        ArrayList<String> answer = dictionary.getAllNcbi2EnsemblOptions(ncbiWord);
        if (answer !=null && answer.size() == 0) {
            getOnlineTranslationNcbi(ncbiWord);
            answer = dictionary.getAllNcbi2EnsemblOptions(ncbiWord);
        }
        return answer;
    }

    public String NCBI2Ensembl(String ncbiWord) {
        String answer = null;
        ArrayList<String> temp = NCBI2EnsemblList(ncbiWord);
        if (temp!=null && temp.size() > 0) {
            answer = temp.get(0);
        }
        return answer;
    }

    public ArrayList<String> Ensembl2NCBIList(String ensemblWord) {
        ArrayList<String> answer = dictionary.getAllEnsembl2NcbiOptions(ensemblWord);
        if (answer.size() == 0) {
            getOnlineTranslationEnsembl(ensemblWord);
            answer = dictionary.getAllEnsembl2NcbiOptions(ensemblWord);
        }
        return answer;
    }

    public String Ensembl2NCBI(String ensemblWord) {
        String answer = null;
        ArrayList<String> temp = Ensembl2NCBIList(ensemblWord);
        if (temp.size() > 0) {
            answer = temp.get(0);
        }
        return answer;
    }

    private void getOnlineTranslationNcbi(String ncbiWord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void getOnlineTranslationEnsembl(String ensemblWord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
