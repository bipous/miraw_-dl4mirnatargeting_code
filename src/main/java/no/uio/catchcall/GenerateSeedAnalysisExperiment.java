/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * This class was used to generate the 'executions' of all the miRAW
 * training/testing process to determine and study the influence of the seed
 * region size.
 * 
 * It is not part of miRAW core.
 *
 *
 * @author apla
 */
public class GenerateSeedAnalysisExperiment {

    public static void main(String args[]) throws FileNotFoundException {
        generatePropertyFiles();
        //compileInformation();
    }

    private static void generatePropertyFiles() throws FileNotFoundException {
        String folder = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/ConfFiles/SeedAnalysis";
        new File(folder).mkdir();
        String forwardFolder = folder + "/Forward";
        new File(forwardFolder).mkdir();

        String forwardExecutionFolder = "/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/ConfFiles/seedAnalysis/Forward";
        String backwardExecutionFolder = "/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/ConfFiles/seedAnalysis/Backward";

        String backwardFolder = folder + "/Backward";
        new File(backwardFolder).mkdir();

        String experimentFolder = "/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/GenePredResults/SeedAnalysis";

        PrintWriter executionBWriter = new PrintWriter(folder + "/executeForward.sh");
        PrintWriter executionFWriter = new PrintWriter(folder + "/executeBackward.sh");

        //forward with different seeds
        for (int i = 5; i <= 20; i++) {
            //one fold per seed
            for (int j = 0; j < 10; j++) {
                PrintWriter writer = new PrintWriter(forwardFolder + "/Forward_seed_" + i + "_fold_" + j + ".properties");
                writer.println("########################################");
                writer.println("##########Forward " + i + "-" + j + "##################");
                writer.println("########################################");
                writer.println("ExperimentName=Forward_" + i + "_fold_" + j);
                writer.println("ExperimentFolder=" + experimentFolder);
                writer.println("UnifiedFile=/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/DataSet/balanced10/randomLeveragedTestSplit_" + j + ".csv");
                writer.println("DLModel=/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/CVResults/RTSBioinformaticsCV/set3/bestModel.bin");
                writer.println("CandidateSiteFinder.Type=Personalized;" + i + ";0");
                writer.close();
                executionFWriter.println("java -jar miRAWGPU.jar geneprediction evaluate " + forwardExecutionFolder + "/Forward_seed_" + i + "_fold_" + j + ".properties");
            }

            for (int j = 0; j < 10; j++) {
                PrintWriter writer = new PrintWriter(backwardFolder + "/Backward_seed_" + i + "_fold_" + j + ".properties");
                writer.println("########################################");
                writer.println("##########Backward " + i + "-" + j + "##################");
                writer.println("########################################");
                writer.println("ExperimentName=Backward_" + i + "_fold_" + j);
                writer.println("ExperimentFolder=" + experimentFolder);
                writer.println("UnifiedFile=/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/DataSet/balanced10/randomLeveragedTestSplit_" + j + ".csv");
                writer.println("DLModel=/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/CVResults/RTSBioinformaticsCV/set3/bestModel.bin");
                writer.println("CandidateSiteFinder.Type=Personalized;" + i + ";" + (20 - i));
                writer.close();
                executionBWriter.println("java -jar miRAWGPU.jar geneprediction evaluate " + backwardExecutionFolder + "/Backward_seed_" + i + "_fold_" + j + ".properties");
            }
        }
        executionFWriter.flush();
        executionBWriter.close();

    }

    private static void compileInformation() {
        String experimentFolder = "/mnt/MyBookDuo/Data/miRAW/Bioinformatics/BioinformaticsPaper/GenePredResults/SeedAnalysis";
        File eF = new File(experimentFolder + "/ResultSummary");
        eF.mkdir();
        PrintWriter forwardResults = null;
        PrintWriter backwardResults = null;
        //forward with different seeds
        for (int i = 5; i <= 20; i++) {
            //one fold per seed
            for (int j = 0; j < 10; j++) {
                String folder2Analys = experimentFolder + "/Forward_" + i + "_fold_" + j;
                //compute staff

            }
            //compute staff
            forwardResults.println();

            for (int j = 0; j < 10; j++) {
                String folder2Analys = experimentFolder + "/Forward_" + i + "_fold_" + j;
            }
            //compute staff
            backwardResults.println();

        }
        forwardResults.flush();
        forwardResults.close();

        backwardResults.flush();
        backwardResults.close();

    }

}
