/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall.stats;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author apla
 */
public class KEvaluation {

    public static int[][] evaluateK(String File, double step) throws FileNotFoundException, IOException {
        BufferedReader fileRead = new BufferedReader(new FileReader(File));

        String fileHeader[] = fileRead.readLine().split("\t");
        int realClass, predicted;
        realClass = predicted = 0;
        for (int i = 0; i < fileHeader.length; i++) {
            String val = fileHeader[i].toLowerCase();
            switch (val) {
                case "realclass":
                    realClass = i;
                    break;
                case "prediction":
                    predicted = i;
                    break;
            }
        }
        String line;

        int nKs = 0;
        for (double k = 0; k < 1.01; k = k + step) {
            nKs++;
        }

        int[][] results = new int[5][nKs];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < nKs; j++) {
                results[i][j] = 0;
            }
        }

        while ((line = fileRead.readLine()) != null) {
            double predictedVal;
            predictedVal = Double.parseDouble(line.split("\t")[predicted]);

            int realVal;
            realVal = Integer.parseInt(line.split("\t")[realClass]);
            int clas;
            int nK = 0;
            for (double k = 0; k < 1.01; k = k + step) {
                
                clas = -1;
                if (predictedVal - k > 0) {
                    clas = 1;
                } else if ((predictedVal + k) <= 0) {
                    clas = 0;
                }
                
                if (k==0 && clas == -1){
                    int ja=23;
                }

                if (clas == -1) {
                    results[4][nK]++;
                } else if (clas == 1) {
                    if (realVal == 1) {//TP
                        results[0][nK]++;
                    } else {
                        results[1][nK]++; //FP
                    }
                } else if (realVal == 1) {//FN
                    results[3][nK]++;
                } else {
                    results[2][nK]++; //TN
                }
                nK++;
            }
        }

        return results;
    }

    public static void main(String args[]) throws IOException {
        int[][][] total = new int[10][5][11];
        double[][] acu = new double[11][11];

        for (int i = 0; i < 10; i++) {
            String folder = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/miRAW balanced/";
            String file = "Balanced_PITA_" + i + "/targetPredictionOutput.csv";
            int[][] res = evaluateK(folder + file, 0.100000000001);
            total[i] = res;
            for (int k = 0; k < 5; k++) {
                for (int k2 = 0; k2 < 11; k2++) {
                    acu[k][k2] = acu[k][k2] + res[k][k2];
                }
            }
            for (int j = 0; j < 11; j++) {
                double acur, prec, sens, neg, spec, fsco;
                double TP, FP, TN, FN;

                TP = res[0][j];
                FP = res[1][j];
                TN = res[2][j];
                FN = res[3][j];
                acur = (TP + TN) / (TP + TN + FP + FN);
                prec = TP / (TP + FP);
                sens = (TP) / (TP + FN);
                neg = TN / (TN + FN);
                spec = TN / (TN + FP);
                fsco = 2 * (prec * sens) / (prec + sens);

                acu[5][j] = acu[5][j] + acur;
                acu[6][j] = acu[6][j] + prec;
                acu[7][j] = acu[7][j] + sens;
                acu[8][j] = acu[8][j] + neg;
                acu[9][j] = acu[9][j] + spec;
                acu[10][j] = acu[10][j] + fsco;
            }
        }

        System.out.println("K\tTP\tFP\tTN\tFN\tU\tAccuracy\tPrecision\tSensitivity\tNeg. Precision\tSpecificity\tf-score");
        for (int i = 0; i < 11; i++) {
            System.out.print("0." + i + "\t");

            for (int j = 0; j < 11; j++) {
                System.out.print((double) acu[j][i] / (double) 10 + "\t");
            }
            System.out.println();

        }

    }

}
