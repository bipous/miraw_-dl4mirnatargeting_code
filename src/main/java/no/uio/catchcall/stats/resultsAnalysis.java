/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall.stats;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author apla
 */
public class resultsAnalysis {

    public static void analyzeNegativeTargetSitesFile(String fileLocation) throws FileNotFoundException, IOException {
        analyzeNegativeTargetSitesFile(fileLocation, null);
    }

    public static void analyzeNegativeTargetSitesFile(String fileLocation, String outputFile) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileLocation));

        int miRNAGeneCount = 0;
        String currentMiRNA = "";
        String currentGene = "";

        HashMap<String, Integer> falsePositivesByType = new HashMap<>();
        HashMap<String, ArrayList<String>> causeOfFalsePositive = new HashMap<>();

        int falsePositiveSites = 0;
        int falsePositiveGenes = 0;

        String line = br.readLine();
        if (line != null) {
            line = br.readLine();

        }
        while (line != null) {
            String[] values = line.split("\t");
            if (values.length == 3) {
                miRNAGeneCount++;
                currentGene = values[0];
                currentMiRNA = values[1];
                causeOfFalsePositive.put(currentGene + "-" + currentMiRNA, new ArrayList<>());
                falsePositiveGenes++;
            } else if (values.length == 10) {
                String cause = values[9];
                if (!falsePositivesByType.containsKey(cause)) {
                    falsePositivesByType.put(cause, 1);
                } else {
                    falsePositivesByType.put(cause, falsePositivesByType.get(cause) + 1);
                }
                String key = currentGene + "-" + currentMiRNA;
                if (!causeOfFalsePositive.containsKey(key)) {
                    causeOfFalsePositive.put(key, new ArrayList<String>());
                }
                if (!causeOfFalsePositive.get(key).contains(cause)) {
                    causeOfFalsePositive.get(key).add(cause);
                }
                falsePositiveSites++;
            }

            line = br.readLine();
        }

        if (outputFile != null) {
            PrintWriter pw = new PrintWriter(new FileOutputStream(
                    new File(outputFile),
                    true));

            pw.println("Results of "+fileLocation); //print results
            pw.println("False Positive Sites\t"+falsePositiveSites);
            pw.println("False Positive Genes\t"+falsePositiveGenes);
            
            pw.println();
            pw.println("Types of false positive sites:");

            for (String keyS : falsePositivesByType.keySet()) {
                float perc = ((float) falsePositivesByType.get(keyS) / (float) falsePositiveSites) * 100;
                pw.println(keyS + "\t" + falsePositivesByType.get(keyS) + "\t" + perc + "%");

            }

            HashMap<String, Integer> causesCount = new HashMap<>();
            for (String keyS : causeOfFalsePositive.keySet()) {
                ArrayList<String> causes = causeOfFalsePositive.get(keyS);

                Collections.sort(causes);

                String cause = "-";
                for (String s : causes) {
                    cause = cause + s + "-";
                }

                if (!causesCount.containsKey(cause)) {
                    causesCount.put(cause, 1);
                } else {
                    causesCount.put(cause, causesCount.get(cause) + 1);
                }
            }

            pw.println();
            pw.println("Causes of false positives sites:");
            for (String keyS : causesCount.keySet()) {
                float perc = ((float) causesCount.get(keyS) / (float) falsePositiveGenes) * 100;
                pw.println(keyS + "\t" + causesCount.get(keyS) + "\t" + perc + "%");
            }
            pw.flush();
            pw.close();

        }

        //print results
        System.out.println(falsePositiveSites + " false positive candidate sites ");
        System.out.println(falsePositiveGenes + " false positive target genes ");
        System.out.println();
        System.out.println("Types of false positive sites:");

        for (String keyS : falsePositivesByType.keySet()) {
            float perc = ((float) falsePositivesByType.get(keyS) / (float) falsePositiveSites) * 100;
            System.out.println(keyS + " - " + falsePositivesByType.get(keyS) + " (" + perc + "%)");

        }

        HashMap<String, Integer> causesCount = new HashMap<>();
        for (String keyS : causeOfFalsePositive.keySet()) {
            ArrayList<String> causes = causeOfFalsePositive.get(keyS);

            Collections.sort(causes);

            String cause = "-";
            for (String s : causes) {
                cause = cause + s + "-";
            }

            if (!causesCount.containsKey(cause)) {
                causesCount.put(cause, 1);
            } else {
                causesCount.put(cause, causesCount.get(cause) + 1);
            }
        }

        System.out.println();
        System.out.println("Causes of false positives sites:");
        for (String keyS : causesCount.keySet()) {
            float perc = ((float) causesCount.get(keyS) / (float) falsePositiveGenes) * 100;
            System.out.println(keyS + " - " + causesCount.get(keyS) + " (" + perc + "%)");

        }

    }

    public static void main(String[] args) throws IOException {
        System.out.println("#################################");
        System.out.println("#####@##Target Scan CVV##########");
        System.out.println("#################################");
        analyzeNegativeTargetSitesFile("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/NegativeEvaluationCPU_targetScan_CVV/targetSites.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/stats.csv");

        System.out.println("#################################");
        System.out.println("#####@##Target Scan 90##########");
        System.out.println("#################################");
        analyzeNegativeTargetSitesFile("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/NegativeEvaluationCPU_targetScan_90/targetSites.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/stats.csv");

        System.out.println("#################################");
        System.out.println("#####@##DEFAULT CVV##########");
        System.out.println("#################################");
        analyzeNegativeTargetSitesFile("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/NegativeEvaluationCPU_DEFAULT_CVV/targetSites.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/stats.csv");

        System.out.println("#################################");
        System.out.println("###########DEFAULT 90############");
        System.out.println("#################################");
        analyzeNegativeTargetSitesFile("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/NegativeEvaluationCPU_DEFAULT_90/targetSites.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/stats.csv");

       /* System.out.println("#################################");
        System.out.println("##########PITA Scan CVV##########");
        System.out.println("#################################");
        analyzeNegativeTargetSitesFile("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/NegativeEvaluationCPU_PITA_CVV/targetSites.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/stats.csv");*/

        System.out.println("#################################");
        System.out.println("########## PITA 90 ##############");
        System.out.println("#################################");
        analyzeNegativeTargetSitesFile("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/NegativeEvaluationCPU_PITA_90/targetSites.csv",
                "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/tempdellResults/stats.csv");

    }

}
