/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;

/**
 *
 * @author apla
 */
public class SummarizeTargetPositionsIn3UTR {

    public static void main(String args[]) throws FileNotFoundException, IOException {
        String UTRLocation = "/Users/apla/Documents/MirnaTargetDatasets/ensembl/ensembl_hsa_3utrs.fa";
        String TSPrediction = "/Users/apla/Documents/MirnaTargetDatasets/"
                + "putatitve targets DDBB/targetscanData/AllPredictions4RepresentativeScripts/"
                + "Nonconserved_Site_Context_Scores.txt";
                //+ "Conserved_Site_Context_Scores.txt";

        int proportionalBinds = 100;
        int maxUTRLength = 0;
        int absoluteBindSize = 50;

        //Read 3'UTRs
        HashMap<String, String> UTRSDataAux = FastaAccess.obtainDataAndSequences(UTRLocation);
        HashMap<String, String> UTRSData = new HashMap<>();
        
       
        //Initialize counting of targets per transcript
        HashMap<String, Integer> miRNATranscriptTargetCount = new HashMap<>();        
        
        //Keep the sequence for each transcript
        for (String key : UTRSDataAux.keySet()) {
            maxUTRLength = Math.max(maxUTRLength, UTRSDataAux.get(key).length());
            UTRSData.put(key.split("\\|")[1], UTRSDataAux.get(key));  
        }
        
         //Initialize counters
        int[] siteNucleotides = new int[maxUTRLength / absoluteBindSize + 1];
        Arrays.fill(siteNucleotides, 0);
        int[] relativePosition = new int[proportionalBinds];
        Arrays.fill(relativePosition, 0);
        int[] targetsPerTransLength = new int[maxUTRLength / absoluteBindSize + 1];
        Arrays.fill(targetsPerTransLength, 0);
        int[] transcriptsPerTransLength = new int[maxUTRLength / absoluteBindSize + 1];
        Arrays.fill(transcriptsPerTransLength, 0);
        
        for (String key : UTRSDataAux.keySet()) {
            int transSize=UTRSDataAux.get(key).length();
            int sLengthPos = transSize/absoluteBindSize;
            transcriptsPerTransLength[sLengthPos]++;
        }
        

        

        //File Reader
        BufferedReader br = new BufferedReader(new FileReader(TSPrediction));
        
        int outsider = 0;
        int total=0;
        try {
            //read header
            String line = br.readLine();
            //start reading
            line = br.readLine();
            
            //For each TS file
            while (line != null) {
                String[] parts = line.split("\t");
                String transId = parts[2].split("\\.")[0];
                int utrStart = Integer.parseInt(parts[6]);
                
                //If it's human and we have the transcript of the 3'UTR
                if (parts[4].startsWith("hsa") && UTRSData.containsKey(transId) && !UTRSData.get(transId).startsWith("Sequence")){
                    total++;
                    int transSize=UTRSData.get(transId).length();
                    
                    //Get the position in the 3'UTR (discretized by absoluteBindSize)
                    int absPos = utrStart/absoluteBindSize;
                    
                    //Get the relative position
                    float perc = ((float)utrStart/(float)transSize)*100;
                    int relPos = (int)Math.floor(perc);
                    
                    //Some targets fall behind the 3'UTR. WHY?
                    if (relPos>99){
                        relPos=99;
                        outsider++;
                        System.out.println("OUTSIDER: "+transId+"("+parts[1].split("\\.")[0]+")"+"("+parts[4]+") SiteStart:"+utrStart+" transSize:"+UTRSData.get(transId).length());
                    };
                    
                    //Increase counters
                    relativePosition[relPos]++;
                    siteNucleotides[absPos]++;
                    
                    //Save teh miRNA-TranscriptTarget - If the target has multiple sites, only one is saved
                    miRNATranscriptTargetCount.put(parts[4]+"|"+transId, 1);
                    
                }
                line = br.readLine();
            }
            
        } finally {
            br.close();
        }
        
        //remove targets that fell behind the 3'UTR
        relativePosition[99]=relativePosition[99]-outsider;
        
        //Count the number of miRNA-transcript pairs according to the transcript Size.
        for (String key: miRNATranscriptTargetCount.keySet()){
            String transId = key.split("\\|")[1];
            int transSize=UTRSData.get(transId).length();
            int sLengthPos = transSize/absoluteBindSize;
            targetsPerTransLength[sLengthPos]++;
        }
        
        System.out.println("Relative Positions");
        for (int i=0; i<relativePosition.length;i++){
            System.out.println(i+"\t"+relativePosition[i]);
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("Nucleotide Positions\tAbsolute Position in Transcript\t TargetsPerTranscriptLength\tNormalizedTargetsPerTranscriptLength");
        for (int i=0; i<siteNucleotides.length-1;i++){
            System.out.println((i*absoluteBindSize)+"-"+((i+1)*absoluteBindSize)+"\t"+siteNucleotides[i]+"\t"+targetsPerTransLength[i]+"\t"+
                    ((float)targetsPerTransLength[i]/(float)transcriptsPerTransLength[i]));
        }
        
        System.out.println();
        System.out.println();
        System.out.println("Outsiders = "+outsider);
        System.out.println("Sites = "+total);
    }

}
