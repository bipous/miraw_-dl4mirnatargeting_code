/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall.amg.experiments;

import java.io.File;
import java.io.IOException;
import no.uio.dl4miRNA.DCP.Binarization;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

/**
 *
 * @author apla
 */
public class PairWiseTests {

    public static void main(String arts[]) throws IOException, InterruptedException {
        String folder = "/Users/apla/Documents/MirnaTargetDatasets/RobustTraining/provaTrainFiles";

        MultiLayerNetwork veryShortModel = ModelSerializer.restoreMultiLayerNetwork("/Users/apla/Documents/MirnaTargetDatasets/RobustTraining/Models/VeryShort/bestModel.bin");
        MultiLayerNetwork shortModel = ModelSerializer.restoreMultiLayerNetwork("/Users/apla/Documents/MirnaTargetDatasets/RobustTraining/Models/Short/bestModel.bin");
        MultiLayerNetwork regularModel = ModelSerializer.restoreMultiLayerNetwork("/Users/apla/Documents/MirnaTargetDatasets/RobustTraining/Models/Normal/bestModel.bin");
        MultiLayerNetwork longModel = ModelSerializer.restoreMultiLayerNetwork("/Users/apla/Documents/MirnaTargetDatasets/RobustTraining/Models/Long/bestModel.bin");

        Binarization.removeUnnecesaryCommaFromCSV(folder + "VeryShort/pairWiseTrain/binary/set_0_test.csv");
        Binarization.removeUnnecesaryCommaFromCSV(folder + "Short/pairWiseTrain/binary/set_0_test.csv");
        Binarization.removeUnnecesaryCommaFromCSV(folder + "/pairWiseTrain/binary/set_0_test.csv");
        Binarization.removeUnnecesaryCommaFromCSV(folder + "Long/pairWiseTrain/binary/set_0_test.csv");
        
        
        RecordReader rrTest = new CSVRecordReader();
        rrTest.initialize(new FileSplit(new File(folder + "VeryShort/pairWiseTrain/binary/set_0_test.csv")));
        DataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, 50, 0, 2);
        Evaluation veryShortEval = evaluateModel(veryShortModel, testIter);
        System.out.println(veryShortEval.stats());

        rrTest = new CSVRecordReader();
        rrTest.initialize(new FileSplit(new File(folder + "Short/pairWiseTrain/binary/set_0_test.csv")));
        testIter = new RecordReaderDataSetIterator(rrTest, 50, 0, 2);
        Evaluation shortEval = evaluateModel(shortModel, testIter);
        System.out.println(shortEval.stats());

        rrTest = new CSVRecordReader();
        rrTest.initialize(new FileSplit(new File(folder + "/pairWiseTrain/binary/set_0_test.csv")));
        testIter = new RecordReaderDataSetIterator(rrTest, 50, 0, 2);
        Evaluation regularEval = evaluateModel(regularModel, testIter);
        System.out.println(regularEval.stats());

        rrTest = new CSVRecordReader();
        rrTest.initialize(new FileSplit(new File(folder + "Long/pairWiseTrain/binary/set_0_test.csv")));
        testIter = new RecordReaderDataSetIterator(rrTest, 50, 0, 2);
        Evaluation longEval = evaluateModel(longModel, testIter);
        System.out.println(longEval.stats());

    }

    private static Evaluation evaluateModel(MultiLayerNetwork model, DataSetIterator testIter) {

        Evaluation eval = new Evaluation(2);

        //use DL4J tools to evaluate the network.
        while (testIter.hasNext()) {
            DataSet t = testIter.next();
            INDArray features = t.getFeatureMatrix();
            INDArray labels = t.getLabels();
            INDArray predicted = model.output(features, false);
            eval.eval(labels, predicted);

        }

        return eval;
    }
}

/* WITH WRONG TRAINNING SIZE WE GOT

Examples labeled as 0 classified by model as 0: 332 times
Examples labeled as 0 classified by model as 1: 168 times
Examples labeled as 1 classified by model as 0: 250 times
Examples labeled as 1 classified by model as 1: 250 times


==========================Scores========================================
 Accuracy:        0.582
 Precision:       0.5843
 Recall:          0.582
 F1 Score:        0.5831
========================================================================

Examples labeled as 0 classified by model as 0: 336 times
Examples labeled as 0 classified by model as 1: 164 times
Examples labeled as 1 classified by model as 0: 233 times
Examples labeled as 1 classified by model as 1: 267 times


==========================Scores========================================
 Accuracy:        0.603
 Precision:       0.605
 Recall:          0.603
 F1 Score:        0.604
========================================================================

Examples labeled as 0 classified by model as 0: 331 times
Examples labeled as 0 classified by model as 1: 169 times
Examples labeled as 1 classified by model as 0: 219 times
Examples labeled as 1 classified by model as 1: 281 times


==========================Scores========================================
 Accuracy:        0.612
 Precision:       0.6131
 Recall:          0.612
 F1 Score:        0.6126
========================================================================

Examples labeled as 0 classified by model as 0: 256 times
Examples labeled as 0 classified by model as 1: 244 times
Examples labeled as 1 classified by model as 0: 195 times
Examples labeled as 1 classified by model as 1: 305 times


==========================Scores========================================
 Accuracy:        0.561
 Precision:       0.5616
 Recall:          0.561
 F1 Score:        0.5613
========================================================================

*/