/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall.amg.experiments;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import no.uio.dl4miRNA.DCP.dataConstruction;
import no.uio.dl4miRNA.Data.gathering.EnsemblAccess;
import no.uio.dl4miRNA.Data.gathering.EutilsAccess;
import org.xml.sax.SAXException;

/**
 *
 * @author albertpp
 */
public class NinasGroup {
    
    
    
    public static void main(String args[]) throws ParserConfigurationException, SAXException, IOException{
        //#############################################
        //############### Get Data ####################
        
        
        /*
        //load 3'UTR using ncbi
        
        String TFPI_eId=EutilsAccess.get_GeneEntrezId("TFPI", "homo sapiens");//7035
        ArrayList<String> TFPI_transcriptIds = EutilsAccess.get_mRNATranscriptIdsUsingEntrezID(TFPI_eId);
        String TFPI_transcript=EutilsAccess.get_mRNATranscript(TFPI_transcriptIds.get(0));
        System.out.println(TFPI_eId+":"+TFPI_transcript);
        
        String TFPI2_eId=EutilsAccess.get_GeneEntrezId("TFPI2", "homo sapiens");//7035
        ArrayList<String> TFPI2_transcriptIds = EutilsAccess.get_mRNATranscriptIdsUsingEntrezID(TFPI2_eId);
        String TFPI2_transcript=EutilsAccess.get_mRNATranscript(TFPI2_transcriptIds.get(0));
        System.out.println(TFPI2_eId+":"+TFPI2_transcript);
        
        //load 3'UTR using ensembl
        String TFPI_embl = EnsembleAccess.getTranscriptOfGeneName("TFPI");
        String TFPI2_embl = EnsembleAccess.getTranscriptOfGeneName("TFPI2");
        String F5_embl = EnsembleAccess.getTranscriptOfGeneName("F5");
        
        System.out.println(TFPI_embl);
        System.out.println(TFPI2_embl);
        System.out.println(F5_embl);
        */

        //Load unified file with miRNA transcripts and 3'UTRs
        String matureMirnaFile = "//home/albertpp/miRAW/miRNAData/mirBASE/r21/mature.fa";
        String ensemblThreeUTRFile = "/home/albertpp/miRAW/NinasGroup/3prime/biomart.fa";
        String unifiedFileDestination ="/home/albertpp/miRAW/NinasGroup/miRAWFile/unified.csv";
        dataConstruction.buildUnifiedFile(matureMirnaFile, ensemblThreeUTRFile, unifiedFileDestination, "hsa");
        
        
        //
    }
    
}
