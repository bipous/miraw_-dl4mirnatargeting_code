/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall.amg.experiments;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import no.uio.dl4miRNA.DCP.dataConstruction;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;

/**
 *
 * @author apla
 */
public class isoformUnifiedFile {

    public static void main2(String[] args) throws IOException {
        int genes2Evaluate = 5000;

        String UTRFile = "/Users/apla/Documents/MirnaTargetDatasets/ensembl/ensembl_hsa_3utrs.fa";
        String miRNAFileA = "/Users/apla/Dropbox/UiO/NGSBCN/DataSets/mirnaTranscripts125a.fa";
        String miRNAFileB = "/Users/apla/Dropbox/UiO/NGSBCN/DataSets/mirnaTranscripts146a.fa";
        String evaluationGenes = "/Users/apla/Dropbox/UiO/NGSBCN/DataSets/evaluatedTranscripts.fa";
        PrintWriter writer = new PrintWriter(evaluationGenes);

        HashMap<String, String> utrs = FastaAccess.obtainDataAndSequences(UTRFile);

        HashMap<String, String> uniqueUTRS = new HashMap<>();

        int counter = 0;
        for (String header : utrs.keySet()) {
            String transcript = utrs.get(header);
            if (!transcript.equalsIgnoreCase("Sequence unavailable")) {
                String gene = header.split("\\|")[0];
                if (uniqueUTRS.containsKey(gene)) {
                    //if (transcript.length() < uniqueUTRS.get(gene).length()) {
                    //    uniqueUTRS.put(gene, transcript);
                    //}
                } else if (transcript.length() > 60) {
                    writer.println(">" + header);
                    writer.println(transcript);
                    uniqueUTRS.put(gene, transcript);
                }
            }
            if (counter > genes2Evaluate) {
                break;
            }
            counter++;
        }
        writer.flush();
        writer.close();

        dataConstruction.buildUnifiedFile(miRNAFileA, evaluationGenes, "/Users/apla/Dropbox/UiO/NGSBCN/DataSets/unifiedFile125a.csv", "");
        dataConstruction.buildUnifiedFile(miRNAFileB, evaluationGenes, "/Users/apla/Dropbox/UiO/NGSBCN/DataSets/unifiedFile146a.csv", "");

    }

    public static void main(String[] args) throws IOException {

        String UTRFile = "/Users/apla/Documents/MirnaTargetDatasets/ensembl/ensembl_hsa_3utrs.fa";
        String mirnaFolders = "/Users/apla/Dropbox/UiO/NGSBCN/JoeyFatimaDS2/mirnas/";
        String originalMiRNAFile = mirnaFolders + "sequenceForTargetPredict.fa";
        String unifiedFolders = "/Users/apla/Dropbox/UiO/NGSBCN/JoeyFatimaDS2/unifiedFiles/";
        String confFiles = "/Users/apla/Dropbox/UiO/NGSBCN/JoeyFatimaDS2/confFiles/";

        HashMap<String, String> mirnas = FastaAccess.obtainDataAndSequences(originalMiRNAFile);

        for (String header : mirnas.keySet()) {
            String transcript = mirnas.get(header);
            PrintWriter writer = new PrintWriter(mirnaFolders + header + ".fa");
            String header2=header.replace(" ", "");
            writer.println(">" + header2);
            writer.print(transcript);
            writer.flush();
            writer.close();

            dataConstruction.buildUnifiedFile(mirnaFolders + header + ".fa", UTRFile, unifiedFolders + "unified_" + header2 + ".csv", "");

            //ConfFIle
            PrintWriter writerConf = new PrintWriter(confFiles + header2 + ".properties");
            String configuration = "########################################\n"
                    + "###########GenePrediction###############\n"
                    + "########################################\n"
                    + "ExperimentName="+header2+"\n"
                    + "ExperimentFolder=./Results/\n"
                    + "UnifiedFile=./unifiedFiles/unified_"+header+".csv\n"
                    + "DLModel=./DNNModel/bestModel.bin\n"
                    + "CandidateSiteFinder.Type=Regular\n"
                    + "#CandidateSiteFinder.Type=PITA\n"
                    + "#CandidateSiteFinder.Type=targetScan\n"
                    + "MaxSiteLength:60\n"
                    + "SeedAlignementOffset:10";
            writerConf.println(configuration);
            writerConf.flush();
            writerConf.close();
        }
    }
}
