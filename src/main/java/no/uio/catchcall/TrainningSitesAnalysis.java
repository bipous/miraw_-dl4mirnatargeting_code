/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author apla
 */
public class TrainningSitesAnalysis {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String trainPos = "/Users/apla/Documents/TrainningSites/positiveGrosswendtTrainning.csv";
        String trainNeg = "/Users/apla/Documents/TrainningSites/negativeSites.csv";;

        BufferedReader brPos = new BufferedReader(new FileReader(trainPos));
        BufferedReader brNeg = new BufferedReader(new FileReader(trainNeg));

        HashMap<String, Integer> mirnaPos = new HashMap<>();
        HashMap<String, Integer> mirnaNeg = new HashMap<>();

        String line = brPos.readLine();
        while (line != null) {
            String mirna = line.split("\t")[3];
            if (mirnaPos.containsKey(mirna)) {
                mirnaPos.put(mirna, mirnaPos.get(mirna) + 1);
            } else {
                mirnaPos.put(mirna, 1);
            }

            line = brPos.readLine();
        }
        line = brNeg.readLine();
        while (line != null) {
            String mirna = line.split("\t")[3];
            if (mirnaNeg.containsKey(mirna)) {
                mirnaNeg.put(mirna, mirnaNeg.get(mirna) + 1);
            } else {
                mirnaNeg.put(mirna, 1);
            }

            line = brNeg.readLine();
        }

        brPos.close();
        brNeg.close();

        Set<String> mirnasInTrain = new HashSet(mirnaPos.keySet());

        mirnasInTrain.addAll(mirnaNeg.keySet());

        System.out.println("miRNA\t positiveAppearences\t negativeAppearences");
        for (String mirna : mirnasInTrain) {
            if (mirnaPos.get(mirna) != null && mirnaNeg.get(mirna) != null) {
                System.out.println(mirna + "\t" + mirnaPos.get(mirna) + "\t" + mirnaNeg.get(mirna) + "\t");
            } else if (mirnaPos.get(mirna) != null) {
                System.out.println(mirna + "\t" + mirnaPos.get(mirna) + "\t0\t");
            } else if (mirnaNeg.get(mirna) != null) {
                System.out.println(mirna + "\t0\t" + mirnaNeg.get(mirna) + "\t");
            }
        }

    }

}
