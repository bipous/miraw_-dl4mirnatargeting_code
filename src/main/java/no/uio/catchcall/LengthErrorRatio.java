/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author apla
 */
public class LengthErrorRatio {

    /**
     * @param args the command line arguments 
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        int bindSize = 200;
        int[] correct = new int[ Math.floorDiv(5000, bindSize)+1];
        int[] wrong = new int[ Math.floorDiv(5000, bindSize)+1];
        for (int i = 0; i < correct.length; i++) {
            correct[i] = 0;
            wrong[i] = 0;
        }

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter('\t');

        String unifiedFileName = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/BalancedDataSets/randomLeveragedTestSplit_0.csv";
        String resultsFileName = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/Balanced/miRAW balanced Good/Balanced_Reg_0/targetPredictionOutput.csv";

        FileReader fileReaderUF = new FileReader(unifiedFileName);
        FileReader fileReaderResults = new FileReader(resultsFileName);
        CSVParser csvFileParserUF = new CSVParser(fileReaderUF, csvFileFormat);
        CSVParser csvFileParserRes = new CSVParser(fileReaderResults, csvFileFormat);
        List<CSVRecord> recordsUF = csvFileParserUF.getRecords();
        List<CSVRecord> recordsRes = csvFileParserRes.getRecords();

        for (int i = 1; i < recordsUF.size(); i++) {
            if (recordsRes.get(i).get(4).equalsIgnoreCase("0")) {
                int length = recordsUF.get(i).get(5).length();
                double pred = Double.parseDouble(recordsRes.get(i).get(3));
                double real = Double.parseDouble(recordsRes.get(i).get(4));
                if (pred > 0) {
                    pred = 1;
                } else {
                    pred = 0;
                }
                int bind = Math.floorDiv(length, bindSize);
                if (length > 5000) {
                    bind = Math.floorDiv(5000, bindSize);
                }
                if (pred == real) {
                    correct[bind]++;
                } else {
                    wrong[bind]++;
                }
            }
        }

        System.out.println("Bind\tCor.\tWrong\tError");
        for (int i = 0; i < correct.length; i++) {
            double error = (double) (wrong[i]) / (double) (wrong[i] + correct[i]);
            System.out.println((i * bindSize) + "\t" + correct[i] + "\t" + wrong[i] + "\t" + error);
        }
    }

}
