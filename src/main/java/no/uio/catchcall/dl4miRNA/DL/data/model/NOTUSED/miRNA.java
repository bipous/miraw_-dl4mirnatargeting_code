/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall.dl4miRNA.DL.data.model.NOTUSED;

import java.util.ArrayList;

/**
 *
 * @author apla
 */
public class miRNA {
    
    //from 3' to 5'
    protected String premirna;
    protected String primirna;
    protected String matureMirna;
    protected String altMatureMirna;

    protected String name;
    protected String species;
    protected ArrayList<String> conservation;
    
   

    //ids
    public miRNA() {
    }

    public miRNA(String premirna, String name, String species, ArrayList<String> conservation) {
        this.premirna = premirna;
        this.name = name;
        this.species = species;
        this.conservation = conservation;
    }

    //getters & setters
    public String getPremirna() {
        return premirna;
    }

    public void setPremirna(String premirna) {
        this.premirna = premirna;
    }

    public String getPrimirna() {
        return primirna;
    }

    public void setPrimirna(String primirna) {
        this.primirna = primirna;
    }

    public String getMatureMirna() {
        return matureMirna;
    }

    public void setMatureMirna(String matureMirna) {
        this.matureMirna = matureMirna;
    }

    public String getAltMatureMirna() {
        return altMatureMirna;
    }

    public void setAltMatureMirna(String altMatureMirna) {
        this.altMatureMirna = altMatureMirna;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public ArrayList<String> getConservation() {
        return conservation;
    }

    public void setConservation(ArrayList<String> conservation) {
        this.conservation = conservation;
    }

    //methods for getting nucleotide sequence
    public String getNucelotideInMatureMirna(int position) {
        if (position >= matureMirna.length() || position < 0) {
            return null;
        }

        return ("") + this.matureMirna.charAt(position);

    }

    public String getNucleotidesInMatureMirna(int initialPosition, int finalPosition) {
        if (initialPosition >= matureMirna.length() || initialPosition < 0) {
            return null;
        }
        if (finalPosition >= matureMirna.length() || finalPosition < 0) {
            return null;
        }
        return (String) matureMirna.subSequence(finalPosition, finalPosition);

    }

}
