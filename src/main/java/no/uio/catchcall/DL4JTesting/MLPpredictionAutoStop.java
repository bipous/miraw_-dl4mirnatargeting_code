package no.uio.catchcall.DL4JTesting;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;

import java.io.File;
import java.io.PrintWriter;
import java.util.Date;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;
import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculator;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.nn.api.Model;
import static org.reflections.vfs.Vfs.DefaultUrlTypes.directory;
import org.slf4j.LoggerFactory;
//import org.deeplearning4j.examples.feedforward.classification.PlotUtil;

/**
 * This class was used to understand how DL4J works
 * 
 * It is based on DL4J example:
 * "Saturn" Data Classification Example
 *
 * Based on the data from Jason Baldridge:
 * https://github.com/jasonbaldridge/try-tf/tree/master/simdata
 *
 * @author Josh Patterson
 * @author Alex Black (added plots)
 * @author Albert Pla (adaptation to miRNA target prediction)
 */
public class MLPpredictionAutoStop {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MLPprediction.class);
    
    public static void main(String[] args) throws Exception {
        Nd4j.ENFORCE_NUMERICAL_STABILITY = true;
        int batchSize = 50;
        int seed = 101;
        double learningRate = 0.005;
        //Number of epochs (full passes of the data)
        int nEpochs = 50;
        int numInputs = 241;
        int numOutputs = 2;
        int numHiddenNodes = 400;
        int[] networkShape = {241, numHiddenNodes, numHiddenNodes / 2, numHiddenNodes / 4, numHiddenNodes / 4, numOutputs};
        int hiddenLayers = networkShape.length - 2;
        int nSets = 10;

        String expName = "fullVal60-20-20";

        String experimentDirectory = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/experimentResults/" + expName;
        if (!(new File(experimentDirectory)).exists()) {
            new File(experimentDirectory).mkdir();
        }

        String dataSource = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/CVV60-20-20/CV602020_";

        PrintWriter evalResults = new PrintWriter(experimentDirectory + "/evaluation.txt");
        PrintWriter expNotes = new PrintWriter(experimentDirectory + "/notesBis.txt");

        expNotes.println("Nsets = " + nSets);
        expNotes.println("Data = " + dataSource);
        expNotes.println("Hidden layers = " + hiddenLayers);
        expNotes.print("Configurtion =");
        for (int l : networkShape) {
            expNotes.print(l + ", ");
        }
        expNotes.println();
        expNotes.println("Epochs = " + nEpochs);
        expNotes.println("Learning rate = " + learningRate);
        expNotes.println("BatchSize = " + batchSize);
        expNotes.println("========================Experimentation=======================");
        

        evalResults.println("Set\tTN\tFN\tTP\tFP\tAccuracy\tPrecision\tREcall\tF1Score");
        evalResults.flush();
        for (int set = 0; set < nSets; set++) {
            try {
                long date1 = new Date().getTime();
                log.info("############################# SET " + set + " #############################");

                log.info("Loading Training data");
                //Load the training data:
                RecordReader rrTrain = new CSVRecordReader();
                //rrTrain.initialize(new FileSplit(new File("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/Short/" + set + "_train.csv")));
                rrTrain.initialize(new FileSplit(new File(dataSource + set + "_train.csv")));
                //rrTrain.initialize(new FileSplit(new File("/Users/apla/Documents/DeepLearning4J/dl4j-0.4-examples/dl4j-examples/src/main/resources/classification/saturn_data_train2.csv")));

                DataSetIterator trainIter = new RecordReaderDataSetIterator(rrTrain, batchSize, 0, 2);

                //Load the model validation data:
                RecordReader rrVal = new CSVRecordReader();
                //rrTest.initialize(new FileSplit(new File("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/Short/" + set + "_test.csv")));
                rrVal.initialize(new FileSplit(new File(dataSource + set + "_val.csv")));
                
                //rrTest.initialize(new FileSplit(new File("/Users/apla/Documents/DeepLearning4J/dl4j-0.4-examples/dl4j-examples/src/main/resources/classification/saturn_data_train2.csv")));
                DataSetIterator valIter = new RecordReaderDataSetIterator(rrVal, batchSize, 0, 2);

                //Load the test/evaluation data:
                RecordReader rrTest = new CSVRecordReader();
                //rrTest.initialize(new FileSplit(new File("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/Short/" + set + "_test.csv")));
                rrTest.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                //rrTest.initialize(new FileSplit(new File("/Users/apla/Documents/DeepLearning4J/dl4j-0.4-examples/dl4j-examples/src/main/resources/classification/saturn_data_train2.csv")));
                DataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, batchSize, 0, 2);

                log.info("Creating model");
                //log.info("Build model....");
                MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                        .seed(seed)
                        .iterations(1)
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .learningRate(learningRate)
                        .updater(Updater.NESTEROVS).momentum(0.9)
                        .list()
                        .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                                .build())
                        .layer(1, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes / 2)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                                .build())
                        .layer(2, new DenseLayer.Builder().nIn(numHiddenNodes / 2).nOut(numHiddenNodes / 4)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                                .build())
                        .layer(3, new DenseLayer.Builder().nIn(numHiddenNodes / 4).nOut(numHiddenNodes / 4)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                                .build())
                        .layer(4, new OutputLayer.Builder(LossFunction.NEGATIVELOGLIKELIHOOD)
                                .weightInit(WeightInit.XAVIER)
                                .activation("softmax")
                                .nIn(numHiddenNodes / 4).nOut(numOutputs).build())
                        .pretrain(false).backprop(true).build();

                /*System.out.println("Initializing model");
                MultiLayerNetwork model = new MultiLayerNetwork(conf);
                model.init();
                model.setListeners(new ScoreIterationListener(10));    //Print score every 10 parameter updates
                 */
                //Configure earlyTrainer
                if (!(new File(experimentDirectory + "/set" + set)).exists()) {
                    new File(experimentDirectory + "/set" + set).mkdir();
                }

                EarlyStoppingConfiguration esConf = new EarlyStoppingConfiguration.Builder()
                        .epochTerminationConditions(new MaxEpochsTerminationCondition(nEpochs))
                        //.iterationTerminationConditions(new MaxTimeIterationTerminationCondition(20, TimeUnit.MINUTES))
                        .scoreCalculator(new DataSetLossCalculator(valIter, true))
                        
                        .evaluateEveryNEpochs(1)
                        .modelSaver(new LocalFileModelSaver(experimentDirectory + "/set" + set))
                        .build();

                EarlyStoppingTrainer trainer = new EarlyStoppingTrainer(esConf, conf, trainIter);
                
                EarlyStoppingResult<MultiLayerNetwork> trainingResult = trainer.fit();
                log.info("Best model epoch: "+trainingResult.getBestModelEpoch());
                //Print out the results:
                log.info("Training complete");
                log.info("Termination reason: " + trainingResult.getTerminationReason());
                log.info("Termination details: " + trainingResult.getTerminationDetails());
                log.info("Total epochs: " + trainingResult.getTotalEpochs());
                log.info("Best epoch number: " + trainingResult.getBestModelEpoch());
                log.info("Score at best epoch: " + trainingResult.getBestModelScore());
                
                MultiLayerNetwork model = trainingResult.getBestModel();
                
                long date2 = new Date().getTime();

                log.info("Evaluate model....");
                Evaluation eval = new Evaluation(numOutputs);
                while (testIter.hasNext()) {
                    DataSet t = testIter.next();
                    INDArray features = t.getFeatureMatrix();
                    INDArray lables = t.getLabels();
                    INDArray predicted = model.output(features, false);

                    eval.eval(lables, predicted);

                }
                
                long date3 = new Date().getTime();
                
                expNotes.println("Set "+set+" training time: "+((date2-date1)/1000));
                expNotes.println("Set "+set+" evaluation time: "+((date3-date2)/1000));
                
                log.info(eval.stats());
                
                 evalResults.println(set + "\t"
                        + eval.trueNegatives().get(1) + "\t"
                        + eval.falseNegatives().get(1) + "\t"
                        + eval.truePositives().get(1) + "\t"
                        + eval.falsePositives().get(1) + "\t"
                        + eval.accuracy() + "\t"
                        + eval.precision() + "\t"
                        + eval.recall() + "\t"
                        + eval.f1() + "\t");
                evalResults.flush();
                    

            } catch (Exception e) {
                e.printStackTrace();
                evalResults.println("Error! in set: " + set + "\t" + e.toString());
                evalResults.flush();
            }

        }
        evalResults.close();
        //------------------------------------------------------------------------------------
        //Training is complete. Code that follows is for plotting the data & predictions only
/*

        double xMin = -15;
        double xMax = 15;
        double yMin = -15;
        double yMax = 15;

        
        //Get test data, run the test data through the network to generate predictions, and plot those predictions:
        rrTest.initialize(new FileSplit(new File("src/main/resources/classification/saturn_data_eval2.csv")));
        rrTest.reset();
        int nTestPoints = 100;
        testIter = new RecordReaderDataSetIterator(rrTest,nTestPoints,0,2);
        ds = testIter.next();
        INDArray testPredicted = model.output(ds.getFeatures());
        PlotUtil.plotTestData(ds.getFeatures(), ds.getLabels(), testPredicted, allXYPoints, predictionsAtXYPoints, nPointsPerAxis);

        log.info("****************Example finished********************");

         */
    }

}
