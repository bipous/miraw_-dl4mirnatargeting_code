package no.uio.catchcall.DL4JTesting;

import no.uio.dl4miRNA.DCP.Binarization;
import java.io.BufferedOutputStream;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Date;
import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
//import org.deeplearning4j.ui.weights.HistogramIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.slf4j.LoggerFactory;
//import org.deeplearning4j.examples.feedforward.classification.PlotUtil;

/**
 * This class was used to understand how DL4J works
 * 
 * It is based on DL4J example:
 * "Saturn" Data Classification Example
 *
 * Based on the data from Jason Baldridge:
 * https://github.com/jasonbaldridge/try-tf/tree/master/simdata
 *
 * @author Josh Patterson
 * @author Alex Black (added plots)
 * @author Albert Pla (adaptation to miRNA target prediction)
 *
 */
public class MLPprediction {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MLPprediction.class);
    
    public static void main(String[] args) throws Exception {
        Nd4j.ENFORCE_NUMERICAL_STABILITY = true;
        int batchSize = 50;
        int seed = 101;
        double learningRate = 0.005;
        //Number of epochs (full passes of the data)
        int nEpochs = 1;
        int numInputs = 240;
        int numOutputs = 2;
        int numHiddenNodes = 400;
        int[] networkShape = {numInputs, numHiddenNodes, numHiddenNodes / 2, numHiddenNodes / 4, numHiddenNodes / 4, numOutputs};
        int hiddenLayers = networkShape.length - 2;
        int nSets = 10;

        String expName = "ShortExp";
        //String expName = "UnbalancedShuffled1";


        String experimentDirectory = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/experimentResults/" + expName;
        

        //String dataSource = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/CV75-25Shuffled/CV7525_";
        //String dataSource = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/unbalancedSHCVV_37i5-50-12i5-12i5/Set_";
        String dataSource = "/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/short/binary/set_";
        
        PrintWriter evalResults = new PrintWriter(experimentDirectory + "/lastModelEval.txt");
        PrintWriter evalResultsBest = new PrintWriter(experimentDirectory + "/BestModelEval.txt");

        PrintWriter expNotes = new PrintWriter(experimentDirectory + "/notes.txt");

        evalResults.println("Set\tTN\tFN\tTP\tFP\tAccuracy\tPrecision\tREcall\tF1Score");
        evalResults.flush();
        evalResultsBest.println("Set\tTN\tFN\tTP\tFP\tAccuracy\tPrecision\tREcall\tF1Score");
        evalResultsBest.flush();

        expNotes.println("========================Configuration========================");
        expNotes.println("Nsets = " + nSets);
        expNotes.println("Data = " + dataSource);
        expNotes.println("Hidden layers = " + hiddenLayers);
        expNotes.print("Configuration=");
        for (int l : networkShape) {
            expNotes.print(l + ", ");
        }
        expNotes.println();
        expNotes.println("Epochs = " + nEpochs);
        expNotes.println("Learning rate = " + learningRate);
        expNotes.println("BatchSize = " + batchSize);

        expNotes.println("========================Experimentation=======================");
        expNotes.flush();
        

        for (int set = 0; set < nSets; set++) {
            try {
                
                //SmallTrick to deal with wrong formated CSVs with an extra ',' at the end of each row and 'repair' the file.
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_train.csv");
                Binarization.removeUnnecesaryCommaFromCSV(dataSource + set + "_test.csv");
                
                
                long date1 = new Date().getTime();
                
                log.info("############################# SET " + set + " #############################");
                expNotes.println("############################# SET " + set + " #############################");

                log.info("Loading Training data");
                //Load the training data:

                RecordReader rrTrain = new CSVRecordReader();
                //rrTrain.initialize(new FileSplit(new File("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/Short/" + set + "_train.csv")));
                rrTrain.initialize(new FileSplit(new File(dataSource + set + "_train.csv")));
                
                
                //rrTrain.initialize(new FileSplit(new File("/Users/apla/Documents/DeepLearning4J/dl4j-0.4-examples/dl4j-examples/src/main/resources/classification/saturn_data_train2.csv")));
                
                DataSetIterator trainIter = new RecordReaderDataSetIterator(rrTrain, batchSize, 0, 2);
                
                

                //Load the test/evaluation data:
                RecordReader rrTest = new CSVRecordReader();
                //rrTest.initialize(new FileSplit(new File("/Users/apla/Documents/MirnaTargetDatasets/selfGenerated/DataSet/CrossVal/Short/" + set + "_test.csv")));
                rrTest.initialize(new FileSplit(new File(dataSource + set + "_test.csv")));
                //rrTest.initialize(new FileSplit(new File("/Users/apla/Documents/DeepLearning4J/dl4j-0.4-examples/dl4j-examples/src/main/resources/classification/saturn_data_train2.csv")));
                DataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, batchSize, 0, 2);

                log.info("Creating model");
                //log.info("Build model....");
                MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                        .seed(seed)
                        .iterations(1)
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .learningRate(learningRate)
                        .updater(Updater.NESTEROVS).momentum(0.9)
                        .list()
                        .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                               .build())
                        .layer(1, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes / 2)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                                .build())
                        .layer(2, new DenseLayer.Builder().nIn(numHiddenNodes / 2).nOut(numHiddenNodes / 4)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                                .build())
                        .layer(3, new DenseLayer.Builder().nIn(numHiddenNodes / 4).nOut(numHiddenNodes / 4)
                                .weightInit(WeightInit.XAVIER)
                                .activation("relu")
                                .build())
                        .layer(4, new OutputLayer.Builder(LossFunction.NEGATIVELOGLIKELIHOOD)
                                .weightInit(WeightInit.XAVIER)
                                .activation("softmax")
                                .nIn(numHiddenNodes / 4).nOut(numOutputs).build())
                        .pretrain(false).backprop(true).build();

                log.info("Initializing model");
                MultiLayerNetwork model = new MultiLayerNetwork(conf);
                model.init(); 
//                model.setListeners(new ScoreIterationListener(500));    //Print score every 10 parameter updates
//                model.setListeners(new HistogramIterationListener(1));
                
                
                
                log.info("Training model");
                double bestScore = 99999999;
                int bestScoreEpoch = 0;
                MultiLayerNetwork bestModel = model.clone();
                for (int n = 0; n < nEpochs; n++) {
                    log.debug("Set " + set + " Epoch " + n + "/" + nEpochs);
                    model.fit(trainIter);

                    expNotes.println("Set " + set + ": Epoch " + n + " - score " + model.score());
                    if (bestScore >= Math.abs(model.score())) {
                        bestScoreEpoch = n;
                        bestModel = model.clone();
                        bestScore=Math.abs(model.score());
                    }
                    expNotes.flush();
                }
                long date2 = new Date().getTime();
                File setFile = new File(experimentDirectory + "/Set" + set);
                setFile.mkdir();

                //Save models
                File modelFile = new File(setFile.getPath()+"/bestModel.bin");
                ModelSerializer.writeModel(bestModel, modelFile, true);
                File modelFile2 = new File(setFile.getPath()+"/lastModel.bin");
                ModelSerializer.writeModel(bestModel, modelFile, true);
                ModelSerializer.writeModel(bestModel, modelFile2, true);
                
                //load model
                //model.setParams(Nd4j.readBinary(new File(setFile.getPath()+"/lastModel.bin")));

                expNotes.println("Set " + set + ": Best epoch- " + bestScoreEpoch + " Score: " + bestScore);
                expNotes.flush();

                log.info("Evaluate model....");
                Evaluation eval = new Evaluation(numOutputs);
                Evaluation evalBest = new Evaluation(numOutputs);
                while (testIter.hasNext()) {
                    DataSet t = testIter.next();
                    INDArray features = t.getFeatureMatrix();
                    INDArray lables = t.getLabels();
                    INDArray predicted = model.output(features, false);

                    INDArray predictedBest = bestModel.output(features, false);

                    eval.eval(lables, predicted);
                    evalBest.eval(lables, predictedBest);

                }
                long date3 = new Date().getTime();
                expNotes.println("Set "+set+" training time: "+((date2-date1)/1000));
                expNotes.println("Set "+set+" evaluation time: "+((date3-date2)/2000)); //2 evaluations done at the same time
                expNotes.flush();
                
                log.info(eval.stats());

                evalResults.println(set + "\t"
                        + eval.trueNegatives().get(1) + "\t"
                        + eval.falseNegatives().get(1) + "\t"
                        + eval.truePositives().get(1) + "\t"
                        + eval.falsePositives().get(1) + "\t"
                        + eval.accuracy() + "\t"
                        + eval.precision() + "\t"
                        + eval.recall() + "\t"
                        + eval.f1() + "\t");
                evalResults.flush();

                evalResultsBest.println(set + "\t"
                        + evalBest.trueNegatives().get(1) + "\t"
                        + evalBest.falseNegatives().get(1) + "\t"
                        + evalBest.truePositives().get(1) + "\t"
                        + evalBest.falsePositives().get(1) + "\t"
                        + evalBest.accuracy() + "\t"
                        + evalBest.precision() + "\t"
                        + evalBest.recall() + "\t"
                        + evalBest.f1() + "\t");
                evalResultsBest.flush();

            } catch (Exception e) {
                e.printStackTrace();
                evalResults.println("Error! in set: " + set + "\t" + e.toString());
                evalResults.flush();
                evalResultsBest.println("Error! in set: " + set + "\t" + e.toString());
                evalResultsBest.flush();
            }

        }
        evalResults.close();
        evalResultsBest.close();
        expNotes.close();
        //------------------------------------------------------------------------------------
        //Training is complete. Code that follows is for plotting the data & predictions only
/*

        double xMin = -15;
        double xMax = 15;
        double yMin = -15;
        double yMax = 15;

        
        //Get test data, run the test data through the network to generate predictions, and plot those predictions:
        rrTest.initialize(new FileSplit(new File("src/main/resources/classification/saturn_data_eval2.csv")));
        rrTest.reset();
        int nTestPoints = 100;
        testIter = new RecordReaderDataSetIterator(rrTest,nTestPoints,0,2);
        ds = testIter.next();
        INDArray testPredicted = model.output(ds.getFeatures());
        PlotUtil.plotTestData(ds.getFeatures(), ds.getLabels(), testPredicted, allXYPoints, predictionsAtXYPoints, nPointsPerAxis);

        System.out.println("****************Example finished********************");

         */
    }

}
