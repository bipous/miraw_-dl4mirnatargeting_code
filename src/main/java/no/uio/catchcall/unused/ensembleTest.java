/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.catchcall.unused;

import java.io.IOException;
import java.net.MalformedURLException;
import javax.xml.parsers.ParserConfigurationException;
import no.uio.dl4miRNA.Data.gathering.EnsemblAccess;
import org.xml.sax.SAXException;

/**
 *
 * @author apla
 */
public class ensembleTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, IOException, MalformedURLException, SAXException {
        // TODO code application logic here
        String ensembleId = "ENSG00000139618";
        String species = "human";
        String region= "17:29543960-29543986";
        
        System.out.println(EnsemblAccess.getEnsmbleIdOfGeneName("BRCA2"));
        System.out.println(EnsemblAccess.getTranscriptOfRegion(species,region));
        System.out.println(EnsemblAccess.getTranscriptOfEnsembleId(ensembleId));
        
        
        System.out.println(EnsemblAccess.getTranscriptOfGeneName("BRCA2"));
    }
    
}
