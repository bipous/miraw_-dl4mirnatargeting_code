/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.future.dl4miRNA.DL;

import no.uio.dl4miRNA.DCP.Binarization;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;
import no.uio.catchcall.charts.XYLineChart_AWT;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.arbiter.optimize.api.termination.TerminationCondition;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;

import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculator;
import org.deeplearning4j.earlystopping.termination.EpochTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.termination.ScoreImprovementEpochTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.eval.ROC;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.RBM;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.ui.weights.HistogramIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.LoggerFactory;

/**
 * This class creates and trains miRAW's Deep Neural Network. The neural network
 * is created using DL4J library.
 *
 * @author apla
 */
public class MLFullUTR {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MLFullUTR.class);

    protected int batchSize;        //Number of instances that are feed into the 
    //neural network at every iteration.
    protected int randomSeed;             //Seed used for the random number generation
    protected double learningRate;  //Speed at which the neural network learns
    protected int nEpochs;          //Number of epochs that can be used for trainning the network

    private int numInputs;          //Number of neural network input nodes (number of miRNA nodes + number of Sequence nodes)
    protected int numOutputs;       //Number of neural network output nodes (2, positive and negative)
    protected int numMidLayer;

    protected int miRNAInputs;      //Number of neural network input nodes corresponding to the miRNA
    protected int seqInputs;        //Number of neural network input nodes corresponding to the sequence;

    protected int[] networkShape;
    //Shape of the network defined by nodes at each layer
    protected int hiddenLayers;     //Number of hidden layers 
    protected int nSets;            //Number of sets/folds used when performing Cross Validation
    protected double dropout;       //Dropout rate, % of nodes deactivated at each iteration in order to prevent overtrainning
    protected int epochsWithoutChange;  //When using a CrossValidation (TTV), the learning will stop
    //after 'epochsWithoutChange' Number of epochs in which the network
    //has not improved
    protected int labelLocation;    //Class column index

    /**
     * Default constructor. Batch size = 50, randomSeed=101, nEpochs=50,
     * learningRate=0.005, numInputs=240, outputs=2,numHiddenNodes=400;
     * nSets=10; dropout=0.2; hiddenLayers = 4;
     */
    public MLFullUTR() {
        batchSize = 50;
        randomSeed = 101;
        learningRate = 0.005;
        nEpochs = 50;

        miRNAInputs = 4 * 30;
        seqInputs = 4 * 1000;

        numInputs = miRNAInputs + seqInputs;
        numOutputs = seqInputs / 4;
        hiddenLayers = 5;
        numMidLayer = 500;
        networkShape = makeShape();

        nSets = 10;

        dropout = 0.2;

        epochsWithoutChange = 25;

        labelLocation = 0;

    }

    /**
     * Makes the shape of the neural network into a compress deocmpress
     *
     * @return shape wit the number of nodes per layer.
     */
    private int[] makeShape() {

        int[] newShape = new int[this.hiddenLayers * 2 + 1];

        newShape[0] = numInputs;
        newShape[newShape.length - 1] = numOutputs;

        int difNodesPerLayerCompress = (numInputs - numMidLayer) / this.hiddenLayers;
        int difNodesPerLayerDecompress = (numOutputs - numMidLayer) / this.hiddenLayers;

        for (int i = 1; i < hiddenLayers; i++) {
            newShape[i] = numInputs - difNodesPerLayerCompress * i;
        }
        int j = 1;
        newShape[hiddenLayers] = this.numMidLayer;
        for (int i = hiddenLayers + 1; i < newShape.length - 1; i++) {
            newShape[i] = numMidLayer + (difNodesPerLayerDecompress * j);
            j++;
        }

        return newShape;
    }

    /**
     * Creates a neural network following the number of nodes and layers defined
     * in the "shape" attribute.
     *
     * @param webListeners adds a listener to the neural network so its training
     * can be monitorized in a web browser.
     * @return MutilLayer Neural Network with the desired shape.
     */
    private MultiLayerNetwork createCustomModel(boolean webListeners) {

        int iterations = 10;
        int listenerFreq = 1;

        log.info("Creating model");
        //log.info("Build model....");

        //Create empty model with agreed parameters;
        NeuralNetConfiguration.ListBuilder LB = new NeuralNetConfiguration.Builder()
                .seed(randomSeed)
                .iterations(iterations)
                //.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .optimizationAlgo(OptimizationAlgorithm.LINE_GRADIENT_DESCENT)
                .learningRate(learningRate)
                .updater(Updater.NESTEROVS).momentum(0.9)
                .dropOut(dropout)
                .list();

        //add firstLayer
        /*LB.layer(0, new DenseLayer.Builder().nIn(networkShape[0]).nOut(networkShape[1])
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.RELU)
                .build()).pretrain(false).
                backprop(true);*/
        LB.layer(0, new RBM.Builder().nIn(networkShape[0]).nOut(networkShape[1]).lossFunction(LossFunctions.LossFunction.KL_DIVERGENCE).build()).pretrain(false).
                backprop(true);
        log.debug("Input Layer " + networkShape[0] + " to " + networkShape[1]);

        //add hidden layers
        for (int i = 1; i < this.networkShape.length - 2; i++) {
            //for each layer, obtain the number of desired hidden nodes.
            log.debug("Hidden layer " + networkShape[i] + " to " + networkShape[i + 1]);
            /*LB.layer(i, new DenseLayer.Builder().nIn(networkShape[i]).nOut(networkShape[i + 1])
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.RELU)
                    .build()).pretrain(false).
                    backprop(true);*/
            LB.layer(i, new RBM.Builder().nIn(networkShape[i]).nOut(networkShape[i + 1])
                    .build()).pretrain(false).
                    backprop(true);

        }

        //add output layer
        LB.layer(networkShape.length - 2, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.SOFTMAX)
                .nIn(networkShape[networkShape.length - 2]).nOut(networkShape[networkShape.length - 1]).build())
                .pretrain(false).backprop(true);

        log.debug("Output Layer " + networkShape[networkShape.length - 2] + " to " + networkShape[networkShape.length - 1]);

        //build model
        log.info("Initializing model");
        MultiLayerConfiguration conf = LB.build();
        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        model.setListeners(new ScoreIterationListener(listenerFreq));

        if (webListeners) {
            UIServer uiServer = UIServer.getInstance();
            //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
            StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for saving and loading later
            //Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to be visualized
            uiServer.attach(statsStorage);

            model.setListeners(new StatsListener(statsStorage));
            //model.setListeners(new ScoreIterationListener(500),new HistogramIterationListener(1));    //Print score every 10 parameter updates
        }

        UIServer uiServer = UIServer.getInstance();
        StatsStorage statsStorage = new InMemoryStatsStorage();
        uiServer.attach(statsStorage);
        model.setListeners(new StatsListener(statsStorage),new ScoreIterationListener(listenerFreq));

        return model;
    }

    /*CrossValidation Methods*/
    public void crossValidate(String inputDirectory, String outputDirectory) throws IOException, InterruptedException {
        new File(outputDirectory).mkdir();

        for (int i = 0; i < this.nSets; i++) {
            MultiLayerNetwork model5 = trainSet(inputDirectory + "/set" + i + "/5prime");
            evaluateUNET(model5, inputDirectory + "/set" + i + "/5prime/inputData.csv", inputDirectory + "/set" + i + "/5prime/labels.csv");

            new File(outputDirectory + "/" + i).mkdir();
            File modelFile5 = new File(outputDirectory + "/" + i + "/model5prime.bin");
            ModelSerializer.writeModel(model5, modelFile5, true);

            MultiLayerNetwork model3 = trainSet(inputDirectory + "/set" + i + "/3prime");
            evaluateUNET(model3, inputDirectory + "/set" + i + "/3prime/inputData.csv", inputDirectory + "/set" + i + "/3prime/labels.csv");
            File modelFile3 = new File(outputDirectory + "/" + i + "/model3prime.bin");
            ModelSerializer.writeModel(model3, modelFile3, true);

        }

    }

    public MultiLayerNetwork trainSet(String directory) throws IOException, InterruptedException {
        MultiLayerNetwork model = createCustomModel(false);

        for (int n = 0; n < nEpochs; n++) {
            log.info(n + "/" + nEpochs + " epochs trained ");
            //Load the training data:
            RecordReader rrTrainInput = new CSVRecordReader();
            rrTrainInput.initialize(new FileSplit(new File(directory + "/inputData.csv")));
            DataSetIterator trainIterInput = new RecordReaderDataSetIterator(rrTrainInput, batchSize);

            RecordReader rrTrainOutput = new CSVRecordReader();
            rrTrainOutput.initialize(new FileSplit(new File(directory + "/labels.csv")));
            DataSetIterator trainIterOutput = new RecordReaderDataSetIterator(rrTrainOutput, batchSize);
            int i = 0;
            while (trainIterInput.hasNext() && trainIterOutput.hasNext()) {
                DataSet in = trainIterInput.next();
                DataSet out = trainIterOutput.next();
                model.fit(new DataSet(in.getFeatureMatrix(), out.getFeatureMatrix()));
                i++;
            }
            log.info("Epoch {}/{}", (n + 1), nEpochs);
        }

        return model;

    }

    public void evaluateUNET(MultiLayerNetwork model, String inputTestFile, String outputTestFile) throws IOException, InterruptedException {
//Load the testing data:
        RecordReader rrTestInput = new CSVRecordReader();
        rrTestInput.initialize(new FileSplit(new File(inputTestFile)));
        DataSetIterator testIterInput = new RecordReaderDataSetIterator(rrTestInput, batchSize);

        RecordReader rrTestOutput = new CSVRecordReader();
        rrTestOutput.initialize(new FileSplit(new File(outputTestFile)));
        DataSetIterator testIterOutput = new RecordReaderDataSetIterator(rrTestOutput, batchSize);

        while (testIterInput.hasNext() && testIterOutput.hasNext()) {
            INDArray inputData = testIterInput.next().getFeatureMatrix();
            INDArray expectedData = testIterOutput.next().getFeatureMatrix();
            List<INDArray> activatedLayers = model.feedForward(inputData);
            INDArray obtainedData = activatedLayers.get(model.getnLayers() - 1);

            double error = compareTwoArrays(expectedData, obtainedData);
        }

    }

    public double compareTwoArrays(INDArray arr1, INDArray arr2) {
        int size1 = arr1.size(0);
        int size2 = arr2.size(0);
        ArrayList<Double> expectedData = new ArrayList<>();
        ArrayList<Double> obtainedData = new ArrayList<>();
        ArrayList<Double> errorInData = new ArrayList<>();
        if (size1 == size2) {
            double error = 0;
            for (int i = 0; i < size1; i++) {
                error = +Math.abs(arr1.getDouble(i) - arr2.getDouble(i));
                expectedData.add(arr1.getDouble(i));
                obtainedData.add(arr2.getDouble(i));
                errorInData.add(error);
            }
            XYLineChart_AWT plot = XYLineChart_AWT.showSimpleXYPlot(obtainedData);
            plot.addSerie(expectedData, "expectedData");
            plot.addSerie(errorInData, "error");
            plot.updateChartDataSoftLine();
            plot.updateChartData();
            return error / size1;
        }

        return 1;
    }

    /*#########################################
    ############# Getters & Setters ###########
    #########################################*/
    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public int getRandomSeed() {
        return randomSeed;
    }

    public void setRandomSeed(int randomSeed) {
        this.randomSeed = randomSeed;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public int getnEpochs() {
        return nEpochs;
    }

    public void setnEpochs(int nEpochs) {
        this.nEpochs = nEpochs;
    }

    public int getNumInputs() {
        return numInputs;
    }

    public int getMiRNAInputs() {
        return miRNAInputs;
    }

    public void setMiRNAInputs(int miRNAInputs) {
        this.miRNAInputs = miRNAInputs;
        this.setNumInputs(this.miRNAInputs + this.seqInputs);
    }

    public int getSeqInputs() {
        return seqInputs;
    }

    public void setSeqInputs(int seqInputs) {
        this.seqInputs = seqInputs;
        this.setNumInputs(this.miRNAInputs + this.seqInputs);
    }

    public void setNumInputs(int miRNAInputs, int seqInputs) {
        this.miRNAInputs = miRNAInputs;
        this.seqInputs = seqInputs;
        this.setNumInputs(this.miRNAInputs + this.seqInputs);
    }

    private void setNumInputs(int numInputs) {
        this.numInputs = numInputs;
        this.networkShape = this.makeShape();
    }

    public int getNumOutputs() {
        return numOutputs;
    }

    public void setNumOutputs(int numOutputs) {
        this.numOutputs = numOutputs;
    }

    public int[] getNetworkShape() {
        return networkShape;
    }

    public void updateNetworkShape() {
        this.makeShape();
    }

    public void setNetworkShape(int[] networkShape) {
        int[] tempNetworkShape = networkShape;
        if (tempNetworkShape != null && tempNetworkShape.length > 1) {
            //checks if the number of input nodes is correct;
            if (tempNetworkShape[0] != this.numInputs) {
                log.warn("Incorrect number of input nodes; the network shape of the first layer will be corrected to" + numInputs);
                tempNetworkShape[0] = this.numInputs;
            }

            if (tempNetworkShape[tempNetworkShape.length - 1] != this.numOutputs) {
                log.warn("Incorrect number of output nodes; the network shape of the first layer will be corrected to" + numOutputs);
                tempNetworkShape[tempNetworkShape.length - 1] = this.numOutputs;
            }

            this.networkShape = tempNetworkShape;
        } else {
            log.warn("Empty or too small network shape, it won't be updated.");
        }

    }

    public int getHiddenLayers() {
        return hiddenLayers;
    }

    public void setHiddenLayers(int hiddenLayers) {
        this.hiddenLayers = hiddenLayers;
        this.networkShape=this.makeShape();
    }

    public int getnSets() {
        return nSets;
    }

    public void setnSets(int nSets) {
        this.nSets = nSets;
    }

    public double getDropout() {
        return dropout;
    }

    public void setDropout(double dropout) {
        this.dropout = dropout;
    }

    public int getEpochsWithoutChange() {
        return epochsWithoutChange;
    }

    public void setEpochsWithoutChange(int epochsWithoutChange) {
        this.epochsWithoutChange = epochsWithoutChange;
    }

    public int getLabelLocation() {
        return labelLocation;
    }

    public void setLabelLocation(int labelLocation) {
        this.labelLocation = labelLocation;
    }

    public int getNumMidLayer() {
        return numMidLayer;
    }

    public void setNumMidLayer(int numMidLayer) {
        this.numMidLayer = numMidLayer;
        this.networkShape=this.makeShape();
    }

    public static void main(String args[]) throws IOException, InterruptedException {
        /* INDArray ar1 = Nd4j.zeros(1000, 1);
        INDArray ar2 = Nd4j.ones(1000, 1);

        ar1.put(99,0,1);
        ar1.put(100,0,1);
        ar1.put(101,0,1);
        ar1.put(102,0,1);
        ar1.put(103,0,1);
        ar1.put(104,0,1);
        ar1.put(105,0,1);
        ar1.put(106,0,1);
        ar1.put(107,0,1);
       
        learner.compareTwoArrays(ar1, ar2);*/

        MLFullUTR learner = new MLFullUTR();
        learner.setMiRNAInputs(30 * 4);
        learner.setSeqInputs(1000 * 4);
        learner.setHiddenLayers(3);
        learner.setNumOutputs(1000);
        learner.setNumMidLayer(1000);

        
        
        learner.setnSets(1);
        learner.setnEpochs(10);
        learner.setDropout(0);
        //learner.setLearningRate(0.0025);

        learner.crossValidate("/Users/apla/Documents/MirnaTargetDatasets/UnetTraining/CVData/", "/Users/apla/Documents/MirnaTargetDatasets/UnetTraining/Resutls/test1");

    }
}
