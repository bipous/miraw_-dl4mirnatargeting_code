/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.future.dl4miRNA.data.model;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author apla
 */
public class UnetDataInstance {
    
    private String UTR3transcript;
    private String miRNATranscript;
    
    private String mirnaName;
    private String transcriptId;
    
    private ArrayList<Integer> targetStartIndexes;
    private ArrayList<Integer> targetEndIndexes;
    
    private int[] outputLabels;

    public UnetDataInstance(String UTR3transcript, String miRNATranscript) {
        this.UTR3transcript = UTR3transcript;
        this.miRNATranscript = miRNATranscript;
        this.targetStartIndexes = new ArrayList<>();
        this.targetEndIndexes = new ArrayList<>();
    }

    public UnetDataInstance(String UTR3transcript, String miRNATranscript, String mirnaName, String transcriptId) {
        this.UTR3transcript = UTR3transcript;
        this.miRNATranscript = miRNATranscript;
        this.mirnaName = mirnaName;
        this.transcriptId = transcriptId;
        
        this.targetStartIndexes = new ArrayList<>();
        this.targetEndIndexes = new ArrayList<>();
    }

    public String getUTR3transcript() {
        return UTR3transcript;
    }

    public void setUTR3transcript(String UTR3transcript) {
        this.UTR3transcript = UTR3transcript;
    }

    public String getMiRNATranscript() {
        return miRNATranscript;
    }

    public void setMiRNATranscript(String miRNATranscript) {
        this.miRNATranscript = miRNATranscript;
    }

    public String getMirnaName() {
        return mirnaName;
    }

    public void setMirnaName(String mirnaName) {
        this.mirnaName = mirnaName;
    }

    public String getTranscriptId() {
        return transcriptId;
    }

    public void setTranscriptId(String transcriptId) {
        this.transcriptId = transcriptId;
    }

    public ArrayList<Integer> getTargetStartIndexes() {
        return targetStartIndexes;
    }
 
    public ArrayList<Integer> getTargetEndIndexes() {
        return targetEndIndexes;
    }

      public int[] getOutputLabels() {
        return outputLabels;
    }
    
    
    /* ###########################################
    ############### METHODS #####################
    ########################################### */
    
    public void addTarget(int start, int end){
        int startaux = start;
        int endaux = end;
        if (startaux>endaux){
            startaux=end;
            endaux=start;
        }
        this.targetEndIndexes.add(endaux);
        this.targetStartIndexes.add(startaux);
    }
    
    public void generateLabels(){
        
        this.outputLabels = new int[this.UTR3transcript.length()];
        
        
        for (int i=0;i<this.outputLabels.length;i++){
            outputLabels[i]=0;            
        }
        for (int i=0; i<this.targetEndIndexes.size(); i++){
            for (int j= this.targetStartIndexes.get(i)-1;j<this.targetEndIndexes.get(i)-1;j++){    //TODO: I DO NOT REALLY UNDERSTAND WHY THIS -2 IS REQUIRED... REPASSAR
                try{
                outputLabels[j]=1;
                }catch(java.lang.ArrayIndexOutOfBoundsException ex){
                    System.out.println(ex.getMessage());
                    System.out.println("i="+i);
                    System.out.println("j="+j);
                    System.out.println("targetSTartIndexes="+this.targetStartIndexes.get(i));
                    System.out.println("targetEndIndexes="+this.targetEndIndexes.get(i));
                    System.out.println("UTRLEngth "+this.UTR3transcript.length());
                    System.out.println();
                    
                }
            }
        }
        
    }
    
    
    
    
}
