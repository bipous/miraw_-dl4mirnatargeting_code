/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.future.dl4miRNA.DCP;

import no.uio.future.dl4miRNA.data.model.UnetDataInstance;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import no.uio.dl4miRNA.DCP.Binarization;
import no.uio.dl4miRNA.Data.gathering.FastaAccess;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author apla
 */
public class Data4UNET {

    ArrayList<UnetDataInstance> mirnaUtrPairs;
    int maxMirnaLength;
    int maxUTRLength;

    public Data4UNET() {
        this.mirnaUtrPairs = new ArrayList<>();
    }

    public ArrayList<UnetDataInstance> getMirnaUtrPairs() {
        return mirnaUtrPairs;
    }

    public void setMaxMirnaLength(int maxMirnaLength) {
        this.maxMirnaLength = maxMirnaLength;
    }

    public void setMaxUTRLength(int maxUTRLength) {
        this.maxUTRLength = maxUTRLength;
    }

    public int searchPair(String miRNA, String transcriptID) {
        for (int i = 0; i < this.mirnaUtrPairs.size(); i++) {
            if (mirnaUtrPairs.get(i).getMirnaName().equalsIgnoreCase(miRNA)
                    && mirnaUtrPairs.get(i).getTranscriptId().equalsIgnoreCase(transcriptID)) {
                return i;
            }
        }

        return -1;
    }

    public void readMirnaUPairs(String mirBaseFile, String targetsFile) throws IOException {
        HashMap<String, String> mirnaTranscripts = FastaAccess.obtainNamesAndSequences(mirBaseFile, "hsa");

        FileReader dataFileReader = new FileReader(targetsFile);
        CSVFormat csvFileFormat = CSVFormat.RFC4180.withHeader().withDelimiter('\t');
        CSVParser csvFileParser = new CSVParser(dataFileReader, csvFileFormat);
        List<CSVRecord> targetRecords = csvFileParser.getRecords();

        String indSiteStart = null, indSiteEnd = null, indMirnaName = null, indMirnaTranscript = null, indTranscriptId = null, ind3UTRTranscript = null;

        if (csvFileParser.getHeaderMap().keySet().contains("siteStart3UTR")) {
            indSiteStart = "siteStart3UTR";
            indSiteEnd = "siteEnd3UTR";
            indMirnaName = "mirnaName";
            indMirnaTranscript = "mirnaSequence";
            indTranscriptId = "TranscriptId";
            ind3UTRTranscript = "mRNA3utr";
        } else if (csvFileParser.getHeaderMap().keySet().contains("seqStart")) {
            indSiteStart = "seqStart";
            indSiteEnd = "seqEnd";
            indMirnaName = "mirnaName";
            indMirnaTranscript = "mirnaSequence";
            indTranscriptId = "TrasncriptId";
            ind3UTRTranscript = "ThreeUtr";
        }

        for (CSVRecord record : targetRecords) {
            String transcriptId = record.get(indTranscriptId);
            String mirnaName = record.get(indMirnaName);
            String mirnaSequence = record.get(indMirnaTranscript);
            int siteStart = Integer.parseInt(record.get(indSiteStart));
            int siteEnd = Integer.parseInt(record.get(indSiteEnd));
            String[] res = this.transformToStandardMirnaFormat(mirnaName, mirnaSequence, mirnaTranscripts);
            mirnaName = res[0];
            mirnaSequence = res[1];

            int ind = this.searchPair(mirnaName, transcriptId);
            if (ind < 0) {
                UnetDataInstance pair = new UnetDataInstance(record.get(ind3UTRTranscript), mirnaSequence, mirnaName, transcriptId);
                if (pair.getTargetEndIndexes().size() > 0) {
                    System.out.println("More than one target!!!");
                }
                pair.addTarget(siteStart, siteEnd);
                this.mirnaUtrPairs.add(pair);
            } else {
                this.mirnaUtrPairs.get(ind).addTarget(siteStart, siteEnd);
            }

        }
    }

    public void writeToFiles(String outputDirectory, boolean generateLabels) throws FileNotFoundException {
        Binarization bin = new Binarization();

        bin.setMaxMirnaLength(this.maxMirnaLength);
        bin.setMaxSiteLength(this.maxUTRLength);
        new File(outputDirectory + "/5prime").mkdir();
        new File(outputDirectory + "/3prime").mkdir();

        PrintWriter writer5prime = new PrintWriter(new File(outputDirectory + "/5prime/inputData.csv"));
        PrintWriter writer5primeOutput = new PrintWriter(new File(outputDirectory + "/5prime/labels.csv"));
        PrintWriter writer3prime = new PrintWriter(new File(outputDirectory + "/3prime/inputData.csv"));
        PrintWriter writer3primeOutput = new PrintWriter(new File(outputDirectory + "/3prime/labels.csv"));

        Collections.shuffle(this.mirnaUtrPairs);
        
        for (UnetDataInstance pair : this.mirnaUtrPairs) {
            if (generateLabels) {
                pair.generateLabels();
            }

            String utr = pair.getUTR3transcript();
            String utr5end, utr3end;
            int[] labels5end = new int[maxUTRLength], labels3end = new int[maxUTRLength];

            //3UTRs can be longer than the maxUTRLength. We create two classifiers, one aligned in the 5'end, one aligned in the 5'end. In future we can also create one for the 'middle'
            if (utr.length() > this.maxUTRLength) {
                utr5end = utr.substring(0, this.maxUTRLength);
                utr3end = utr.substring(utr.length() - this.maxUTRLength, utr.length());
                pair.getOutputLabels();

                if (generateLabels) {
                    labels5end = Arrays.copyOfRange(pair.getOutputLabels(), 0, maxUTRLength);
                    labels3end = Arrays.copyOfRange(pair.getOutputLabels(), utr.length() - this.maxUTRLength, utr.length());
                }
            } else {
                if (generateLabels) {
                    for (int i = 0; i < this.maxUTRLength; i++) {
                        labels5end[i] = 0;
                        labels3end[i] = 0;
                    }
                }
                utr5end = utr;
                utr3end = utr;
                while (utr5end.length() < maxUTRLength) {
                    utr5end = utr5end + "L";
                    utr3end = "L" + utr3end;
                }
                if (generateLabels) {
                    for (int i = 0; i < pair.getOutputLabels().length; i++) {
                        labels5end[i] = pair.getOutputLabels()[i];
                    }
                    int j = 1;
                    for (int i = pair.getOutputLabels().length - 1; i >= 0; i--) {
                        labels3end[labels3end.length - j] = pair.getOutputLabels()[i];
                        j++;
                    }
                }
            }

            //binarize and write to files
            int[] binarizedMirna = bin.binarizeMiRNA(pair.getMiRNATranscript());
            int[] binarizedUTR5end = bin.binarizeTranscript(utr5end, maxUTRLength);
            int[] binarizedUTR3end = bin.binarizeTranscript(utr3end, maxUTRLength);

            for (int val : binarizedMirna) {
                writer5prime.print(val + ",");
                writer3prime.print(val + ",");
            }
            for (int i = 0; i < binarizedUTR5end.length - 1; i++) {
                writer5prime.print(binarizedUTR5end[i] + ",");
                writer3prime.print(binarizedUTR3end[i] + ",");
            }
            writer5prime.println(binarizedUTR5end[binarizedUTR5end.length - 1]);
            writer3prime.println(binarizedUTR5end[binarizedUTR3end.length - 1]);
            if (generateLabels) {
                for (int i = 0; i < labels5end.length-1; i++) {
                    writer5primeOutput.print(labels5end[i] + ",");
                    writer3primeOutput.print(labels3end[i] + ",");
                    
                }
                writer5primeOutput.println(labels5end[labels5end.length - 1]);
                writer3primeOutput.println(labels3end[labels3end.length - 1]);
            }
            writer5prime.flush();
            writer5primeOutput.flush();
            writer3prime.flush();
            writer3primeOutput.flush();
        }
        writer5prime.close();
        writer5primeOutput.close();
        writer3prime.close();
        writer3primeOutput.close();
    }

    private String[] transformToStandardMirnaFormat(String mirnaName, String mirnaSequence, HashMap<String, String> mirnaTranscripts) {
        String mirnaID = mirnaName.split("\\*")[0];
        String providedMirnaSequence = mirnaSequence;
        String mirnaIdAux = mirnaID;

        //if the mirna is in MirBase, we get it from there
        String mirnaTranscript = mirnaTranscripts.get(mirnaID.toLowerCase());

        //if its not in mirBase
        if (mirnaTranscript == null && providedMirnaSequence.length() >= 8) {
            String mirnaID5p = mirnaID + "-5p";
            String mirnaID3p = mirnaID + "-3p";
            String providedTranscriptStart = providedMirnaSequence.substring(0, 7).replace("T", "U"); //we get the beggining of the provided transcript
            String transcript3 = mirnaTranscripts.get(mirnaID3p.toLowerCase()); //we check if mirbase has the 3p transcript
            String transcript5 = mirnaTranscripts.get(mirnaID5p.toLowerCase()); //we check if mirbase has teh 5p transcript

            //check if the provided transcript can be mapped with the 3p or 5p version of the mature mirna
            if (transcript3 != null && transcript3.startsWith(providedTranscriptStart)) {    //matches with 3p
                mirnaTranscript = transcript3;
                mirnaIdAux = mirnaID3p;

            } else if (transcript5 != null && transcript5.startsWith(providedTranscriptStart)) {     //matches with 5p
                mirnaTranscript = transcript5;
                mirnaIdAux = mirnaID5p;

            } else {
                //it does not map with mirbase. We use the provided one
                mirnaTranscript = providedMirnaSequence.replace("T", "U").replace("X", "L");
                //use the provided transcript. 
                //Some notations have an X meaning it does not matter the nucleotide. 
                //For that purpose we use "L" as in ViennRNA
            }
        } //now we have obtained the mirna transcriptç
        mirnaID = mirnaIdAux;

        String[] retMirna = {mirnaID, mirnaTranscript};
        return retMirna;

    }

    public static void main(String args[]) throws IOException {
        Data4UNET d4unet = new Data4UNET();
        d4unet.setMaxMirnaLength(30);
        d4unet.setMaxUTRLength(1000);
        String mirBaseFile = "/Users/apla/Documents/MirnaTargetDatasets/mirBase/r21/mature.fa";
        String targetsFileGrosswendt = "/Users/apla/Documents/MirnaTargetDatasets/UnetTraining/crossRefDatasets/GrosswendtValidated_FirstTarBase.txt";
        String targetsFileHelwak = "/Users/apla/Documents/MirnaTargetDatasets/UnetTraining/crossRefDatasets/HelwakValidated2_FirstDiana.txt";
        String outputDirectory = "/Users/apla/Documents/MirnaTargetDatasets/UnetTraining/CVData/set0";
        d4unet.readMirnaUPairs(mirBaseFile, targetsFileGrosswendt);
        d4unet.readMirnaUPairs(mirBaseFile, targetsFileHelwak);
        d4unet.writeToFiles(outputDirectory, true);

    }
}
