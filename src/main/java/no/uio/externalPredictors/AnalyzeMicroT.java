/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.externalPredictors;

import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;

/**
 * Implementation of AnalyzeExteranlTranslatedPredictor for microT datasets.
 *
 * @author apla
 */
public class AnalyzeMicroT extends AnalyzeExternalPredictor {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AnalyzeMicroT.class);

    @Override
    /**
     * {@inheritDoc}
     *
     */
    protected void obtainIndexes(String header) {
        String[] tsHeader = header.split(",");
        int[] indexes = new int[3];
        for (int i = 0; i < tsHeader.length; i++) {
            String val = tsHeader[i].toLowerCase();
            switch (val) {
                case "mirna-name(mirbase-version)":
                    indMirna = i;
                    break;
                case "geneid(name)":
                    indGN = i;
                    indGId = i;
                    break;
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * No PreProcessing required for microT
     * 
     * @param originalDataFile
     * @return
     */
    @Override
    protected String preProcess(String originalDataFile) {
        return originalDataFile;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    protected String[] processLine(String line, String speciesPrefix) {
        String vals[] = line.split(",");
        String interaction[] = null;
        //System.out.println(line);
        if (vals[indMirna].startsWith(speciesPrefix) && !vals[indGId].contains(":")) {

            interaction = new String[3];
            interaction[0] = vals[indMirna].split("\\(")[0];
            interaction[2] = vals[indGId].split("\\(")[0];
            /*
            String a = vals[indGN];
            String a2 = a.split("\\(")[1];
            String a3 = a2.split("\\)")[0];
            
            System.out.println(a);
            System.out.println(a2);
            System.out.println(a3);
            
            if (a3.equalsIgnoreCase("HOXD1")){
                int z=1;
            }
            System.out.println(vals[indGN]);
            System.out.println(vals[indGN].split("\\(")[1]);
            System.out.println(vals[indGN].split("\\(")[1].split("\\)")[0]);*/
            if (vals[indGN].split("\\(")[1].length() > 1) {
                interaction[1] = vals[indGN].split("\\(")[1].split("\\)")[0];
            } else {

                interaction[1] = "--NoName--";
            }

            //tsInteractMirna.add(interaction[0]);
            //tsInteractGN.add(interaction[1]);
            //tsInteractGId.add(interaction[2]);
        }
        return interaction;
    }

    public static void main1(String[] args) throws IOException {
        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/BalancedDataSets/randomLeveragedTestSplit_";
        String predictions = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/microTCDS/microT_CDS_data.csv";
        String speciesPrefix = "hsa";

        ArrayList<String> uFiles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            uFiles.add(uFile + i + ".csv");
        }
        AnalyzeExternalPredictor analyzer = new AnalyzeMicroT();
        analyzer.evaluateFile(uFiles, predictions, speciesPrefix, "results.txt", "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/microT balanced");
        System.out.println(predictions);
    }

    public static void main(String[] args) throws IOException {
        String predictions = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/microTCDS/microT_CDS_data.csv";
        String speciesPrefix = "hsa";

        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/UnbalancedDataSets/unbalancedTestSplit.csv";
        ArrayList<String> uFiles = new ArrayList<>();
        uFiles.add(uFile);

        AnalyzeExternalPredictor analyzer = new AnalyzeMicroT();
        analyzer.evaluateFile(uFiles, predictions, speciesPrefix, "results.txt", "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/Unbalanced/microT/");
        System.out.println(predictions);
    }

}
