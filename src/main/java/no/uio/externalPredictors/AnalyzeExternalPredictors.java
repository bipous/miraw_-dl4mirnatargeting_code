/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.externalPredictors;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import no.uio.dl4miRNA.DL.GenePredictionExecution;
import org.slf4j.LoggerFactory;

/**
 * Old class which was the origin of AnalyzeExternalPredictor. SHould be delted.
 * 
 * @author apla
 */
public class AnalyzeExternalPredictors {

    private ArrayList<HashMap<String, Boolean>> negativeInteractions;
    private ArrayList<HashMap<String, Boolean>> positiveInteractions;
    private final int TARGET_SCAN_BATCH_SIZE = 1000000;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AnalyzeExternalPredictors.class);

    public void evaluateFile(ArrayList<String> unifiedFiles, String targetScanFile, String speciesPrefix) {
        negativeInteractions = new ArrayList<>();
        positiveInteractions = new ArrayList<>();
        for (int i = 0; i < unifiedFiles.size(); i++) {
            negativeInteractions.add(new HashMap<>());
            positiveInteractions.add(new HashMap<>());
        }

        if (speciesPrefix == null) {
            speciesPrefix = "hsa";
        }
    }

    public void evaluateTargetScan(ArrayList<String> unifiedFiles, String targetScanFile, String speciesPrefix) throws FileNotFoundException, IOException {

        //Begin remove
        negativeInteractions = new ArrayList<>();
        positiveInteractions = new ArrayList<>();
        for (int i = 0; i < unifiedFiles.size(); i++) {
            negativeInteractions.add(new HashMap<>());
            positiveInteractions.add(new HashMap<>());
        }
        
        int cTP[] = new int[unifiedFiles.size()];
        int cTN[] = new int[unifiedFiles.size()];
        int cFP[] = new int[unifiedFiles.size()];
        int cFN[] = new int[unifiedFiles.size()];
        for (int i = 0; i < cTP.length; i++) {
            cTP[i] = cTN[i] = cFP[i] = cFN[i] = 0;
        }
        
       
        if (speciesPrefix == null) {
            speciesPrefix = "hsa";
        }

        //end remove
        
        //Start reading TargetScan File
        BufferedReader tsFRead = new BufferedReader(new FileReader(targetScanFile));
        //Get headers
        String[] tsHeader = tsFRead.readLine().split("\t");

        LOG.debug("Reading target scan headers");
        int tsIndMirna, tsIndGN, tsIndGId;
        
        //ABSTRACT METHOD STARTS HERE
        tsIndMirna = tsIndGN = tsIndGId = 0;
        for (int i = 0; i < tsHeader.length; i++) {
            String val = tsHeader[i].toLowerCase();
            switch (val) {
                case "mirna":
                    tsIndMirna = i;
                    break;
                case "gene symbol":
                    tsIndGN = i;
                    break;
                case "gene id":
                    tsIndGId = i;
                    break;
            }
        }
        //ABSTRACT METHOD ENDS
        
        //header has been read. Index of miRNAid, geneId and Gene simbol obtained

        String line;
        int nLines = 0;
        int species = 0;

        ArrayList<String[]> tsBatchOfInteractions = new ArrayList<>();
        int cBatch = 0;
        int iBatch = 0;
        LOG.debug("Starting {} batch of data", iBatch);
        while ((line = tsFRead.readLine()) != null) {

            //ABSTRACT PROCESS LINE BEGIN
            String vals[] = line.split("\t");
            if (vals[tsIndMirna].startsWith(speciesPrefix)) {
                String interaction[] = new String[3];
                interaction[0] = vals[tsIndMirna];
                interaction[1] = vals[tsIndGN];
                interaction[2] = vals[tsIndGId].split("\\.")[0];
                tsBatchOfInteractions.add(interaction);
                //tsInteractMirna.add(interaction[0]);
                //tsInteractGN.add(interaction[1]);
                //tsInteractGId.add(interaction[2]);

                species++;
                cBatch++;
            }
            //ABSTRACT PROCESSLINE END
            if (nLines % 1000000 == 0) {
                LOG.info("{} lines processed, {} {} interactions detected", nLines, species, speciesPrefix);
            }
            nLines++;

            if (cBatch >= TARGET_SCAN_BATCH_SIZE) {
                LOG.debug("Processing{} batch of data", iBatch);
                int j = 0;
                for (String unifiedFile : unifiedFiles) {
                    LOG.debug("Processing {} batch of data with the file: ", iBatch, unifiedFile);
                    int partialResults[] = checkStats(unifiedFile, tsBatchOfInteractions, j);
                    cTP[j] = partialResults[0] + cTP[j];
                    //cTN[j] = partialResults[1] + cTN[j];
                    //cFP[j] = partialResults[2] + cFP[j];
                    cFN[j] = partialResults[1] + cFN[j];
                    j++;
                }
                LOG.debug("{} batch of data successfully processed", iBatch);
                tsBatchOfInteractions = new ArrayList<>();
                cBatch = 0;
                iBatch++;
                LOG.debug("Starting {} batch of data", iBatch);

            }
        }

        int j = 0;
        for (String unifiedFile : unifiedFiles) {
            int partialResults[] = checkStats(unifiedFile, tsBatchOfInteractions, j);
            cTP[j] = partialResults[0] + cTP[j];
            //cTN[j] = partialResults[1] + cTN[j];
            //cFP[j] = partialResults[2] + cFP[j];
            cFN[j] = partialResults[1] + cFN[j];
            j++;
        }

        //countint TN & FP
        j = 0;
        for (HashMap<String, Boolean> setNegative : this.negativeInteractions) {
            for (String key : setNegative.keySet()) {
                if (setNegative.get(key)) {
                    cTN[j]++;
                } else {
                    cFP[j]++;
                }
            }
            j++;
        }

        j = 0;
        for (HashMap<String, Boolean> setPositive : this.positiveInteractions) {
            for (String key : setPositive.keySet()) {
                if (setPositive.get(key)) {
                    cTP[j]++;
                } else {
                    cFN[j]++;
                }
            }
            j++;
        }

        tsBatchOfInteractions = new ArrayList<>();
        cBatch = 0;

        LOG.info("TP\tFP\tTN\tFN\tAccuracy\tPrecision\tRecall(TPR)\tNeg.Precision\tSpecificity(TNR)\tF-score");
        double accu[] = new double[cTP.length];
        double precision[] = new double[cTP.length];
        double negprecision[] = new double[cTP.length];
        double recall[] = new double[cTP.length];
        double specificity[] = new double[cTP.length];
        double fscore[] = new double[cTP.length];
        for (int i = 0; i < cTP.length; i++) {
            double total = cTP[i] + cTN[i] + cFP[i] + cFN[i];
            accu[i] = (double) (cTP[i] + cTN[i]) / (total);
            precision[i] = (double) (cTP[i]) / (double) (cTP[i] + cFP[i]);
            negprecision[i] = (double) (cTN[i]) / (double) (cTN[i] + cFN[i]);
            recall[i] = (double) (cTP[i]) / (double) (cTP[i] + cFN[i]);
            specificity[i] = (double) (cTN[i]) / (double) (cTN[i] + cFP[i]);
            fscore[i] = 2 * (precision[i] * recall[i]) / (precision[i] + recall[i]);
            LOG.info("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t", cTP[i], cFP[i], cTN[i], cFN[i], accu[i], precision[i], recall[i], negprecision[i], specificity[i], fscore[i]);
        }

    }

    /**
     * Given a unified file, computes the statistics for the predicted batch of
     * interactions @interactionsBatch.
     *
     * @param unifiedFile File containing the experimentally verified targets
     * @param interactionsBatch List of String[3] tables where [0] is the miRNA
     * identifier, [1] the gene Symbol, and [2] the gene Id (in Ensembl format)
     * @param set The set that is being evaluated.
     * @return
     * @throws IOException
     */
    private int[] checkStats(String unifiedFile, ArrayList<String[]> interactionsBatch, int set) throws IOException {

        LOG.debug("Comparing with the different unified files");

        /*int cTP[], cTN[], cFP[], cFN[];
        cTP = new int[unifiedFiles.size()];
        cTN = new int[unifiedFiles.size()];
        cFP = new int[unifiedFiles.size()];
        cFN = new int[unifiedFiles.size()];

        for (int i = 0; i < unifiedFiles.size(); i++) {
            cTP[i] = cTN[i] = cFP[i] = cFN[i] = 0;
        }*/
        int cTP, cTN, cFP, cFN;
        cFP = 0;
        cTN = 0;
        cFN = 0;
        cTP = 0;

        BufferedReader unifFRead = new BufferedReader(new FileReader(unifiedFile));
        String unifHeader[] = unifFRead.readLine().split("\t");
        int uIndMirna, uIndGN, uIndGId, uIndClass;
        uIndMirna = uIndGN = uIndGId = uIndClass = 0;
        for (int i = 0; i < unifHeader.length; i++) {
            String val = unifHeader[i].toLowerCase();
            switch (val) {
                case "mirna":
                    uIndMirna = i;
                    break;
                case "gene_name":
                    uIndGN = i;
                    break;
                case "ensemblid":
                    uIndGId = i;
                    break;
                case "positive_negative":
                    uIndClass = i;
                    break;
            }
        }
        String line;
        while ((line = unifFRead.readLine()) != null) {
            String[] ufInteraction = line.split("\t");
            boolean TP, FP, TN, FN;
            TP = FP = TN = FN = false;
            if (ufInteraction[uIndClass].equalsIgnoreCase("1")) {
                //VAlidated positive case

                if (!this.positiveInteractions.get(set).containsKey(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN])) {
                    positiveInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], false);
                }

                for (String[] tsInteraction : interactionsBatch) {
                    //for (int j=0; j<tsInteractMirna.size();j++) {
                    // String tsInteraction[] = new String[3];
                    //tsInteraction[0] = tsInteractMirna.get(j);
                    //tsInteraction[1] = tsInteractGN.get(j);
                    //tsInteraction[2] = tsInteractGId.get(j);

                    if (tsInteraction[0].equalsIgnoreCase(ufInteraction[uIndMirna])
                            && (tsInteraction[1].equalsIgnoreCase(ufInteraction[uIndGN])
                            || tsInteraction[2].equalsIgnoreCase(ufInteraction[uIndGId]))) { //is predicted by TS
                        //TP
                        positiveInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], true);
                        //cTP++;
                        LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": TP");
                        break;
                    }
                }
                if (!TP) { //not predicted by TS
                    //FN
                    //FN = true;
                    LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": FN");
                    //cFN++;
                }
            } else //Validated negative case
            {
                if (!this.negativeInteractions.get(set).containsKey(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN])) {
                    negativeInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], true);
                }

                for (String[] tsInteraction : interactionsBatch) {
                    //for (int j=0; j<tsInteractMirna.size();j++) {
                    // String tsInteraction[] = new String[3];
                    //tsInteraction[0] = tsInteractMirna.get(j);
                    //tsInteraction[1] = tsInteractGN.get(j);
                    //tsInteraction[2] = tsInteractGId.get(j);

                    if (tsInteraction[0].equalsIgnoreCase(ufInteraction[uIndMirna])
                            && (tsInteraction[1].equalsIgnoreCase(ufInteraction[uIndGN])
                            || tsInteraction[2].equalsIgnoreCase(ufInteraction[uIndGId]))) { //is predicted by TS
                        //FP
                        negativeInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], false);
                        LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": FP");
                        //cFP++;
                        break;
                    }
                }

                if (!FP) {
                    //FN
                    //TN = true;
                    LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": TN");
                    //cTN++;
                }
            }
        }
        int stats[] = new int[4];
        //stats[0] = cTP;
        //stats[1] = cTN;
        //stats[2] = cFP;
        //stats[1] = cFN;
        return stats;
    }

    public static void main(String[] args) throws IOException {
        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/BalancedDataSets/randomLeveragedTestSplit_";
        String conservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/targetscanData/AllPredictions4RepresentativeScripts/Conserved_Site_Context_Scores.txt";
        String nonConservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/targetscanData/AllPredictions4RepresentativeScripts/Nonconserved_Site_Context_Scores.txt";
        String speciesPrefix = "hsa";

        ArrayList<String> uFiles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            uFiles.add(uFile + i + ".csv");
        }
        AnalyzeExternalPredictors analyzer = new AnalyzeExternalPredictors();
        analyzer.evaluateTargetScan(uFiles, nonConservedTsFile, speciesPrefix);
    }
}
