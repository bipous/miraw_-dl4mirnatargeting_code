/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.externalPredictors;

import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;

/**
 *
 * @author apla
 */
public class AnalyzeTSData extends AnalyzeExternalPredictor {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AnalyzeTSData.class);
    @Override
    /**
     * {@inheritDoc}
     */
    protected void obtainIndexes(String header) {
        String[] tsHeader = header.split("\t");
        int[] indexes = new int[3];
        for (int i = 0; i < tsHeader.length; i++) {
            String val = tsHeader[i].toLowerCase();
            switch (val) {
                case "mirna":
                    indMirna = i;
                    break;
                case "gene symbol":
                    indGN = i;
                    break;
                case "gene id":
                    indGId = i;
                    break;
            }
        }
    }
    
    @Override
    /**
     * {@inheritDoc}
     * 
     * No preprocess required
     */
    protected String preProcess(String originalDataFile) {
        return originalDataFile;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    protected String[] processLine(String line, String speciesPrefix) {
        String vals[] = line.split("\t");
        String interaction[] = null;
        if (vals[indMirna].startsWith(speciesPrefix)) {
            interaction = new String[3];
            interaction[0] = vals[indMirna];
            interaction[1] = vals[indGN];
            interaction[2] = vals[indGId].split("\\.")[0];

            //tsInteractMirna.add(interaction[0]);
            //tsInteractGN.add(interaction[1]);
            //tsInteractGId.add(interaction[2]);
        }
        return interaction;
    }
    
    public static void main1(String[] args) throws IOException {
        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/BalancedDataSets/randomLeveragedTestSplit_";
        String conservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/targetscanData/AllPredictions4RepresentativeScripts/Conserved_Site_Context_Scores.txt";
        String nonConservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/targetscanData/AllPredictions4RepresentativeScripts/Nonconserved_Site_Context_Scores.txt";
        String speciesPrefix = "hsa";

        ArrayList<String> uFiles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            uFiles.add(uFile + i + ".csv");
        }
        AnalyzeExternalPredictor analyzer = new AnalyzeTSData();
        analyzer.evaluateFile(uFiles, conservedTsFile, speciesPrefix, "results.txt","targetScanFolder");
        
    }
    
    public static void main(String[] args) throws IOException {
        String conservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/targetscanData/AllPredictions4RepresentativeScripts/Conserved_Site_Context_Scores.txt";
        String nonConservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/targetscanData/AllPredictions4RepresentativeScripts/Nonconserved_Site_Context_Scores.txt";
        String speciesPrefix = "hsa";

        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/UnbalancedDataSets/unbalancedTestSplit.csv";
        ArrayList<String> uFiles = new ArrayList<>();
        uFiles.add(uFile);
        
        AnalyzeExternalPredictor analyzer = new AnalyzeTSData();
        analyzer.evaluateFile(uFiles, conservedTsFile, speciesPrefix, "results.txt","/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/Unbalanced/targetScan conserved");
        
        analyzer = new AnalyzeTSData();
        analyzer.evaluateFile(uFiles, nonConservedTsFile, speciesPrefix, "results.txt","/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/Unbalanced/targetScan unconserved");
        
    }

}
