/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.externalPredictors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.LoggerFactory;

/**
 * This class is an abstract class that defines the process of comparing the
 * predictions stored in a database released by a target prediction tool, with a
 * particular unified file. It is useful for evaluating the performance of
 * different prediction tools using the same benchmark. This abstract class must
 * be implemented for each type of target prediction tool that needs to be
 * analyzed.
 *
 * Abstract methods are {@link obtainIndexes} (which searches in the header file
 * for the miRNa, geneId and Gene name positions), {@link preProcess} (that
 * performs any preprocessing action required in the originalDataFile), and
 * {@link processLine} (that reads a line of the original data file and returns
 * the miRNA identifier, the gene Symbol, and the gene Id).
 *
 * @author apla
 */
public abstract class AnalyzeExternalPredictor {

    private ArrayList<HashMap<String, Boolean>> negativeInteractions;
    private ArrayList<HashMap<String, Boolean>> positiveInteractions;
    private final int BATCH_SIZE = 1000000;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AnalyzeExternalPredictor.class);
    int indMirna;
    protected int indGId;
    protected int indGN;

    /**
     * Analyzes how many of the targets defined in a unified file where
     * correctly predicted by a prediction tool. Predictions are stored in the
     * {@link originalDataFile}.
     *
     * @param unifiedFiles unified files containing the benchmark targets
     * @param originalDataFile location of the file containing the predictions
     * @param speciesPrefix String that will define which targets are considered
     * @param destFileName Name of the file that will contain the evaluation
     * results
     * @param destFolder Name of the folder that will contain the evaluation
     * results
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void evaluateFile(ArrayList<String> unifiedFiles, String originalDataFile, String speciesPrefix, String destFileName, String destFolder) throws FileNotFoundException, IOException {

        String dataFile = preProcess(originalDataFile); //ABSTRACT METHOD

        //initialize variables
        negativeInteractions = new ArrayList<>();
        positiveInteractions = new ArrayList<>();
        for (int i = 0; i < unifiedFiles.size(); i++) {
            negativeInteractions.add(new HashMap<>());
            positiveInteractions.add(new HashMap<>());
        }

        int cTP[] = new int[unifiedFiles.size()];
        int cTN[] = new int[unifiedFiles.size()];
        int cFP[] = new int[unifiedFiles.size()];
        int cFN[] = new int[unifiedFiles.size()];
        for (int i = 0; i < cTP.length; i++) {
            cTP[i] = cTN[i] = cFP[i] = cFN[i] = 0;
        }

        //set default species prefix and separtor
        if (speciesPrefix == null) {
            speciesPrefix = "hsa";
        }

        //obtain indexes of miRNAid, geneId and genSimbol
        //read the header of the file
        BufferedReader fRead = new BufferedReader(new FileReader(dataFile));
        String header = fRead.readLine();

        obtainIndexes(header);  //ABSTRACT METHOD

        //initializing variables
        String line;
        int nLines = 0; //interactions analized
        int species = 0; //interactions corresponding to the 'target' species.

        ArrayList<String[]> batchOfInteractions = new ArrayList<>();
        int cBatch = 0; //interactions inside the current batch
        int iBatch = 0; //identifier of the current batch

        LOG.debug("Starting {} batch of data", iBatch);
        while ((line = fRead.readLine()) != null) {
            //process a line
            String interaction[] = processLine(line, speciesPrefix); //ABSTRACT METHOD

            if (interaction != null) {
                //if the interaction is not null (the line has species-related conten),
                //add the itneraction to the batch
                batchOfInteractions.add(interaction);
                species++;
                cBatch++;
            }
            if (nLines % 1000000 == 0) {
                LOG.info("{} lines processed, {} {} interactions detected", nLines, species, speciesPrefix);
            }
            nLines++;

            if (cBatch >= BATCH_SIZE) {
                LOG.debug("Processing{} batch of data", iBatch);
                int j = 0;
                for (String unifiedFile : unifiedFiles) {
                    LOG.debug("Processing {} batch of data with the file: ", iBatch, unifiedFile);
                    checkStats(unifiedFile, batchOfInteractions, j);
                    j++;
                }
                LOG.debug("{} batch of data successfully processed", iBatch);
                batchOfInteractions = new ArrayList<>();
                cBatch = 0;
                iBatch++;
                LOG.debug("Starting {} batch of data", iBatch);

            }
        }

        //evaluate the ramaining interactions
        int j = 0;
        for (String unifiedFile : unifiedFiles) {
            checkStats(unifiedFile, batchOfInteractions, j);
            j++;
        }

        //countint TN & FP
        j = 0;
        for (HashMap<String, Boolean> setNegative : this.negativeInteractions) {
            for (String key : setNegative.keySet()) {
                if (setNegative.get(key)) {
                    cTN[j]++;
                } else {
                    cFP[j]++;
                }
            }
            j++;
        }
        //Counting TP & FN

        j = 0;
        for (HashMap<String, Boolean> setPositive : this.positiveInteractions) {
            for (String key : setPositive.keySet()) {
                if (setPositive.get(key)) {
                    cTP[j]++;
                } else {
                    cFN[j]++;
                }
            }
            j++;
        }

        batchOfInteractions = new ArrayList<>();
        cBatch = 0;

        new File(destFolder).mkdir();

        PrintWriter writer = new PrintWriter(destFolder + "/" + destFileName);

        //Saving results
        LOG.info("TP\tFP\tTN\tFN\tAccuracy\tPrecision\tRecall(TPR)\tNeg.Precision\tSpecificity(TNR)\tF-score");
        writer.println("TP\tFP\tTN\tFN\tAccuracy\tPrecision\tRecall(TPR)\tNeg.Precision\tSpecificity(TNR)\tF-score");
        double accu[] = new double[cTP.length];
        double precision[] = new double[cTP.length];
        double negprecision[] = new double[cTP.length];
        double recall[] = new double[cTP.length];
        double specificity[] = new double[cTP.length];
        double fscore[] = new double[cTP.length];
        for (int i = 0; i < cTP.length; i++) {
            double total = cTP[i] + cTN[i] + cFP[i] + cFN[i];
            accu[i] = (double) (cTP[i] + cTN[i]) / (total);
            precision[i] = (double) (cTP[i]) / (double) (cTP[i] + cFP[i]);
            negprecision[i] = (double) (cTN[i]) / (double) (cTN[i] + cFN[i]);
            recall[i] = (double) (cTP[i]) / (double) (cTP[i] + cFN[i]);
            specificity[i] = (double) (cTN[i]) / (double) (cTN[i] + cFP[i]);
            fscore[i] = 2 * (precision[i] * recall[i]) / (precision[i] + recall[i]);
            LOG.info("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t", cTP[i], cFP[i], cTN[i], cFN[i], accu[i], precision[i], recall[i], negprecision[i], specificity[i], fscore[i]);
            writer.println(cTP[i] + "\t" + cFP[i] + "\t" + cTN[i] + "\t" + cFN[i] + "\t" + accu[i] + "\t" + precision[i] + "\t" + recall[i] + "\t" + negprecision[i] + "\t" + specificity[i] + "\t" + fscore[i]);
        }
        writer.close();

    }

    /**
     * Given a unified file, computes the statistics for the predicted batch of
     * interactions @interactionsBatch. A batch is an array containing several
     * instances of the originalDataFile.
     *
     * @param unifiedFile File containing the experimentally verified targets
     * @param interactionsBatch List of String[3] tables where [0] is the miRNA
     * identifier, [1] the gene Symbol, and [2] the gene Id (in Ensembl format)
     * @param set The set that is being evaluated.
     * @return
     * @throws IOException
     */
    private void checkStats(String unifiedFile, ArrayList<String[]> interactionsBatch, int set) throws IOException {

        LOG.debug("Comparing with the different unified files");

        /*int cTP[], cTN[], cFP[], cFN[];
        cTP = new int[unifiedFiles.size()];
        cTN = new int[unifiedFiles.size()];
        cFP = new int[unifiedFiles.size()];
        cFN = new int[unifiedFiles.size()];

        for (int i = 0; i < unifiedFiles.size(); i++) {
            cTP[i] = cTN[i] = cFP[i] = cFN[i] = 0;
        }*/
        BufferedReader unifFRead = new BufferedReader(new FileReader(unifiedFile));
        String unifHeader[] = unifFRead.readLine().split("\t");
        int uIndMirna, uIndGN, uIndGId, uIndClass;
        uIndMirna = uIndGN = uIndGId = uIndClass = 0;
        for (int i = 0; i < unifHeader.length; i++) {
            String val = unifHeader[i].toLowerCase();
            switch (val) {
                case "mirna":
                    uIndMirna = i;
                    break;
                case "gene_name":
                    uIndGN = i;
                    break;
                case "ensemblid":
                    uIndGId = i;
                    break;
                case "positive_negative":
                    uIndClass = i;
                    break;
            }
        }
        String line;
        while ((line = unifFRead.readLine()) != null) {
            String[] ufInteraction = line.split("\t");
            boolean TP, FP, TN, FN;
            TP = FP = TN = FN = false;
            if (ufInteraction[uIndClass].equalsIgnoreCase("1")) {
                //VAlidated positive case

                if (!this.positiveInteractions.get(set).containsKey(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN])) {
                    positiveInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], false);
                }

                for (String[] tsInteraction : interactionsBatch) {
                    //for (int j=0; j<tsInteractMirna.size();j++) {
                    // String tsInteraction[] = new String[3];
                    //tsInteraction[0] = tsInteractMirna.get(j);
                    //tsInteraction[1] = tsInteractGN.get(j);
                    //tsInteraction[2] = tsInteractGId.get(j);

                    if (ufInteraction[uIndMirna].toLowerCase().startsWith(tsInteraction[0].toLowerCase())
                            && (tsInteraction[1].equalsIgnoreCase(ufInteraction[uIndGN])
                            || tsInteraction[2].equalsIgnoreCase(ufInteraction[uIndGId]))) { //is predicted by TS
                        //TP
                        positiveInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], true);
                        //cTP++;
                        LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": TP");
                        TP = true;
                        break;
                    }
                }
                if (!TP) { //not predicted by TS
                    //FN
                    //FN = true;
                    LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": FN");
                    FN = true;
                    //cFN++;
                }
            } else //Validated negative case
            {
                if (!this.negativeInteractions.get(set).containsKey(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN])) {
                    negativeInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], true);
                }

                for (String[] tsInteraction : interactionsBatch) {
                    //for (int j=0; j<tsInteractMirna.size();j++) {
                    // String tsInteraction[] = new String[3];
                    //tsInteraction[0] = tsInteractMirna.get(j);
                    //tsInteraction[1] = tsInteractGN.get(j);
                    //tsInteraction[2] = tsInteractGId.get(j);

                    if (ufInteraction[uIndMirna].toLowerCase().startsWith(tsInteraction[0].toLowerCase())
                            && (tsInteraction[1].equalsIgnoreCase(ufInteraction[uIndGN])
                            || tsInteraction[2].equalsIgnoreCase(ufInteraction[uIndGId]))) { //is predicted by TS
                        //FP
                        negativeInteractions.get(set).put(ufInteraction[uIndMirna] + "+" + ufInteraction[uIndGN], false);
                        LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": FP");
                        FP = true;
                        //cFP++;
                        break;
                    }
                }

                if (!FP) {
                    //FN
                    //TN = true;
                    LOG.debug(ufInteraction[uIndMirna] + " - " + ufInteraction[uIndGN] + ": TN");
                    TN = true;
                    //cTN++;
                }
            }
        }

    }

    /*

    public static void main(String[] args) throws IOException {
        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/BalancedDataSets/randomLeveragedTestSplit_";
        String conservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/targetscanData/AllPredictions4RepresentativeScripts/Conserved_Site_Context_Scores.txt";
        String nonConservedTsFile = "/Users/apla/Documents/MirnaTargetDatasets/targetscanData/AllPredictions4RepresentativeScripts/Nonconserved_Site_Context_Scores.txt";
        String speciesPrefix = "hsa";

        ArrayList<String> uFiles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            uFiles.add(uFile + i + ".csv");
        }
        AnalyzeExternalPredictor analyzer = new AnalyzeExternalPredictor();
        //analyzer.evaluateTargetScan(uFiles, nonConservedTsFile, speciesPrefix);
    }
     */
    /**
     * Given the first line of the dataFile (header) it intializes the indMirna,
     * indGId and indGeneName parameters
     *
     * @param header first line of the dataFile *
     */
    protected abstract void obtainIndexes(String header);

    /**
     * If necessary, preprocesses the originalDataFile and transforms its data.
     * The method returns the string of the location of the file that contains
     * the data to be evaluated. Note that the output might be the same as the
     * input.
     *
     * @param originalDataFile
     * @return
     */
    protected abstract String preProcess(String originalDataFile);

    /**
     * Pocesses a line of the dataFile; if the line corresponds to the target
     * species of @speciesPrefis, it returns a string table where [0] is the
     * miRNA identifier, [1] the gene Symbol, and [2] the gene Id (in Ensembl
     * format); otherwhise, null.
     *
     * @param line
     * @param speciesPrefix
     * @return interaction (string table where [0] is the miRNA identifier, [1]
     * the gene Symbol, and [2] the gene Id (in Ensembl format))
     */
    protected abstract String[] processLine(String line, String speciesPrefix);

}
