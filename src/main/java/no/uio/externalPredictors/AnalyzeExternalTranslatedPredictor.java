/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.externalPredictors;

import no.uio.dl4miRNA.Utils.EnsemblNCBIDictionary;

/**
 * Abstract class to be implemented when the predicted targets database uses gene ids 
 * differents than the ones provided by Ensembl.
 * Here it is assumed that the implemented class will preprocess the data in order
 * to end up with an ensemblID compatible with the notation used in the unified file.
 * This will result in instances containing three indexes: 0-miRNAId, 1-geneId, 2-geneName
 * 
 * @author apla
 */
public abstract class AnalyzeExternalTranslatedPredictor extends AnalyzeExternalPredictor {

    @Override
    protected void obtainIndexes(String header) {
        this.indGId = 1;
        this.indMirna = 0;
        this.indGN= 2;
    }

    /**
     * Processes instances of the predicted targets databases assuming that they 
     * have been processed in such a way that NCBI ids have been translated to 
     * ensembl ids. 
     * @param line
     * @param speciesPrefix
     * @return 
     */
    @Override
    protected String[] processLine(String line, String speciesPrefix) {
        String vals[] = line.split("\t");
        String interaction[] = null;
        if (vals[indMirna].startsWith(speciesPrefix)) {
            interaction = new String[3];
            interaction[0] = vals[indMirna];
            if (vals[indGN] != null) {
                interaction[1] = vals[indGN];
            } else {
                interaction[1] = "-";
            }
            interaction[2] = vals[indGId];

            //tsInteractMirna.add(interaction[0]);
            //tsInteractGN.add(interaction[1]);
            //tsInteractGId.add(interaction[2]);
        }
        return interaction;
    }

}
