/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.externalPredictors;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.uio.dl4miRNA.Utils.EnsemblNCBIDictionary;
import no.uio.dl4miRNA.Utils.Translator;
import org.slf4j.LoggerFactory;

/**
 * Implementation of AnalyzeExteranlTranslatedPredictor for miRanda/mirSVR
 * datasets.
 *
 * @author apla
 */
public class AnalyzeMirSVR extends AnalyzeExternalTranslatedPredictor {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AnalyzeMirSVR.class);

    @Override
    /**
     * {@inheritDoc} FOR miRanda/mirTarget
     */

    protected String preProcess(String originalDataFile) {

        try {

            //initializing data and checking if the file already exists
            File origFile = new File(originalDataFile);
            String transFileName = origFile.getParent() + "/translation/" + origFile.getName();
            new File(origFile.getParent() + "/translation").mkdir();
            File transFile = new File(transFileName);
            if (transFile.exists()) {
                LOG.info("File was already translated");
                return transFileName;
            }

            //loading translation dictionary
            Translator trans = new Translator();
            EnsemblNCBIDictionary dic = new EnsemblNCBIDictionary();
            dic.loadDictionariesFrom("/Users/apla/Documents/DL4miRNA/Code/DL4miRNA/target/classes/ensmbl_ncbiTranscript");
            trans.setDictionary(dic);

            //Reading  & translating file 
            BufferedReader fileReader = null;
            fileReader = new BufferedReader(new FileReader(origFile));
            String line = fileReader.readLine(); //header
            BufferedWriter bw = new BufferedWriter(new FileWriter(transFile));

            while ((line = fileReader.readLine()) != null) {
                String[] elements = line.split("\t");
                String mirna = elements[1].split("\\*")[0];
                String gId = trans.NCBI2Ensembl(elements[5]);
                String gSymbol = elements[3];

                if (gId != null) {
                    bw.write(mirna + "\t" + gId + "\t" + gSymbol);
                    bw.newLine();
                    bw.flush();
                } else {
                    LOG.warn("No translation found for NCBI code " + gId);
                }
            }
            bw.close();
            fileReader.close();
            return transFileName;
        } catch (FileNotFoundException ex) {
            LOG.error("File not found exception");
        } catch (IOException ex) {
            LOG.error("File access exception");
        }
        return null;
    }

    /*
        this.indGId = 1;
        this.indMirna = 0;
        this.indGN = 2;
     */
    public static void main1(String[] args) throws IOException {
        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/BalancedDataSets/randomLeveragedTestSplit_";
        //String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_0_0_aug2010.txt"; //traduit

        //String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_S_0_aug2010.txt"; //traduit
        //String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_S_C_aug2010.txt"; //traduit
        String speciesPrefix = "hsa";

        ArrayList<String> uFiles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            uFiles.add(uFile + i + ".csv");
        }
        AnalyzeExternalPredictor analyzer = new AnalyzeMirSVR();
        String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_S_C_aug2010.txt";
        analyzer.evaluateFile(uFiles, predictorFile, speciesPrefix, "results.txt", "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/mirSVR balanced/SC");

        predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_0_0_aug2010.txt";
        analyzer = new AnalyzeMirSVR();
        analyzer.evaluateFile(uFiles, predictorFile, speciesPrefix, "results.txt", "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/mirSVR balanced/OO");

        analyzer = new AnalyzeMirSVR();
        predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_0_C_aug2010.txt";
        analyzer.evaluateFile(uFiles, predictorFile, speciesPrefix, "results.txt", "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/mirSVR balanced/OC");

        analyzer = new AnalyzeMirSVR();
        predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_S_0_aug2010.txt";
        analyzer.evaluateFile(uFiles, predictorFile, speciesPrefix, "results.txt", "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/mirSVR balanced/SO");

        System.out.println(predictorFile);

    }

    public static void main(String[] args) throws IOException {
        //String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_0_0_aug2010.txt"; //traduit

        //String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_S_0_aug2010.txt"; //traduit
        //String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_S_C_aug2010.txt"; //traduit
        String speciesPrefix = "hsa";
        String uFile = "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/DataSet/Original/UnbalancedDataSets/unbalancedTestSplit.csv";
        ArrayList<String> uFiles = new ArrayList<>();
        uFiles.add(uFile);

        AnalyzeExternalPredictor analyzer = new AnalyzeMirSVR();
        String predictorFile = "/Users/apla/Documents/MirnaTargetDatasets/putatitve targets DDBB/miRANDA-mirSVR/human_predictions_S_C_aug2010.txt";
        analyzer.evaluateFile(uFiles, predictorFile, speciesPrefix, "results.txt", "/Users/apla/Documents/MirnaTargetDatasets/BioinformaticsPaper/GenePredResults/Unbalanced/mirSVR/");

    }

}
