/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author apla
 */
public class TargetScanSiteFinderNGTest {
    
    public TargetScanSiteFinderNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of findCandidates method, of class TargetScanSiteFinder.
     */
    @org.testng.annotations.Test
    public void testFindCandidates() {
        System.out.println("findCandidates");
        String mirna = "";
        String mRNA = "";
        TargetScanSiteFinder instance = new TargetScanSiteFinder();
        ArrayList expResult = null;
        ArrayList result = instance.findCandidates(mirna, mRNA);
        //assertEquals(result, expResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /* ###########################################
    ########CANNONICAL TARGETS TESTS #############
    ##############################################
    */
    /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a perfect canonical 6mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical6mer() {
        String mirna = "UGGAAUAUAAAGAAGUAUGUAU";
        String seqB = "AUGUGAAAGACCAUUAUUCCA";
          System.out.println("Testing canonical 6mer");
        
        TargetScanSiteFinder instance = new TargetScanSiteFinder();
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-5.50);
        expectedResult.setBindsInSeed(6);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Cannonical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a perfect canonical 7mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical7mer() {
        System.out.println("Testing canonical 7mer");
        String mirna = "UAAAGUGCUUAUAGUGCAGGUAG";
        String seqB = "ATGTTTGATTTTATGCACTTTG";
        TargetScanSiteFinder instance = new TargetScanSiteFinder();
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-9.30);
        expectedResult.setBindsInSeed(6);       //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Cannonical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a perfect canonical 8mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical8mer() {
        System.out.println("Testing canonical 7mer");
        String mirna = "UCUUUGGUUAUCUAGCUGUAUGA";
        String seqB = "CCATACATAAAAGAAATAAACCAAAGG";
        TargetScanSiteFinder instance = new TargetScanSiteFinder();
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-13.20);
        expectedResult.setBindsInSeed(6); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Cannonical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /* ###########################################
    ##### NON-CANNONICAL TARGETS TESTS ###########
    ##############################################
    */
    
     /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a 6mer with a missing base pair (6 non consecutive base pairs separated in two blocks);
     * should be null as it is not compensated.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithGap() {
        System.out.println("Testing non canonical 6mer+Gap");
        String mirna = "UAAAGUGCUUAUAGUGCAGGUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        TargetScanSiteFinder instance = new TargetScanSiteFinder();        
        CandidateMBS expectedResult=null;
        /*CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-6.10);
        expectedResult.setBindsInSeed(5); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(5);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Compensatory");
        expectedResult.setIsCanonical(false);*/
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a 6mer with a missing base pair (6 non consecutive base pairs separated in two blocks);
     * should be candidate as it has a compensatory binding of 9.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithGapAndCompensatory() {
        System.out.println("Testing non canonical 6mer+Gap and compensatory pairs");
        String mirna = "UAAAGUGCUUAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        TargetScanSiteFinder instance = new TargetScanSiteFinder();        
        CandidateMBS expectedResult=null;
        
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-11.20);
        expectedResult.setBindsInSeed(5); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(5);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Compensatory");
        expectedResult.setIsCanonical(false);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a 6mer with a wobble pair; should be null because its not centered or compensatory.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithWobble() {
        System.out.println("Testing Compensatory 6mer+wobble");
        String mirna = "UGGAAUGUAAAGAAGUAUGUAU";
        String seqB = "AUGUGAAAGACCAUUAUUCCA";   
        TargetScanSiteFinder instance = new TargetScanSiteFinder();        
        
        CandidateMBS expectedResult = null;
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
      /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 6mer with a missing base pair (6 consecutive base pairs containing 1 wobble);
     * should be candidate because it has compensatory pairing.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithWobbleAndCompensatory() {
        System.out.println("Testing non canonical 6mer+Gap and compensatory pairs");
        String mirna = "UAAAGGGAUUAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT"; 
                
                
        TargetScanSiteFinder instance = new TargetScanSiteFinder();         
        CandidateMBS expectedResult=null;
        
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-11.70);
        expectedResult.setBindsInSeed(6); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(5);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Compensatory");
        expectedResult.setIsCanonical(false);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     /**
     *  Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a 7mer with a wobble pair; should be null.
     */
    @org.testng.annotations.Test
    public void testIsCandidate7merWithGap() {
        System.out.println("Testing non canonical 7mer+Gap");
        String mirna = "UAAAGUGCAUUUAGUGCAGGUAG";
        String seqB = "UAAUUUACAUUAUUGCUCUUUUT";       
        TargetScanSiteFinder instance = new TargetScanSiteFinder();        
        CandidateMBS expectedResult = null;             
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 7mer with a missing base pair (7 non consecutive base pairs separated in two blocks);
     * should be candidate as it has compensatory binding.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithGapAndCompensatory() {
        System.out.println("Testing non canonical 7mer+Gap and compensatory pairs");
        String mirna = "UAAAGUGCAAAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        TargetScanSiteFinder instance = new TargetScanSiteFinder();         
        CandidateMBS expectedResult=null;
        
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-12.60);
        expectedResult.setBindsInSeed(5); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(5);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Compensatory");
        expectedResult.setIsCanonical(false);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
        
     /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a p7mer with a wobble; should be null as it doesn not have enough WC pairs
     * to be compensatory.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithWobble() {
        System.out.println("Testing non canonical 7mer+wobble");
        String mirna = "UGAGAACUGAAUUCCAUGGGUU";
        String seqB = "UCCACUGAGUCUGAGUCUUAAAGUUUUCA";      
        TargetScanSiteFinder instance = new TargetScanSiteFinder();        
        
        CandidateMBS expectedResult = null; 
        /*expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-10.3);
        expectedResult.setBindsInSeed(6);
        expectedResult.setWCpairsInSeed(5);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Compensatory");
        expectedResult.setIsCanonical(false);*/
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 7mer with a missing base pair (7 non consecutive base pairs separated in two blocks);
     * should be candidate.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithWobbleAndCompensatory() {
        System.out.println("Testing non canonical 7mer+Gap and compensatory pairs");
        String mirna = "UAAAGGGCCAAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT"; 
        
        TargetScanSiteFinder instance = new TargetScanSiteFinder();  
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-14.4);
        expectedResult.setBindsInSeed(6);
        expectedResult.setWCpairsInSeed(5);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Compensatory");
        expectedResult.setIsCanonical(false);
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a mirna with a centered site; should be candidate as there is a 12mer starting
     * in the seed and ending beyond nc 11.
     */    
    @org.testng.annotations.Test
    public void testIsCandidateCenteredSite() {
        System.out.println("Testing a Centered Site");
        String mirna = "UUUUGAGAACUGAAUUUUUUUU";
        String seqB = "UCCAGAGUCUUUUUUCAGUUCUCAUUU";   
        TargetScanSiteFinder instance = new TargetScanSiteFinder();        
        
        CandidateMBS expectedResult = null; 
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-14.30);
        expectedResult.setBindsInSeed(4);
        expectedResult.setWCpairsInSeed(4);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("TS:Centered");
        expectedResult.setIsCanonical(false);
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
    
}
