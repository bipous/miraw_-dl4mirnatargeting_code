/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author apla
 */
public class DefaultCandidateSiteFinderNGTest {
    
    public DefaultCandidateSiteFinderNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of findCandidates method, of class DefaultCandidateSiteFinder.
     */
    @org.testng.annotations.Test
    public void testFindCandidates() {
        System.out.println("findCandidates");
        String mirna = "";
        String mRNA = "";
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();
        ArrayList expResult = null;
        ArrayList result = instance.findCandidates(mirna, mRNA);
        //assertEquals(result, expResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /* ###########################################
    ########CANNONICAL TARGETS TESTS #############
    ##############################################
    */
    /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a perfect canonical 6mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical6mer() {
        String mirna = "UGGAAUAUAAAGAAGUAUGUAU";
        String seqB = "AUGUGAAAGACCAUUAUUCCA";
         System.out.println("Testing canonical 6mer");
        
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder(); 
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-5.50);
        expectedResult.setBindsInSeed(6);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: cannonical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a perfect canonical 7mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical7mer() {
        System.out.println("Testing canonical 7mer");
        String mirna = "UAAAGUGCUUAUAGUGCAGGUAG";
        String seqB = "ATGTTTGATTTTATGCACTTTG";
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder(); 
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-9.30);
        expectedResult.setBindsInSeed(8);       //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(8);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: cannonical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a perfect canonical 8mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical8mer() {
        System.out.println("Testing canonical 8mer");
        String mirna = "UCUUUGGUUAUCUAGCUGUAUGA";
        String seqB = "CCATACATAAAAGAAATAAACCAAAGG";
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder(); 
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-13.20);
        expectedResult.setBindsInSeed(8); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(8);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: cannonical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /* ###########################################
    ##### NON-CANNONICAL TARGETS TESTS ###########
    ##############################################
    */
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 6mer with a missing base pair (6 non consecutive base pairs separated in two blocks);
     * should be candidate.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithGap() {
        System.out.println("Testing non canonical 6mer+Gap");
        String mirna = "UAAAGUGCUUAUAGUGCAGGUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();         
          CandidateMBS expectedResult = null; 
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-6.10);
        expectedResult.setBindsInSeed(6);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 6mer with a missing base pair (6 non consecutive base pairs separated in two blocks);
     * should be candidate .
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithGapAndCompensatory() {
        System.out.println("Testing non canonical 6mer+Gap and compensatory pairs");
        String mirna = "UAAAGUGCUUAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();         
        CandidateMBS expectedResult=null;
        
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-11.20);
        expectedResult.setBindsInSeed(6); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 6mer with a wobble pair; should be candidate.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithWobble() {
        System.out.println("Testing Compensatory 6mer+wobble");
        String mirna = "UGGAAUGUAAAGAAGUAUGUAU";
        String seqB = "AUGUGAAAGACCAUUAUUCCA";   
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();         
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        CandidateMBS expectedResult = null; 
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-5.0);
        expectedResult.setBindsInSeed(6);
        expectedResult.setWCpairsInSeed(5);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
      /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 6mer with a missing base pair (6 consecutive base pairs containing 1 wobble);
     * should be candidate .
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithWobbleAndCompensatory() {
        System.out.println("Testing non canonical 6mer+Gap and compensatory pairs");
        String mirna = "UAAAGGGAUUAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT"; 
                
                
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();         
        CandidateMBS expectedResult=null;
        
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-11.70);
        expectedResult.setBindsInSeed(7); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     /**
     *  Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a 7mer with a wobble pair; should be null.
     */
    @org.testng.annotations.Test
    public void testIsCandidate7merWithGap() {
        System.out.println("Testing non canonical 7mer+Gap");
        String mirna = "UAAAGUGCAUUUAGUGCAGGUAG";
        String seqB = "UAAUUUACAUUAUUGCUCUUUUT";       
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();        
                  
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
          
        CandidateMBS expectedResult = null; 
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-8.10);
        expectedResult.setBindsInSeed(7);
        expectedResult.setWCpairsInSeed(7);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }    
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 7mer with a missing base pair (7 non consecutive base pairs separated in two blocks);
     * should be candidate .
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithGapAndCompensatory() {
        System.out.println("Testing non canonical 7mer+Gap and compensatory pairs");
        String mirna = "UAAAGUGCAAAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();         
        CandidateMBS expectedResult=null;
        
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-12.60);
        expectedResult.setBindsInSeed(7); //Targetscan only considers the first 6 nucleotides when searching for a perfect match therefore the seed region has a size of 6
        expectedResult.setWCpairsInSeed(7);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a p7mer with a wobble; should be candidate.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithWobble() {
        System.out.println("Testing non canonical 7mer+wobble");
        String mirna = "UGAGAACUGAAUUCCAUGGGUU";
        String seqB = "UCCACUGAGUCUGAGUCUUAAAGUUUUCA";      
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();         
        
          CandidateMBS expectedResult = null; 
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-9.10);
        expectedResult.setBindsInSeed(7);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 7mer with a missing base pair (7 non consecutive base pairs separated in two blocks);
     * should be candidate.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithWobbleAndCompensatory() {
        System.out.println("Testing non canonical 7mer+Gap and compensatory pairs");
        String mirna = "UAAAGGGCCAAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT"; 
        
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();  
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-14.4);
        expectedResult.setBindsInSeed(7);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a mirna with a centered site; should be candidate
     */    
    @org.testng.annotations.Test
    public void testIsCandidateCenteredSite() {
        System.out.println("Testing a Centered Site");
        String mirna = "UUUUGAGAACUGAAUUUUUUUU";
        String seqB = "UCCAGAGUCUUUUUUCAGUUCUCAUUU";   
        DefaultCandidateSiteFinder instance = new DefaultCandidateSiteFinder();         
        
        CandidateMBS expectedResult = null; 
        expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-14.30);
        expectedResult.setBindsInSeed(7);
        expectedResult.setWCpairsInSeed(7);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(9);
        expectedResult.setComment("DEF: non-cannonical");
        expectedResult.setIsCanonical(false);
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
    
    
    
    
}
