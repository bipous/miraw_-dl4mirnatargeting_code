/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uio.dl4miRNA.SiteFinder;

import java.util.ArrayList;
import no.uio.dl4miRNA.DL.data.model.CandidateMBS;
import static org.testng.Assert.*;

/**
 *
 * @author apla
 */
public class PitaCandidateSiteFinderNGTest {
    
    public PitaCandidateSiteFinderNGTest() {
    }

    @org.testng.annotations.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.testng.annotations.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.testng.annotations.BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @org.testng.annotations.AfterMethod
    public void tearDownMethod() throws Exception {
    }

        /**
     * Test of findCandidates method, of class TargetScanSiteFinder.
     */
    @org.testng.annotations.Test
    public void testFindCandidates() {
        System.out.println("findCandidates");
        String mirna = "";
        String mRNA = "";
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();
        ArrayList expResult = null;
        ArrayList result = instance.findCandidates(mirna, mRNA);
        //assertEquals(result, expResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /* ###########################################
    ########CANNONICAL TARGETS TESTS #############
    ##############################################
    */
    /**
     * Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a perfect canonical 6mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical6mer() {
        String mirna = "UGGAAUAUAAAGAAGUAUGUAU";
        String seqB = "AUGUGAAAGACCAUUAUUCCA";
         System.out.println("Testing canonical 6mer");
        
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-5.50);
        expectedResult.setBindsInSeed(6);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(6);
        expectedResult.setComment("PITA: canonnical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a perfect canonical 7mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical7mer() {
        System.out.println("Testing canonical 7mer");
        String mirna = "UAAAGUGCUUAUAGUGCAGGUAG";
        String seqB = "ATGTTTGATTTTATGCACTTTG";
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-9.30);
        expectedResult.setBindsInSeed(7);
        expectedResult.setWCpairsInSeed(7);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(7);
        expectedResult.setComment("PITA: canonnical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a perfect canonical 8mer. 
     */
    @org.testng.annotations.Test
    public void testIsCandidateCannonical8mer() {
        System.out.println("Testing canonical 7mer");
        String mirna = "UCUUUGGUUAUCUAGCUGUAUGA";
        String seqB = "CCATACATAAAAGAAATAAACCAAAGG";
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-13.20);
        expectedResult.setBindsInSeed(8);
        expectedResult.setWCpairsInSeed(8);
        expectedResult.setWobblePairsInSeed(0);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(8);
        expectedResult.setComment("PITA: canonnical");
        expectedResult.setIsCanonical(true);
        
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /* ###########################################
    ##### NON-CANNONICAL TARGETS TESTS ###########
    ##############################################
    */
    
     /**
     * Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a 6mer with a missing base pair (6 non consecutive base pairs separated in two blocks);
     * should be null.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithGap() {
        System.out.println("Testing non canonical 6mer+Gap");
        String mirna = "UAAAGUGCUUAUAGUGCAGGUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();        
        CandidateMBS expectedResult = null;             
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a 6mer with a missing base pair (6 non consecutive base pairs separated in two blocks);
     * should be null.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithGapAndCompensatory() {
        System.out.println("Testing non canonical 6mer+Gap and compensatory pairs");
        String mirna = "UAAAGUGCUUAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();        
        CandidateMBS expectedResult=null;
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a 6mer with a wobble pair; should be null.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithWobble() {
        System.out.println("Testing canonical 6mer+wobble");
        String mirna = "UGGAAUGUAAAGAAGUAUGUAU";
        String seqB = "AUGUGAAAGACCAUUAUUCCA";   
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();        
        
        CandidateMBS expectedResult = null;
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
      /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 6mer with a missing base pair (6 consecutive base pairs containing 1 wobble);
     * should be null.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate6merWithWobbleAndCompensatory() {
        System.out.println("Testing non canonical 6mer+Gap and compensatory pairs");
        String mirna = "UAAAGGGAUUAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT"; 
                
                
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();         
        CandidateMBS expectedResult=null;
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
    
    /**
     *  Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a 7mer with a wobble pair; should be null.
     */
    @org.testng.annotations.Test
    public void testIsCandidate7merWithGap() {
        System.out.println("Testing non canonical 7mer+Gap");
        String mirna = "UAAAGUGCAUUUAGUGCAGGUAG";
        String seqB = "UAAUUUACAUUAUUGCUCUUUUT";       
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();        
        CandidateMBS expectedResult = null;             
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 7mer with a missing base pair (7 non consecutive base pairs separated in two blocks);
     * should be candidate as it has compensatory binding.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithGapAndCompensatory() {
        System.out.println("Testing non canonical 7mer+Gap and compensatory pairs");
        String mirna = "UAAAGUGCAAAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT";        
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();         
        CandidateMBS expectedResult=null;
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of isCandidate method, of class PitaCandidateSiteFinder when sending
     * a p7mer with a wobble; should be candidate.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithWobble() {
        System.out.println("Testing non canonical 7mer+wobble");
        String mirna = "UGAGAACUGAAUUCCAUGGGUU";
        String seqB = "UCCACUGAGUCUGAGUCUUAAAGUUUUCA";      
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();        
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-9.10);
        expectedResult.setBindsInSeed(7);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(7);
        expectedResult.setComment("PITA: noncanonical");
        expectedResult.setIsCanonical(false);
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class DefaultCandidateStieFinder when sending
     * a 7mer with a missing base pair (7 non consecutive base pairs separated in two blocks);
     * should be candidate.
     */    
    @org.testng.annotations.Test
    public void testIsCandidate7merWithWobbleAndCompensatory() {
        System.out.println("Testing non canonical 7mer+Gap and compensatory pairs");
        String mirna = "UAAAGGGCCAAUAAUGUAAAUAG";
        String seqB = "TAATTTACATTAATGCTCTTTT"; 
        
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();  
        
        CandidateMBS expectedResult = new CandidateMBS();
        expectedResult.setFreeEnergy(-14.4);
        expectedResult.setBindsInSeed(7);
        expectedResult.setWCpairsInSeed(6);
        expectedResult.setWobblePairsInSeed(1);
        expectedResult.setSeedRegionStart(1);
        expectedResult.setSeedRegionEnd(7);
        expectedResult.setComment("PITA: noncanonical");
        expectedResult.setIsCanonical(false);
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of isCandidate method, of class TargetScanSiteFinder when sending
     * a mirna with a centered site; should be null as it has 3 gaps at the 
     * starting of the seed region
     */   
    @org.testng.annotations.Test
    public void testIsCandidateCenteredSite() {
        System.out.println("Testing a Centered Site");
        String mirna = "UUUUGAGAACUGAAUUUUUUUU";
        String seqB = "UCCAGAGUCUUUUUUCAGUUCUCAUUU";   
        PitaCandidateSiteFinder instance = new PitaCandidateSiteFinder();        
        
        CandidateMBS expectedResult = null; 
        
        CandidateMBS result = instance.isCandidate(mirna, seqB);
        
        assertEquals(result, expectedResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
}
