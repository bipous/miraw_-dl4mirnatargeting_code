# README #

This repository contains the source code of miRAW, a deep learning-based method for predicting miRNA targets by analyzing the whole transcript of the miRNA and the target gene 3'UTR.

For mor information refer to the paper 'XXXX'.

All the core classes and packages are under the package 'no.uio.dl4miRNA', rest of packages belong to complementary/additional code used during the developing and testing of miRAW.

###LICENSE AND CITATIONS###

If you use/modify this code or this software for your research, please cite it as 'XXXX'. The code is distributted under GNU GPL3 licence with the additional requirement of including the following citation 'XXXX' for any publication that has derived from its usage or modification.

### How do I get set up? ###
This project is a maven Java project. 
To set it up just check out the project in your computer and open it with your favourite Java IDE.

* This code/software has only  been tested in Linux and Os X, there is no guarantee that it will work under Windows environments.

* Make sure that your POM document is defining the appropiate ND4J package for your system; take into account that the ND4J package determines which logic unit will be used for making neural network arithmetical/logic calculus:

   1. nd4j-native-platform for CPU
   1. nd4j-cuda-7.5-platform for GPU (Cuda 7.5)
   1. nd4j-cuda-8.0-platform for GPU (Cuda 8.0)

 
* To run certain functionalities, you might need to install the [ViennaRNA 2.2 pacakge](http://www.tbi.univie.ac.at/RNA/). We recommend installing in it in its default installation folders as this won't require further configuration for running miRAW.

### Who do I talk to? ###
a.p.planas@medisin.uio.no